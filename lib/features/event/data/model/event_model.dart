import 'package:mersal/features/event/domain/enity/event_entity.dart';
import 'package:meta/meta.dart';

class EventModel extends EventEntity {
  EventModel(
      {@required bool status,
        @required List<EventDataListEntity> eventDataList})
      : super(status: status, eventDataList: eventDataList);

  EventModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['data'] != null) {
      eventDataList = new List<EventDataListEntity>();
      json['data'].forEach((v) {
        eventDataList.add(new EventDataList.fromJson(v));
      });
    }
  }
}

class EventDataList extends EventDataListEntity {
  EventDataList(
      {int id,
        String description,
        String title,
        String link,
        String startDate,
        String endDate,
        String eventDate,
        String created_at})
      : super(
      id: id,
      created_at: created_at,
      eventDate: eventDate,
      startDate: startDate,
      endDate: endDate,
      description: description);

  EventDataList.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    created_at = json['created_at'];
    description = json['description'];
    startDate = json['starts_at'];
    endDate = json['ends_at'];
    eventDate = json['event_date'];
    title = json['title'];
    link = json['link'];
    occasionDate = json['occassion_date'];
  }
}
