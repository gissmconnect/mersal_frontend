import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/save_user_login_data_usecase.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/election/election-domain/entity/election_entity.dart';
import 'package:mersal/features/election/election-presentation/bloc/election_bloc.dart';
import 'package:mockito/mockito.dart';

import '../../../../mocked_classes.dart';

void main() {
  ElectionBloc _bloc;
  MockGetElectionsUseCase _mockGetElectionUseCase;
  MockReAuthenticateUseCase _mockReAuthenticateUseCase;
  MockGetUserLoginDataUseCase _mockGetUserLoginDataUseCase;
  setUp(() {
    _mockGetElectionUseCase = MockGetElectionsUseCase();
    _mockReAuthenticateUseCase = MockReAuthenticateUseCase();
    _mockGetUserLoginDataUseCase = MockGetUserLoginDataUseCase();
    _bloc = ElectionBloc(
        getElectionUseCase: _mockGetElectionUseCase,
        reAuthenticateUseCase: _mockReAuthenticateUseCase,
        getUserLoginDataUseCase: _mockGetUserLoginDataUseCase);
  });

  test('should be ElectionLoading as Initial state', () {
    expect(_bloc.state, ElectionInitial());
  });

  final _tElections = [ElectionDataEntity()];
  group('election_loading', () {
    test('should ElectionData successful retrieved from GetElectionUseCase',
        () async {
      //arrange
      when(_mockGetElectionUseCase(any))
          .thenAnswer((realInvocation) async => Right(_tElections));
      //act
      //act
      _bloc.add(GetElections());
      await untilCalled(_mockGetElectionUseCase(any));

      //assert

      verify(_mockGetElectionUseCase(NoParams()));
    });

    test(
        'should emit [ElectionLoading, ElectionLoaded] when data received successfully from usecase',
        () async {
      //arrange
      when(_mockGetElectionUseCase(any))
          .thenAnswer((_) async => Right(_tElections));

      //act
      _bloc.add(GetElections());

      //assert
      final _expectedStates = [
        ElectionLoading(),
        ElectionLoaded(elections: _tElections)
      ];
      expectLater(_bloc.stream, emitsInOrder(_expectedStates));
    });

    test(
        'should emit [ElectionLoading ElectionLoadFailed] when data fail failed from usecase',
        () {
      //arrange
      when(_mockGetElectionUseCase(any))
          .thenAnswer((realInvocation) async => Left(ServerFailure()));

      //act
      _bloc.add(GetElections());
      //assert
      final _expectedStates = [
        ElectionLoading(),
        ElectionLoadFiled(failure: ServerFailure())
      ];
      expectLater(_bloc.stream, emitsInOrder(_expectedStates));
    });

    test(
        'should call ReAuthenticate Usecase when data failure error is AuthFailure',
        () async{
      when(_mockGetElectionUseCase(any))
          .thenAnswer((realInvocation) async => Left(AuthFailure()));
      when(_mockGetUserLoginDataUseCase(any)).thenAnswer(
          (realInvocation) async =>
              Right(LoginParams(loginId: 'test', password: 'test')));
      when(_mockReAuthenticateUseCase(any))
          .thenAnswer((realInvocation) async => Right(null));
      when(_mockGetElectionUseCase(any))
          .thenAnswer((realInvocation) async => Right(_tElections));
      //act
      _bloc.add(GetElections());

      //assert
      final _expectedStates = [
        ElectionLoading(),
        ElectionLoaded(elections: _tElections)
      ];
      expectLater(_bloc.stream, emitsInOrder(_expectedStates));
    });
  });
}
