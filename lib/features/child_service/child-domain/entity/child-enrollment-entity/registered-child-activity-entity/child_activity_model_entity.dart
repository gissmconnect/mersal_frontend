class ChildActivityModelEntity {
  bool status;
  List<ChildActivityData> childActivityList;

  ChildActivityModelEntity({this.status, this.childActivityList});

  ChildActivityModelEntity copy({
    int status,
    List<ChildActivityData> childDataList,
  }) {
    return ChildActivityModelEntity(
        status: status ?? this.status,
        childActivityList: childDataList ?? this.childActivityList);
  }
}

class ChildActivityData {
  int id;
  String created_at;
  String title;
  String description;

  ChildActivityData({this.id, this.created_at,this.description, this.title});

  ChildActivityData copy({int id, String created_at, String title, String description}) {
    return ChildActivityData(
        id: id ?? this.id,
        created_at: created_at ?? this.created_at,
        description: description ?? this.description,
        title: title ?? this.title);
  }
}
