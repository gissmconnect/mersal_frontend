import 'package:mersal/core/bloc/user_bloc.dart';
import 'package:mersal/core/network/vo/Status.dart';
import 'package:meta/meta.dart';
import 'package:url_launcher/url_launcher.dart';

bool userDataUpdated(UserState oldState, UserState newState) {
  return (oldState.userDataLoadingResource == null ||
      oldState.userDataLoadingResource.status !=
          newState.userDataLoadingResource.status ||
      (oldState.userDataLoadingResource.status == STATUS.SUCCESS &&
          (oldState.userDataLoadingResource.data) !=
              newState.userDataLoadingResource.data));
}

void launchURL({@required url}) async =>
    await canLaunch(url) ? await launch(url) : throw 'Could not launch $url';
