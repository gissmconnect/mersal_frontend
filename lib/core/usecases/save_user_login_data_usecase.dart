import 'dart:core';

import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/auth/auth_domain/repository/auth_repository.dart';
import 'package:meta/meta.dart';

class SaveUserLoginDataUseCase extends UseCase<void, LoginParams> {
  final AuthRepository authRepository;

  SaveUserLoginDataUseCase({@required this.authRepository});

  @override
  Future<Either<Failure, void>> call(LoginParams params) async {
    await authRepository.saveUserLoginData(loginParams: params);
  }
}

class LoginParams extends Equatable{
  String loginId, password,device_token;

  LoginParams({@required this.loginId, @required this.password, @required this.device_token});

  LoginParams.fromJson(Map<String, dynamic> json) {
    loginId = json['login_id'];
    password = json['password'];
    device_token = json['device_token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['login_id'] = this.loginId;
    data['password'] = this.password;
    return data;
  }

  @override
  List<Object> get props => [loginId,password];

}
