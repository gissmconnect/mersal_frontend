import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:mersal/core/network/api_endpoints.dart';
import 'package:mersal/core/network/http_error_handler.dart';
import 'package:mersal/core/widgets/common/Button.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/core/widgets/common/shared_preference.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/dashboard/dashboard_page.dart';
import 'package:mersal/features/payment/payment_webview.dart';
import 'package:mersal/features/wedding_invite_presentation/data/model/wedding_template_image/wedding_template_image_data.dart';
import 'package:mersal/features/wedding_invite_presentation/presentation/ui/wedding_invite_success_page.dart';
import 'package:mersal/features/wedding_invite_presentation/presentation/wedding_invite_list/model/wedding_invite_template_model.dart';
import 'package:mersal/resources/colors.dart';

class WeddingInvitationListSummary extends StatefulWidget {
  final WeddingInviteTemplateModel templateImageList;
  final String invitationType,
      sendPhone,
      weddingInviteDate,
      templateId,
      weddingTime,
      groomName,
      weddingType,
      brideName,
      mobileNumber;

  WeddingInvitationListSummary(
      {this.invitationType,
        this.weddingInviteDate,
        this.sendPhone,
        this.weddingType,
        this.brideName,
        this.groomName,
        this.templateId,
        this.weddingTime,
        this.mobileNumber,
        this.templateImageList});

  @override
  _WeddingInvitationListSummaryState createState() =>
      _WeddingInvitationListSummaryState();
}

class _WeddingInvitationListSummaryState extends State<WeddingInvitationListSummary> {
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  BuildContext mContextLoader;
  String weddingId="";

  @override
  void initState() {
    super.initState();
    _asyncMethod();
  }

  _asyncMethod() async {
    weddingId = await appPreferences.getStringPreference("bookingId");
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(top: 40),
                child: Label(
                  title: "Wedding Invitation Summary",
                  textAlign: TextAlign.center,
                  fontWeight: FontWeight.w700,
                  fontSize: 20,
                  color: Colors.black,
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 40.0),
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 25, right: 25, top: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(height: 40),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Expanded(
                                child: Container(
                                  margin: EdgeInsets.only(left: 15),
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      Label(
                                        title: "Bride Name",
                                        textAlign: TextAlign.center,
                                        fontSize: 18,
                                        color: Colors.black,
                                      ),
                                      SizedBox(height: 10),
                                      Label(
                                        title: "Groom Name",
                                        textAlign: TextAlign.center,
                                        fontSize: 18,
                                        color: Colors.black,
                                      ),
                                      SizedBox(height: 10),
                                      Label(
                                        title: "Invitation Type",
                                        textAlign: TextAlign.center,
                                        fontSize: 18,
                                        color: Colors.black,
                                      ),
                                      SizedBox(height: 10),
                                      Label(
                                        title: "Date",
                                        textAlign: TextAlign.center,
                                        fontSize: 18,
                                        color: Colors.black,
                                      ),
                                      SizedBox(height: 10),
                                      Label(
                                        title: "Time",
                                        textAlign: TextAlign.center,
                                        fontSize: 18,
                                        color: Colors.black,
                                      ),
                                      SizedBox(height: 10),
                                      Label(
                                        title: "Amount",
                                        textAlign: TextAlign.center,
                                        fontSize: 18,
                                        color: Colors.black,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Label(
                                      title: widget.brideName,
                                      textAlign: TextAlign.center,
                                      fontSize: 18,
                                      color: Colors.black,
                                    ),
                                    SizedBox(height: 10),Label(
                                      title: widget.groomName,
                                      textAlign: TextAlign.center,
                                      fontSize: 18,
                                      color: Colors.black,
                                    ),
                                    SizedBox(height: 10),Label(
                                      title: widget.invitationType,
                                      textAlign: TextAlign.center,
                                      fontSize: 18,
                                      color: Colors.black,
                                    ),
                                    SizedBox(height: 10),
                                    Label(
                                      title: widget.weddingInviteDate,
                                      textAlign: TextAlign.center,
                                      fontSize: 18,
                                      color: Colors.black,
                                    ),
                                    SizedBox(height: 10),
                                    Label(
                                      title: widget.weddingTime,
                                      textAlign: TextAlign.center,
                                      fontSize: 18,
                                      color: Colors.black,
                                    ),
                                    SizedBox(height: 10),
                                    Label(
                                      title: "OMR 10.0",
                                      textAlign: TextAlign.center,
                                      fontSize: 18,
                                      color: Colors.black,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          Container(
                            height: 50,
                            margin: EdgeInsets.only(
                                top: 55, bottom: 40, left: 20, right: 20),
                            child: Button(
                              title: "Confirm",
                              color: Color(appColors.buttonColor),
                              borderColor: Colors.transparent,
                              textColor: Colors.white,
                              onTap: () {
                                weddingInviteApi();
                                // CommonMethods().animatedNavigation(context: context,currentScreen:WeddingInvitationSummary(),
                                //     landingScreen: PaymentOptions("invite"));
                              },
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Future<void> weddingInviteApi() async {
    final _token = await appPreferences.getStringPreference("AccessToken");

    var value;
    if (widget.invitationType == "Send invitation to selected number") {
      value = "0";
    } else {
      value = "1";
    }

    final _params = <String, String>{
      'booking_id': weddingId??"",
      'template_id': widget.templateId,
      'template_image_id':  widget.templateImageList!=null?widget.templateImageList.id.toString():"",
      'date': widget.weddingInviteDate,
      'wedding_date': widget.weddingInviteDate,
      'time': widget.weddingTime,
      'groom_name': widget.groomName,
      'bride_name': widget.brideName,
      'phone_numbers': widget.sendPhone,
      'type': value,
      'wedding_type': widget.weddingType.toLowerCase()=="aqid"?"0":"1",
    };

    if (context != null) {
      _showLoader(context);
    }

    print("Invite Params====${_params.toString()}");

    var response = await http.post(Uri.parse(WEDDING_BOOKING_INVITATION),
        headers: {
          'Content-type': 'application/json',
          AUTHORIZATION: BEARER + " " + _token
        },
        body: json.encode(_params));

    if (response.statusCode == 200) {
      if (mContextLoader != null) Navigator.pop(mContextLoader);
      final _responseJson = json.decode(response.body);
      Navigator.pop(context);
      // showSuccessDialog(context);
      CommonMethods().animatedNavigation(
          context: context,
          currentScreen:
          WeddingInvitationListSummary(),
          landingScreen: PaymentWebView(
            type: "lawati-sms",
            id: "1",
            activity: "",
          ));
    }
    else{
      if (mContextLoader != null) Navigator.pop(mContextLoader);
      handleError(response);
    }
  }

  Future<Null> _showLoader(BuildContext context) async {
    return showDialog<Null>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context1) {
        mContextLoader = context1;
        return Scaffold(
          backgroundColor: Colors.transparent.withOpacity(0.3),
          key: _keyLoader,
          body: Container(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    child: CircularProgressIndicator(),
                    height: 40.0,
                    width: 40.0,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  void showSuccessDialog(BuildContext context) {
    showGeneralDialog(
      barrierLabel: "Barrier",
      barrierDismissible: true,
      barrierColor: Colors.black.withOpacity(0.8),
      transitionDuration: Duration(milliseconds: 500),
      context: context,
      pageBuilder: (_, __, ___) {
        return Align(
          alignment: Alignment.center,
          child: Container(
            height: 450,
            margin: EdgeInsets.only(left: 10, right: 10),
            decoration: new BoxDecoration(
              color: const Color(0xFFFFFF),
              borderRadius: new BorderRadius.all(new Radius.circular(32.0)),
            ),
            child: WeddingInviteSuccessPage(),
          ),
        );
      },
      transitionBuilder: (_, anim, __, child) {
        return SlideTransition(
          position: Tween(begin: Offset(0, 1), end: Offset(0, 0)).animate(anim),
          child: child,
        );
      },
    );
  }

}
