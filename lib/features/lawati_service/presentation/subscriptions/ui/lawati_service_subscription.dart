import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mersal/core/localization/app_localization.dart';
import 'package:mersal/core/network/vo/Status.dart';
import 'package:mersal/core/widgets/common/Button.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/core/widgets/message.dart';
import 'package:mersal/core/widgets/widgets.dart';
import 'package:mersal/features/lawati_service/domain/entity/sms_subscription_entity.dart';
import 'package:mersal/features/lawati_service/presentation/service_info/bloc/my_sms_service/my_sms_service_bloc.dart';
import 'package:mersal/features/lawati_service/presentation/subscriptions/bloc/sms_subscriptions_bloc.dart';
import 'package:mersal/features/lawati_service/presentation/subscriptions/ui/subscription_details.dart';
import 'package:mersal/resources/colors.dart';

import '../../../../../injection_container.dart';

class LawatiServiceSubscription extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<SmsSubscriptionsBloc>(
      create: (_) => serviceLocator<SmsSubscriptionsBloc>(),
      child: Scaffold(
        body: _LawatiServiceSubscriptionBody(),
      ),
    );
  }
}

class _LawatiServiceSubscriptionBody extends StatefulWidget {
  @override
  _LawatiServiceSubscriptionState createState() =>
      _LawatiServiceSubscriptionState();
}

class _LawatiServiceSubscriptionState
    extends State<_LawatiServiceSubscriptionBody> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<SmsSubscriptionsBloc>(context).add(GetSubscriptionsEvent());
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocListener(
      listeners: [
        BlocListener<SmsSubscriptionsBloc, SmsSubscriptionsState>(
          listenWhen: (oldState, newState) {
            return oldState.subscriptions == null ||
                oldState.subscriptions.status != newState.subscriptions.status;
          },
          listener: (context, state) {
            if (state.subscriptions.status == STATUS.ERROR) {
              showSnackBarMessage(
                  context: context,
                  message: state.subscriptions.failure.message);
            }
          },
        )
      ],
      child: BlocBuilder<SmsSubscriptionsBloc, SmsSubscriptionsState>(
        builder: (context, state) {
          if (state.subscriptions.status == STATUS.LOADING) {
            return Center(child: CircularProgressIndicator());
          } else if (state.subscriptions.status == STATUS.SUCCESS) {
            final _subscriptions = state.subscriptions.data as List<SmsSubscriptionEntity>;
            return Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/bg_image.jpg"),
                  fit: BoxFit.fill,
                ),
              ),
              child: Column(
                children: [
                  SizedBox(
                    height: 40,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Stack(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(bottom: 5, left: 15),
                        alignment: Alignment.bottomLeft,
                        child: GestureDetector(
                          child: Icon(
                            Icons.arrow_back_ios_rounded,
                            color: Colors.white,
                          ),
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(bottom: 6),
                        alignment: Alignment.bottomCenter,
                        child: Text(
                          AppLocalizations.of(context)
                              .translate("Lawati SMS Service"),
                          textAlign: TextAlign.end,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                    ],
                  ),
                  Expanded(
                    child: _subscriptions.isNotEmpty
                        ? ListView.builder(
                             shrinkWrap: true,
                            itemBuilder: (context, index) {
                              return GestureDetector(
                                onTap: () async {
                                  await subscriptionDetailsBottomSheet(
                                      _subscriptions[index]);
                                  // Navigator.push(
                                  //     context,
                                  //     MaterialPageRoute(
                                  //         builder: (context) =>
                                  //             SubscriptionDetailsScreen(
                                  //                 _subscriptions[index])));
                                },
                                child: Container(
                                  padding: EdgeInsets.all(20),
                                  margin: EdgeInsets.only(
                                      bottom: 10, left: 15, right: 15),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(25)),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Label(
                                          title: _subscriptions[index].firstName != null?_subscriptions[index].firstName:"" +_subscriptions[index].middleName !=null?
                                          _subscriptions[index].middleName :""+ _subscriptions[index].lastName != null
                                              ? _subscriptions[index].lastName
                                              : "",
                                          fontSize: 16,
                                          textAlign: TextAlign.center,
                                          fontWeight: FontWeight.w600,
                                          color: Color(appColors.buttonColor),
                                        ),
                                        /*_subscriptions[index].expiringIn!=null &&  _subscriptions[index].expiringIn <= 7
                                            ? Container(
                                          width: 120,
                                          height: 60,
                                          margin: EdgeInsets.only(left: 30,
                                              right: 30),
                                          child: BlocListener<MySmsServiceBloc, MySmsServiceState>(
                                            listenWhen: (oldState,  newState) =>
                                            (oldState.renewSubscription == null || oldState.renewSubscription.status !=  newState.renewSubscription.status),
                                            listener: (mcontext, state) {
                                              if (state .renewSubscription != null) {
                                                if (state.renewSubscription .status ==STATUS.ERROR) {
                                                  showSnackBarMessage( context: context,  message: state.renewSubscription.failure.message);
                                                } else if (state.renewSubscription .status == STATUS.SUCCESS) {
                                                  showSnackBarMessage(
                                                      context: context,
                                                      message: state.renewSubscription.data);
                                                }
                                              }
                                            },
                                            child: BlocBuilder<MySmsServiceBloc,MySmsServiceState>(
                                              builder:(mcontext, state) {
                                                if (state.renewSubscription != null &&
                                                    state.renewSubscription.status == STATUS.LOADING) {
                                                  return circularProgressIndicator();
                                                }
                                                return Button(
                                                  title: "Renew",
                                                  color: Color(appColors .buttonColor),
                                                  borderColor: Colors.transparent,
                                                  textColor:
                                                  Colors.white,
                                                  onTap: () {
                                                    BlocProvider.of<MySmsServiceBloc>(context).add(
                                                      RenewServiceEvent(subscriptionId: _subscriptions[index].id),
                                                    );
                                                  },
                                                );
                                              },
                                            ),
                                          ),
                                        )
                                            : */Container(
                                          width: 120,
                                          height: 60,
                                        ),
                                      ],
                                        ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Label(
                                            title: _subscriptions[index].phone,
                                            fontSize: 13,
                                            textAlign: TextAlign.center,
                                            color: Color(appColors.buttonColor),
                                          ),
                                          Label(
                                            title:
                                                _subscriptions[index].endDate,
                                            fontSize: 13,
                                            textAlign: TextAlign.center,
                                            color: Color(appColors.buttonColor),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            },
                            itemCount: _subscriptions.length,
                          )
                        : Center(
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 20),
                              child: Text(
                                AppLocalizations.of(context)
                                    .translate("You have not subscribed for lawati SMS service"),
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                  ),
                  // _subscriptions.isNotEmpty
                  //     ? Container(
                  //         height: 60,
                  //         width: 250,
                  //         margin: EdgeInsets.only(top: 35),
                  //         child: Button(
                  //           title: "Renew",
                  //           textColor: Color(appColors.blue00008B),
                  //           onTap: () {
                  //             CommonMethods().animatedNavigation(
                  //                 context: context,
                  //                 currentScreen: LawatiServiceSubscription(),
                  //                 landingScreen: LawatiService("self"));
                  //           },
                  //         ),
                  //       )
                  //     : Container(),
                  // Container(
                  //   height: 60,
                  //   width: 250,
                  //   margin: EdgeInsets.only(top: 35),
                  //   child: Button(
                  //     title: "Add New Subscription",
                  //     textColor: Color(appColors.blue00008B),
                  //     onTap: () {
                  //       CommonMethods().animatedNavigation(
                  //           context: context,
                  //           currentScreen: LawatiServiceSubscription(),
                  //           landingScreen: LawatiService("self"));
                  //     },
                  //   ),
                  // ),
                  // SizedBox(
                  //   height: 40,
                  // ),
                ],
              ),
            );
          }
          return Container();
        },
      ),
    );
  }

  Future<Widget> subscriptionDetailsBottomSheet(_subscriptions) {
    return showModalBottomSheet<Widget>(
      isScrollControlled: true,
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(55.0), topRight: Radius.circular(55.0)),
      ),
      builder: (BuildContext context) {
        return Container(
            height: MediaQuery.of(context).size.height / 2,
            color: Colors.white,
            margin: EdgeInsets.only(top: 40),
            child: SubscriptionDetailsScreen(_subscriptions));
      },
    );
  }
}
