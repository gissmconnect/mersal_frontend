part of 'occasion_bloc.dart';

abstract class OccasionEvent extends Equatable {
  const OccasionEvent();
}

class GetOccasionEvent extends OccasionEvent {
  final DateTime date;

  GetOccasionEvent({@required this.date});

  @override
  List<Object> get props => [date];
}
