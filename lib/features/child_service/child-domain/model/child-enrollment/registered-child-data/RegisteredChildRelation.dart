class RegisteredChildRelation {
  int id;
  int relation_type;
  String title;
  String created_at;

  RegisteredChildRelation({this.relation_type,this.id,this.title,this.created_at});

  factory RegisteredChildRelation.fromJson(Map<String, dynamic> json) {
    return RegisteredChildRelation(
      title: json['title'],
      relation_type: json['relation_type'],
      id: json['id'],
      created_at: json['created_at'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    return data;
  }
}