part of 'prayer_bloc.dart';

class PrayerState extends Equatable {
  final int randomId;
  final Resource prayerTimesResource;
  final PrayerDataEntity  upcomingPrayer;
  final Resource setAlarmResource;

  PrayerState(
      {this.prayerTimesResource,
      this.upcomingPrayer,
      this.randomId = 0,
      this.setAlarmResource});

  factory PrayerState.initial() {
    return PrayerState(prayerTimesResource: Resource.loading());
  }

  PrayerState copy(
      {Resource prayerTimesResource,
      PrayerDataEntity upcomingPrayer,
      int randomId,
      Resource setAlarmResource}) {
    return PrayerState(
        prayerTimesResource: prayerTimesResource ?? this.prayerTimesResource,
        upcomingPrayer: upcomingPrayer ?? this.upcomingPrayer,
        randomId: randomId ?? this.randomId,
        setAlarmResource: setAlarmResource ?? this.setAlarmResource);
  }

  @override
  List<Object> get props => [prayerTimesResource, upcomingPrayer, randomId,setAlarmResource];
}
