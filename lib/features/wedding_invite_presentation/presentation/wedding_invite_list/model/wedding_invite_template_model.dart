class WeddingInviteTemplateModel {
  int id;
  String title;
  String background_image;
  String created_at;
  String image;

  WeddingInviteTemplateModel({this.created_at,this.background_image,this.image, this.id, this.title});

  factory WeddingInviteTemplateModel.fromJson(Map<String, dynamic> json) {
    return WeddingInviteTemplateModel(
        id: json['id'],
        title: json["title"],
        created_at: json["created_at"],
        background_image: json["background_image"],
        image: json['image']);
  }
}
