part of 'otp_bloc.dart';

abstract class OtpEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class OtpRequestEvent extends OtpEvent{

  final String phoneNumber;

  OtpRequestEvent({@required this.phoneNumber});
}


class VerifyOtpEvent extends OtpEvent{
  final String phoneNumber,otp;

  VerifyOtpEvent({@required this.phoneNumber,@required this.otp});
}