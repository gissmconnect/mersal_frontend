import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/lawati_service/domain/entity/sms_subscription_entity.dart';
import 'package:mersal/features/lawati_service/domain/repository/sms_service_repo.dart';
import 'package:meta/meta.dart';

class GetSmsSubscriptionUseCase
    extends UseCase<List<SmsSubscriptionEntity>, NoParams> {
  final SMSServiceRepo smsServiceRepo;

  GetSmsSubscriptionUseCase({@required this.smsServiceRepo});

  @override
  Future<Either<Failure, List<SmsSubscriptionEntity>>> call(
      NoParams params) async {
    return await smsServiceRepo.getSubscriptions();
  }
}
