import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

void showSnackBarMessage({@required BuildContext context,@required  String message}) {
  ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(message)));
}