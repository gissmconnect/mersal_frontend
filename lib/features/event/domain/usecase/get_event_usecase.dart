import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/event/domain/enity/event_entity.dart';
import 'package:mersal/features/event/domain/repository/event_repo.dart';
import 'package:meta/meta.dart';

class GetEventUseCase extends UseCase<EventEntity, String> {
  final EventRepo eventRepo;

  GetEventUseCase({@required this.eventRepo});

  @override
  Future<Either<Failure, EventEntity>> call(String params) async {
    return eventRepo.getEvent(date: params);
  }
}
