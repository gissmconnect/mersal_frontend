
import 'dart:convert';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:mersal/core/bloc/user_bloc.dart';
import 'package:mersal/core/localization/app_localization.dart';
import 'package:mersal/core/network/vo/Status.dart';
import 'package:mersal/core/widgets/common/Button.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/CustomAppBar.dart';
import 'package:mersal/core/widgets/common/InputField.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/core/widgets/common/image_chooser.dart';
import 'package:mersal/core/widgets/message.dart';
import 'package:mersal/core/widgets/widgets.dart';
import 'package:mersal/features/auth/auth_domain/entity/user_data.dart';
import 'package:mersal/resources/colors.dart';

class EditProfileScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        title: AppLocalizations.of(context)
            .translate("Edit Profile"),
        isBackButton: true,
        showText: true,
        rightIconTwo: true,
      ),
      body: _EditProfileBody(),
    );
  }
}

class _EditProfileBody extends StatefulWidget {
  @override
  _EditProfileBodyState createState() => _EditProfileBodyState();
}

class _EditProfileBodyState extends State<_EditProfileBody> {
  final nameController = TextEditingController();
  final dobController = TextEditingController();
  final genderController = TextEditingController();
  final bloodGroupController = TextEditingController();

  final FocusNode nameFocus = new FocusNode();
  final FocusNode genderFocus = new FocusNode();
  final FocusNode bloodGroupFocus = new FocusNode();

  final _formKey = GlobalKey<FormState>();

  ValueNotifier<DateTime> _dateTimeNotifier =
      ValueNotifier<DateTime>(DateTime.now());

  bool isAlreadyLoaded = false, dateBool = false;

  List<String> dummyOptions = ['A+', 'A-', 'O', 'B+', 'B-', 'AB+', 'AB-'];

  String _selectedLocation = 'Select Blood Group', dob = "DOB", dobToSend = "";
  File _image;
  String userImage='';
  FToast fToast;
  String base64Image="";

  @override
  void initState() {
    super.initState();
    BlocProvider.of<UserBloc>(context).add(GetUserSessionEvent());
    fToast = FToast();
    fToast.init(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/images/bg_image.jpg"),
          fit: BoxFit.fill,
        ),
      ),
      child: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: SingleChildScrollView(
          child: BlocListener<UserBloc, UserState>(
            listenWhen: (oldState, newState) {
              return !isAlreadyLoaded;
            },
            listener: (context, state) {
              if (state.userDataLoadingResource.status != null &&
                  state.userDataLoadingResource.status == STATUS.SUCCESS) {
                isAlreadyLoaded = true;
                final UserData _userData = state.userDataLoadingResource.data;
                nameController.text = _userData.name;
                userImage = _userData.image;
                genderController.text = _userData.gender;
                bloodGroupController.text = _userData.bloodGroup;
                _selectedLocation = _userData.bloodGroup ?? "Select Blood group";
                dob= _userData.dob;
                setState(() {});
              }
            },
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: Stack(
                      children: <Widget>[
                        Align(
                          alignment: Alignment.topCenter,
                          child: Container(
                            height: 110,
                            width: 110,
                            padding: const EdgeInsets.all(10.0),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(55),
                              child: Container(
                                child: _image != null
                                    ? Image.file(
                                  _image,
                                  height: 100,
                                  width: 100,
                                  fit: BoxFit.fill,
                                )
                                    : userImage.isEmpty
                                    ? Icon(Icons.person_outline,
                                    size: 70, color: Colors.white)
                                    : CircleAvatar(
                                  radius: 30,
                                  backgroundImage:
                                  CachedNetworkImageProvider(
                                    userImage,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.topCenter,
                          child: Container(
                            margin: const EdgeInsets.only(top: 25, left: 85),
                            child: GestureDetector(
                              onTap: () {
                                imageChooser
                                    .showImageChooser(context)
                                    .then((picture) {
                                  if (picture != null) {
                                    this._image = picture;
                                    List<int> imageBytes = this._image.readAsBytesSync();
                                    print(imageBytes);
                                    base64Image = base64Encode(imageBytes);
                                    setState(() {});
                                    print(picture);
                                    print("Image========="+base64Image);
                                  }
                                });
                              },
                              child: Container(
                                width: 30,
                                height: 30,
                                decoration: new BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colors.black,
                                ),
                                child: Center(
                                  child: Icon(Icons.camera_alt_outlined,
                                      size: 15, color: Colors.white),
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 30, top: 40, right: 30),
                    child: InputField(
                      controller: nameController,
                      focusNode: nameFocus,
                      onSubmitted: (term) {
                        CommonMethods.inputFocusChange(
                            context, nameFocus, genderFocus);
                      },
                      validator: (text) {
                        return text.isEmpty ? AppLocalizations.of(context)
                            .translate('Name cannot be null') : null;
                      },
                      hint: AppLocalizations.of(context)
                          .translate("Name"),
                      maxLength: 20,
                      inputType: TextInputType.name,
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      showDatePickerDialog(context).then((date) {
                        if (date != null) {
                          dob = DateFormat("dd/MM/yyyy").format(date);
                          dobToSend = DateFormat("yyyy-MM-dd").format(date);
                          dateBool = true;
                          setState(() {});
                        }
                      });
                    },
                    child: Container(
                        height: 50,
                        margin: EdgeInsets.only(left: 30, top: 20, right: 30),
                        padding: EdgeInsets.only(left: 20, right: 10),
                        width: MediaQuery.of(context).size.width,
                        alignment: Alignment.centerLeft,
                        decoration: new BoxDecoration(
                          shape: BoxShape.rectangle,
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(25),
                          border: Border.all(color: Colors.white, width: 1),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Label(
                              title: dob,
                              fontSize: 16,
                              color: Colors.black,
                            ),
                            Icon(
                              Icons.date_range_rounded,
                              color: Colors.black,
                            ),
                          ],
                        )),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                        left: 30, top: 20, right: 30, bottom: 10),
                    child: InputField(
                      controller: genderController,
                      focusNode: genderFocus,
                      onSubmitted: (term) {
                        CommonMethods.inputFocusChange(
                            context, genderFocus, bloodGroupFocus);
                      },
                      hint: AppLocalizations.of(context)
                          .translate("Gender"),
                      validator: (text) {
                        return text.isEmpty ? AppLocalizations.of(context)
                            .translate('Gender cannot be null') : null;
                      },
                      inputType: TextInputType.name,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 30, right: 30, top: 10),
                    padding: EdgeInsets.only(left: 20, right: 10),
                    height: 50,
                    alignment: Alignment.centerLeft,
                    width: MediaQuery.of(context).size.width,
                    decoration: new BoxDecoration(
                      shape: BoxShape.rectangle,
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(25),
                      border: Border.all(color: Colors.white, width: 1),
                    ),
                    child: DropdownButton<String>(
                        isExpanded: true,
                        underline: Container(),
                        hint: Row(
                          children: <Widget>[
                            Label(
                              title: _selectedLocation ?? "",
                              color: Colors.black,
                              fontSize: 17,
                              textAlign: TextAlign.center,
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: 5),
                              child: Icon(
                                Icons.keyboard_arrow_down_sharp,
                                color: Colors.white,
                                size: 20,
                              ),
                            ),
                          ],
                        ),
                        items: dummyOptions.map((String val) {
                          return new DropdownMenuItem<String>(
                            value: val,
                            child: Label(
                              title: val,
                              color: Colors.black,
                              fontWeight: FontWeight.w500,
                              fontSize: 16,
                              textAlign: TextAlign.center,
                            ),
                          );
                        }).toList(),
                        onChanged: (newVal) {
                          _selectedLocation = newVal;
                          setState(() {});
                        }),
                  ),
                  Container(
                    width: 350,
                    height: 60,
                    margin: EdgeInsets.only(
                      top: 35,
                      bottom: 40,
                    ),
                    child: BlocListener<UserBloc, UserState>(
                      listenWhen: (oldState, newState) {
                        return oldState.profileUpdatingResource == null ||
                            oldState.profileUpdatingResource.status !=
                                newState.profileUpdatingResource.status;
                      },
                      listener: (context, state) {
                        if (state.profileUpdatingResource != null &&
                            state.profileUpdatingResource.status ==
                                STATUS.ERROR) {
                          CommonMethods.showToast(fToast:fToast,message: state
                              .profileUpdatingResource.failure.message,status:false);
                        } else if (state.profileUpdatingResource != null &&
                            state.profileUpdatingResource.status ==
                                STATUS.SUCCESS) {
                          Navigator.of(context).pop();
                        }
                      },
                      child: BlocBuilder<UserBloc, UserState>(
                        buildWhen: (oldState, newState) {
                          return oldState.profileUpdatingResource == null ||
                              oldState.profileUpdatingResource.status !=
                                  newState.profileUpdatingResource.status;
                        },
                        builder: (context, state) {
                          if (state.profileUpdatingResource != null &&
                              state.profileUpdatingResource.status ==
                                  STATUS.LOADING) {
                            return circularProgressIndicator();
                          }
                          return Button(
                            width: 250,
                            title: AppLocalizations.of(context)
                                .translate("Update"),
                            color: Color(appColors.buttonColor),
                            borderColor:Colors.transparent,
                            textColor: Colors.white,
                            onTap: () {
                              final name = nameController.text.toString().trim();
                              final dobValue = dobToSend.isEmpty?dob:dobToSend;
                              final gender =  genderController.text.toString().trim();
                              final bloodGroup = _selectedLocation;
                              final sendImage = base64Image;

                              BlocProvider.of<UserBloc>(context).add(
                                UpdateProfileEvent(
                                    name: name,
                                    dob: dobValue,
                                    gender:gender.toLowerCase() == 'male' ? 0 : 1,
                                    image:sendImage,
                                    bloodGroup: bloodGroup),
                              );
                            },
                          );
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<DateTime> showDatePickerDialog(_context) {
    Future<DateTime> selectedDate = showDatePicker(
      context: context,
      firstDate: DateTime(1900),
      initialDate: _dateTimeNotifier.value != null
          ? _dateTimeNotifier.value
          : DateTime.now(),
      lastDate: DateTime.now(),
    ).then((DateTime dateTime) => _dateTimeNotifier.value = dateTime);
    return selectedDate;
  }

  @override
  void dispose() {
    super.dispose();
    nameController.dispose();
    dobController.dispose();
    genderController.dispose();
    bloodGroupController.dispose();
  }

}
