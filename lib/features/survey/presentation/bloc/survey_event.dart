part of 'survey_bloc.dart';

abstract class SurveyEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class GetSurveyEvent extends SurveyEvent {}

class ChooseAnswerEvent extends SurveyEvent {
  final questionId, optionId;

  ChooseAnswerEvent({@required this.questionId, @required this.optionId});
}

class SubmitAnswerEvent extends SurveyEvent {

  final  surveyId, surveyQuestionId, surveyOptionId;

  SubmitAnswerEvent({@required this.surveyId, @required this.surveyQuestionId, @required this.surveyOptionId});

  @override
  List<Object> get props => [surveyId, surveyQuestionId, surveyOptionId];
}


