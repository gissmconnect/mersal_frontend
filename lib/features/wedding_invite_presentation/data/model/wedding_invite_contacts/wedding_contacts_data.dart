class WeddingContactsData {
  int id;
  String name;
  String user_name;
  String gender;
  bool isSelected=false;
  String image;
  String phone;

  WeddingContactsData(
      {this.gender, this.user_name, this.phone, this.image, this.id, this.name});

  factory WeddingContactsData.fromJson(Map<String, dynamic> json) {
    return WeddingContactsData(
        id: json['id'],
        name: json['name'],
        phone: json['phone'],
        gender: json['gender'],
        image: json['image'],
        user_name: json['user_name']);
  }
}
