
import 'package:mersal/features/homescreen/home-domain/entity/BannerDataEntity.dart';
import 'package:mersal/features/notification/notification_data/model/ArchivedNotificationModel/ArchivedDataList.dart';

class ArchivedDataModel {
  List<ArchivedDataList> archivedList;
  bool status;

  ArchivedDataModel({this.archivedList,this.status});

  factory ArchivedDataModel.fromJson(Map<String, dynamic> json) {
    return ArchivedDataModel(
      archivedList: json['data'] != null ? (json['data'] as List).map((i) => ArchivedDataList.fromJson(i)).toList() : null,
      status: json['status'],
    );
  }
}