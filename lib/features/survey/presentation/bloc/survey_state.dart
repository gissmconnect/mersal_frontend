part of 'survey_bloc.dart';

class SurveyState extends Equatable {
  final Resource getSurveyResource;
  final int randomInt;

  SurveyState({this.randomInt = 0,this.getSurveyResource});

  factory SurveyState.initial() {
    return SurveyState(getSurveyResource: Resource.loading());
  }

  SurveyState copy({int randomInt,Resource getSurveyResource}) {
    return SurveyState(randomInt: randomInt??this.randomInt,
        getSurveyResource: getSurveyResource ?? this.getSurveyResource);
  }

  @override
  List<Object> get props => [randomInt,getSurveyResource];
}

class SurveyQuestionSubmitting extends SurveyState {}


class SurveyQuestionSuccess extends SurveyState {}

class SurveyQuestionError extends SurveyState {
  final Failure failure;

  SurveyQuestionError(this.failure);
}