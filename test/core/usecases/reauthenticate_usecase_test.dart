import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mersal/core/usecases/reauthenticate_usecase.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mockito/mockito.dart';

import '../../feature/auth/auth_domain/usecases/get_user_session_test.dart';

void main() {
  MockAuthRepository _mockAuthRepository;
  ReAuthenticateUseCase _reauthenticateUseCase;

  setUp(() {
    _mockAuthRepository = MockAuthRepository();
    _reauthenticateUseCase =
        ReAuthenticateUseCase(authRepository: _mockAuthRepository);
  });

  final _loginId = '12345678980';
  final _password = '123456';
  test('should re-authenticate and return nothing when login successful', () async {
    //arrange
    when(
      _mockAuthRepository.login(
        phoneNumber: anyNamed('phoneNumber'),
        password: anyNamed('password'),
      ),
    );

    //act
    final _result = await _reauthenticateUseCase(NoParams());

    //assert
    verify(
        _mockAuthRepository.login(phoneNumber: _loginId, password: _password));
    expect(_result, equals(Right(null)));
  });
}
