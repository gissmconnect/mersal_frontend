import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/notification/notification_domain/repository/notification_repo.dart';
import 'package:meta/meta.dart';

class ArchiveNotificationUseCase extends UseCase<String, int> {
  final NotificationRepo notificationRepo;

  ArchiveNotificationUseCase({@required this.notificationRepo});

  @override
  Future<Either<Failure, String>> call(int params) async {
    return await notificationRepo.archiveNotification(id: params);
  }
}
