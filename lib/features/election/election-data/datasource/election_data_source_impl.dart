import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:mersal/core/error/exceptions.dart';
import 'package:mersal/core/network/api_endpoints.dart';
import 'package:mersal/core/network/http_error_handler.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/election/election-data/datasource/election_data_source.dart';
import 'package:mersal/features/election/election-data/model/election_data_model.dart';
import 'package:mersal/features/election_candidate/election_candidate_data/model/election_candidate_model.dart';
import 'package:meta/meta.dart';

class ElectionDataSourceImpl implements ElectionDataSource {
  final http.Client httpClient;

  ElectionDataSourceImpl({@required this.httpClient});

  @override
  Future<List<ElectionDataModel>> getElections({@required String token}) async {
    var response = await httpClient.get(
      Uri.parse(GET_ELECTIONS_ENDPOINT),
      headers: {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        AUTHORIZATION: BEARER + " " + token
      },
    );
    if (response.statusCode == 200) {
      return ((json.decode(response.body)['data']) as List<dynamic>)
          .map((e) => ElectionDataModel.fromJson(e))
          .toList();
    }
    handleError(response);
  }

  @override
  Future<ElectionCandidateResponseModel> getCandidates(
      {String token, int electionId}) async {
    final _url = GET_ELECTION_CANDIDATES + "?election_id=$electionId";
    var response = await httpClient.get(
      Uri.parse(_url),
      headers: {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        AUTHORIZATION: BEARER + " " + token
      },
    );
    print('getCandidates response ${response.body}');
    if (response.statusCode == 200) {
      ElectionCandidateResponseModel candidateModel =
          ElectionCandidateResponseModel.fromJson(json.decode(response.body));
      return candidateModel;
    }
    handleError(response);
  }

  @override
  Future<void> vote({String token, int electionId, int candidateId}) async {
    final _params = <String, dynamic>{
      'election_id': electionId,
      'elector_id': candidateId
    };
    var response = await httpClient.post(Uri.parse(VOTE),
        headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json',
          AUTHORIZATION: BEARER + " " + token
        },
        body: json.encode(_params));

    print('response ${response.statusCode} and ${response.body}');
    if (response.statusCode == 200) {
      final Map<String, dynamic> _responseJson = json.decode(response.body);
      if (_responseJson.containsKey('data')) {
        return ((json.decode(response.body)['data']) as List<dynamic>)
            .map((e) => ElectionCandidateModel.fromJson(e))
            .toList();
      } else {
        throw ServerException()..message = _responseJson['message'];
      }
    }
    handleError(response);
  }
}
