import 'package:flutter/material.dart';
import 'package:mersal/core/widgets/common/Button.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/CustomAppBar.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/features/payment/payment_option.dart';
import 'package:mersal/resources/colors.dart';

class WeddingSummaryScreen extends StatefulWidget {

  final String weddingName,weddingDate,weddingTime;

  WeddingSummaryScreen(this.weddingName,this.weddingTime,this.weddingDate);

  @override
  _WeddingSummaryScreenState createState() => _WeddingSummaryScreenState();
}

class _WeddingSummaryScreenState extends State<WeddingSummaryScreen> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              child: Label(
                title: "Wedding Booking Summary",
                textAlign: TextAlign.center,
                fontWeight: FontWeight.w700,
                fontSize: 20,
                color: Color(appColors.buttonColor),
              ),
            ),
            weddingSummary(),
          ],
        ),
      ),
    );
  }

  Widget weddingSummary() {
    return Container(
      margin: EdgeInsets.only(left: 25, right: 25, top: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 40),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Label(
                    title: "Name:",
                    textAlign: TextAlign.center,
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                    color: Colors.black,
                  ),
                  SizedBox(height: 10),
                  Label(
                    title: "Date:",
                    textAlign: TextAlign.center,
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                    color: Colors.black,
                  ),
                  SizedBox(height: 10),
                  Label(
                    title: "Time:",
                    textAlign: TextAlign.center,
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                    color: Colors.black,
                  ),
                  SizedBox(height: 10),
                  Label(
                    title: "Amount:",
                    textAlign: TextAlign.center,
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                    color: Colors.black,
                  ),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Label(
                    title: widget.weddingName??"",
                    textAlign: TextAlign.center,
                    fontSize: 18,
                    color: Colors.black,
                  ),
                  SizedBox(height: 10),
                  Label(
                    title: widget.weddingDate??"",
                    textAlign: TextAlign.center,
                    fontSize: 18,
                    color: Colors.black,
                  ),
                  SizedBox(height: 10),
                  Label(
                    title: widget.weddingTime??"",
                    textAlign: TextAlign.center,
                    fontSize: 18,
                    color: Colors.black,
                  ),
                  SizedBox(height: 10),
                  Label(
                    title: "OMR1",
                    textAlign: TextAlign.center,
                    fontSize: 18,
                    color: Colors.black,
                  ),
                ],
              ),
            ],
          ),
          Container(
            height: 60,
            margin: EdgeInsets.only(top: 35, bottom: 40),
            child: Button(
              title: "Confirm",
              color: Color(appColors.buttonColor),
              borderColor:Colors.transparent,
              textColor: Colors.white,
              onTap: () {
                paymentBottomSheet();
              },
            ),
          ),
        ],
      ),
    );
  }

  Future<Widget> paymentBottomSheet() {
    return showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (context) {
          return Container(
              margin: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height / 3.5),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.vertical(top: Radius.circular(30.0)),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black,
                    offset: Offset(0.0, 1.0), //(x,y)
                    blurRadius: 5.0,
                  ),
                ],
              ),
              child:PaymentOptions("wedding"));
        });
  }



}
