import 'package:mersal/features/wedding_invite_presentation/domain/entity/wedding_template_entity.dart';
import 'package:meta/meta.dart';

class WeddingTemplateModel extends WeddingTemplateEntity {
  WeddingTemplateModel(
      {@required bool status, @required List<TemplateModel> data})
      : super(status: status, data: data);

  WeddingTemplateModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['data'] != null) {
      data = new List<TemplateModelEntity>();
      json['data'].forEach((v) {
        data.add(new TemplateModel.fromJson(v));
      });
    }
  }
}

class TemplateModel extends TemplateModelEntity {
  TemplateModel({int id, String title, String template, String created_at})
      : super(id: id, title: title, template: template, created_at: created_at);

  TemplateModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    template = json['template'];
    created_at = json['created_at'];
  }
}
