import 'dart:async';
import 'dart:math';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/network/vo/Resource.dart';
import 'package:mersal/core/usecases/get_user_login_data_usecase.dart';
import 'package:mersal/core/usecases/reauthenticate_usecase.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/prayer_timeline/data/data/prayer_data_model.dart';
import 'package:mersal/features/prayer_timeline/domain/entity/prayer_data_entity.dart';
import 'package:mersal/features/prayer_timeline/domain/usecase/get_prayertimeline_usecase.dart';
import 'package:mersal/features/prayer_timeline/domain/usecase/set_alarm_usecase.dart';
import 'package:meta/meta.dart';

part 'prayer_event.dart';

part 'prayer_state.dart';

class PrayerBloc extends Bloc<PrayerEvent, PrayerState> {
  final GetPrayerTimeLineUseCase getPrayerTimeLineUseCase;
  final SetAlarmUseCase setAlarmUseCase;
  final GetUserLoginDataUseCase getUserLoginDataUseCase;
  final ReAuthenticateUseCase reAuthenticateUseCase;
  PrayerDataEntity upcomingPrayer;

  PrayerBloc(
      {@required this.getPrayerTimeLineUseCase,
      @required this.getUserLoginDataUseCase,
      @required this.reAuthenticateUseCase,
      @required this.setAlarmUseCase})
      : super(PrayerState.initial());

  @override
  Stream<PrayerState> mapEventToState(
    PrayerEvent event,
  ) async* {
    if (event is GetPrayerTimesEvent) {
      yield state.copy(prayerTimesResource: Resource.loading());
      final _prayerResponse = await getPrayerTimeLineUseCase.call(NoParams());
      yield* _prayerResponse.fold(
        (l) async* {
          if (l is AuthFailure) {
            final _reauthenticateResponse =
                await reAuthenticateUseCase(NoParams());
            _reauthenticateResponse.fold((l) async* {
              yield Left(l);
            }, (r) => add(GetPrayerTimesEvent()));
          } else {
            yield state.copy(prayerTimesResource: Resource.error(failure: l));
          }
        },
        (r) async* {
          PrayerDataEntity _recentPrayerTime;
          for (final element in r) {
            print('length name ${element.name} ${element.isNext} and ${element.isPrayerOver}');
            if (element.isNext == 1 && !element.isPrayerOver) {
              _recentPrayerTime = element;
              break;
            }
          }
          yield state.copy(
              prayerTimesResource: Resource.success(data: r),
              upcomingPrayer: _recentPrayerTime);
        },
      );
    }

    if (event is AddPrayerEvent) {
      List<PrayerDataEntity> _prayers =
          (state.prayerTimesResource.data as List<PrayerDataEntity>)
              .map((e) => e.doDomain()).toList();
      int _index = _prayers.indexWhere((element) => element.id == event.id);
      _prayers[_index] = (_prayers[_index].copy(settingAlarm: true));
      yield state.copy(
          prayerTimesResource: Resource.success(data: _prayers),
          randomId: Random().nextInt(10000),
          setAlarmResource: Resource.loading());

      final _response = await setAlarmUseCase.call(event.id);
      _prayers[_index] = (_prayers[_index].copy(settingAlarm: false));
      yield* _response.fold(
        (l) async* {
          if (l is AuthFailure) {
            final _reauthenticateResponse =
                await reAuthenticateUseCase(NoParams());
            _reauthenticateResponse.fold((l) async* {
              yield Left(l);
            }, (r) => add(GetPrayerTimesEvent()));
          } else {
            yield state.copy(setAlarmResource: Resource.error(failure: l));
          }
        },
        (r) async* {
          yield state.copy(
              prayerTimesResource: Resource.success(data: _prayers),
              randomId: Random().nextInt(10000),
              setAlarmResource: Resource.success(data: r));
          add(GetPrayerTimesEvent());
        },
      );
    }
  }
}
