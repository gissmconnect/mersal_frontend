import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/get_user_login_data_usecase.dart';
import 'package:mersal/core/usecases/reauthenticate_usecase.dart';
import 'package:mersal/core/usecases/save_user_login_data_usecase.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/banner/banner-domain/entity/banner_entity.dart';
import 'package:mersal/features/banner/banner-domain/usecases/get_banner_usercase.dart';
import 'package:mersal/features/election/election-domain/entity/election_entity.dart';
import 'package:mersal/features/election/election-domain/usecases/get_elections_usecase.dart';
import 'package:meta/meta.dart';

part 'banner_event.dart';

part 'banner_state.dart';

class BannerBloc extends Bloc<BannerEvent, BannerState> {
  final GetBannerUseCase getBannerUseCase;

  BannerBloc(
      {@required this.getBannerUseCase})
      : super(BannerLoading());

  @override
  Stream<BannerState> mapEventToState(
      BannerEvent event,
      ) async* {
    if (event is GetBanners) {
      yield BannerLoading();
      final _elections = await getBannerUseCase(NoParams());
      yield* _elections.fold(
            (failure) async* {
            yield BannerFailed(failure: failure);
        },
            (data) async* {
          yield BannerLoaded(banners: data);
        },
      );
    }
  }

}
