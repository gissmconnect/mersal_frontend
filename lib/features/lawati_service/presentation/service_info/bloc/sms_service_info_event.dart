part of 'sms_service_info_bloc.dart';

abstract class SmsServiceInfoEvent extends Equatable {
  const SmsServiceInfoEvent();
  @override
  List<Object> get props => [];
}

class GetSmsServiceInfoEvent extends SmsServiceInfoEvent{}

class SubscribeEvent extends SmsServiceInfoEvent{
  final SubscriptionRequestParams params;

  SubscribeEvent({@required this.params});

  @override
  List<Object> get props => [params.id];
}
