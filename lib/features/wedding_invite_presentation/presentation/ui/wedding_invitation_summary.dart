import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:mersal/core/network/api_endpoints.dart';
import 'package:mersal/core/network/http_error_handler.dart';
import 'package:mersal/core/widgets/common/Button.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/core/widgets/common/shared_preference.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/dashboard/dashboard_page.dart';
import 'package:mersal/features/payment/payment_webview.dart';
import 'package:mersal/features/wedding_invite_presentation/data/model/wedding_template_image/wedding_template_image_data.dart';
import 'package:mersal/features/wedding_invite_presentation/presentation/ui/wedding_invite_success_page.dart';
import 'package:mersal/resources/colors.dart';

class WeddingInvitationSummary extends StatefulWidget {
  final WeddingTemplateImageData templateImageList;
  final String invitationType,
      sendPhone,
      weddingInviteDate,
      templateId,
      weddingTime,
      groomName,
      brideName,
      weddingType,
      mobileNumber;

  final PickedFile userSelectedImage;

  WeddingInvitationSummary({this.invitationType,
    this.weddingInviteDate,
    this.sendPhone,
    this.userSelectedImage,
    this.weddingType,
    this.brideName,
    this.groomName,
    this.templateId,
    this.weddingTime,
    this.mobileNumber,
    this.templateImageList});

  @override
  _WeddingInvitationSummaryState createState() =>
      _WeddingInvitationSummaryState();
}

class _WeddingInvitationSummaryState extends State<WeddingInvitationSummary> {
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  BuildContext mContextLoader;
  String weddingId = "";
  String base64Image = "";
  List<int> list;
  FToast fToast;

  @override
  void initState() {
    super.initState();
    _asyncMethod();
    fToast = FToast();
    fToast.init(context);
  }

  _asyncMethod() async {
    weddingId = await appPreferences.getStringPreference("bookingId");
    if(widget.userSelectedImage!=null){
      final bytes = File(widget.userSelectedImage.path).readAsBytesSync();
      base64Image = base64Encode(bytes);
      print(bytes);
      setState(() {});
      print("Image=========" + base64Image);
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Container(
        height: MediaQuery
            .of(context)
            .size
            .height,
        width: MediaQuery
            .of(context)
            .size
            .width,
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(top: 40),
                child: Label(
                  title: "Wedding Invitation Summary",
                  textAlign: TextAlign.center,
                  fontWeight: FontWeight.w700,
                  fontSize: 20,
                  color: Colors.black,
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 40.0),
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 25, right: 25, top: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          widget.userSelectedImage!=null &&   widget.userSelectedImage.path.isNotEmpty ? Container(
                            decoration: BoxDecoration(
                                border: Border.all(
                                    color: Color(appColors.buttonColor),
                                    width: 2)),
                            child: Image.file(
                              File(widget.userSelectedImage.path),
                              height: 200,
                              width: 200,
                              fit: BoxFit.fill,
                            ),
                          ) : Container(),
                          SizedBox(height: 40),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Expanded(
                                child: Container(
                                  margin: EdgeInsets.only(left: 15),
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      Label(
                                        title: "Bride Name",
                                        textAlign: TextAlign.center,
                                        fontSize: 18,
                                        color: Colors.black,
                                      ),
                                      SizedBox(height: 10),
                                      Label(
                                        title: "Groom Name",
                                        textAlign: TextAlign.center,
                                        fontSize: 18,
                                        color: Colors.black,
                                      ),
                                      SizedBox(height: 10),
                                      Label(
                                        title: "Invitation Type",
                                        textAlign: TextAlign.center,
                                        fontSize: 18,
                                        color: Colors.black,
                                      ),
                                      SizedBox(height: 10),
                                      Label(
                                        title: "Date",
                                        textAlign: TextAlign.center,
                                        fontSize: 18,
                                        color: Colors.black,
                                      ),
                                      SizedBox(height: 10),
                                      Label(
                                        title: "Time",
                                        textAlign: TextAlign.center,
                                        fontSize: 18,
                                        color: Colors.black,
                                      ),
                                      SizedBox(height: 10),
                                      Label(
                                        title: "Amount",
                                        textAlign: TextAlign.center,
                                        fontSize: 18,
                                        color: Colors.black,
                                      ),

                                    ],
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Label(
                                      title: widget.brideName,
                                      textAlign: TextAlign.center,
                                      fontSize: 18,
                                      color: Colors.black,
                                    ),
                                    SizedBox(height: 10), Label(
                                      title: widget.groomName,
                                      textAlign: TextAlign.center,
                                      fontSize: 18,
                                      color: Colors.black,
                                    ),
                                    SizedBox(height: 10), Label(
                                      title: widget.invitationType,
                                      textAlign: TextAlign.center,
                                      fontSize: 18,
                                      color: Colors.black,
                                    ),
                                    SizedBox(height: 10),
                                    Label(
                                      title: widget.weddingInviteDate,
                                      textAlign: TextAlign.center,
                                      fontSize: 18,
                                      color: Colors.black,
                                    ),
                                    SizedBox(height: 10),
                                    Label(
                                      title: widget.weddingTime,
                                      textAlign: TextAlign.center,
                                      fontSize: 18,
                                      color: Colors.black,
                                    ),
                                    SizedBox(height: 10),
                                    Label(
                                      title: "OMR 10.0",
                                      textAlign: TextAlign.center,
                                      fontSize: 18,
                                      color: Colors.black,
                                    ),

                                  ],
                                ),
                              ),
                            ],
                          ),
                          Container(
                            height: 50,
                            margin: EdgeInsets.only(
                                top: 55, bottom: 40, left: 20, right: 20),
                            child: Button(
                              title: "Confirm",
                              color: Color(appColors.buttonColor),
                              borderColor: Colors.transparent,
                              textColor: Colors.white,
                              onTap: () {
                                weddingInviteApi();
                                // if(weddingId.isNotEmpty){
                                // weddingInviteApi();
                                // }
                                // else{
                                //   CommonMethods.showToast(
                                //       fToast: fToast,
                                //       message: "Please create a wedding booking",
                                //       status: false);
                                // }
                                // CommonMethods().animatedNavigation(context: context,currentScreen:WeddingInvitationSummary(),
                                //     landingScreen: PaymentOptions("invite"));
                              },
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Future<void> weddingInviteApi() async {
    final _token = await appPreferences.getStringPreference("AccessToken");

    print("Image=========" + base64Image);
    var value;
    if (widget.invitationType == "Send invitation to selected number") {
      value = "0";
    } else {
      value = "1";
    }

    final _params = <String, String>{
      'booking_id': "1",
      'template_id': widget.templateId??"",
      'template_image_id': widget.templateImageList != null ? widget.templateImageList.id.toString() : "",
      'date': widget.weddingInviteDate,
      'wedding_date': widget.weddingInviteDate,
      'time': widget.weddingTime,
      'groom_name': widget.groomName,
      'bride_name': widget.brideName,
      'phone_numbers': widget.sendPhone,
      'type': value,
      'image': base64Image,
      'wedding_type': widget.weddingType.toLowerCase() == "aqid" ? "0" : "1",
    };

    if (context != null) {
      _showLoader(context);
    }

    print("Invite Params====${_params.toString()}");

    var response = await http.post(Uri.parse(WEDDING_BOOKING_INVITATION),
        headers: {
          'Content-type': 'application/json',
          AUTHORIZATION: BEARER + " " + _token
        },
        body: json.encode(_params));

    if (response.statusCode == 200) {
      if (mContextLoader != null) Navigator.pop(mContextLoader);
      final _responseJson = json.decode(response.body);
      // Fluttertoast.showToast(
      //     msg: _responseJson['message'],
      //     toastLength: Toast.LENGTH_LONG,
      //     gravity: ToastGravity.BOTTOM,
      //     timeInSecForIosWeb: 1,
      //     backgroundColor: Colors.grey.shade200.withOpacity(0.7),
      //     textColor: Colors.black,
      //     fontSize: 16.0);

      Navigator.pop(context);

      CommonMethods().animatedNavigation(
          context: context,
          currentScreen:
          WeddingInvitationSummary(),
          landingScreen: PaymentWebView(
            type: "invitation",
            id:_responseJson['data']['id'].toString(),
            activity: "",
          ));
      // showSuccessDialog(context);
    }
    else {
      if (mContextLoader != null) Navigator.pop(mContextLoader);
      handleError(response);
    }
  }

  Future<Null> _showLoader(BuildContext context) async {
    return showDialog<Null>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context1) {
        mContextLoader = context1;
        return Scaffold(
          backgroundColor: Colors.transparent.withOpacity(0.3),
          key: _keyLoader,
          body: Container(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    child: CircularProgressIndicator(),
                    height: 40.0,
                    width: 40.0,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  void showSuccessDialog(BuildContext context) {
    showGeneralDialog(
      barrierLabel: "Barrier",
      barrierDismissible: true,
      barrierColor: Colors.black.withOpacity(0.8),
      transitionDuration: Duration(milliseconds: 500),
      context: context,
      pageBuilder: (_, __, ___) {
        return Align(
          alignment: Alignment.center,
          child: Container(
            height: 450,
            margin: EdgeInsets.only(left: 10, right: 10),
            decoration: new BoxDecoration(
              color: const Color(0xFFFFFF),
              borderRadius: new BorderRadius.all(new Radius.circular(32.0)),
            ),
            child: WeddingInviteSuccessPage(),
          ),
        );
      },
      transitionBuilder: (_, anim, __, child) {
        return SlideTransition(
          position: Tween(begin: Offset(0, 1), end: Offset(0, 0)).animate(anim),
          child: child,
        );
      },
    );
  }

}

// Invite Params===={booking_id: 10, template_id: , template_image_id: , date: 2021-11-21, wedding_date: 2021-11-21, time: 16:31, groom_name: Qwerty, bride_name: Ya, phone_numbers: , type: 1, image: /9j
// ErrorException: Trying to get property 'id' of non-object in file /var/www/html/vendor/laravel/framework/src/Illuminate/Http/Resources/DelegatesToResource.php on line 136
// I/flutter (24560):
// I/flutter (24560): #0 /var/www/html/vendor/laravel/framework/src/Illuminate/Http/Resources/DelegatesToResource.php(136): Illuminate\Foundation\Bootstrap\HandleExceptions-&gt;handleError()
// I/flutter (24560): #1 /var/www/html/app/Http/Resources/WeddingInvitationResource.php(18): Illuminate\Http\Resources\Json\JsonResource-&gt;__get()
// I/flutter (24560): #2 /var/www/html/vendor/laravel/framework/src/Illuminate/Http/Resources/Json/JsonResource.php(95):