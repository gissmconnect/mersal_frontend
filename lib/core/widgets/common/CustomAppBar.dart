import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mersal/resources/colors.dart';
import 'package:mersal/resources/fonts.dart';

class CustomAppBar extends StatefulWidget implements PreferredSizeWidget {
  @override
  _CustomAppBarState createState() => _CustomAppBarState();

  final String title, rightText, rightIconTwoValue;
  final double height = 90;
  final bool isBackButton,
      leftIcon,
      rightIconOne,
      rightIconTwo,
      showText,
      showHambBool;
  CustomAppBar({
    Key key,
    @required this.isBackButton,
    this.title,
    this.leftIcon,
    this.rightIconTwoValue,
    this.rightIconOne,
    this.rightIconTwo,
    this.showText,
    this.showHambBool,
    this.rightText,
  }) : super(key: key);

  @override
  Size get preferredSize => Size.fromHeight(height);
}

class _CustomAppBarState extends State<CustomAppBar> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context0) {
    return Container(
      padding: EdgeInsets.only(left: 14, bottom: 10, top: 20),
      height: widget.height,
      color: Color(appColors.blue),
      child: Stack(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(bottom: 5),
            alignment: Alignment.bottomLeft,
            child: GestureDetector(
              child: widget.showHambBool != null && widget.showHambBool
                  ? Icon(
                      Icons.menu,
                      size: 30,
                    )
                  : Icon(
                      Icons.arrow_back_ios_rounded,
                      color: Colors.white,
                    ),
              onTap: () {
                if (widget.isBackButton) Navigator.of(context).pop(false);
              },
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 6),
            alignment: Alignment.bottomCenter,
            child: Text(
              widget.title ?? "",
              textAlign: TextAlign.end,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                  fontFamily: fonts.segoeui,
                  fontWeight: FontWeight.w400),
            ),
          ),
          // Container(
          //   margin: EdgeInsets.only(bottom: 4),
          //   alignment: Alignment.bottomRight,
          //   child:Row(
          //           children: [
          //             widget.rightIconOne != null && widget.rightIconOne
          //                 ? GestureDetector(
          //                     onTap: () {
          //                       CommonMethods().animatedNavigation(
          //                           landingScreen: NotificationScreen(),
          //                           currentScreen: DashboardPage(),
          //                           context: context);
          //                     },
          //                     child: Container(
          //                       margin: EdgeInsets.only(right: 20),
          //                       alignment: Alignment.centerRight,
          //                       child: Icon(
          //                         Icons.notifications,
          //                         size: 30,
          //                       ),
          //                     ),
          //                   )
          //                 : Container(
          //                     margin: EdgeInsets.only(right: 20),
          //                     alignment: Alignment.centerRight,
          //                   ),
          //             widget.rightIconTwo != null && widget.rightIconTwo
          //                 ? Container(
          //                     margin: EdgeInsets.only(right: 20),
          //                     alignment: Alignment.centerRight,
          //                     child: Icon(
          //                       Icons.info,
          //                       size: 30,
          //                     ),
          //                   )
          //                 : Container(
          //                     margin: EdgeInsets.only(right: 20),
          //                     alignment: Alignment.centerRight,
          //                   )
          //           ],
          //         ),
          // ),
        ],
      ),
      // child: Row(
      //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
      //   children: <Widget>[
      //     widget.isBackButton? Container(
      //       margin: EdgeInsets.only(bottom: 10),
      //       alignment: Alignment.bottomLeft,
      //       child: GestureDetector(
      //         child: Icon(Icons.arrow_back_ios_rounded),
      //         onTap: () {
      //           if (widget.isBackButton) Navigator.of(context).pop(false);
      //         },
      //       ),
      //     ):Container(),
      //     if (widget.showText != null && widget.showText)
      //       Center(
      //         child: Container(
      //           child: Text(
      //             widget.title,
      //             style: TextStyle(
      //                 color: Color(appColors.blue00008B),
      //                 fontSize: 18,
      //                 fontFamily: fonts.appFont,
      //                 fontWeight: FontWeight.w600),
      //           ),
      //         ),
      //       )
      //     else
      //       Container(
      //         alignment: Alignment.bottomCenter,
      //         // child:SvgPicture.asset(appImages.splashImage,width: 20,height: 40,),
      //       ),
      //     Row(
      //       children: [
      //         widget.rightIconOne != null && widget.rightIconOne
      //             ? GestureDetector(
      //                 onTap: () {
      //                   CommonMethods().animatedNavigation(
      //                       landingScreen: NotificationScreen(),
      //                       currentScreen: DashboardPage(),
      //                       context: context);
      //                 },
      //                 child: Container(
      //                   margin: EdgeInsets.only(right: 20),
      //                   alignment: Alignment.centerRight,
      //                   child: Icon(
      //                     Icons.notifications,
      //                     size: 30,
      //                   ),
      //                 ),
      //               )
      //             : Container(
      //                 margin: EdgeInsets.only(right: 20),
      //                 alignment: Alignment.centerRight,
      //               ),
      //         widget.rightIconTwo != null && widget.rightIconTwo
      //             ? Container(
      //                 margin: EdgeInsets.only(right: 20),
      //                 alignment: Alignment.centerRight,
      //                 child: Icon(
      //                   Icons.info,
      //                   size: 30,
      //                 ),
      //               )
      //             : Container(
      //                 margin: EdgeInsets.only(right: 20),
      //                 alignment: Alignment.centerRight,
      //               )
      //       ],
      //     )
      //   ],
      // ),
    );
  }
}
