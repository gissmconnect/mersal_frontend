part of 'prayer_bloc.dart';

abstract class PrayerEvent extends Equatable {
  const PrayerEvent();
}

class GetPrayerTimesEvent extends PrayerEvent {
  @override
  List<Object> get props => [];
}

class AddPrayerEvent extends PrayerEvent {
  final int id;

  AddPrayerEvent({@required this.id});

  @override
  List<Object> get props => [id];
}
