import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:mersal/core/localization/app_localization.dart';
import 'package:mersal/core/network/api_endpoints.dart';
import 'package:mersal/core/widgets/common/Button.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/core/widgets/common/shared_preference.dart';
import 'package:mersal/core/widgets/message.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/wedding_invite_presentation/data/model/wedding_invite_contacts/wedding_contact_model.dart';
import 'package:mersal/features/wedding_invite_presentation/data/model/wedding_invite_contacts/wedding_contacts_data.dart';
import 'package:mersal/features/wedding_invite_presentation/presentation/wedding_invite_list/model/wedding_invite_template_model.dart';
import 'package:mersal/features/wedding_invite_presentation/presentation/wedding_invite_list/ui/wedding_invitation_list_summary.dart';
import 'package:mersal/resources/colors.dart';

class WeddingInviteContactsPage extends StatefulWidget {
  final WeddingInviteTemplateModel templateImageList;
  final String invitationType,
      templateId,
      weddingInviteDate,
      weddingTime,
      weddingType,
      brideName,
      groomName,
      mobileNumber;

  WeddingInviteContactsPage(
      {this.invitationType,
      this.weddingInviteDate,
      this.templateId,
      this.weddingType,
      this.weddingTime,
      this.groomName,
      this.brideName,
      this.mobileNumber,
      this.templateImageList});

  @override
  _WeddingInviteContactsPageState createState() =>
      _WeddingInviteContactsPageState();
}

class _WeddingInviteContactsPageState extends State<WeddingInviteContactsPage> {
  Iterable<Contact> _contacts;

  int currentIndex = 0;

  List<bool> contactsBoolList = [];
  List<String> phoneNumbersList = [];
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  BuildContext mContextLoader;
  WeddingContactDataModel _weddingContactDataModel;
  List<WeddingContactsData> weddingContactsDataList = [];

  FToast fToast;


  @override
  void initState() {
    // getContacts();
    super.initState();
    contactsApiCall();
    fToast = FToast();
    fToast.init(context);
  }

  Future<void> getContacts() async {
    final Iterable<Contact> contacts = await ContactsService.getContacts();
    setState(() {
      _contacts = contacts;
      for (int i = 0; i < contacts.length; i++) {
        contactsBoolList.add(false);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bg_image.jpg"),
            fit: BoxFit.fill,
          ),
        ),
        child: Column(
          children: [
            SizedBox(
              height: 50,
            ),
            Stack(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(bottom: 5, left: 15),
                  alignment: Alignment.bottomLeft,
                  child: GestureDetector(
                    child: Icon(
                      Icons.arrow_back_ios_rounded,
                      color: Colors.white,
                    ),
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 6),
                  alignment: Alignment.bottomCenter,
                  child: Text(
                    AppLocalizations.of(context)
                        .translate("Wedding Invitation"),
                    textAlign: TextAlign.end,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.w400),
                  ),
                ),
              ],
            ),
            weddingContactsDataList != null &&
                    weddingContactsDataList.length > 0
                ? Expanded(
                    child: ListView.builder(
                      itemCount: weddingContactsDataList?.length ?? 0,
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                          onTap: () {
                            currentIndex = index;
                            weddingContactsDataList[index].isSelected =
                                !weddingContactsDataList[index].isSelected;
                            if (weddingContactsDataList[index].isSelected) {
                              phoneNumbersList.add(weddingContactsDataList
                                  .elementAt(index)
                                  .phone);
                            } else {
                              phoneNumbersList.remove(weddingContactsDataList
                                  .elementAt(index)
                                  .phone);
                            }
                            print("Proinfff $phoneNumbersList");
                            setState(() {});
                          },
                          child: ListTile(
                            contentPadding: const EdgeInsets.symmetric(
                                vertical: 5, horizontal: 18),
                            leading: (weddingContactsDataList[index].image !=
                                        null &&
                                    weddingContactsDataList[index]
                                        .image
                                        .isNotEmpty)
                                ? CachedNetworkImage(
                                    imageUrl:
                                        weddingContactsDataList[index].image,
                                    fit: BoxFit.cover,
                                    height: 50,
                                    width: 50,
                                  )
                                : Icon(Icons.person_outline,
                                    size: 40, color: Colors.white),
                            title: Label(
                              title: weddingContactsDataList[index].name ??
                                  'No Name',
                              fontSize: 20,
                              color: Colors.white,
                            ),
                            trailing: Container(
                                margin: EdgeInsets.only(right: 10),
                                child: Icon(
                                    weddingContactsDataList[index].isSelected
                                        ? Icons.radio_button_on
                                        : Icons.radio_button_unchecked,
                                    color: weddingContactsDataList[index]
                                            .isSelected
                                        ? Color(appColors.buttonColor)
                                        : Colors.white)),
                          ),
                        );
                      },
                    ),
                  )
                : Expanded(
                    child: Center(
                    child: Container(
                      child: Label(
                        title: "No Record(s) found",
                        color: Colors.black,
                        fontWeight: FontWeight.w600,
                        fontSize: 18,
                      ),
                    ),
                  )),
            Container(
              height: 60,
              margin: EdgeInsets.only(top: 20, bottom: 10, left: 20, right: 20),
              child: Button(
                title: AppLocalizations.of(context)
                    .translate("Next"),
                color: Color(appColors.buttonColor),
                borderColor: Colors.transparent,
                textColor: Colors.white,
                onTap: () {
                  if (phoneNumbersList.length > 0) {
                    weddingInvitationBottomSheet();
                  } else {
                    CommonMethods.showToast(
                        fToast: fToast,
                        message:
                        AppLocalizations.of(context)
                            .translate("Please select contact to invite"),
                        status: false);
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> contactsApiCall() async {
    final _token = await appPreferences.getStringPreference("AccessToken");
    if (context != null) {
      _showLoader(context);
    }
    var response = await http.get(
      Uri.parse(USER_CONTACTS),
      headers: {
        'Content-type': 'application/json',
        AUTHORIZATION: BEARER + " " + _token
      },
    );

    if (response.statusCode == 200) {
      if (mContextLoader != null) Navigator.pop(mContextLoader);
      final Map<String, dynamic> _responseJson = json.decode(response.body);

      // print('ContactsFinalResponse ${response.statusCode} and ${_responseJson.toString()}');
      _weddingContactDataModel =
          WeddingContactDataModel.fromJson(_responseJson);
      weddingContactsDataList = _weddingContactDataModel.weddingDataList;
      setState(() {});
      // for (int i = 0; i < weddingContactsDataList.length; i++) {
      //   contactsBoolList.add(false);
      // }
      // print('ContactsFinalResponse ${weddingContactsDataList}');
    }
  }

  Future<Null> _showLoader(BuildContext context) async {
    return showDialog<Null>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context1) {
        mContextLoader = context1;
        return Scaffold(
          backgroundColor: Colors.transparent.withOpacity(0.3),
          key: _keyLoader,
          body: Container(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    child: CircularProgressIndicator(),
                    height: 40.0,
                    width: 40.0,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Future<Widget> weddingInvitationBottomSheet() {
    String sendPhone = "";

    for (int i = 0; i < phoneNumbersList.length; i++) {
      if (i == 0) {
        sendPhone = phoneNumbersList[i].toString();
      } else {
        sendPhone = sendPhone + "," + phoneNumbersList[i].toString();
      }
    }
    print("pffff" + sendPhone);
    return showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (context) {
          return Container(
              margin: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height / 3.5),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.vertical(top: Radius.circular(30.0)),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black,
                    offset: Offset(0.0, 1.0), //(x,y)
                    blurRadius: 5.0,
                  ),
                ],
              ),
              child: WeddingInvitationListSummary(
                  sendPhone: sendPhone,
                  templateId: widget.templateId,
                  invitationType: widget.invitationType,
                  weddingInviteDate: widget.weddingInviteDate,
                  weddingTime: widget.weddingTime,
                  groomName: widget.groomName,
                  brideName: widget.brideName,
                  weddingType: widget.weddingType,
                  mobileNumber: widget.mobileNumber,
                  templateImageList: widget.templateImageList));
        });
  }
}
