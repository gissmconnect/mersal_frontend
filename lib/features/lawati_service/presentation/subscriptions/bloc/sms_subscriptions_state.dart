part of 'sms_subscriptions_bloc.dart';

class SmsSubscriptionsState extends Equatable {
  final Resource subscriptions;

  SmsSubscriptionsState({this.subscriptions});

  factory SmsSubscriptionsState.initial() {
    return SmsSubscriptionsState(subscriptions: Resource.loading());
  }

  SmsSubscriptionsState copy({Resource subscriptions}) {
    return SmsSubscriptionsState(
        subscriptions: subscriptions ?? this.subscriptions);
  }

  @override
  List<Object> get props => [subscriptions];
}
