part of 'committe_management_bloc.dart';

abstract class CommitteManagementEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class GetCommitteesEvent extends CommitteManagementEvent{}