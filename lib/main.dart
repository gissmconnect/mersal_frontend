import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:mersal/core/bloc/user_bloc.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/features/auth/auth_presentation/login_presentation/ui/login_page.dart';
import 'package:mersal/features/homescreen/home-presentation/ui/home_screen.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'core/localization/app_language.dart';
import 'core/localization/app_localization.dart';
import 'features/splash_presentation/ui/splash_page.dart';
import 'injection_container.dart' as di;
import 'injection_container.dart';

final navigatorKey = GlobalKey<NavigatorState>();

const AndroidNotificationChannel channel = AndroidNotificationChannel(
    "Mersal_App_Channel", "Notification",
    importance: Importance.high, playSound: true);

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
  print("A message notification showed${message.messageId}");
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await di.init();
  await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
          AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);
  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
      alert: true, badge: true, sound: true);
  AppLanguage appLanguage = AppLanguage();
  await appLanguage.fetchLocale();

  runApp(
    MultiBlocProvider(
      providers: [
        BlocProvider<UserBloc>(
          create: (_) => serviceLocator<UserBloc>(),
        ),
      ],
      child: MyApp(appLanguage: appLanguage),
    ),
  );
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  AppLanguage appLanguage;

  MyApp({this.appLanguage});

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
    notificationReceived();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<AppLanguage>(
      create: (_) => widget.appLanguage,
      child: Consumer<AppLanguage>(
        builder: (context, model, child) => MaterialApp(
          navigatorKey: navigatorKey, // Setting a global key for navigator
          debugShowCheckedModeBanner: false, 
          localizationsDelegates: [
            AppLocalizations.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
          locale: model.appLocal,
          supportedLocales: [
            const Locale('en'), // English
            const Locale('ar'), // Arabic
            // ... other locales the app supports
          ],
          title: 'Flutter Demo',
          theme: ThemeData(
            primarySwatch: Colors.blue,
          ),
          home: SplashPage(),
        ),
      ),
    );
  }

  void notificationReceived() {
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification notification = message.notification;
      AndroidNotification androidNotification = message.notification?.android;
      if (notification != null && androidNotification != null) {
        flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
                android: AndroidNotificationDetails(
                    channel.id, channel.name,
                    color: Colors.blue,
                    playSound: true,
                    icon: "@mipmap/ic_launcher")));
      }
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      RemoteNotification notification = message.notification;
      AndroidNotification androidNotification = message.notification?.android;
      if (notification != null && androidNotification != null) {}
    });
  }
}

// set up the button
Widget okButton = TextButton(
    child: Text("OK"),
    onPressed: () async {
      Navigator.pop(navigatorKey.currentState.context, 'OK');
      SharedPreferences preferences = await SharedPreferences.getInstance();
      await preferences.clear();
      CommonMethods().animatedNavigation(
      context: navigatorKey.currentState.context,
      currentScreen: HomeScreen(),
      landingScreen: LoginPage());
    },
  );

void showMyDialog(String title, String content) {
  AlertDialog alert = AlertDialog(
    title: Text(title),
    content: Text(content),
    actions: [
      okButton,
    ],
  );
  showDialog(
    barrierDismissible: false,
    context: navigatorKey.currentState.context,
    builder: (BuildContext context) {
      return alert;
    }
  );
}