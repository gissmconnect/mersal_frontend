import 'package:flutter_test/flutter_test.dart';
import 'package:mersal/features/homescreen/home-presentation/bloc/home_bloc.dart';

import '../../../mocked_classes.dart';

void main() {
  MockGetUserSession mockGetUserSession;
  HomeBloc _homeBloc;
  setUp(() {
    mockGetUserSession = MockGetUserSession();
    _homeBloc = HomeBloc(getUserSessionUseCase: mockGetUserSession);
  });

  test('should be in HomeInitial when started', () {
    expect(_homeBloc.state, equals(HomeInitial()));
  });
}
