import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/election/election-domain/repository/election_repository.dart';
import 'package:meta/meta.dart';

class VoteUseCase extends UseCase<void, Params> {
  final ElectionRepository electionRepository;

  VoteUseCase({@required this.electionRepository});

  @override
  Future<Either<Failure, void>> call(Params params) async {
    return await electionRepository.vote(
        electionId: params.electionId, candidateId: params.candidateId);
  }
}

class Params {
  final int electionId, candidateId;

  Params({@required this.electionId, @required this.candidateId});
}
