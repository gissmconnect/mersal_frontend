import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/features/election/election-domain/entity/election_entity.dart';
import 'package:mersal/features/election_candidate/election_candidate_domain/entity/election_candidate_entity.dart';
import 'package:meta/meta.dart';

abstract class CalendarRepository {

  Future<Either<Failure, List<ElectionDataEntity>>> getElections();

  Future<Either<Failure, List<ElectionCandidateEntity>>> getCandidates(
      {@required int electionId});

  Future<Either<Failure, void>> vote(
      {@required int electionId, @required int candidateId});
}
