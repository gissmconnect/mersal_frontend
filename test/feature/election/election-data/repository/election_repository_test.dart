import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mersal/core/error/exceptions.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/features/election/election-data/model/election_data_model.dart';
import 'package:mersal/features/election/election-data/repository/election_repository_impl.dart';
import 'package:mockito/mockito.dart';

import '../../../../mocked_classes.dart';

void main() {
  MockElectionDataSource _mockElectionDataSource;
  MockNetworkInfo _mockNetworkInfo;
  ElectionRepositoryImpl _electionRepository;
  MockUserDataLocalDataSource _mockUserDataLocalDataSource;

  setUp(() {
    _mockElectionDataSource = MockElectionDataSource();
    _mockNetworkInfo = MockNetworkInfo();
    _mockUserDataLocalDataSource = MockUserDataLocalDataSource();
    _electionRepository = ElectionRepositoryImpl(
        networkInfo: _mockNetworkInfo,
        electionDataSource: _mockElectionDataSource,
        userDataLocalDataSource: _mockUserDataLocalDataSource);
  });

  test('should check the device is Online', () async {
    //arrange
    when(_mockNetworkInfo.isConnected)
        .thenAnswer((realInvocation) async => true);

    //act
    await _electionRepository.getElections();

    //assert
    verify(_mockNetworkInfo.isConnected);
  });

  group('device offline', () {
    test(
        'should return NetworkFailure when device is not connected to the internet',
        () async {
      //arrange
      when(_mockNetworkInfo.isConnected).thenAnswer((_) async => false);
      //act
      final _result = await _electionRepository.getElections();

      //assert
      verifyZeroInteractions(_mockElectionDataSource);
      expect(_result, Left(NetworkFailure()));
    });
  });

  group('device online', () {
    setUp(() {
      when(_mockNetworkInfo.isConnected)
          .thenAnswer((realInvocation) async => true);
    });

    final List<ElectionDataModel> _electionsModel = [
      ElectionDataModel(
          id: 1,
          date: 'test date',
          endDate: 'test end date',
          endTime: 'test end time',
          noOfElectorsSelect: 1,
          shortDescription: 'test',
          title: 'test')
    ];
    final _electionsEntity = _electionsModel;

    test(
        'should return remote data when call to remote datasource is successful',
        () async {
      //arrange
      when(_mockElectionDataSource.getElections(token: anyNamed('token')))
          .thenAnswer((_) async => _electionsModel);
      //act
      final _result = await _electionRepository.getElections();

      //assert
      verify(_mockUserDataLocalDataSource.getAuthToken());
      verify(_mockElectionDataSource.getElections(token: anyNamed('token')));

      expect(_result, equals(Right(_electionsEntity)));
    });

    test(
        'should return ServerFailure when remote datasource throws ServerExcepetion',
        () async {
      //arrange
      when(_mockElectionDataSource.getElections(token: anyNamed('token')))
          .thenThrow(ServerException());

      //act
      final _result = await _electionRepository.getElections();
      //assert
      expect(_result, equals(Left(ServerFailure())));
    });

    test(
        'should return ServerFailure when remote datasource throws unknown exception',
        () async {
      //arrange
      when(_mockElectionDataSource.getElections(token: anyNamed('token')))
          .thenThrow(Exception());

      //act
      final _result = await _electionRepository.getElections();

      //assert
      expect(_result, equals(Left(ServerFailure())));
    });

    test(
      'should return AuthFailure when remote datasource throws AuthException',
      () async {
        //arrange
        when(_mockElectionDataSource.getElections(token: anyNamed('token')))
            .thenThrow(AuthException());

        //act
        final _result = await _electionRepository.getElections();

        //assert
        expect(_result, equals(Left(AuthFailure())));
      },
    );
  });
}
