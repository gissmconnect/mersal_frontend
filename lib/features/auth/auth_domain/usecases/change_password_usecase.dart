import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/auth/auth_domain/repository/auth_repository.dart';
import 'package:meta/meta.dart';

class ChangePasswordUseCase extends UseCase<void, Params> {
  final AuthRepository authRepository;

  ChangePasswordUseCase({@required this.authRepository});

  @override
  Future<Either<Failure, void>> call(Params params) async {
    return await authRepository.changePassword(
        oldPassword: params.oldPassword, newPassword: params.newPassword);
  }
}

class Params {
  final String oldPassword, newPassword;

  Params({@required this.oldPassword, @required this.newPassword});
}
