import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/get_user_login_data_usecase.dart';
import 'package:mersal/core/usecases/reauthenticate_usecase.dart';
import 'package:mersal/core/usecases/save_user_login_data_usecase.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/election/election-domain/entity/election_entity.dart';
import 'package:mersal/features/election/election-domain/usecases/get_elections_usecase.dart';
import 'package:meta/meta.dart';

part 'election_event.dart';

part 'election_state.dart';

class ElectionBloc extends Bloc<ElectionEvent, ElectionState> {
  final GetElectionUseCase getElectionUseCase;
  final ReAuthenticateUseCase reAuthenticateUseCase;
  final GetUserLoginDataUseCase getUserLoginDataUseCase;

  ElectionBloc(
      {@required this.getElectionUseCase,
      @required this.reAuthenticateUseCase,
      @required this.getUserLoginDataUseCase,})
      : super(ElectionLoading());

  @override
  Stream<ElectionState> mapEventToState(ElectionEvent event,) async* {
    if (event is GetElections) {
      yield ElectionLoading();
      final _elections = await getElectionUseCase(NoParams());
      yield* _elections.fold(
        (failure) async* {
          if (failure is AuthFailure) {
            final _loginData = await getUserLoginDataUseCase(NoParams());
            yield* _handleLoginDataUseCase(_loginData);
          } else {
            yield ElectionLoadFiled(failure: failure);
          }
        },
        (data) async* {
          yield ElectionLoaded(elections: data);
        },
      );
    }
  }

  Stream<ElectionState> _handleLoginDataUseCase(
      Either<Failure, LoginParams> loginData) async* {
    yield* loginData.fold(
      (l) async* {
        yield ElectionLoadFiled(failure: l);
      },
      (r) async* {
        final _reAuthenticateResponse = await reAuthenticateUseCase(
            NoParams());
        yield* _handleReAuthentication(_reAuthenticateResponse);
      },
    );
  }

  Stream<ElectionState> _handleReAuthentication(
      Either<Failure, void> reAuthenticateResponse) async* {
    yield* reAuthenticateResponse.fold(
      (l) async* {
        yield ElectionLoadFiled(failure: l);
      },
      (r) async* {
        add(GetElections());
      },
    );
  }
}
