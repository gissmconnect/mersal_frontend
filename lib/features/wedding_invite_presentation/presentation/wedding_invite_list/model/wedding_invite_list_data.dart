import 'package:mersal/features/wedding_invite_presentation/presentation/wedding_invite_list/model/wedding_invite_tempate_data.dart';
import 'package:mersal/features/wedding_invite_presentation/presentation/wedding_invite_list/model/wedding_invite_template_model.dart';

class WeddingInviteListData {
  int id;
  String bride_name;
  String groom_name;
  String wedding_date;
  String preview_url;
  String time;
  String link;
  WeddingInviteTemplateData weddingInviteTemplate;
  WeddingInviteTemplateModel templateModel;

  WeddingInviteListData(
      {this.groom_name,
      this.weddingInviteTemplate,
      this.bride_name,
      this.preview_url,
      this.templateModel,
      this.id,
      this.wedding_date,
      this.time,
      this.link});

  factory WeddingInviteListData.fromJson(Map<String, dynamic> json) {
    return WeddingInviteListData(
      id: json['id'],
      bride_name: json['bride_name'],
      groom_name: json['groom_name'],
      preview_url: json['preview_url']!=""?json['preview_url']:"",
      wedding_date: json['wedding_date'],
      time: json['time'],
      weddingInviteTemplate: json['template'] != null
          ? WeddingInviteTemplateData.fromJson(json['template'])
          : null,
      templateModel: json['template_image'] != ""
          ? WeddingInviteTemplateModel.fromJson(json['template_image'])
          : null,
    );
  }
}
