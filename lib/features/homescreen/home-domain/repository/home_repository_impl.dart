import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/exceptions.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/network/network_info.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/banner/banner-domain/entity/banner_entity.dart';
import 'package:mersal/features/homescreen/home-data/datasource/home_data_source.dart';
import 'package:mersal/features/homescreen/home-domain/entity/BannerListDataEntity.dart';
import 'package:mersal/features/homescreen/home-domain/repository/home_repository.dart';
import 'package:meta/meta.dart';

class HomeRepositoryImpl implements HomeRepository {
  final NetworkInfo networkInfo;
  final HomeDataSource homeDataSource;
  final UserDataLocalDataSource userDataLocalDataSource;

  HomeRepositoryImpl(
      {@required this.networkInfo,
        @required this.homeDataSource,
        @required this.userDataLocalDataSource});

  @override
  Future<Either<Failure, List<BannerEntity>>> getBanners() async {
    if (await networkInfo.isConnected) {
      try {
        final _token = await userDataLocalDataSource.getAuthToken();
        final banners = await homeDataSource.getBanners(token: _token);
        return Right(banners);
      } on ServerException catch (ex) {
        return Left(ServerFailure()..message = ex.message);
      } on AuthException catch (ex) {
        return Left(AuthFailure()..message = ex.message);
      } on Exception {
        return Left(ServerFailure());
      }
    } else {
      return Left(NetworkFailure());
    }
  }

}
