import 'package:meta/meta.dart';
import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/save_user_login_data_usecase.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/auth/auth_domain/repository/auth_repository.dart';

class GetUserLoginDataUseCase extends UseCase<LoginParams,NoParams>{
  final AuthRepository authRepository;

  GetUserLoginDataUseCase({@required this.authRepository});
  @override
  Future<Either<Failure, LoginParams>> call(NoParams params)async {
    return await authRepository.getLoginData();
  }
}