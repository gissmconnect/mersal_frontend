import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mersal/core/widgets/common/Button.dart';
import 'package:mersal/resources/colors.dart';

class ImageCapturePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(appColors.blue),
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: Icon(Icons.arrow_back_ios_rounded),
          color: Colors.white,
        ),
        backgroundColor: Color(appColors.blue),
      ),
      body: _ImageCapturePageBody(),
    );
  }
}

class _ImageCapturePageBody extends StatefulWidget {
  @override
  __ImageCapturePageBodyState createState() => __ImageCapturePageBodyState();
}

class __ImageCapturePageBodyState extends State<_ImageCapturePageBody> {
  List<CameraDescription> cameras;
  CameraController controller;
  var _image = <XFile>[];

  @override
  void initState() {
    super.initState();
    _initCameras();
  }

  @override
  Widget build(BuildContext context) {
    if (controller == null || !controller.value.isInitialized) {
      return Container();
    } else
      return Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            child: CameraPreview(
              controller,
            ),
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 20,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                children: [
                  Container(
                    height: 150,
                    child: ListView.separated(
                      separatorBuilder: (_, __) {
                        return SizedBox(
                          width: 10,
                        );
                      },
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, index) {
                        return Image.file(File(_image[index].path));
                      },
                      itemCount: _image.length,
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  _image.length == 2
                      ? TextButton(
                          onPressed: () {
                            _image.clear();
                            setState(() {});
                          },
                          child: Text(
                            'Retake',
                            style: TextStyle(fontSize: 20),
                          ),
                        )
                      : Column(
                          children: [
                            Text(
                              _image.isEmpty
                                  ? 'Capture front Side'
                                  : 'Capture Back Side',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 20),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                          ],
                        ),
                  _image.length == 2
                      ? Button(
                          onTap: () {
                            Navigator.of(context).pop(_image);
                          },
                          title: "Done",
                          color: Color(appColors.buttonColor),
                          textColor: Colors.white,
                          borderColor: Colors.transparent,
                        )
                      : Button(
                          onTap: () async {
                            _image.add(await controller.takePicture());
                            setState(() {});
                          },
                          title: "Capture",
                          color: Color(appColors.buttonColor),
                          textColor: Colors.white,
                          borderColor: Colors.transparent,
                        )
                ],
              ),
            ),
          )
        ],
      );
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  void _initCameras() async {
    cameras = await availableCameras();
    controller = CameraController(cameras[0], ResolutionPreset.max);
    controller.initialize().then((_) {
      if (!mounted) {
        return;
      }
      setState(() {});
    });
  }
}
