import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/notification/notification_domain/entity/notification_entity.dart';
import 'package:mersal/features/notification/notification_domain/repository/notification_repo.dart';
import 'package:meta/meta.dart';

class GetNotificationUseCase
    extends UseCase<List<NotificationEntity>, NoParams> {
  final NotificationRepo notificationRepo;

  GetNotificationUseCase({@required this.notificationRepo});

  @override
  Future<Either<Failure, List<NotificationEntity>>> call(
      NoParams params) async {
    return await notificationRepo.getNotifications();
  }
}
