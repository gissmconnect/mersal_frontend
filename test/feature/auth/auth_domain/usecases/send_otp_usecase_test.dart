import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mersal/features/auth/auth_domain/usecases/send_otp_usecase.dart';
import 'package:mockito/mockito.dart';

import '../../../../mocked_classes.dart';

void main() {
  MockAuthRepository mockAuthRepository;
  SendOtpUseCase sendOtpUseCase;

  setUp(() {
    mockAuthRepository = MockAuthRepository();
    sendOtpUseCase = SendOtpUseCase(authRepository: mockAuthRepository);
  });

  final _phoneNumber = '123456789';
  test('should return Right with null when otp send successfully', () async {
    //arrange
    when(mockAuthRepository.sendOtp(phoneNumber: anyNamed('phoneNumber')))
        .thenAnswer((realInvocation) async => Right(null));

    //act
    final _result = await mockAuthRepository.sendOtp(phoneNumber: _phoneNumber);
    //assert
    verify(mockAuthRepository.sendOtp(phoneNumber: _phoneNumber));
    expect(_result, equals(Right(null)));
  });
}
