part of 'wedding_invite_template_bloc.dart';

abstract class WeddingInviteTemplateState extends Equatable {}

class WeddingInviteTemplateInitial extends WeddingInviteTemplateState {
  @override
  List<Object> get props => [];
}

class WeddingInviteTemplateInitialising extends WeddingInviteTemplateState {
  @override
  List<Object> get props => [];
}

class WeddingInviteTemplateLoaded extends WeddingInviteTemplateState {
  final WeddingTemplateModel activity;

  WeddingInviteTemplateLoaded({@required this.activity});

  @override
  List<Object> get props => [activity];
}

class WeddingInviteTemplateError extends WeddingInviteTemplateState {
  final Failure failure;

  WeddingInviteTemplateError(this.failure);

  @override
  List<Object> get props => [failure];
}
