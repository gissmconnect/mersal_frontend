import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/committee_management/domain/entity/committee_member_entity.dart';
import 'package:mersal/features/committee_management/domain/repository/committe_repo.dart';
import 'package:meta/meta.dart';

class GetCommitteeMemberUseCase extends UseCase<List<CommitteeMemberEntity>, int> {
  final CommitteeRepo committeeRepo;

  GetCommitteeMemberUseCase({@required this.committeeRepo});

  @override
  Future<Either<Failure, List<CommitteeMemberEntity>>> call(int params) {
    return committeeRepo.getCommitteeMember(committeeId: params);
  }
}
