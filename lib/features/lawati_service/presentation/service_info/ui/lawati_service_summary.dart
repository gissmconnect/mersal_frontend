import 'package:flutter/material.dart';
import 'package:mersal/core/widgets/common/Button.dart';
import 'package:mersal/core/widgets/common/CustomAppBar.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/features/lawati_service/domain/entity/sms_serviceInfo_entity.dart';
import 'package:mersal/features/payment/payment_option.dart';
import 'package:mersal/resources/colors.dart';

class LawatiSummaryScreen extends StatelessWidget {
  final SMSServiceInfoEntity service;
  final String fName, number;

  const LawatiSummaryScreen({Key key,
    @required this.service,
    @required this.number,
    @required this.fName})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
          title: "Lawati SMS Service", isBackButton: true, showText: true),
      body: _LawatiSummaryScreenBody(
        service: service,
        number: number,
        fName: fName,
      ),
    );
  }
}

class _LawatiSummaryScreenBody extends StatefulWidget {
  final SMSServiceInfoEntity service;
  final String fName, number;

  const _LawatiSummaryScreenBody({Key key, @required this.service,
    @required this.number,
    @required this.fName})
      : super(key: key);

  @override
  _LawatiSummaryScreenBodyState createState() =>
      _LawatiSummaryScreenBodyState();
}

class _LawatiSummaryScreenBodyState extends State<_LawatiSummaryScreenBody> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Container(
        height: MediaQuery
            .of(context)
            .size
            .height,
        width: MediaQuery
            .of(context)
            .size
            .width,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bg_image.jpg"),
            fit: BoxFit.fill,
          ),
        ),
        child: SingleChildScrollView(
          child: Column(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10.0),
                child: Container(
                  margin: const EdgeInsets.only(top: 40.0),
                  decoration: BoxDecoration(
                    borderRadius:
                    BorderRadius.vertical(top: Radius.circular(30.0)),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black,
                        offset: Offset(0.0, 1.0), //(x,y)
                        blurRadius: 10.0,
                      ),
                    ],
                    image: DecorationImage(
                      image: AssetImage("assets/images/bg_image.jpg"),
                      fit: BoxFit.fill,
                    ),
                  ),
                  child: Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 20),
                        child: Label(
                          title: "Summary",
                          textAlign: TextAlign.center,
                          fontWeight: FontWeight.w700,
                          fontSize: 20,
                          color: Colors.white,
                        ),
                      ),
                      lawatiServiceSummary()
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget lawatiServiceSummary() {
    return Container(
      margin: EdgeInsets.only(left: 25, right: 25, top: 20),
      decoration: new BoxDecoration(
        shape: BoxShape.rectangle,
        border: Border.all(color: Colors.black, width: 2),
        color: Colors.white,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 40),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Label(
                    title: "MSISDN:",
                    textAlign: TextAlign.center,
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                    color: Colors.black,
                  ),
                  SizedBox(height: 10),
                  Label(
                    title: "First Name:",
                    textAlign: TextAlign.center,
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                    color: Colors.black,
                  ),
                  SizedBox(height: 10),
                  Label(
                    title: "Fee:",
                    textAlign: TextAlign.center,
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                    color: Colors.black,
                  ),
                  SizedBox(height: 10),
                  Label(
                    title: "Validity:",
                    textAlign: TextAlign.center,
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                    color: Colors.black,
                  ),
                  SizedBox(height: 10),
                  Label(
                    title: "Notification:",
                    textAlign: TextAlign.center,
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                    color: Colors.black,
                  ),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Label(
                    title: widget.number,
                    textAlign: TextAlign.center,
                    fontSize: 18,
                    color: Colors.black,
                  ),
                  SizedBox(height: 10),
                  Label(
                    title: widget.fName,
                    textAlign: TextAlign.center,
                    fontSize: 18,
                    color: Colors.black,
                  ),
                  SizedBox(height: 10),
                  Label(
                    title: widget.service.price,
                    textAlign: TextAlign.center,
                    fontSize: 18,
                    color: Colors.black,
                  ),
                  SizedBox(height: 10),
                  Label(
                    title:
                    "${widget.service.duration} Year${widget.service.duration >
                        1 ? "s" : ""}",
                    textAlign: TextAlign.center,
                    fontSize: 18,
                    color: Colors.black,
                  ),
                  SizedBox(height: 10),
                  Label(
                    title: "SMS",
                    textAlign: TextAlign.center,
                    fontSize: 18,
                    color: Colors.black,
                  ),
                ],
              ),
            ],
          ),
          Container(
            height: 60,
            width: 200,
            margin: EdgeInsets.only(top: 35, bottom: 40),
            child: Button(
              title: "Confirm",
              textColor: Color(appColors.blue00008B),
              borderColor: Color(appColors.blue00008B),
              onTap: () {
                paymentBottomSheet();
              },
            ),
          ),
        ],
      ),
    );
  }

  Future<Widget> paymentBottomSheet() {
    return showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (context) {
          return Container(
              margin: EdgeInsets.only(
                  top: MediaQuery
                      .of(context)
                      .size
                      .height / 3.5),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.vertical(top: Radius.circular(30.0)),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black,
                    offset: Offset(0.0, 1.0), //(x,y)
                    blurRadius: 5.0,
                  ),
                ],
              ),
              child: PaymentOptions("service"));
        });
  }
}
