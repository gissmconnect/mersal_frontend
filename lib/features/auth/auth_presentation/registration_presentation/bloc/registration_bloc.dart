import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/save_user_login_data_usecase.dart';
import 'package:mersal/features/auth/auth_domain/usecases/register_user_usecase.dart';
import 'package:meta/meta.dart';

part 'registration_event.dart';

part 'registration_state.dart';

class RegistrationBloc extends Bloc<RegistrationEvent, RegistrationState> {
  final RegisterUserUseCase registerUserUseCase;
  final SaveUserLoginDataUseCase saveUserLoginDataUseCase;

  RegistrationBloc(
      {@required this.registerUserUseCase,
      @required this.saveUserLoginDataUseCase})
      : super(RegistrationInitial());

  @override
  Stream<RegistrationState> mapEventToState(
    RegistrationEvent event,
  ) async* {
    if (event is UserRegistrationEvent) {
      yield Registering();
      final result = await registerUserUseCase(Params(
          phoneNumber: event.phoneNumber,
          password: event.password,
          userName: event.userName,
          gender: event.gender));
      yield* result.fold(
        (l) async* {
          yield RegistrationError(l);
        },
        (r) async* {
          await saveUserLoginDataUseCase(LoginParams(
              loginId: event.phoneNumber, password: event.password));
          yield RegistrationSuccess();
        },
      );
    }
  }
}
