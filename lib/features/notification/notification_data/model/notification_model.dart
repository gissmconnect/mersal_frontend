import 'package:mersal/features/notification/notification_domain/entity/notification_entity.dart';

class NotificationModel extends NotificationEntity {
  NotificationModel({int id, String title,  String text, int status})
      : super(id: id, title: title, text: text, status: status);

  NotificationModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    text = json['text'];
    title = json['title'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['text'] = this.text;
    data['title'] = this.title;
    data['status'] = this.status;
    return data;
  }
}
