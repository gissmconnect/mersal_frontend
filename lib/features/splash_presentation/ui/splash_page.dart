import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mersal/core/widgets/common/shared_preference.dart';
import 'package:mersal/features/splash_presentation/bloc/splash_bloc.dart';

import '../../../injection_container.dart';
import 'widgets/user_session_validation_widget.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {

  FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;

  @override
  void initState() {
    super.initState();
    firebaseCloudMessaging_Listeners();
  }

  void firebaseCloudMessaging_Listeners() {
    _firebaseMessaging.getToken().then((token) async {
      appPreferences.saveStringPreference("fcmToken", token.toString());

      final _token = await appPreferences.getStringPreference("fcmToken");
      print("fcmTokenPrint========" + _token.toString());

    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => serviceLocator<SplashBloc>(),
      child: Scaffold(
        body: Container(
          child: buildBody(context),
        ),
      ),
    );
  }

  Widget buildBody(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Column(
        children: <Widget>[
          Expanded(child: Container()),
          SizedBox(height: 20),
          UserSessionValidationWidget(),
          SizedBox(height: 20),
        ],
      ),
    );
  }
}
