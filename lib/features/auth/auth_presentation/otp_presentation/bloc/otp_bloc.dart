import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/features/auth/auth_domain/usecases/send_otp_usecase.dart';
import 'package:mersal/features/auth/auth_domain/usecases/verify_otp_usecase.dart';

part 'otp_event.dart';

part 'otp_state.dart';

class OtpBloc extends Bloc<OtpEvent, OtpState> {
  OtpBloc.initial(this.otpUseCase, this.verifyOtpUseCase) : super(null);
  final SendOtpUseCase otpUseCase;
  final VerifyOtpUseCase verifyOtpUseCase;

  OtpBloc({@required this.otpUseCase, @required this.verifyOtpUseCase})
      : super(OtpInitial());

  @override
  Stream<OtpState> mapEventToState(
    OtpEvent event,
  ) async* {
    if (event is VerifyOtpEvent) {
      yield OtpVerifyingState();
      final _result = await verifyOtpUseCase(
        VerifyOtpParams(phoneNumber: event.phoneNumber, otp: event.otp),
      );
      yield* _result.fold((l) async* {
        yield OtpVerificationFailureState(failure: l);
      }, (r) async* {
        yield OtpVerificationSuccessState();
      });
    }
    if (event is OtpRequestEvent) {
      yield OtpRequesting();
      final result = await otpUseCase(
        Params(phoneNumber: event.phoneNumber),
      );
      yield* result.fold((l) async* {
        yield OtpError(failure: l);
      }, (r) async* {
        yield OtpRequestSuccessState(message: r);
      });
    }
  }
}
