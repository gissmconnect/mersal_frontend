import 'package:flutter/material.dart';
import 'package:mersal/core/widgets/common/Button.dart';
import 'package:mersal/core/widgets/common/CustomAppBar.dart';
import 'package:mersal/core/widgets/common/InputField.dart';
import 'package:mersal/features/auth/auth_presentation/email_verification_presentation/ui/email_otp_screen.dart';
import 'package:mersal/resources/colors.dart';

class EmailVerificationScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
          title: "Change Password", isBackButton: true, showText: true),
      body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/bg_image.jpg"),
              fit: BoxFit.fill,
            ),
          ),
          child: _EmailVerificationBody()),
    );
  }
}

class _EmailVerificationBody extends StatefulWidget {
  @override
  _EmailVerificationBodyState createState() => _EmailVerificationBodyState();
}

class _EmailVerificationBodyState extends State<_EmailVerificationBody> {
  final emailController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Form(
              key: _formKey,
              child: Container(
                margin: EdgeInsets.only(
                    left: 30, top: 20, right: 30, bottom: 10),
                child: InputField(
                  controller: emailController,
                  onSubmitted: (term) {
                    FocusScope.of(context).unfocus();
                  },
                  hint: "Enter E-mail ID",
                  validator: (text) {
                    bool emailValid = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(text);
                    if (text.isEmpty) {
                      return 'Email cannot be empty';
                    }
                    else if(!emailValid){
                      return 'Please enter valid Email';
                    }
                  },
                  inputType: TextInputType.emailAddress,
                ),
              ),
            ),
            Container(
              height: 60,
              width: 250,
              margin: EdgeInsets.only(left: 30, top: 35, right: 30),
              child: Button(
                title: "Get Otp",
                textColor: Colors.white,
                borderColor: Colors.transparent,
                color: Colors.black,
                onTap: () {
                  if (_formKey.currentState.validate()) {
                    verifyOtpBottomSheet();
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<Widget> verifyOtpBottomSheet() {
    return showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (context) {
          return Container(
              margin: EdgeInsets.only(top: MediaQuery.of(context).size.height / 3.5),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.vertical(top: Radius.circular(30.0)),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black,
                    offset: Offset(0.0, 1.0), //(x,y)
                    blurRadius: 5.0,
                  ),
                ],
              ),
              child: EmailOtpScreen());
        });
  }

}
