import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mersal/core/localization/app_localization.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/features/election_candidate/election_candidate_domain/entity/election_candidate_entity.dart';
import 'package:mersal/features/election_candidate/election_candidate_presentation/bloc/election_candidate_bloc.dart';
import 'package:mersal/features/election_candidate/election_candidate_presentation/ui/candidate_details.dart';
import 'package:mersal/injection_container.dart';

class CandidateScreen extends StatelessWidget {
  final int electionId;

  const CandidateScreen({Key key, @required this.electionId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ElectionCandidateBloc>(
      create: (_) => serviceLocator<ElectionCandidateBloc>(),
      child: Scaffold(
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/bg_image.jpg"),
              fit: BoxFit.fill,
            ),
          ),
          child: _CandidatesBody(
            electionId: electionId,
          ),
        ),
      ),
    );
  }
}

class _CandidatesBody extends StatefulWidget {
  final int electionId;

  const _CandidatesBody({Key key, @required this.electionId}) : super(key: key);

  @override
  _CandidateBodyState createState() => _CandidateBodyState();
}

class _CandidateBodyState extends State<_CandidatesBody> {
  FToast fToast;

  @override
  void initState() {
    super.initState();
    BlocProvider.of<ElectionCandidateBloc>(context)
        .add(GetElectionCandidatesEvent(electionId: widget.electionId));
    fToast = FToast();
    fToast.init(context);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 60,
        ),
        Stack(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(bottom: 5, left: 15),
              alignment: Alignment.bottomLeft,
              child: GestureDetector(
                child: Icon(
                  Icons.arrow_back_ios_rounded,
                  color: Colors.white,
                ),
                onTap: () {
                  Navigator.of(context).pop();
                },
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 6),
              alignment: Alignment.bottomCenter,
              child: Text(
                "Poll",
                textAlign: TextAlign.end,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.w400),
              ),
            ),
          ],
        ),
        Expanded(
          child: BlocListener<ElectionCandidateBloc, ElectionCandidateState>(
            listener: (context, state) {
              if (state is CandidatesLoadFailure) {
                CommonMethods.showToast(
                    fToast: fToast,
                    message: state.failure.message,
                    status: false);
              }
            },
            child: BlocBuilder<ElectionCandidateBloc, ElectionCandidateState>(
              builder: (context, state) {
                if (state is CandidatesLoading) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                } else if (state is CandidatesLoaded) {
                  return Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(bottom: 40, top: 25),
                        child: Label(
                          title: state.candidatesResponse.hasVoted == 0
                              ? AppLocalizations.of(context)
                                  .translate("Click on Image to Vote")
                              : AppLocalizations.of(context)
                                  .translate("You have already cast your vote"),
                          textAlign: TextAlign.center,
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                          color: Colors.white,
                        ),
                      ),
                      GridView.builder(
                        padding: EdgeInsets.only(top: 7),
                        shrinkWrap: true,
                        physics: ScrollPhysics(),
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                        ),
                        itemCount: state.candidatesResponse.candidates.length,
                        itemBuilder: (BuildContext context, int index) {
                          return menuGridItem(
                              state.candidatesResponse.candidates[index],state.candidatesResponse.hasVoted);
                        },
                      ),
                    ],
                  );
                }
                return Container();
              },
            ),
          ),
        ),
      ],
    );
  }

  Widget menuGridItem(ElectionCandidateEntity entity,int hasVoted) {
    return GestureDetector(
      onTap: () {
        candidateDetailsBottomSheet(entity,hasVoted);
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
            child: CircleAvatar(
              radius: 400,
              backgroundImage:
                  CachedNetworkImageProvider(entity.candidateImage),
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 10, top: 10),
            child: Label(
              title: entity.candidateName,
              textAlign: TextAlign.center,
              fontSize: 16,
              fontWeight: FontWeight.w600,
              color: Colors.white,
            ),
          )
        ],
      ),
    );
  }

  Future<Widget> candidateDetailsBottomSheet(ElectionCandidateEntity entity, int hasVoted) {
    print('hast votedt $hasVoted');
    return showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30.0), topRight: Radius.circular(30.0)),
        ),
        builder: (context) {
          return Container(
              height: MediaQuery.of(context).size.height / 1.5,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.vertical(top: Radius.circular(30.0)),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black,
                    offset: Offset(0.0, 1.0), //(x,y)
                    blurRadius: 5.0,
                  ),
                ],
              ),
              child: CandidateDetails(
                electionCandidateEntity: entity,
                hasVoted: hasVoted,
              ));
        });
  }
}
