import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:mersal/core/localization/app_localization.dart';
import 'package:mersal/core/network/api_endpoints.dart';
import 'package:mersal/core/widgets/common/Button.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/core/widgets/common/shared_preference.dart';
import 'package:mersal/core/widgets/message.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/child_service/child-domain/entity/child-enrollment-entity/registered-child-activity-entity/child_activity_model_entity.dart';
import 'package:mersal/features/child_service/child-domain/model/child-enrollment/registered-child-data/RegisteredChildData.dart';
import 'package:mersal/features/child_service/child-presentation/child-enrollment/ui/childEnrollSuccess.dart';
import 'package:mersal/features/payment/payment_option.dart';
import 'package:mersal/features/payment/payment_webview.dart';
import 'package:mersal/resources/colors.dart';

class ChildEnrollmentSummary extends StatefulWidget {
  final RegisteredChildData registeredChildData;
  final ChildActivityData childActivityData;
  final String className, phone, childEmail;

  ChildEnrollmentSummary(this.registeredChildData, this.childActivityData,
      this.childEmail, this.phone, this.className);

  @override
  _ChildEnrollmentSummaryState createState() =>
      _ChildEnrollmentSummaryState();
}

class _ChildEnrollmentSummaryState extends State<ChildEnrollmentSummary> {

  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  BuildContext mContextLoader;

  FToast fToast;

  @override
  void initState() {
    super.initState();
    fToast = FToast();
    fToast.init(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            child: Column(
              children: [
                Label(
                  title: AppLocalizations.of(context)
                      .translate("Child Enrollment Summary"),
                  textAlign: TextAlign.center,
                  fontWeight: FontWeight.w600,
                  fontSize: 20,
                  color: Color(appColors.buttonColor),
                ),
                Container(
                  margin: const EdgeInsets.only(top: 40.0),
                  child: Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 25, right: 25, top: 20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                      child: Label(
                                        title: AppLocalizations.of(context)
                                            .translate("Child Name:"),
                                        textAlign: TextAlign.center,
                                        fontSize: 15,
                                        color: Color(appColors.buttonColor),
                                      ),
                                    ),
                                    Expanded(
                                      child: Label(
                                        title: widget.registeredChildData.name,
                                        textAlign: TextAlign.center,
                                        fontSize: 15,
                                        color: Color(appColors.buttonColor),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                              /*  Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                      child: Label(
                                        title: "Child Email:",
                                        textAlign: TextAlign.center,
                                        fontSize: 15,
                                        color: Color(appColors.buttonColor),
                                      ),
                                    ),
                                    Expanded(
                                      child: Label(
                                        title: widget.childEmail,
                                        textAlign: TextAlign.center,
                                        fontSize: 15,
                                        color: Color(appColors.buttonColor),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 15,
                                ),*/
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                      child: Label(
                                        title: AppLocalizations.of(context)
                                            .translate("Child Current Class:"),
                                        textAlign: TextAlign.center,
                                        fontSize: 15,
                                        color: Color(appColors.buttonColor),
                                      ),
                                    ),
                                    Expanded(
                                      child: Label(
                                        title: widget.className,
                                        textAlign: TextAlign.center,
                                        fontSize: 15,
                                        color: Color(appColors.buttonColor),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                      child: Label(
                                        title:AppLocalizations.of(context)
                                            .translate("Child Phone Number:") ,
                                        textAlign: TextAlign.center,
                                        fontSize: 15,
                                        color: Color(appColors.buttonColor),
                                      ),
                                    ),
                                    Expanded(
                                      child: Label(
                                        title: widget.phone,
                                        textAlign: TextAlign.center,
                                        fontSize: 15,
                                        color: Color(appColors.buttonColor),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                      child: Label(
                                        title: AppLocalizations.of(context)
                                            .translate("Activity:"),
                                        textAlign: TextAlign.center,
                                        fontSize: 15,
                                        color: Color(appColors.buttonColor),
                                      ),
                                    ),
                                    Expanded(
                                      child: Label(
                                        title: widget.childActivityData.title,
                                        textAlign: TextAlign.center,
                                        fontSize: 15,
                                        color: Color(appColors.buttonColor),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                      child: Label(
                                        title:AppLocalizations.of(context)
                                            .translate("DOB:") ,
                                        textAlign: TextAlign.center,
                                        fontSize: 15,
                                        color: Color(appColors.buttonColor),
                                      ),
                                    ),
                                    Expanded(
                                      child: Label(
                                        title: widget.registeredChildData.dob,
                                        textAlign: TextAlign.center,
                                        fontSize: 15,
                                        color: Color(appColors.buttonColor),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                      child: Label(
                                        title:AppLocalizations.of(context)
                                            .translate("Amount:") ,
                                        textAlign: TextAlign.center,
                                        fontSize: 15,
                                        color: Color(appColors.buttonColor),
                                      ),
                                    ),
                                    Expanded(
                                      child: Label(
                                        title: "OMR 1",
                                        textAlign: TextAlign.center,
                                        fontSize: 15,
                                        color: Color(appColors.buttonColor),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            Container(
                              height: 50,
                              margin: EdgeInsets.only(
                                  top: 55, bottom: 40, left: 20, right: 20),
                              child: Button(
                                title: AppLocalizations.of(context)
                                    .translate("Confirm"),
                                textColor: Colors.white,
                                borderColor: Colors.transparent,
                                color: Color(appColors.buttonColor),
                                onTap: () {
                                  apiEnrollCall();
                                },
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> apiEnrollCall() async {
    final _params = <String, String>{
      'child_id': widget.registeredChildData.id.toString(),
      'email': widget.childEmail,
      'activity_id': widget.childActivityData.id.toString(),
      "class_name": widget.className,
      "phone": widget.phone
    };

    final _token = await appPreferences.getStringPreference("AccessToken");

    if (context != null) {
      _showLoader(context);
    }
    var response = await http.post(Uri.parse(CHILD_REGISTRATION_ACTIVITY),
        headers: {
          'Content-type': 'application/json',
          AUTHORIZATION: BEARER + " " + _token
        },
        body: json.encode(_params));

    if (response.statusCode == 200) {
      final Map<String, dynamic> _responseJson = json.decode(response.body);
      if (mContextLoader != null) Navigator.pop(mContextLoader);
      print( 'ChildEnrollmentFinalResponse ${response.statusCode} and ${_responseJson.toString()}');
      Navigator.pop(context);
      // showEnrollDialog(context);
      CommonMethods().animatedNavigation(
          context: context,
          currentScreen:
          ChildEnrollmentSummary(null,null,null,null,null),
          landingScreen: PaymentWebView(
            type: "child-registrations",
            id:_responseJson['data']['id'].toString(),
            activity: widget.childActivityData.title,
          ));
    } else {
      if (mContextLoader != null) Navigator.pop(mContextLoader);
      CommonMethods.showToast(fToast:fToast,message:  "Something went wrong",status:false);
      Navigator.pop(context);
    }
  }

  Future<Null> _showLoader(BuildContext context) async {
    return showDialog<Null>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context1) {
        mContextLoader = context1;
        return Scaffold(
          backgroundColor: Colors.transparent.withOpacity(0.3),
          key: _keyLoader,
          body: Container(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    child: CircularProgressIndicator(),
                    height: 40.0,
                    width: 40.0,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Future<Widget> paymentOptionBottomSheet() {
    return showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (context) {
          return Container(
              margin: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height / 3.5),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.vertical(top: Radius.circular(50.0)),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black,
                    offset: Offset(0.0, 1.0), //(x,y)
                    blurRadius: 5.0,
                  ),
                ],
              ),
              child: PaymentOptions("child"));
        });
  }

  void showEnrollDialog(BuildContext context) {
    showGeneralDialog(
      barrierLabel: "Barrier",
      barrierDismissible: true,
      barrierColor: Colors.black.withOpacity(0.8),
      transitionDuration: Duration(milliseconds: 500),
      context: context,
      pageBuilder: (_, __, ___) {
        return Align(
          alignment: Alignment.center,
          child: Container(
            height: 450,
            margin: EdgeInsets.only(left: 10, right: 10),
            decoration: new BoxDecoration(
              color: const Color(0xFFFFFF),
              borderRadius: new BorderRadius.all(new Radius.circular(32.0)),
            ),
            child: ChildEnrollSuccess(),
          ),
        );
      },
      transitionBuilder: (_, anim, __, child) {
        return SlideTransition(
          position: Tween(begin: Offset(0, 1), end: Offset(0, 0)).animate(anim),
          child: child,
        );
      },
    );
  }

}
