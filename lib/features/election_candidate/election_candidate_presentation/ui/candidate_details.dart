import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mersal/core/widgets/common/Button.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/core/widgets/message.dart';
import 'package:mersal/core/widgets/widgets.dart';
import 'package:mersal/features/auth/auth_presentation/doc_upload_presentation/ui/doc_upload_page.dart';
import 'package:mersal/features/election_candidate/election_candidate_domain/entity/election_candidate_entity.dart';
import 'package:mersal/features/election_candidate/election_candidate_presentation/bloc/election_candidate_bloc.dart';
import 'package:mersal/injection_container.dart';
import 'package:mersal/resources/colors.dart';

class CandidateDetails extends StatelessWidget {
  final ElectionCandidateEntity electionCandidateEntity;
  final int hasVoted;

  const CandidateDetails(
      {Key key,
      @required this.electionCandidateEntity,
      @required this.hasVoted})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        create: (_) => serviceLocator<ElectionCandidateBloc>(),
        child: _CandidateDetailsBody(
          electionCandidateEntity: electionCandidateEntity,
          hasVoted: hasVoted,
        ),
      ),
    );
  }
}

class _CandidateDetailsBody extends StatefulWidget {
  final ElectionCandidateEntity electionCandidateEntity;

  final int hasVoted;

  const _CandidateDetailsBody(
      {Key key,
      @required this.electionCandidateEntity,
      @required this.hasVoted})
      : super(key: key);

  @override
  _CandidateDetailsBodyState createState() => _CandidateDetailsBodyState();
}

class _CandidateDetailsBodyState extends State<_CandidateDetailsBody> {
  FToast fToast;

  @override
  void initState() {
    super.initState();
    fToast = FToast();
    fToast.init(context);
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: 150,
            height: 150,
            margin: EdgeInsets.only(top: 20),
            decoration: new BoxDecoration(
              shape: BoxShape.circle,
              color: Color(appColors.blue),
              border: Border.all(
                color: Color(appColors.buttonColor),
                width: 1,
              ),
            ),
            child: Center(
              child: CachedNetworkImage(
                imageUrl: widget.electionCandidateEntity.candidateImage,
                fit: BoxFit.cover,
                width: 120,
              ),
            ),
          ),
          Container(
              margin: EdgeInsets.only(bottom: 10, top: 20),
              child: Label(
                title: widget.electionCandidateEntity.candidateName,
                textAlign: TextAlign.center,
                fontSize: 18,
                fontWeight: FontWeight.w600,
                color: Color(appColors.buttonColor),
              )),
          Container(
              margin: EdgeInsets.only(bottom: 10, top: 20),
              child: Label(
                title: widget.electionCandidateEntity.candidateDescription,
                textAlign: TextAlign.center,
                fontSize: 20,
                color: Color(appColors.buttonColor),
              )),
          widget.hasVoted == 1
              ? Container()
              : Container(
                  height: 60,
                  margin: EdgeInsets.only(left: 40, top: 35, right: 40),
                  child: BlocBuilder<ElectionCandidateBloc,
                      ElectionCandidateState>(
                    builder: (context, state) => Button(
                      title: "Vote",
                      color: Color(appColors.buttonColor),
                      borderColor: Colors.transparent,
                      textColor: Colors.white,
                      onTap: () {
                        if (widget.hasVoted == 1) {
                          CommonMethods.showToast(
                              fToast: fToast,
                              message: "Already voted",
                              status: false);
                          return;
                        }
                        Navigator.pop(context);
                        voteBottomSheet();
                      },
                    ),
                  ),
                ),
        ],
      ),
    );
  }

  Future<Widget> voteBottomSheet() {
    return showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30.0), topRight: Radius.circular(30.0)),
        ),
        builder: (context) {
          return BlocProvider(
            create: (_) => serviceLocator<ElectionCandidateBloc>(),
            child: Container(
              height: MediaQuery.of(context).size.height / 1.5,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    width: 150,
                    height: 150,
                    margin: EdgeInsets.only(top: 20),
                    decoration: new BoxDecoration(
                      shape: BoxShape.circle,
                      color: Color(appColors.blue),
                      border: Border.all(
                        color: Color(appColors.buttonColor),
                        width: 1,
                      ),
                    ),
                    child: Center(
                      child: CachedNetworkImage(
                        imageUrl: widget.electionCandidateEntity.candidateImage,
                        fit: BoxFit.cover,
                        width: 120,
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 20, top: 20),
                    child: Label(
                      title: widget.electionCandidateEntity.candidateName,
                      textAlign: TextAlign.center,
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                      color: Color(appColors.buttonColor),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                        bottom: 40, top: 10, left: 20, right: 20),
                    child: Label(
                      title:
                          "Are you sure you want to vote for this candidate?",
                      textAlign: TextAlign.center,
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                      color: Color(appColors.buttonColor),
                    ),
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: Container(
                          height: 60,
                          margin: EdgeInsets.only(left: 10, top: 20, right: 10),
                          child: Button(
                            title: "No",
                            color: Colors.transparent,
                            borderColor: Color(appColors.buttonColor),
                            textColor: Color(appColors.buttonColor),
                            onTap: () {
                              Navigator.pop(context);
                              // Navigator.push(context, MaterialPageRoute(builder: (context) => MainActivity()));
                            },
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          height: 60,
                          margin: EdgeInsets.only(left: 10, top: 20, right: 10),
                          child: BlocListener<ElectionCandidateBloc,
                              ElectionCandidateState>(
                            listener: (context, state) {
                              print('state $state');
                              if (state is VotingFailedState) {
                                // CommonMethods.showToast(
                                //     fToast: fToast,
                                //     message:state.failure.message,
                                //     status: false);
                                showSnackBarMessage(
                                    context: context,
                                    message: state.failure.message);
                                Navigator.of(context).pop();
                              }
                              if (state is VotedState) {
                                Navigator.of(context).pop();
                                Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => DocUploadPage("vote"),
                                  ),
                                );
                              }
                            },
                            child: BlocBuilder<ElectionCandidateBloc,
                                ElectionCandidateState>(
                              builder: (context, state) {
                                if (state is VotingState) {
                                  return circularProgressIndicator();
                                }
                                return Button(
                                  title: "Yes",
                                  color: Color(appColors.buttonColor),
                                  borderColor: Colors.transparent,
                                  textColor: Colors.white,
                                  onTap: () {
                                    BlocProvider.of<ElectionCandidateBloc>(
                                            context)
                                        .add(
                                      VoteEvent(
                                          electionId: widget
                                              .electionCandidateEntity
                                              .electionId,
                                          candidateId: widget
                                              .electionCandidateEntity
                                              .candidateId),
                                    );
                                  },
                                );
                              },
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  )
                ],
              ),
            ),
          );
        });
  }
}
