import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:mersal/core/localization/app_localization.dart';
import 'package:mersal/core/network/api_endpoints.dart';
import 'package:mersal/core/widgets/common/Button.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/CustomAppBar.dart';
import 'package:mersal/core/widgets/common/InputField.dart';
import 'package:mersal/core/widgets/message.dart';
import 'package:mersal/core/widgets/widgets.dart';
import 'package:mersal/features/auth/auth_presentation/change_password_presentation/bloc/change_password_bloc.dart';
import 'package:mersal/features/auth/auth_presentation/login_presentation/ui/login_page.dart';
import 'package:mersal/injection_container.dart';
import 'package:mersal/resources/colors.dart';

class ResetPassword extends StatelessWidget {
  final String phoneValue;

  ResetPassword({ this.phoneValue});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => serviceLocator<ChangePasswordBloc>(),
      child: Scaffold(
        appBar: CustomAppBar(
            title: AppLocalizations.of(context).translate("Reset Password"),
            isBackButton: true, showText: true),
        body: _ResetPasswordBody( phoneValue),
      ),
    );
  }
}

class _ResetPasswordBody extends StatefulWidget {
  final String phoneValue;

  _ResetPasswordBody(this.phoneValue);

  @override
  _ResetPasswordBodyState createState() => _ResetPasswordBodyState();
}

class _ResetPasswordBodyState extends State<_ResetPasswordBody> {
  final newPasswordController = TextEditingController();
  final otpController = TextEditingController();
  bool obscureText = true, newPassObscure = true;
  final FocusNode oldFocus = new FocusNode();
  final FocusNode otpFocus = new FocusNode();
  final FocusNode newPasswordFocus = new FocusNode();
  final _formKey = GlobalKey<FormState>();

  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  BuildContext mContextLoader;
  FToast fToast;

  @override
  void initState() {
    super.initState();
    fToast = FToast();
    fToast.init(context);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bg_image.jpg"),
            fit: BoxFit.fill,
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Form(
              key: _formKey,
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 30, top: 25, right: 30),
                    child: InputField(
                      controller: otpController,
                      focusNode: otpFocus,
                      onSubmitted: (term) {
                        FocusScope.of(context).unfocus();
                      },
                      validator: (text) {
                        if (text.isEmpty) {
                          return 'OTP cannot be empty';
                        }
                        return null;
                      },
                      hint:  AppLocalizations.of(context).translate("Enter OTP"),
                      inputType: TextInputType.phone,
                    ),
                  ),
                  Container(
                    height: 80,
                    margin: EdgeInsets.only(left: 30, top: 15, right: 30),
                    child: TextFormField(
                      controller: newPasswordController,
                      focusNode: newPasswordFocus,
                      obscureText: newPassObscure,
                      validator: (text) {
                        if (text.isEmpty) {
                          return AppLocalizations.of(context).translate('New Password cannot be empty');
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                          filled: true,
                          counterText: "",
                          fillColor: Colors.white,
                          hintStyle: TextStyle( color: Colors.black,),
                          hintText: AppLocalizations.of(context).translate("Enter New Password"),
                          contentPadding: const EdgeInsets.only(
                              left: 20.0, bottom: 15.0, top: 15.0,right: 10),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                            borderRadius: BorderRadius.circular(25.7),
                          ),
                          errorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(25.0),
                              borderSide: BorderSide(
                                color: Colors.red,
                              )),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                            borderRadius: BorderRadius.circular(25.7),
                          ),
                          suffixIcon: IconButton(
                            onPressed: () {
                              newPassObscure = !newPassObscure;
                              setState(() {});
                            },
                            icon: Icon(
                                newPassObscure
                                    ? Icons.remove_red_eye_outlined
                                    : Icons.visibility_off_outlined,
                                size: 30,
                                color: Colors.black),
                          )),
                      onFieldSubmitted: (term) {
                        FocusScope.of(context).unfocus();
                      },
                      maxLength: 20,
                      keyboardType: TextInputType.name,
                    ),
                  ),
],
              ),
            ),
            Container(
              height: 60,
              margin: EdgeInsets.only(left: 30, top: 35, right: 30),
              child: BlocListener<ChangePasswordBloc, ChangePasswordState>(
                listener: (context, state) {
                  if (state is ChangePasswordSuccess) {
                    Navigator.of(context).pop();
                  } else if (state is ChangePasswordFailed) {
                    showSnackBarMessage(
                        context: context, message: state.failure.message);
                  }
                },
                child: BlocBuilder<ChangePasswordBloc, ChangePasswordState>(
                  builder: (context, state) {
                    if (state is ChangePasswordInitiated) {
                      return circularProgressIndicator();
                    }
                    return Button(
                      title:AppLocalizations.of(context).translate("Update Password"),
                      textColor: Colors.white,
                      borderColor: Colors.transparent,
                      color: Color(appColors.buttonColor),
                      onTap: () {
                        if (_formKey.currentState.validate()) {
                          resetPasswordApi();
                        }
                      },
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Future<void> resetPasswordApi() async {
    final _newPassword = newPasswordController.text.toString().trim();
    final _otp = otpController.text.toString().trim();
    final _params = <String, String>{
      'email': widget.phoneValue,
      'code': _otp,
      'password': _newPassword,
    };

    if (context != null) {
      _showLoader(context);
    }

    print("Reset Params====${_params.toString()}");

    var response = await http.post(Uri.parse(RESET_PASSWORD),
        headers: {'Content-type': 'application/json'},
        body: json.encode(_params));
    final _responseJson = json.decode(response.body);
    if (response.statusCode == 200) {
      if (mContextLoader != null) {
        Navigator.pop(mContextLoader);
      }
      CommonMethods.showToast(fToast: fToast, message: _responseJson['message'], status: true);
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => LoginPage(),
        ),
      );
    } else {
      if (mContextLoader != null) Navigator.pop(mContextLoader);
      CommonMethods.showToast(
          fToast: fToast, message: _responseJson['message'], status: false);
    }
  }

  Future<Null> _showLoader(BuildContext context) async {
    return showDialog<Null>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context1) {
        mContextLoader = context1;
        return Scaffold(
          backgroundColor: Colors.transparent.withOpacity(0.3),
          key: _keyLoader,
          body: Container(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    child: CircularProgressIndicator(),
                    height: 40.0,
                    width: 40.0,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
