import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/widgets/message.dart';
import 'package:mersal/features/auth/auth_presentation/login_presentation/ui/login_page.dart';
import 'package:mersal/features/dashboard/dashboard_page.dart';
import 'package:mersal/features/splash_presentation/bloc/splash_bloc.dart';

class UserSessionValidationWidget extends StatefulWidget {
  const UserSessionValidationWidget({
    Key key,
  }) : super(key: key);

  @override
  _UserSessionValidationWidgetState createState() =>
      _UserSessionValidationWidgetState();
}

class _UserSessionValidationWidgetState
    extends State<UserSessionValidationWidget> {
  @override
  void initState() {
    BlocProvider.of<SplashBloc>(context).add(GetUserSessionEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SplashBloc, SplashState>(
      listener: (context, state) {
        if (state is Error) {
          if (state.failure is UserSessionFailure) {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => LoginPage()),
                (_) => false);
          } else {
            showSnackBarMessage(
                context: context, message: state.failure.message);
          }
        } else if (state is Loaded) {
          Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (context) => DashboardPage()));
        }
      },
      child: BlocBuilder<SplashBloc, SplashState>(builder: (context, state) {
        if (state is Loading) {
          return CircularProgressIndicator();
        }
        if (state is Error) {
          return Container(
            child: Text(state.failure.message),
          );
        }
        return Container();
      }),
    );
  }
}
