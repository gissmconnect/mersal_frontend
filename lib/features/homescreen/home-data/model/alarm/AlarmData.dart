class AlarmData {
  int id;
  String name;
  String prayerTime;

  AlarmData({this.prayerTime, this.id, this.name});

  factory AlarmData.fromJson(Map<String, dynamic> json) {
    return AlarmData(
        id: json["alarm"]['id'], name: json["alarm"]['name'], prayerTime: json["alarm"]['prayer_time']);
  }
}
