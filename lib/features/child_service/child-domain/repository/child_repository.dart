import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/features/child_service/child-data/model/child-enroll-model/registered-child-activity/child_activity_model.dart';
import 'package:mersal/features/child_service/child-data/model/child-enroll-model/registered-child-details/child_details_model.dart';
import 'package:mersal/features/election/election-domain/entity/election_entity.dart';
import 'package:mersal/features/election_candidate/election_candidate_domain/entity/election_candidate_entity.dart';
import 'package:meta/meta.dart';

abstract class ChildRepository {
  Future<Either<Failure, ChildActivityModel>> getActivity();

  Future<Either<Failure,ChildDetailsModel>> getChildDetails();

  Future<Either<Failure, void>> enrollChild(
      {@required int electionId, @required int candidateId});
}
