import 'package:mersal/features/lawati_service/domain/entity/sms_serviceInfo_entity.dart';
import 'package:mersal/features/lawati_service/domain/entity/sms_subscription_entity.dart';
import 'package:meta/meta.dart';

class SMSSubscriptionModel extends SmsSubscriptionEntity {
  SMSSubscriptionModel({
    @required int id,
    @required String phone,
    @required String firstName,
    @required String middleName,
    @required String lastName,
    @required String startDate,
    @required String endDate,
    @required int expiringIn,
    @required String relation,
    @required int notificationType,
  }) : super(
          id: id,
          phone: phone,
          firstName: firstName,
          middleName: middleName,
          lastName: lastName,
          startDate: startDate,
          endDate: endDate,
          expiringIn: expiringIn,
          relation: relation,
          notificationType: notificationType,
        );

  SMSSubscriptionModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    phone = json['phone'];
    firstName = json['first_name'];
    middleName = json['middle_name'];
    lastName = json['last_name'];
    startDate = json['start_date'];
    endDate = json['end_date'];
    expiringIn = json['expiring_in'];
    relation = ""; //json['relation'];
    notificationType = json['notification_type'];
  }

  Map<String, dynamic> toJSON() {
    return {
      'id': id,
      'phone': phone,
      'first_name': firstName,
      'middle_name': middleName,
      'last_name': lastName,
      'start_date': startDate,
      'end_date': endDate,
      'expiring_in': expiringIn,
      'relation': relation,
      'notification_type': notificationType
    };
  }
}
