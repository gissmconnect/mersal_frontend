import 'package:equatable/equatable.dart';

class UserData extends Equatable {
  int id;
  int userId;
  String name;
  String userName;
  String email;
  String phone;
  String gender;
  String authToken;
  String bloodGroup;
  String dob;
  String address;
  String image;
  bool isVender;
  Document document;
  String documentStatus;
  bool addServices;

  UserData(
      {this.id,
      this.userId,
      this.name,
      this.userName,
      this.email,
      this.phone,
      this.gender,
      this.authToken,
      this.bloodGroup,
      this.dob,
      this.address,
      this.image,
      this.isVender,
      this.document,
      this.documentStatus,
      this.addServices});

  @override
  List<Object> get props => [
        id,
        userId,
        name,
        userName,
        email,
        phone,
        gender,
        authToken,
        bloodGroup,
        dob,
        address,
        image,
        isVender,
        document,
        documentStatus,
        addServices
      ];
}

class Document extends Equatable {
  int id = 0;
  String idName;
  String idNumber;
  String idDob;
  String idExpiryDate;
  String idFront;
  String idBack;
  int documentStatus;

  Document(
      {this.id,
      this.idName,
      this.idNumber,
      this.idDob,
      this.idExpiryDate,
      this.idFront,
      this.idBack,
      this.documentStatus});

  @override
  List<Object> get props => [
        id,
        idName,
        idNumber,
        idDob,
        idExpiryDate,
        idFront,
        idBack,
        documentStatus
      ];
}
