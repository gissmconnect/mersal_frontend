import 'package:mersal/features/prayer_timeline/data/data/prayer_data_model.dart';

abstract class PrayerDataSource{
  Future<List<PrayerDataModel>> getPrayerTimes({String token});
  Future<String> setAlarm({String token, int id});
}