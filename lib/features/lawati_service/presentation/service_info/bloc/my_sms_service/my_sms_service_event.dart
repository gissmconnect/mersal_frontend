part of 'my_sms_service_bloc.dart';

abstract class MySmsServiceEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class GetMyServiceEvent extends MySmsServiceEvent {
  @override
  List<Object> get props => [];
}

class GetServiceInfoEvent extends MySmsServiceEvent{}

class SubscribeEvent extends MySmsServiceEvent{
  final SubscriptionRequestParams params;

  SubscribeEvent({@required this.params});

  @override
  List<Object> get props => [params.id];
}

class RenewServiceEvent extends MySmsServiceEvent{
  final int subscriptionId;

  RenewServiceEvent({@required this.subscriptionId});
}
