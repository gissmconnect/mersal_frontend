part of 'notification_bloc.dart';

abstract class NotificationEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class NotificationsEvent extends NotificationEvent {
  NotificationsEvent();
}

class DeleteNotificationEvent extends NotificationEvent{
  final int id;

  DeleteNotificationEvent({@required this.id});
}
class ArchiveNotificationEvent extends NotificationEvent{
  final int id;

  ArchiveNotificationEvent({@required this.id});
}