import 'package:mersal/features/homescreen/home-data/model/alarm/AlarmData.dart';
import 'package:mersal/features/homescreen/home-data/model/alarm/HomeOccasionModel.dart';
import 'package:mersal/features/homescreen/home-data/model/alarm/NextPrayerModel.dart';

class AlarmModel {
  AlarmData alarmData;
  HomeOccasionModelData occasionModel;
  NextPrayerModel nextPrayerModel;

  AlarmModel({this.alarmData, this.occasionModel, this.nextPrayerModel});

  factory AlarmModel.fromJson(Map<String, dynamic> json) {
    return AlarmModel(
      alarmData:
          json['alarm'] != null ? AlarmData.fromJson(json['alarm']) : null,
      occasionModel: json['occasion'] != null
          ? HomeOccasionModelData.fromJson(json['occasion'])
          : null,
      nextPrayerModel: json['prayer'] != null
          ? NextPrayerModel.fromJson(json['prayer'])
          : null,
    );
  }
}
