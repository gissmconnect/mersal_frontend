part of 'user_bloc.dart';

abstract class UserEvent extends Equatable {
  const UserEvent();
}

class LogoutEvent extends UserEvent {
  @override
  List<Object> get props => [];
}

class VerifyDocumentStatusEvent extends UserEvent {
  @override
  List<Object> get props => [];
}

class GetUserSessionEvent extends UserEvent {
  @override
  List<Object> get props => [];
}

class UpdateProfileEvent extends UserEvent {
  final String name, dob, bloodGroup, image;
  final int gender;

  UpdateProfileEvent(
      {@required this.name,
      @required this.image,
      @required this.dob,
      @required this.gender,
      @required this.bloodGroup});

  @override
  List<Object> get props => [name, dob, gender, bloodGroup, image];
}
