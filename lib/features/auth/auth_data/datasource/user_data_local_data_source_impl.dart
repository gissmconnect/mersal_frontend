import 'dart:convert';

import 'package:mersal/core/error/exceptions.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/save_user_login_data_usecase.dart';
import 'package:mersal/core/util/constants.dart';
import 'package:mersal/features/auth/auth_data/model/user_data_model.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'user_data_local_data_source.dart';

class UserDataLocalDataSourceImpl implements UserDataLocalDataSource {
  final SharedPreferences sharedPreferences;

  UserDataLocalDataSourceImpl({@required this.sharedPreferences});

  @override
  Future<UserDataModel> getUserData() async {
    final jsonString = sharedPreferences.getString(USER_DATA);
    if (jsonString != null) {
      return Future.value(UserDataModel.fromJson(json.decode(jsonString)));
    } else {
      throw UserSessionFailure();
    }
  }

  @override
  Future<void> saveUserData({UserDataModel userDataModel}) async {
    final _jsonString =  json.encode(userDataModel);
    await sharedPreferences.setString(USER_DATA, _jsonString);
  }

  @override
  Future<void> saveAuthToken({String authToken}) async {
    await sharedPreferences.setString(AUTH_TOKEN, authToken);
  }

  @override
  Future<String> getAuthToken() async {
    final _token = sharedPreferences.getString(AUTH_TOKEN);
    return Future.value(_token);
  }

  @override
  Future<void> saveLoginData({LoginParams loginParams}) async {
    final _jsonString = json.encode(loginParams);
    await sharedPreferences.setString(LOGIN_DATA, _jsonString);
  }

  @override
  Future<LoginParams> getLoginData() async {
    final _loginJson = sharedPreferences.getString(LOGIN_DATA);
    if (_loginJson == null) {
      throw CacheException();
    } else {
      return Future.value(LoginParams.fromJson(json.decode(_loginJson)));
    }
  }

  @override
  Future<void> clear() async {
    await sharedPreferences.clear();
  }

  @override
  Future<int> getDocumentStatus() async {
    final jsonString = sharedPreferences.getString(USER_DATA);
    if (jsonString != null) {
      final _documentStatus = UserDataModel.fromJson(json.decode(jsonString))
          .document?.documentStatus??DOCUMENT_STATUS_UN_UPLOADED;
      return Future.value(_documentStatus);
    } else {
      throw CacheException();
    }
  }

  @override
  Future<void> updatePassword({String password}) async {
    final _loginJson = sharedPreferences.getString(LOGIN_DATA);
    if (_loginJson == null) {
      throw CacheException();
    } else {
      final LoginParams _loginParams = LoginParams.fromJson(json.decode(_loginJson));
      _loginParams.password = password;
      await saveLoginData(loginParams: _loginParams);
    }
  }
}
