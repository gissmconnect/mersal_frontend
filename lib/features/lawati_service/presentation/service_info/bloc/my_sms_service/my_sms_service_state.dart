part of 'my_sms_service_bloc.dart';

class MySmsServiceState extends Equatable {
  final Resource serviceInfoResource,
      mySubscriptionInfo,
      subscribeService,
      renewSubscription;

  MySmsServiceState(
      {this.serviceInfoResource,
      this.mySubscriptionInfo,
      this.subscribeService,
      this.renewSubscription});

  @override
  List<Object> get props => [
        serviceInfoResource,
        mySubscriptionInfo,
        subscribeService,
        renewSubscription
      ];

  factory MySmsServiceState.initial() {
    return MySmsServiceState(
        serviceInfoResource: Resource.loading(),
        mySubscriptionInfo: Resource.loading());
  }

  MySmsServiceState copy(
      {Resource serviceInfoResource,
      Resource mySubscriptionInfo,
      Resource subscribeService,
      Resource renewSubscription}) {
    return MySmsServiceState(
        serviceInfoResource: serviceInfoResource ?? this.serviceInfoResource,
        mySubscriptionInfo: mySubscriptionInfo ?? this.mySubscriptionInfo,
        subscribeService: subscribeService ?? this.subscribeService,
        renewSubscription: renewSubscription ?? this.renewSubscription);
  }
}
