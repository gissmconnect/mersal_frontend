import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/auth/auth_domain/repository/auth_repository.dart';
import 'package:meta/meta.dart';

class GetUserDocumentStatusUseCase extends UseCase<int, NoParams> {
  final AuthRepository authRepository;

  GetUserDocumentStatusUseCase({@required this.authRepository});

  @override
  Future<Either<Failure, int>> call(NoParams params) {
   return authRepository.getDocumentStatus();
  }
}
