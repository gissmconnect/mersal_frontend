import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mersal/core/localization/app_localization.dart';
import 'package:mersal/core/network/vo/Status.dart';
import 'package:mersal/core/widgets/common/Button.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/core/widgets/message.dart';
import 'package:mersal/core/widgets/widgets.dart';
import 'package:mersal/features/lawati_service/data/model/sms_subscriptions_model.dart';
import 'package:mersal/features/lawati_service/presentation/service_info/bloc/my_sms_service/my_sms_service_bloc.dart';
import 'package:mersal/features/lawati_service/presentation/service_info/ui/self_sms_subscription_form_page.dart';
import 'package:mersal/features/lawati_service/presentation/subscriptions/ui/subscription_details.dart';
import 'package:mersal/injection_container.dart';
import 'package:mersal/resources/colors.dart';

class SelfLawatiService extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<MySmsServiceBloc>(
        create: (_) => serviceLocator<MySmsServiceBloc>(),
        child: SelfSmsSubscriptionBody());
  }
}

class SelfSmsSubscriptionBody extends StatefulWidget {
  @override
  _SelfSmsSubscriptionBodyState createState() =>
      _SelfSmsSubscriptionBodyState();
}

class _SelfSmsSubscriptionBodyState extends State<SelfSmsSubscriptionBody> {

  @override
  void initState() {
    super.initState();
    BlocProvider.of<MySmsServiceBloc>(context).add(GetMyServiceEvent());
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<MySmsServiceBloc, MySmsServiceState>(
      listenWhen: (context, state) {
        return state.mySubscriptionInfo.status !=
            state.mySubscriptionInfo.status;
      },
      listener: (context, state) {
        if (state.mySubscriptionInfo.status == STATUS.ERROR) {
          showSnackBarMessage(
              context: context,
              message: state.mySubscriptionInfo.failure.message);
        }
      },
      child: BlocBuilder<MySmsServiceBloc, MySmsServiceState>(
        builder: (context, state) {
          if (state.mySubscriptionInfo.status == STATUS.LOADING) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (state.mySubscriptionInfo.status == STATUS.SUCCESS) {
            if (state.mySubscriptionInfo.data is SMSSubscriptionModel) {
              SMSSubscriptionModel _subscriptionData = state.mySubscriptionInfo.data;
              return Column(
                children: [
                  Container(
                    child: SubscriptionDetailsBody(
                      subscription: state.mySubscriptionInfo.data,
                    ),
                  ),
                  _subscriptionData.expiringIn <= 7
                      ? Container(
                          width: 220,
                          height: 60,
                          margin:
                              EdgeInsets.only(left: 30, bottom: 20, right: 30,top: 25),
                          child:
                              BlocListener<MySmsServiceBloc, MySmsServiceState>(
                            listenWhen: (oldState, newState) =>
                                (oldState.renewSubscription == null ||
                                    oldState.renewSubscription.status !=
                                        newState.renewSubscription.status),
                            listener: (context, state) {
                              if (state.renewSubscription != null) {
                                if (state.renewSubscription.status ==
                                    STATUS.ERROR) {
                                  showSnackBarMessage(
                                      context: context,
                                      message: state
                                          .renewSubscription.failure.message);
                                } else if (state.renewSubscription.status ==
                                    STATUS.SUCCESS) {
                                  showSnackBarMessage(
                                      context: context,
                                      message: state.renewSubscription.data);
                                }
                              }
                            },
                            child: BlocBuilder<MySmsServiceBloc,
                                MySmsServiceState>(
                              builder: (context, state) {
                                if (state.renewSubscription != null &&
                                    state.renewSubscription.status ==
                                        STATUS.LOADING) {
                                  return circularProgressIndicator();
                                }
                                return Button(
                                  title: "Renew",
                                  textColor: Color(appColors.blue00008B),
                                  onTap: () {
                                    BlocProvider.of<MySmsServiceBloc>(context)
                                        .add(
                                      RenewServiceEvent(
                                          subscriptionId: _subscriptionData.id),
                                    );
                                  },
                                );
                              },
                            ),
                          ),
                        )
                      : Container(),
                ],
              );
            }
          }
          return Column(
            children: [
              Container(
                margin: EdgeInsets.only(left: 20, bottom: 50, right: 20),
                child: Label(
                  title: AppLocalizations.of(context).translate("Select to whom you want to enable the service"),
                  color: Colors.white,
                  textAlign: TextAlign.center,
                  fontWeight: FontWeight.w600,
                  fontSize: 18,
                ),
              ),
              GestureDetector(
                onTap: () async {
                  bool isSuccess = await Navigator.of(context).push(
                      MaterialPageRoute(
                          builder: (context) =>
                              SelfSmsSubscriptionFormPage()));
                  print('subscription created $isSuccess');
                  if (isSuccess) {
                    BlocProvider.of<MySmsServiceBloc>(context)
                        .add(GetMyServiceEvent());
                  }
                },
                child: Container(
                  width: 220,
                  alignment: Alignment.center,
                  padding: EdgeInsets.only(
                      left: 10, right: 10, bottom: 30, top: 10),
                  margin: EdgeInsets.only(top: 40.0, right: 10, left: 20),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20)),
                  child: Column(
                    children: [
                      SvgPicture.asset(
                        "assets/images/sms_option_icon.svg",
                        width: 60,
                        height: 60,
                        color: Colors.black,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Label(
                          title: AppLocalizations.of(context).translate("For Self"),
                          textAlign: TextAlign.center,
                          fontSize: 16,
                          color: Color(appColors.buttonColor)),
                    ],
                  ),
                ),
              ),
            ],
          );
        },
      ),
    );
  }

}
