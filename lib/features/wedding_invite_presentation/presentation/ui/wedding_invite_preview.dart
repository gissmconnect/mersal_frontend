import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_downloader/image_downloader.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:mersal/core/widgets/common/Button.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/features/wedding_invite_presentation/data/model/wedding_template_image/wedding_template_image_data.dart';
import 'package:mersal/features/wedding_invite_presentation/domain/entity/wedding_template_entity.dart';
import 'package:mersal/features/wedding_invite_presentation/presentation/ui/wedding_invitation_summary.dart';
import 'package:mersal/resources/colors.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WeddingInvitePreview extends StatefulWidget {
  final TemplateModelEntity templateData;
  final WeddingTemplateImageData templateImageList;
  final String invitationType,
      weddingInviteDate,
      weddingTime,
      weddingType,
      groomName,
      brideName,
      mobileNumber;

  final PickedFile userSelectedImage;

  WeddingInvitePreview(
      {this.invitationType,
      this.templateData,
      this.weddingInviteDate,
      this.weddingType,
      this.userSelectedImage,
      this.groomName,
      this.brideName,
      this.weddingTime,
      this.mobileNumber,
      this.templateImageList});

  @override
  _WeddingInvitePreviewState createState() => _WeddingInvitePreviewState();
}

class _WeddingInvitePreviewState extends State<WeddingInvitePreview> {
  bool showImageBool = false;
  var outputDate;

  @override
  void initState() {
    super.initState();
    if (widget.invitationType == "With Image") {
      showImageBool = true;
    }
    DateTime parseDate = new DateFormat("yyyy-MM-dd").parse(widget.weddingInviteDate);
    var inputDate = DateTime.parse(parseDate.toString());
    var outputFormat = DateFormat('dd-MM-yyyy');
    outputDate = outputFormat.format(inputDate);
    print(outputDate);
    setState(() {});
  }

  _requestPermission() async {
    Map<Permission, PermissionStatus> statuses = await [
      Permission.storage,
    ].request();
    final info = statuses[Permission.storage].toString();
    print(info);
    if (info != "PermissionStatus.denied") {
      var imageId =
          await ImageDownloader.downloadImage(widget.templateImageList.image);
      // var imageId =  await ImageDownloader.downloadImage(widget.templateImageList.image);
      var path = await ImageDownloader.findPath(imageId);
      // await ImageDownloader.open(path);
      // // Below is a method of obtaining saved image information.
      // var fileName = await ImageDownloader.findName(imageId);
      // // var path = await ImageDownloader.findPath(imageId);
      // var size = await ImageDownloader.findByteSize(imageId);
      // var mimeType = await ImageDownloader.findMimeType(imageId);
    }
  }

  String selectedUrl = 'https://flutter.io';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/bg_image.jpg"),
              fit: BoxFit.fill,
            ),
          ),
          child: Column(
            children: [
              SizedBox(
                height: 50,
              ),
              Stack(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(bottom: 5, left: 15),
                    alignment: Alignment.bottomLeft,
                    child: GestureDetector(
                      child: Icon(
                        Icons.arrow_back_ios_rounded,
                        color: Colors.white,
                      ),
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 6),
                    alignment: Alignment.bottomCenter,
                    child: Text(
                      "Wedding Invitation",
                      textAlign: TextAlign.end,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                ],
              ),
              Expanded(
                child: ListView(
                  shrinkWrap: true,
                  children: [
                    widget.invitationType == "With Image"
                        ? Container(
                            margin: EdgeInsets.only(left: 20, top: 20, right: 20),
                      height:MediaQuery.of(context).size.height ,
                      padding: EdgeInsets.only(left: 10, right: 10),
                            decoration: new BoxDecoration(
                              borderRadius: BorderRadius.circular(25),
                              color: Colors.white,
                            ),
                            child: Column(
                              children: [
                                Expanded(
                                  flex: 2,
                                  child: Container(
                                    width: MediaQuery.of(context).size.width,
                                    margin: EdgeInsets.only(top: 20),
                                    child: WebView(
                                      gestureNavigationEnabled: true,
                                      javascriptMode: JavascriptMode.unrestricted,
                                      initialUrl:
                                          // "http://93.115.18.236/public/wedding-card/preview/4?brideName=Neha&groomName=Mohit&weddingDate=06-06-2020&weddingType=Aqid",
                                          'http://93.115.18.236/public/wedding-card/preview/4?brideName=${widget.brideName}&groomName=${widget.groomName}&weddingDate=$outputDate&weddingType=${widget.weddingType}',
                                      onPageFinished: (value) {
                                        setState(() {
                                          print("====your page is load");
                                        });
                                      },
                                      onProgress: (int progress) {
                                        print(
                                            "WebView is loading (progress : $progress%)");
                                      },
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    margin: EdgeInsets.only(
                                        bottom: 20, left: 5, right: 5),
                                    child: Label(
                                      title: widget.templateData!=null?widget.templateData.template :"",
                                      fontSize: 16,
                                      textAlign: TextAlign.center,
                                      fontWeight: FontWeight.bold,
                                      color: Color(appColors.buttonColor),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )
                        : Container(
                            margin:  EdgeInsets.only(left: 20, top: 20, right: 20),
                            padding: EdgeInsets.only(left: 20, right: 10),
                            height: 250,
                            alignment: Alignment.center,
                            width: MediaQuery.of(context).size.width,
                            decoration: new BoxDecoration(
                              borderRadius: BorderRadius.circular(25),
                              color: Colors.white,
                            ),
                            child: widget.invitationType == "Text"? Label(
                              title: widget.templateData!=null?widget.templateData.template:"",
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                              color: Color(appColors.navyBlue),
                            ):Center(
                              child: widget.userSelectedImage != null
                                  ? Image.file(
                                File(widget.userSelectedImage.path),
                                fit: BoxFit.fill,
                              )
                                  : Container(),
                            ),
                          ),
                    /* showImageBool
                        ? Container(
                            height: 60,
                            margin:
                                EdgeInsets.only(top: 45, left: 20, right: 20),
                            child: Button(
                              title: 'Download Image',
                              color: Color(appColors.buttonColor),
                              borderColor: Colors.transparent,
                              textColor: Colors.white,
                              onTap: () async {
                                try {
                                  // if(_requestPermission()){
                                  //
                                  // }
                                  _requestPermission();
                                } on PlatformException catch (error) {
                                  print(error);
                                }
                              },
                            ),
                          )
                        : Container(),*/
                    Container(
                      height: 60,
                      margin: EdgeInsets.only(
                          top: 45, left: 20, right: 20, bottom: 30),
                      child: Button(
                        // title: "Select Contacts",
                        title: "Confirm",
                        color: Color(appColors.buttonColor),
                        borderColor: Colors.transparent,
                        textColor: Colors.white,
                        onTap: () async {
                          weddingInvitationBottomSheet();
                          /* final PermissionStatus permissionStatus =
                              await _getPermission();
                          if (permissionStatus == PermissionStatus.granted) {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        WeddingInviteContactsPage(
                                            invitationType:
                                                widget.invitationType,
                                            templateId: widget.templateData.id
                                                .toString(),
                                            weddingInviteDate:
                                                widget.weddingInviteDate,
                                            weddingTime: widget.weddingTime,
                                            groomName: widget.groomName,
                                            brideName: widget.brideName,
                                            mobileNumber: widget.mobileNumber,
                                            templateImageList:
                                                widget.templateImageList)));*/
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  //Check contacts permission
  Future<PermissionStatus> _getPermission() async {
    PermissionStatus permission = await Permission.contacts.status;
    if (permission != PermissionStatus.granted &&
        permission != PermissionStatus.permanentlyDenied) {
      PermissionStatus permissionStatus = await Permission.contacts.request();
      return permissionStatus;
    } else {
      return permission;
    }
  }

  Future<Widget> weddingInvitationBottomSheet() {
    String sendPhone = "";

    print("pffff" + sendPhone);
    return showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (context) {
          return Container(
              margin: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height / 3.5),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.vertical(top: Radius.circular(30.0)),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black,
                    offset: Offset(0.0, 1.0), //(x,y)
                    blurRadius: 5.0,
                  ),
                ],
              ),
              child: WeddingInvitationSummary(
                  sendPhone: "",
                  templateId: widget.templateData!=null?widget.templateData.id.toString():"",
                  invitationType: widget.invitationType,
                  weddingInviteDate: widget.weddingInviteDate,
                  weddingTime: widget.weddingTime,
                  groomName: widget.groomName,
                  brideName: widget.brideName,
                  userSelectedImage: widget.userSelectedImage,
                  mobileNumber: widget.mobileNumber,
                  weddingType: widget.weddingType,
                  templateImageList: widget.templateImageList));
        });
  }
}
