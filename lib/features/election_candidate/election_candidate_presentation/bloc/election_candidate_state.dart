part of 'election_candidate_bloc.dart';

abstract class ElectionCandidateState extends Equatable {
  const ElectionCandidateState();

  @override
  List<Object> get props => [];
}

class ElectionCandidateInitial extends ElectionCandidateState {}

class CandidatesLoading extends ElectionCandidateState {}

class CandidatesLoaded extends ElectionCandidateState {
  final ElectionCandidateResponseModel candidatesResponse;

  CandidatesLoaded({this.candidatesResponse});
}

class CandidatesLoadFailure extends ElectionCandidateState {
  final Failure failure;

  CandidatesLoadFailure({@required this.failure});
}

class VotingState extends ElectionCandidateState {}

class VotedState extends ElectionCandidateState {}

class VotingFailedState extends ElectionCandidateState {
  final Failure failure;

  VotingFailedState({@required this.failure});
}
