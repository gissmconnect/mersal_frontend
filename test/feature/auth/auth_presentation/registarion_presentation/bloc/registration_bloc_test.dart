import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/features/auth/auth_domain/usecases/register_user_usecase.dart';
import 'package:mersal/features/auth/auth_presentation/registration_presentation/bloc/registration_bloc.dart';
import 'package:mockito/mockito.dart';

class MockRegisterUserUseCase extends Mock implements RegisterUserUseCase {}

void main() {
  RegisterUserUseCase _registerUserUseCase;
  RegistrationBloc _registrationBloc;

  setUp(() {
    _registerUserUseCase = MockRegisterUserUseCase();
    _registrationBloc =
        RegistrationBloc(registerUserUseCase: _registerUserUseCase);
  });

  test('should initial state be RegistrationInitial state', () {
    expect(_registrationBloc.state, equals(RegistrationInitial()));
  });

  final _phoneNumber = '1234567890';
  final _password = 'abcd';
  final _confirmPassword = 'abcd';

  test(
      'should emit [Registering, Success] state when registration is successful',
      () async {
    when(_registerUserUseCase(any)).thenAnswer((_) async => Right(null));

    //act
    _registrationBloc.add(
        UserRegistrationEvent(phoneNumber: _phoneNumber, password: _password));

    //assert
    final _expectedStates = [Registering(), RegistrationSuccess()];
    expectLater(_registrationBloc.stream, emitsInOrder(_expectedStates));
  });

  final _serverFailure = ServerFailure();
  test('should return [Registering,Failure] when user registration fails',
      () async {
    when(_registerUserUseCase(any))
        .thenAnswer((_) async => Left(_serverFailure));

    //act
    _registrationBloc.add(
        UserRegistrationEvent(phoneNumber: _phoneNumber, password: _password));

    //assert
    final _expectedState = [
      Registering(),
      RegistrationError(_serverFailure)
    ];
    expectLater(_registrationBloc.stream, emitsInOrder(_expectedState));
  });
}
