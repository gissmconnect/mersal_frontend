import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/exceptions.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/network/network_info.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/child_service/child-data/datasource/child_data_source.dart';
import 'package:mersal/features/child_service/child-data/model/child-enroll-model/registered-child-activity/child_activity_model.dart';
import 'package:mersal/features/child_service/child-data/model/child-enroll-model/registered-child-details/child_details_model.dart';
import 'package:mersal/features/child_service/child-domain/repository/child_repository.dart';
import 'package:meta/meta.dart';

class ChildRepositoryImpl implements ChildRepository {
  final NetworkInfo networkInfo;
  final ChildDataSource childDataSource;
  final UserDataLocalDataSource userDataLocalDataSource;

  ChildRepositoryImpl(
      {@required this.networkInfo,
      @required this.childDataSource,
      @required this.userDataLocalDataSource});

  @override
  Future<Either<Failure, ChildActivityModel>> getActivity() async {
    if (await networkInfo.isConnected) {
      try {
        final _token = await userDataLocalDataSource.getAuthToken();
        final activity = await childDataSource.getActivity(token: _token);
        return Right(activity);
      } on ServerException catch (ex) {
        return Left(ServerFailure()..message = ex.message);
      } on AuthException catch (ex) {
        return Left(AuthFailure()..message = ex.message);
      } on Exception {
        return Left(ServerFailure());
      }
    } else {
      return Left(NetworkFailure());
    }
  }

  @override
  Future<Either<Failure, void>> enrollChild(
      {int electionId, int candidateId}) async {
    if (await networkInfo.isConnected) {
      try {
        final _token = await userDataLocalDataSource.getAuthToken();
        await childDataSource.enrollChild(
            token: _token, electionId: electionId, candidateId: candidateId);
      } on ServerException catch (ex) {
        return Left(ServerFailure()..message = ex.message);
      } on Exception {
        return Left(ServerFailure());
      }
    }
  }

  @override
  Future<Either<Failure, ChildDetailsModel>> getChildDetails(
      {int electionId}) async {
    if (await networkInfo.isConnected) {
      try {
        final _token = await userDataLocalDataSource.getAuthToken();
        final _childDetails = await childDataSource.getChildDetails(token: _token);
        return Right(_childDetails);
      } on ServerException catch (ex) {
        return Left(ServerFailure()..message = ex.message);
      } on Exception {
        return Left(ServerFailure());
      }
    } else {
      return Left(NetworkFailure());
    }
  }
}
