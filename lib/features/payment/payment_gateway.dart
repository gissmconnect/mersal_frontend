import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mersal/core/widgets/common/Button.dart';
import 'package:mersal/core/widgets/common/InputField.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/core/widgets/common/MaskedInputFormatter.dart';
import 'package:mersal/core/widgets/message.dart';
import 'package:mersal/features/payment/payment_failed.dart';
import 'package:mersal/resources/colors.dart';

class PaymentGateWay extends StatefulWidget {
  final String from;

  PaymentGateWay(this.from);

  @override
  _PaymentGateWayState createState() => _PaymentGateWayState();
}

class _PaymentGateWayState extends State<PaymentGateWay> {
  final cardNumberController = TextEditingController();
  final holderNameController = TextEditingController();
  final monthController = TextEditingController();
  final yearController = TextEditingController();
  final cvvController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bg_image.jpg"),
            fit: BoxFit.fill,
          ),
        ),
        child: GestureDetector(
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  SizedBox(
                    height: 50,
                  ),
                  Stack(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(bottom: 5, left: 15),
                        alignment: Alignment.bottomLeft,
                        child: GestureDetector(
                          child: Icon(
                            Icons.arrow_back_ios_rounded,
                            color: Colors.white,
                          ),
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(bottom: 6),
                        alignment: Alignment.bottomCenter,
                        child: Text(
                          "Payment Gateway",
                          textAlign: TextAlign.end,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                    ],
                  ),
                  SvgPicture.asset(
                    "assets/images/atm_img.svg",
                    fit: BoxFit.cover,
                    width: 250,
                    height: 250,
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 30, top: 40, right: 30),
                    child: TextFormField(
                      controller: holderNameController,
                      validator: (text) {
                        return text.isEmpty
                            ? 'Card Holder name cannot be null'
                            : null;
                      },
                      decoration: InputDecoration(
                        filled: true,
                        counterText: "",
                        fillColor: Colors.white,
                        hintText: "Enter Card Holder Name",
                        hintStyle: TextStyle(color: Colors.black),
                        contentPadding:
                            const EdgeInsets.only(bottom: 15.0, top: 15.0),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(25.7),
                        ),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(25.7),
                        ),
                        focusedErrorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(25.0),
                            borderSide: BorderSide(
                              color: Colors.red,
                            )),
                        errorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(25.0),
                            borderSide: BorderSide(
                              color: Colors.red,
                            )),
                        prefixIcon: Container(
                          margin: EdgeInsets.only(left: 15),
                          width: 100,
                          child: Row(
                            children: [
                              IconButton(
                                onPressed: () {},
                                icon: Icon(Icons.person,
                                    size: 30,
                                    color: Color(appColors.buttonColor)),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 15),
                                width: 1,
                                height: 30,
                                color: Colors.black,
                              )
                            ],
                          ),
                        ),
                      ),
                      keyboardType: TextInputType.name,
                      onChanged: (text) {},
                      inputFormatters: [
                        MaskedTextInputFormatter(
                          mask: 'xxxx-xxxx-xxxx-xxxx-xxxx',
                          separator: '-',
                        )
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 30, top: 40, right: 30),
                    child: TextFormField(
                      controller: cardNumberController,
                      validator: (text) {
                        return text.isEmpty
                            ? 'Card number cannot be null'
                            : null;
                      },
                      decoration: InputDecoration(
                        filled: true,
                        counterText: "",
                        fillColor: Colors.white,
                        hintText: "Enter Card Number",
                        hintStyle: TextStyle(color: Colors.black),
                        contentPadding:
                            const EdgeInsets.only(bottom: 15.0, top: 15.0),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(25.7),
                        ),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(25.7),
                        ),
                        focusedErrorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(25.0),
                            borderSide: BorderSide(
                              color: Colors.red,
                            )),
                        errorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(25.0),
                            borderSide: BorderSide(
                              color: Colors.red,
                            )),
                        prefixIcon: Container(
                          margin: EdgeInsets.only(left: 15),
                          width: 100,
                          child: Row(
                            children: [
                              IconButton(
                                onPressed: () {},
                                icon: Icon(Icons.credit_card,
                                    size: 30, color: Colors.black),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 15),
                                width: 1,
                                height: 30,
                                color: Colors.black,
                              )
                            ],
                          ),
                        ),
                      ),
                      keyboardType: TextInputType.phone,
                      onChanged: (text) {},
                      inputFormatters: [
                        MaskedTextInputFormatter(
                          mask: 'xxxx-xxxx-xxxx-xxxx-xxxx',
                          separator: '-',
                        )
                      ],
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Container(
                      margin: EdgeInsets.only(top: 40.0, left: 35),
                      child: Label(
                        title: "Valid Till",
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Container(
                    child: Row(
                      children: [
                        Expanded(
                          child: Container(
                            margin:
                                EdgeInsets.only(left: 30, top: 25, right: 30),
                            child: InputField(
                              controller: monthController,
                              hint: "Month",
                              maxLength: 2,
                              inputType: TextInputType.phone,
                              // validator: (text) {
                              //   return text.isEmpty
                              //       ? 'Month cannot be null'
                              //       : null;
                              // },
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.only(top: 25, right: 30),
                            child: InputField(
                              controller: yearController,
                              hint: "Year",
                              maxLength: 4,
                              inputType: TextInputType.phone,
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.only(top: 25, right: 30),
                            child: InputField(
                              controller: cvvController,
                              hint: "CVV",
                              maxLength: 4,
                              inputType: TextInputType.phone,
                              // validator: (text) {
                              //   return text.isEmpty
                              //       ? 'CVV cannot be null'
                              //       : null;
                              // },
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 40, bottom: 20, left: 20, right: 20),
                    height: 60,
                    child: Button(
                      title: "Pay OMR1",
                      textColor: Colors.white,
                      borderColor: Colors.transparent,
                      color: Color(appColors.buttonColor),
                      onTap: () {
                        if (monthController.text.isEmpty) {
                          showSnackBarMessage(
                              context: context, message: "Please enter month");
                        } else if (yearController.text.isEmpty) {
                          showSnackBarMessage(
                              context: context, message: "Please enter year");
                        } else if (cvvController.text.isEmpty) {
                          showSnackBarMessage(
                              context: context, message: "Please enter cvv");
                        } else if (_formKey.currentState.validate()) {
                          // Navigator.push(
                          //     context,
                          //     MaterialPageRoute(
                          //         builder: (context) =>
                          //             DocUploadPage(widget.from)));
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      PaymentFailedScreen(widget.from)));
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }



}
