import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/save_user_login_data_usecase.dart';
import 'package:mersal/features/auth/auth_domain/usecases/login_usecase.dart';
import 'package:meta/meta.dart';

part 'login_event.dart';

part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final LoginUseCase loginUseCase;
  final SaveUserLoginDataUseCase saveUserLoginDataUseCase;

  LoginBloc(
      {@required this.loginUseCase, @required this.saveUserLoginDataUseCase})
      : super(LoginInitial());

  @override
  Stream<LoginState> mapEventToState(
    LoginEvent event,
  ) async* {
    if (event is LoginRequestEvent) {
      yield LoginInitiated();

      final _result = await loginUseCase(
          Params(phoneNumber: event.phoneNumber, password: event.password, device_token: event.device_token));

      yield* _result.fold(
        (l) async* {
          yield LoginError(failure: l);
        },
        (r) async* {
          await saveUserLoginDataUseCase(LoginParams(
              loginId: event.phoneNumber, password: event.password, device_token: event.device_token));
          yield LoginSuccess();
        },
      );
    }
  }
}
