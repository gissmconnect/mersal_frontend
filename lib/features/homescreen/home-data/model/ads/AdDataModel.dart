
import 'package:mersal/features/homescreen/home-data/model/ads/AdDataList.dart';

class AdDataModel {
  List<AdDataList> adList;
  String message;
  bool status;

  AdDataModel({this.adList,this.status});

  factory AdDataModel.fromJson(Map<String, dynamic> json) {
    return AdDataModel(
      adList: json['data'] != null ? (json['data'] as List).map((i) => AdDataList.fromJson(i)).toList() : null,
      status: json['status'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.adList != null) {
      data['data'] = this.adList.map((v) => v.toJson()).toList();
    }
    return data;
  }
}