part of 'wedding_booking_bloc.dart';

abstract class WeddingBookingState extends Equatable {
  @override
  List<Object> get props => [];
}

class WeddingBookingInitial extends WeddingBookingState {}

class WeddingRegistering extends WeddingBookingState {}

class WeddingSuccess extends WeddingBookingState {}

class WeddingError extends WeddingBookingState {
  final Failure failure;

  WeddingError(this.failure);
}
