import 'package:mersal/features/donation/domain/entity/donation_type_entity.dart'
as donationEntity;

class DonationTypeModel extends donationEntity.DonationTypeEntity {
  String createdAt;

  DonationTypeModel(
      {int id, String reason, String description, String createdAt})
      : super(id: id, reason: reason, description: description);

  factory DonationTypeModel.fromJson(Map<String, dynamic> json) {
    return DonationTypeModel(
        id: json['id'],
        reason: json['reason'],
        description: json['description'],
        createdAt: json['created_at']);
  }

  donationEntity.DonationTypeEntity toDomain() {
    var d =  donationEntity.DonationTypeEntity(reason: this.reason,
        isSelected: this.isSelected,
        description: this.description,
        id: this.id);
    print('toDomain $d');
    return d;
  }
}

/*
class Reason extends donationEntity.Reason {
  String createdAt, updatedAt, deletedAt;

  Reason(
      {int id, String reason, this.createdAt, this.updatedAt, this.deletedAt})
      : super(id: id, reason: reason);

  factory Reason.fromJson(Map<String, dynamic> json) {
    return Reason(
        id: json['id'],
        reason: json['reason'],
        createdAt: json['created_at'],
        updatedAt: json['updated_at'],
        deletedAt: json['deleted_at']);
  }
}
*/
