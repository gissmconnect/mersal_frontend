part of 'login_bloc.dart';

abstract class LoginEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class LoginRequestEvent extends LoginEvent {
  final String phoneNumber, password, device_token;

  LoginRequestEvent({@required this.phoneNumber, @required this.password, @required this.device_token});
}
