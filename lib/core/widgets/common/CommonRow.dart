import 'package:flutter/material.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/resources/colors.dart';

class CommonRow extends StatelessWidget {

  final String firstText,secondText;

  CommonRow(this.firstText,this.secondText);

  @override
  Widget build(BuildContext context) {
    return
      Container(
        padding: EdgeInsets.only(bottom: 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              margin: EdgeInsets.only(left: 20),
              child:Label(
                title: firstText,
                color: Color(appColors.blue007D9E),
                fontSize: 18,
              ),),
            Container(
              margin: EdgeInsets.only(right: 20),
              child:Label(
                title: secondText,
                color: Color(appColors.blue00008B),
                fontSize: 18,
              ),),
// Icon(Icons.chevron_right_outlined)
          ],
        ),
      );
  }
}
