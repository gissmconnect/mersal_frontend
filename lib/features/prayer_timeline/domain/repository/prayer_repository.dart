import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/features/prayer_timeline/domain/entity/prayer_data_entity.dart';
import 'package:meta/meta.dart';

abstract class PrayerRepository{
  Future<Either<Failure,List<PrayerDataEntity>>> getPrayerTimes();
  Future<Either<Failure,String>> setAlarm({@required int id});
}