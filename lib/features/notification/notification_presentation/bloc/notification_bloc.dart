import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/network/vo/Resource.dart';
import 'package:mersal/core/usecases/get_user_login_data_usecase.dart';
import 'package:mersal/core/usecases/reauthenticate_usecase.dart';
import 'package:mersal/core/usecases/save_user_login_data_usecase.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/notification/notification_domain/entity/notification_entity.dart';
import 'package:mersal/features/notification/notification_domain/usecases/archive_notification_usecase.dart';
import 'package:mersal/features/notification/notification_domain/usecases/delete_notification_usecase.dart';
import 'package:mersal/features/notification/notification_domain/usecases/get_notification_usecase.dart';
import 'package:meta/meta.dart';

part 'notification_event.dart';

part 'notification_state.dart';

class NotificationBloc extends Bloc<NotificationEvent, NotificationState> {
  final GetNotificationUseCase notificationUseCase;
  final GetUserLoginDataUseCase getUserLoginDataUseCase;
  final ReAuthenticateUseCase reAuthenticateUseCase;
  final DeleteNotificationUseCase deleteNotificationUseCase;
  final ArchiveNotificationUseCase archiveNotificationUseCase;

  NotificationBloc(
      {@required this.notificationUseCase,
      @required this.getUserLoginDataUseCase,
      @required this.reAuthenticateUseCase,
      @required this.deleteNotificationUseCase,
      @required this.archiveNotificationUseCase})
      : super(NotificationState.initial());

  @override
  Stream<NotificationState> mapEventToState(
    NotificationEvent event,
  ) async* {
    if (event is NotificationsEvent) {
      yield state.copy(getNotificationResource: Resource.loading());

      final result = await notificationUseCase(NoParams());

      yield* result.fold((failure) async* {
        if (failure is AuthFailure) {
          final _loginData = await getUserLoginDataUseCase(NoParams());
          yield* _handleLoginDataUseCase(_loginData);
        } else {
          yield state.copy(
              getNotificationResource: Resource.error(failure: failure));
        }
      }, (r) async* {
        yield state.copy(getNotificationResource: Resource.success(data: r));
      });
    }

    if (event is DeleteNotificationEvent) {
      yield state.copy(deleteNotificationResource: Resource.loading());
      final _surveysCall = await deleteNotificationUseCase(event.id);
      yield* _surveysCall.fold((l) async* {
        if (l is AuthFailure) {
          final _reauthenticateResponse =
              await reAuthenticateUseCase(NoParams());
          _reauthenticateResponse.fold((l) async* {
            yield Left(l);
          }, (r) => add(DeleteNotificationEvent(id: event.id)));
        } else {
          yield state.copy(
              deleteNotificationResource: Resource.error(failure: l));
        }
      }, (r) async* {
        final List<NotificationEntity> _notificationList =
            state.getNotificationResource.data;
        _notificationList.removeWhere((element) => element.id == event.id);
        yield state.copy(
            getNotificationResource: Resource.success(data: _notificationList),
            deleteNotificationResource: Resource.success(data: r));
      });
    }
    if (event is ArchiveNotificationEvent) {
      yield state.copy(archiveNotificationResource: Resource.loading());
      final _surveysCall = await archiveNotificationUseCase(event.id);
      yield* _surveysCall.fold((l) async* {
        if (l is AuthFailure) {
          final _reauthenticateResponse =
              await reAuthenticateUseCase(NoParams());
          _reauthenticateResponse.fold((l) async* {
            yield Left(l);
          }, (r) => add(ArchiveNotificationEvent(id: event.id)));
        } else {
          yield state.copy(
              archiveNotificationResource: Resource.error(failure: l));
        }
      }, (r) async* {
        final List<NotificationEntity> _notificationList =
            state.getNotificationResource.data;
        _notificationList.removeWhere((element) => element.id == event.id);
        yield state.copy(
            getNotificationResource: Resource.success(data: _notificationList),
            archiveNotificationResource: Resource.success(data: r));
      });
    }
  }

  @override
  NotificationState get initialState => NotificationState.initial();

  Stream<NotificationState> _handleLoginDataUseCase(
      Either<Failure, LoginParams> loginData) async* {
    yield* loginData.fold((failure) async* {
      if (failure is AuthFailure) {
        final _loginData = await getUserLoginDataUseCase(NoParams());
        yield* _handleLoginDataUseCase(_loginData);
      } else {
        yield state.copy(
            getNotificationResource: Resource.error(failure: failure));
      }
    }, (r) async* {
      final _reAuthenticateResponse = await reAuthenticateUseCase(NoParams());
      yield* _handleReAuthentication(_reAuthenticateResponse);
    });
  }

  Stream<NotificationState> _handleReAuthentication(
      Either<Failure, void> reAuthenticateResponse) async* {
    yield* reAuthenticateResponse.fold((l) async* {
      yield state.copy(getNotificationResource: Resource.error(failure: l));
    }, (r) async* {
      add(NotificationsEvent());
    });
  }
}
