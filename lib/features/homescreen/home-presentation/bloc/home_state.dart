part of 'home_bloc.dart';

abstract class HomeState extends Equatable {
  @override
  List<Object> get props => [];
}

class HomeInitial extends HomeState {
  @override
  List<Object> get props => [];
}

class HomeLoading extends HomeState {
  @override
  List<Object> get props => [];
}

class HomeLoaded extends HomeState {
  final List<BannerEntity> bannerList;

  HomeLoaded({@required this.bannerList});

  @override
  List<Object> get props => [bannerList];
}

class HomeLoadFiled extends HomeState {
  final Failure failure;

  HomeLoadFiled({this.failure});

  @override
  List<Object> get props => [failure];
}
