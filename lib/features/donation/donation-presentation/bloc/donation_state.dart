part of 'donation_bloc.dart';

class DonationState extends Equatable {
  final Resource donationTypeResource;
   DonationTypeEntity selectedDonationType;
  final Resource donateResource;
  int randomNum = 0;
  DonationState(
      {this.donationTypeResource,
      this.selectedDonationType,
      this.donateResource,
      this.randomNum});

  @override
  List<Object> get props =>
      [donationTypeResource, selectedDonationType, donateResource,randomNum];

  factory DonationState.initial() {
    return DonationState(donationTypeResource: Resource.loading());
  }

  DonationState copy(
      {Resource donationTypeResource,
      DonationTypeEntity selectedDonationType,
      Resource donateResource,
      int randomNum}) {
    return DonationState(
        donationTypeResource: donationTypeResource ?? this.donationTypeResource,
        selectedDonationType: selectedDonationType ?? this.selectedDonationType,
        donateResource: donateResource ?? this.donateResource,
    randomNum: randomNum?? this.randomNum);
  }
}
