class OccasionEntity {
  bool status;
  List<OccasionDataListEntity> occasionDataList;

  OccasionEntity({this.status, this.occasionDataList});

  OccasionEntity copy({
    int id,
    String title,
    String description,
    List<OccasionDataListEntity> donationDataList,
  }) {
    return OccasionEntity(
        status: status ?? this.status,
        occasionDataList: donationDataList ?? this.occasionDataList);
  }
}

class OccasionDataListEntity {
  int id;
  String description;
  String title;
  String link;
  String occasionDate;
  String created_at;

  OccasionDataListEntity({this.id, this.title,  this.link, this.occasionDate, this.created_at,  this.description});

  OccasionDataListEntity copy(
      {int id,
        String title,
        String link,
        String created_at,
        String occasionDate,
        int answerId}) {
    return OccasionDataListEntity(
        id: id ?? this.id,
        title: title ?? this.title,
        link: link ?? this.link,
        occasionDate: occasionDate ?? this.occasionDate,
        created_at: created_at ?? this.created_at,
        description: description ?? this.description);
  }
}
