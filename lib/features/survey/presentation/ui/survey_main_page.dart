import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:mersal/core/localization/app_localization.dart';
import 'package:mersal/core/network/api_endpoints.dart';
import 'package:mersal/core/network/vo/Status.dart';
import 'package:mersal/core/widgets/common/Button.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/InputField.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/core/widgets/common/shared_preference.dart';
import 'package:mersal/core/widgets/message.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/info_presentation/ui/info_screen.dart';
import 'package:mersal/features/survey/domain/entity/survey_entity.dart';
import 'package:mersal/features/survey/presentation/bloc/survey_bloc.dart';
import 'package:mersal/resources/colors.dart';

import '../../../../injection_container.dart';

class SurveyMainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<SurveyBloc>(
      create: (_) => serviceLocator<SurveyBloc>(),
      child: Scaffold(
        body: Container(
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/bg_image.jpg"),
                fit: BoxFit.fill,
              ),
            ),
            child: _SurveyPageBody()),
      ),
    );
  }
}

class _SurveyPageBody extends StatefulWidget {
  @override
  _SurveyPageBodyState createState() => _SurveyPageBodyState();
}

class _SurveyPageBodyState extends State<_SurveyPageBody> {
  bool hitService = false, showTextFieldView = false;
  SurveyEntity _surveyEntity;
  int currentNewQuestion = 0,
      currentQuestionCount = 1,
      surveyOptionId,
      currentIndex = 0,
      selectedAnswer = -1;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  BuildContext mContextLoader;
  final answerController = TextEditingController();
  Questions _question;
  FToast fToast;

  @override
  void initState() {
    BlocProvider.of<SurveyBloc>(context).add(GetSurveyEvent());
    super.initState();
    fToast = FToast();
    fToast.init(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SurveyBloc, SurveyState>(
      builder: (context, state) {
        if (state.getSurveyResource != null &&
            state.getSurveyResource.status == STATUS.LOADING) {
          return Center(
            child: CircularProgressIndicator(
              backgroundColor: Colors.white,
            ),
          );
        } else if (state.getSurveyResource != null &&
            state.getSurveyResource.status == STATUS.SUCCESS) {
          _surveyEntity = state.getSurveyResource.data;
          return GestureDetector(
            onTap: () {
              FocusScope.of(context).unfocus();
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: 50,
                ),
                Stack(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(bottom: 5, left: 15),
                      alignment: Alignment.bottomLeft,
                      child: GestureDetector(
                        child: Icon(
                          Icons.arrow_back_ios_rounded,
                          color: Colors.white,
                        ),
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(bottom: 6),
                      alignment: Alignment.bottomCenter,
                      child: Text(
                        AppLocalizations.of(context)
                            .translate("Survey")  ,
                        textAlign: TextAlign.end,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(right: 15),
                      alignment: Alignment.centerRight,
                      child: GestureDetector(
                        child: Container(
                          margin: EdgeInsets.only(left: 20),
                          child: SvgPicture.asset(
                            "assets/images/info_icon.svg",
                            width: 30,
                            height: 30,
                          ),
                        ),
                        onTap: () {
                          CommonMethods().animatedNavigation(
                              context: context,
                              currentScreen: SurveyMainPage(),
                              landingScreen: InfoScreen());
                        },
                      ),
                    ),
                  ],
                ),
                Expanded(
                  child: ListView(
                    children: [
                      Center(
                        child: Image.asset(
                          "assets/images/survey_logo.png",
                          scale: 2,
                        ),
                      ),
                      Center(
                        child: Container(
                          margin: EdgeInsets.only(left: 20),
                          child: Label(
                            title: _surveyEntity.title,
                            fontWeight: FontWeight.w600,
                            color: Color(appColors.buttonColor),
                            fontSize: 18,
                          ),
                        ),
                      ),
                      Center(
                        child: Container(
                          margin: EdgeInsets.only(top: 30),
                          child: Label(
                            title:
                            " ${AppLocalizations.of(context)
                                .translate("Total Questions:")} ${_surveyEntity.questions.length.toString()}",
                            color: Colors.white,
                            fontSize: 18,
                          ),
                        ),
                      ),
                      Center(
                        child: Container(
                          margin: EdgeInsets.only(top: 30),
                          child: Label(
                            title: "${AppLocalizations.of(context)
                                .translate("Current Question:")} $currentQuestionCount",
                            color: Colors.white,
                            fontSize: 18,
                          ),
                        ),
                      ),
                      questionsWidget(),
                    ],
                  ),
                ),
              ],
            ),
          );
        }
        return Container();
      },
    );
  }

  Widget questionsWidget() {
    return Container(
      height: 450,
      child: Column(
        children: [
          Expanded(
            child: ListView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (context, index) {
                currentIndex = index;
                _question = _surveyEntity.questions[currentNewQuestion];
                if (_question.options.length < 1) {
                  if (mounted) {
                    Future.delayed(Duration(milliseconds: 1000), () {
                      showTextFieldView = true;
                      setState(() {});
                    });
                  }
                }
                /* for (int i = 0; i < _question.options.length; i++) {
                  if (mounted) {
                    if (_question.options[index].optionType == 2) {
                      showTextFieldView = true;
                      Future.delayed(Duration(milliseconds: 1000), () {
                        showTextFieldView = true;
                        setState(() {});
                      });
                    }
                  }
                }*/
                return Container(
                  margin: EdgeInsets.only(left: 20, top: 30),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Label(
                        title: _question.question,
                        color: Color(appColors.buttonColor),
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                      ),
                      !showTextFieldView
                          ? ListView.builder(
                              padding: EdgeInsets.zero,
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: _question.options.length,
                              itemBuilder: (context, int index) {
                                return GestureDetector(
                                    onTap: () {
                                      hitService = true;
                                      setState(() {});
                                      surveyOptionId =
                                          _question.options[index].id;
                                      // BlocProvider.of<SurveyBloc>(context).add(
                                      //   ChooseAnswerEvent(
                                      //       questionId: _question.id,
                                      //       optionId: _question.options[index].id),
                                      // );
                                      selectedAnswer = index;
                                    },
                                    child: _question
                                                .options[index].optionType ==
                                            1
                                        ? buttonViewWidget(index, _question)
                                        : checkBoxViewWidget(index, _question));
                              })
                          : textFieldViewWidget()
                    ],
                  ),
                );
              },
              itemCount: 1,
            ),
          ),
          optionsWidget()
        ],
      ),
    );
  }

  Widget optionsWidget() {
    return Center(
      child: BlocListener<SurveyBloc, SurveyState>(
        listener: (context, state) {
          if (state is SurveyQuestionError) {
            CommonMethods.showToast(fToast:fToast,message:state.failure.message,status:false);
          } else if (state is SurveyQuestionSuccess) {
            currentNewQuestion = currentNewQuestion + 1;
            currentQuestionCount = currentQuestionCount + 1;
            hitService = false;
            CommonMethods.showToast(fToast:fToast,message:"Success Submit",status:true);
          }
        },
        child: BlocBuilder<SurveyBloc, SurveyState>(builder: (_, state) {
          if (state is SurveyQuestionSubmitting) {
            return Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [CircularProgressIndicator()],
            );
          } else {
            return Row(
              children: [
                currentNewQuestion != 0
                    ? Expanded(
                  child: Container(
                    margin:EdgeInsets.only(bottom: 20, left: 20, right: 20),
                    height: 60,
                    width: 200,
                    child: Button(
                      title: AppLocalizations.of(context)
                          .translate("Previous"),
                      color: Colors.transparent,
                      borderColor: Colors.white,
                      textColor: Colors.white,
                      onTap: () {
                        if (_surveyEntity.questions.length >
                            currentNewQuestion) {
                          currentNewQuestion = currentNewQuestion - 1;
                          currentQuestionCount = currentQuestionCount - 1;
                          hitService = true;
                          setState(() {});
                        } else {
                          CommonMethods.showToast(fToast:fToast,message:AppLocalizations.of(context)
                              .translate("Please fill survey"),status:false);
                        }
                      },
                    ),
                  ),
                )
                    : Container(),
                Expanded(
                  child: Container(
                    margin: EdgeInsets.only(bottom: 20, left: 20, right: 20),
                    height: 60,
                    width: 200,
                    child: Button(
                      title:currentQuestionCount < _surveyEntity.questions.length?
                      AppLocalizations.of(context)
                          .translate("Next"): AppLocalizations.of(context)
                          .translate("Submit"),
                      color: Color(appColors.buttonColor),
                      borderColor: Colors.transparent,
                      textColor: Colors.white,
                      onTap: () {
                        if (hitService) {
                          if (currentQuestionCount < _surveyEntity.questions.length) {
                            // BlocProvider.of<SurveyBloc>(context).add(
                            //   SubmitAnswerEvent(
                            //     surveyId: _surveyEntity.id,
                            //     surveyQuestionId: _surveyEntity
                            //         .questions[currentNewQuestion].id,
                            //     surveyOptionId: surveyOptionId,
                            //   ),
                            // );
                            apiCall();
                          } else {
                            showConfirmDialog(context);
                          }
                          setState(() {});

                          // Navigator.push(
                          //     context,
                          //     MaterialPageRoute(
                          //         builder: (context) => DocUploadPage("survey")));
                        } else {
                          CommonMethods.showToast(fToast:fToast,message:AppLocalizations.of(context)
                              .translate("Please fill survey"),status:false);
                        }
                      },
                    ),
                  ),
                ),
              ],
            );
          }
        }),
      ),
    );
  }

  void showConfirmDialog(BuildContext context) {
    showGeneralDialog(
      barrierLabel: "Barrier",
      barrierDismissible: true,
      barrierColor: Colors.black.withOpacity(0.8),
      transitionDuration: Duration(milliseconds: 500),
      context: context,
      pageBuilder: (_, __, ___) {
        return Align(
          alignment: Alignment.center,
          child: Container(
            height: 200,
            margin: EdgeInsets.only(left: 10, right: 10),
            child: Card(
              elevation: 20,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    child: Label(
                      title: AppLocalizations.of(context)
                          .translate("Do you want to submit?"),
                      color: Color(appColors.buttonColor),
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        width: 120,
                        height: 40,
                        margin: EdgeInsets.only(left: 10, top: 35, right: 30),
                        child: Button(
                          title: AppLocalizations.of(context)
                              .translate("No"),
                          color: Colors.transparent,
                          textColor: Color(appColors.buttonColor),
                          borderColor: Color(appColors.buttonColor),
                          onTap: () async {
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                      Container(
                        width: 120,
                        height: 40,
                        margin: EdgeInsets.only(left: 10, top: 35, right: 30),
                        child: Button(
                          title: AppLocalizations.of(context)
                              .translate("Yes"),
                          color: Color(appColors.buttonColor),
                          borderColor: Colors.transparent,
                          textColor: Colors.white,
                          onTap: () {
                            apiSubmitCall();
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        );
      },
      transitionBuilder: (_, anim, __, child) {
        return SlideTransition(
          position: Tween(begin: Offset(0, 1), end: Offset(0, 0)).animate(anim),
          child: child,
        );
      },
    );
  }

  Future<void> apiCall() async {
    final answer = answerController.text.toString().trim();
    final _params = <String, String>{
      'survey_id': _surveyEntity.id.toString(),
      'survey_question_id':
          _surveyEntity.questions[currentNewQuestion].id.toString(),
      'survey_option_id':
          surveyOptionId.toString() != "null" ? surveyOptionId.toString() : "",
      'answer': answer ?? ""
    };

    final _token = await appPreferences.getStringPreference("AccessToken");

    if (context != null) {
      _showLoader(context);
    }
    var response = await http.post(Uri.parse(SUBMIT_SURVEY_QUESTION_ENDPOINT),
        headers: {
          'Content-type': 'application/json',
          AUTHORIZATION: BEARER + " " + _token
        },
        body: json.encode(_params));

    if (response.statusCode == 200) {
      final Map<String, dynamic> _responseJson = json.decode(response.body);
      if (mContextLoader != null) Navigator.pop(mContextLoader);
      print( 'SuccessSurveyQuestion ${response.statusCode} and ${_responseJson.toString()}');
      selectedAnswer = -1;
      currentNewQuestion = currentNewQuestion + 1;
      currentQuestionCount = currentQuestionCount + 1;
      hitService = false;
      CommonMethods.showToast(fToast:fToast,message:AppLocalizations.of(context)
          .translate("Answer Submitted"),status:true);
      answerController.text = "";

      _question = _surveyEntity.questions[currentNewQuestion];
      if (_question.options.length < 0) {
        if (mounted) {
          Future.delayed(Duration(milliseconds: 1000), () {
            showTextFieldView = true;
            setState(() {});
          });
        }
      }
      /*for (int i = 0; i < _question.options.length; i++) {
        if (mounted) {
          if (_question.options[currentIndex].optionType == 2) {
            showTextFieldView = true;
            Future.delayed(Duration(milliseconds: 1000), () {
              showTextFieldView = true;
              setState(() {});
            });
          }
        }
      }*/
      setState(() {});
    }
  }

  Future<void> apiSubmitCall() async {
    final _params = <String, String>{'survey_id': _surveyEntity.id.toString()};

    final _token = await appPreferences.getStringPreference("AccessToken");

    if (context != null) {
      _showLoader(context);
    }
    var response = await http.post(Uri.parse(SURVEY_COMPLETE_ENDPOINT),
        headers: {
          'Content-type': 'application/json',
          AUTHORIZATION: BEARER + " " + _token
        },
        body: json.encode(_params));

    if (response.statusCode == 200) {
      final Map<String, dynamic> _responseJson = json.decode(response.body);
      if (mContextLoader != null) Navigator.pop(mContextLoader);

      print(
          'SuccessSurveyFinalResponse ${response.statusCode} and ${_responseJson.toString()}');
      CommonMethods.showToast(fToast:fToast,message:AppLocalizations.of(context)
          .translate("Survey Completed Successfully"),status:true);
      Future.delayed(Duration(milliseconds: 500), () {
        Navigator.of(context).pop();
      });
      setState(() {});
    }
  }

  Future<Null> _showLoader(BuildContext context) async {
    return showDialog<Null>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context1) {
        mContextLoader = context1;
        return Scaffold(
          backgroundColor: Colors.transparent.withOpacity(0.3),
          key: _keyLoader,
          body: Container(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    child: CircularProgressIndicator(),
                    height: 40.0,
                    width: 40.0,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Widget buttonViewWidget(
    int index,
    Questions _question,
  ) {
    return Container(
      decoration: BoxDecoration(
          color: selectedAnswer == index ? Colors.white : Colors.transparent,
          border: Border.all(color: Colors.white),
          borderRadius: BorderRadius.circular(25)),
      padding: EdgeInsets.only(top: 15, bottom: 15),
      margin: EdgeInsets.only(top: 20, left: 60, right: 60),
      child: Label(
          title: _question.options[index].option,
          textAlign: TextAlign.center,
          fontWeight: FontWeight.w500,
          fontSize: 16,
          color: selectedAnswer == index ? Colors.black : Colors.white),
    );
  }

  Widget checkBoxViewWidget(
    int index,
    Questions _question,
  ) {
    return Container(
      margin: EdgeInsets.only(
          top: 30,
          left: MediaQuery.of(context).size.width / 4.5,
          right: MediaQuery.of(context).size.width / 4.5),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            margin: EdgeInsets.only(right: 20),
            alignment: Alignment.centerRight,
            child: Icon(
              selectedAnswer == index
                  ? Icons.check_box
                  : Icons.check_box_outline_blank,
              size: 30,
              color: Color(appColors.buttonColor),
            ),
          ),
          Label(
              title: _question.options[index].option,
              textAlign: TextAlign.center,
              fontWeight: FontWeight.w500,
              fontSize: 16,
              color: selectedAnswer == index ? Colors.black : Colors.white)
        ],
      ),
    );
  }

  Widget textFieldViewWidget() {
    return Container(
        margin: EdgeInsets.only(top: 30, left: 20, right: 20),
        child: Container(
          margin: EdgeInsets.only(left: 30, top: 25, right: 30),
          child: InputField(
            controller: answerController,
            onSubmitted: (term) {
              setState(() {
                hitService = true;
              });
              FocusScope.of(context).unfocus();
            },
            onChanged: (v) {
              if (v.length > 0) {
                setState(() {
                  hitService = true;
                });
              }
            },
            validator: (text) {
              return text.isEmpty ? AppLocalizations.of(context)
                  .translate('Answer cannot be null') : null;
            },
            hint:AppLocalizations.of(context)
                .translate("Enter Your Answer") ,
            inputType: TextInputType.name,
          ),
        ));
  }
}
