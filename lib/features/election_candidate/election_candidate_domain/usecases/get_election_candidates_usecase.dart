import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/election/election-domain/repository/election_repository.dart';
import 'package:mersal/features/election_candidate/election_candidate_data/model/election_candidate_model.dart';
import 'package:mersal/features/election_candidate/election_candidate_domain/entity/election_candidate_entity.dart';
import 'package:meta/meta.dart';

class GetElectionCandidatesUseCase extends UseCase<ElectionCandidateResponseModel, Params> {
  final ElectionRepository electionRepository;

  GetElectionCandidatesUseCase({@required this.electionRepository});

  @override
  Future<Either<Failure, ElectionCandidateResponseModel>> call(
      Params params) async {
    return await electionRepository.getCandidates(
        electionId: params.electionId);
  }
}

class Params {
  final int electionId;

  Params({@required this.electionId});
}
