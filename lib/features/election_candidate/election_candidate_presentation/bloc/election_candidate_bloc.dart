import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/features/election/election-domain/usecases/vote_usecase.dart'
    as voteCase;
import 'package:mersal/features/election_candidate/election_candidate_data/model/election_candidate_model.dart';
import 'package:mersal/features/election_candidate/election_candidate_domain/entity/election_candidate_entity.dart';
import 'package:mersal/features/election_candidate/election_candidate_domain/usecases/get_election_candidates_usecase.dart';
import 'package:meta/meta.dart';

part 'election_candidate_event.dart';

part 'election_candidate_state.dart';

class ElectionCandidateBloc extends Bloc<ElectionCandidateEvent, ElectionCandidateState> {
  final GetElectionCandidatesUseCase getElectionCandidatesUseCase;
  final voteCase.VoteUseCase voteUseCase;
  bool isVoted = false;

  ElectionCandidateBloc(
      {@required this.getElectionCandidatesUseCase, @required this.voteUseCase})
      : super(ElectionCandidateInitial());

  @override
  Stream<ElectionCandidateState> mapEventToState(
    ElectionCandidateEvent event,
  ) async* {
    if (event is GetElectionCandidatesEvent) {
      yield CandidatesLoading();
      final _candidateLoadResponse = await getElectionCandidatesUseCase(
        Params(electionId: event.electionId),
      );
      yield* _candidateLoadResponse.fold(
        (l) async* {
          yield CandidatesLoadFailure(failure: l);
        },
        (r) async* {
          yield CandidatesLoaded(candidatesResponse: r);
        },
      );
    }
    if (event is VoteEvent) {
      yield VotingState();
      final _voteResponse = await voteUseCase(voteCase.Params(electionId: event.electionId, candidateId: event.candidateId));
      yield* _voteResponse.fold((l) async* {
        yield VotingFailedState(failure: l);
      }, (r) async* {
        yield VotedState();
      });
    }
  }
}
