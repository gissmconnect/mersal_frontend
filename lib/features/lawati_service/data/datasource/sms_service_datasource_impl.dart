import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:mersal/core/error/exceptions.dart';
import 'package:mersal/core/network/api_endpoints.dart';
import 'package:mersal/core/network/http_call.dart';
import 'package:mersal/core/network/http_error_handler.dart';
import 'package:mersal/features/lawati_service/data/datasource/sms_service_datasource.dart';
import 'package:mersal/features/lawati_service/data/model/sms_service_info_model.dart';
import 'package:mersal/features/lawati_service/data/model/sms_subscriptions_model.dart';
import 'package:mersal/features/lawati_service/domain/usecase/subscribe_sms_subscription_usecase.dart';
import 'package:meta/meta.dart';

class SMSServiceDataSourceImpl implements SMSServiceDataSource {
  final http.Client client;

  SMSServiceDataSourceImpl({@required this.client});

  @override
  Future<SMSServiceInfoModel> getServiceInfo({String token}) async {
    final _response = await httpGetRequest(httpClient: client, url: SMS_SERVICE_INFO_ENDPOINT, token: token);
    if (_response.statusCode == 200) {
      final _serviceResponse = json.decode(_response.body);
      print('getServiceInfo response ${_serviceResponse}');
      return SMSServiceInfoModel.fromJson(_serviceResponse['data']);
    } else {
      handleError(_response);
    }
  }

  @override
  Future<List<SMSSubscriptionModel>> getSubscriptions({String token}) async {
    final _response = await httpGetRequest(
        httpClient: client, url: SMS_SUBSCRIPTIONS_ENDPOINT, token: token);
    if (_response.statusCode == 200) {
      final Map<String, dynamic> _responseJson = json.decode(_response.body);
      if (_responseJson.containsKey('data')) {
        return ((json.decode(_response.body)['data']) as List<dynamic>)
            .map((e) => SMSSubscriptionModel.fromJson(e))
            .toList();
      } else {
        throw ServerException()..message = _responseJson['message'];
      }
    } else {
      handleError(_response);
    }
  }

  @override
  Future<dynamic> subscribe(
      {String token, SubscriptionRequestParams params}) async {
    final _response = await httpPostRequest(
        httpClient: client,
        url: SMS_SUBSCRIBE_ENDPOINT,
        token: token,
        params: params.toJSON());
    print('response ${_response.statusCode}and ${_response.body}');
    if (_response.statusCode == 200) {
      final Map<String, dynamic> _responseJson = json.decode(_response.body);
      if (_responseJson.containsKey('message')) {
        return _responseJson['data'];
      } else {
        throw ServerException()..message = _responseJson['message'];
      }
    } else {
      handleError(_response);
    }
  }

  @override
  Future<SMSSubscriptionModel> getMySubscription({String token}) async {
    final _response = await httpGetRequest(
        httpClient: client, url: MY_SMS_SUBSCRIPTION_ENDPOINT, token: token);
    print('my sms ${_response.body}');
    if (_response.statusCode == 200) {
      final Map<String, dynamic> _responseJson = json.decode(_response.body);
      if (_responseJson.containsKey('data') &&
          _responseJson['data'] != null) {
        return SMSSubscriptionModel.fromJson(
            json.decode(_response.body)['data']);
      } else {
        throw ServerException()..message = _responseJson['message'];
      }
    } else {
      handleError(_response);
    }
  }

  @override
  Future<String> renewSubscription({String token, int subscriptionId}) async {
    final _params = <String, String>{'subscription_id': "$subscriptionId"};
    final _response = await httpPostRequest(
        httpClient: client,
        url: RENEWS_SMS_SUBSCRIPTION_ENDPOINT,
        params: _params,
        token: token);
    print('renewSubscription ${_response.body}');
    if (_response.statusCode == 200) {
      final Map<String, dynamic> _responseJson = json.decode(_response.body);
      if (_responseJson.containsKey('data')) {
        return json.decode(_response.body)['message'];
      } else {
        throw ServerException()..message = _responseJson['message'];
      }
    } else {
      handleError(_response);
    }
  }
}
