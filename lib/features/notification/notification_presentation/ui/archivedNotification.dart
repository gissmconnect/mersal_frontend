import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:mersal/core/network/api_endpoints.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/core/widgets/common/shared_preference.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/notification/notification_data/model/ArchivedNotificationModel/ArchivedDataList.dart';
import 'package:mersal/features/notification/notification_data/model/ArchivedNotificationModel/ArchivedNotificationModel.dart';
import 'package:mersal/resources/colors.dart';

import 'notification_detail.dart';

enum PopUpValue { Archive, Delete }

class ArchivedNotification extends StatefulWidget {
  @override
  _ArchivedNotificationState createState() => _ArchivedNotificationState();
}

class _ArchivedNotificationState extends State<ArchivedNotification> {
  List<ArchivedDataList> archivedNotificationList = [];
  ArchivedDataModel archivedDataModel;

  @override
  void initState() {
    super.initState();
    apiArchivedCall();
  }

  @override
  Widget build(BuildContext context) {
    return  archivedNotificationList.length > 0
        ? ListView.builder(
            padding: EdgeInsets.zero,
            shrinkWrap: true,
            itemCount: archivedNotificationList.length,
            itemBuilder: (context, int index) {
              return archiveNotificationListItem(index);
            },
          )
        : Center(
            child: Container(
              child: Label(
                title: "No Record(s) found",
                color: Colors.black,
                fontWeight: FontWeight.w600,
                fontSize: 18,
              ),
            ),
          );
  }

  Widget archiveNotificationListItem(int index) {
    return GestureDetector(
      onTap: () {
        CommonMethods().animatedNavigation(
            context: context,
            landingScreen: NotificationDetails(),
            currentScreen: ArchivedNotification());
      },
      child: Container(
        margin: EdgeInsets.only(top: 20, left: 20, right: 20),
        padding: EdgeInsets.only(bottom: 20, left: 10),
        decoration: new BoxDecoration(
            borderRadius: BorderRadius.circular(22), color: Colors.white),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Row(
                    children: [
                      Container(
                        padding: EdgeInsets.only(left: 5, top: 10, bottom: 10),
                        margin: EdgeInsets.only(
                            left: 15, top: 20, bottom: 20, right: 20),
                        width: 45,
                        height: 45,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.grey.shade300),
                      ),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(left: 20, right: 20),
                          child: Label(
                            title: archivedNotificationList[index].title,
                            color: Color(appColors.buttonColor),
                            fontSize: 16,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                PopupMenuButton<PopUpValue>(
                  onSelected: (PopUpValue result) {},
                  itemBuilder: (BuildContext context) =>
                      <PopupMenuEntry<PopUpValue>>[
                    const PopupMenuItem<PopUpValue>(
                      value: PopUpValue.Archive,
                      child: Text('Remove'),
                    ),
                    const PopupMenuItem<PopUpValue>(
                      value: PopUpValue.Delete,
                      child: Text('Delete'),
                    ),
                  ],
                )
              ],
            ),
            Container(
              margin: EdgeInsets.only(left: 20,right: 20),
              child: Label(
                title: archivedNotificationList[index].text,
                color: Color(appColors.buttonColor),
                fontSize: 14,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> apiArchivedCall() async {
    final _token = await appPreferences.getStringPreference("AccessToken");

    var response = await http.get(Uri.parse(ARCHIVED_NOTIFICATION_LIST),
        headers: {
          'Content-type': 'application/json',
          AUTHORIZATION: BEARER + " " + _token
        });

    if (response.statusCode == 200) {
      try {

        final Map<String, dynamic> _responseJson = json.decode(response.body);

        print('AcrhivedResponse ${response.statusCode} and ${_responseJson.toString()}');

        archivedDataModel = ArchivedDataModel.fromJson(_responseJson);
        archivedNotificationList = archivedDataModel.archivedList;
        setState(() {});
      } catch (ex) {}
    }
  }
}
