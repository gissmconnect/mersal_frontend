part of 'wedding_invite_bloc.dart';

abstract class WeddingInviteState extends Equatable {
  @override
  List<Object> get props => [];
}

class WeddingInviteInitial extends WeddingInviteState {}

class WeddingInviting extends WeddingInviteState {}

class WeddingInviteSuccess extends WeddingInviteState {}

class WeddingInviteError extends WeddingInviteState {
  final Failure failure;

  WeddingInviteError(this.failure);
}
