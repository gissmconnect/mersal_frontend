import 'package:mersal/features/prayer_timeline/domain/entity/prayer_data_entity.dart';

class PrayerDataModel extends PrayerDataEntity {
  String updatedAt, createdAt, deletedAt;

  PrayerDataModel(
      {int id,
      String name,
      prayerTime,
      String createdAt,
      String updatedAt,
      String deletedAt,
      int isNext})
      : super(id: id, name: name, prayerTime: prayerTime, isNext: isNext);

  PrayerDataModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    prayerTime = json['prayer_time'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
    isNext = json['isNext'];
  }
}
