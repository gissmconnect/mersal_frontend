import 'package:mersal/features/survey/domain/entity/survey_entity.dart';
import 'package:meta/meta.dart';

class SurveyModel extends SurveyEntity {
  SurveyModel(
      {@required int id,
      @required String title,
      @required String description,
      @required int status,
      @required List<QuestionModel> questions})
      : super(
            id: id,
            title: title,
            description: description,
            questions: questions);

  SurveyModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    description = json['description'];
    if (json['questions'] != null) {
      questions = new List<Questions>();
      json['questions'].forEach((v) {
        questions.add(new QuestionModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'title': title,
      'description': description,
      'questions': questions,
    };
  }
}

class QuestionModel extends Questions {
  QuestionModel({int id, String question, List<OptionModel> options})
      : super(id: id, options: options, question: question);

  QuestionModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    question = json['question'];
    if (json['options'] != null) {
      options = [];
      json['options'].forEach((v) {
        options.add(new OptionModel.fromJson(v));
      });
    }
  }



  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = id;
    data['question'] = question;
    if (this.options != null) {
      data['options'] =
          options.map((v) => (v as OptionModel).toJson()).toList();
    }
    return data;
  }
}

class OptionModel extends Option {
  OptionModel(
      {int id,
      int surveyId,
      int questionId,
      String option,
      int optionType,
      String createdAt,
      String updatedAt,
      String deletedAt})
      : super(
          id: id,
          option: option,
          optionType: optionType,
          questionId: questionId,
          surveyId: surveyId,
        );

  OptionModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    surveyId = json['survey_id'];
    optionType = json['option_type'];
    questionId = json['question_id'];
    option = json['option'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['survey_id'] = this.surveyId;
    data['question_id'] = this.questionId;
    data['option'] = this.option;
    return data;
  }
}
