import 'package:equatable/equatable.dart';
import 'package:mersal/core/util/date_formatter.dart';

class PrayerDataEntity extends Equatable {
  int id, isNext;
  String name, prayerTime;
  bool settingAlarm;

  PrayerDataEntity(
      {this.id,
      this.isNext,
      this.name,
      this.prayerTime,
      this.settingAlarm = false});

  @override
  List<Object> get props => [id, name, prayerTime, isNext];

  bool get isPrayerOver {
    DateTime _currentTime = DateTime.now();
    DateTime _prayerTimeInMillis = formatPrayerTimeToMillis(dateTime: prayerTime);
    bool _isOver = _prayerTimeInMillis.isBefore(_currentTime);
    return _isOver;
  }

  PrayerDataEntity copy(
      {int id, int isNext, String name, String prayerTime, bool settingAlarm}) {
    return PrayerDataEntity(
        id: id ?? this.id,
        isNext: isNext ?? this.isNext,
        name: name ?? this.name,
        prayerTime: prayerTime ?? this.prayerTime,
        settingAlarm: settingAlarm ?? this.settingAlarm);
  }

  PrayerDataEntity doDomain() {
    return this;
  }

  @override
  String toString() {
    return 'PrayerDataEntity{id: $id, isNext: $isNext, name: $name, prayerTime: $prayerTime, settingAlarm: $settingAlarm}';
  }
}
