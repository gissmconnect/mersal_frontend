import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/exceptions.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/network/network_info.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/prayer_timeline/data/datasource/prayer_datasource.dart';
import 'package:mersal/features/prayer_timeline/domain/entity/prayer_data_entity.dart';
import 'package:mersal/features/prayer_timeline/domain/repository/prayer_repository.dart';
import 'package:meta/meta.dart';

class PrayerRepositoryImpl implements PrayerRepository {
  final NetworkInfo networkInfo;
  final PrayerDataSource prayerDataSource;
  final UserDataLocalDataSource userDataLocalDataSource;

  PrayerRepositoryImpl(
      {@required this.networkInfo,
      @required this.prayerDataSource,
      @required this.userDataLocalDataSource});

  @override
  Future<Either<Failure, List<PrayerDataEntity>>> getPrayerTimes() async {
    if (await networkInfo.isConnected) {
      try {
        final _token = await userDataLocalDataSource.getAuthToken();
        final _prayerTimes =
            await prayerDataSource.getPrayerTimes(token: _token);
        return Right(_prayerTimes);
      } on ServerException catch (ex) {
        return Left(ServerFailure()..message = ex.message);
      } on AuthException catch (ex) {
        return Left(AuthFailure()..message = ex.message);
      } on Exception {
        return Left(ServerFailure());
      }
    } else {
      return Left(NetworkFailure());
    }
  }

  @override
  Future<Either<Failure, String>> setAlarm({int id}) async {
    if (await networkInfo.isConnected) {
      try {
        final _token = await userDataLocalDataSource.getAuthToken();
        final _message = await prayerDataSource.setAlarm(token: _token, id: id);
        return Right(_message);
      } on ServerException catch (ex) {
        return Left(ServerFailure()..message = ex.message);
      } on AuthException catch (ex) {
        return Left(AuthFailure()..message = ex.message);
      } on Exception {
        return Left(ServerFailure());
      }
    } else {
      return Left(NetworkFailure());
    }
  }
}
