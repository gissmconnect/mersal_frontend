import 'dart:convert';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:mersal/core/localization/app_localization.dart';
import 'package:mersal/core/network/api_endpoints.dart';
import 'package:mersal/core/widgets/common/Button.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/core/widgets/common/image_chooser.dart';
import 'package:mersal/core/widgets/common/shared_preference.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/wedding_invite_presentation/data/model/wedding_template_image/wedding_template_image_data.dart';
import 'package:mersal/features/wedding_invite_presentation/data/model/wedding_template_image/wedding_template_image_model.dart';
import 'package:mersal/features/wedding_invite_presentation/domain/entity/wedding_template_entity.dart';
import 'package:mersal/features/wedding_invite_presentation/presentation/ui/wedding_invite_preview.dart';
import 'package:mersal/features/wedding_invite_presentation/presentation/wedding_invite_template/bloc/wedding_invite_template_bloc.dart';
import 'package:mersal/resources/colors.dart';

import '../../../../../injection_container.dart';

class WeddingInviteTemplate extends StatelessWidget {
  final String invitationType,
      weddingInviteDate,
      weddingTime,
      brideName,
      weddingType,
      groomName,
      mobileNumber;

  WeddingInviteTemplate(
      {this.invitationType,
      this.weddingInviteDate,
      this.groomName,
      this.weddingType,
      this.brideName,
      this.weddingTime,
      this.mobileNumber});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (_) => serviceLocator<WeddingInviteTemplateBloc>(),
        child: Scaffold(
            body: WeddingInviteTemplateBody(
                invitationType: invitationType,
                weddingInviteDate: weddingInviteDate,
                groomName: groomName,
                brideName: brideName,
                weddingType: weddingType,
                weddingTime: weddingTime,
                mobileNumber: mobileNumber)));
  }
}

class WeddingInviteTemplateBody extends StatefulWidget {
  final String invitationType,
      weddingInviteDate,
      weddingTime,
      brideName,
      weddingType,
      groomName,
      mobileNumber;

  WeddingInviteTemplateBody(
      {this.invitationType,
      this.weddingInviteDate,
      this.groomName,
      this.brideName,
      this.weddingType,
      this.weddingTime,
      this.mobileNumber});

  @override
  _WeddingInviteTemplateBodyState createState() =>
      _WeddingInviteTemplateBodyState();
}

class _WeddingInviteTemplateBodyState extends State<WeddingInviteTemplateBody> {
  String templateId = "", weddingInviteDate = "Select Date";

  bool templateBool = false, showImageBool = false;

  List<WeddingTemplateImageData> templateImageList;

  List<bool> templateImageBoolList = [];
  int indexValue = -1;
  WeddingTemplateImagesModel weddingTemplateImagesModel;

  TemplateModelEntity templateData;
  PickedFile templateImage;
  FToast fToast;
  String _selectTemplate = 'Select Template';

  @override
  void initState() {
    super.initState();
    BlocProvider.of<WeddingInviteTemplateBloc>(context).add(GetTemplateEvent());
    templateImageApi();

    fToast = FToast();
    fToast.init(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<WeddingInviteTemplateBloc, WeddingInviteTemplateState>(
      builder: (context, state) {
        if (state is WeddingInviteTemplateInitialising) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        if (state is WeddingInviteTemplateLoaded) {
          List<TemplateModelEntity> templateList = state.activity.data;
          return GestureDetector(
            onTap: () {
              FocusScope.of(context).unfocus();
            },
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/bg_image.jpg"),
                  fit: BoxFit.fill,
                ),
              ),
              child: ListView(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 20,
                      ),
                      Stack(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(bottom: 5, left: 15),
                            alignment: Alignment.bottomLeft,
                            child: GestureDetector(
                              child: Icon(
                                Icons.arrow_back_ios_rounded,
                                color: Colors.white,
                              ),
                              onTap: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(bottom: 6),
                            alignment: Alignment.bottomCenter,
                            child: Text(
                              AppLocalizations.of(context)
                                  .translate("Wedding Invitation"),
                              textAlign: TextAlign.end,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                        ],
                      ),
                      widget.invitationType == "Upload own image"
                          ? Container()
                          : Container(
                              alignment: Alignment.center,
                              padding: EdgeInsets.only(left: 20, right: 20),
                              margin:
                                  EdgeInsets.only(left: 30, right: 30, top: 80),
                              decoration: new BoxDecoration(
                                shape: BoxShape.rectangle,
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(25),
                              ),
                              child: DropdownButton<TemplateModelEntity>(
                                  underline: Container(),
                                  isExpanded: true,
                                  hint: Row(
                                    children: <Widget>[
                                      Label(
                                        title: _selectTemplate ?? "",
                                        color: Colors.black,
                                        fontSize: 16,
                                        textAlign: TextAlign.center,
                                      ),
                                      Icon(
                                        Icons.keyboard_arrow_down_sharp,
                                        color: Colors.white,
                                        size: 20,
                                      ),
                                    ],
                                  ),
                                  items: templateList
                                      .map((TemplateModelEntity val) {
                                    return new DropdownMenuItem<
                                        TemplateModelEntity>(
                                      value: val,
                                      child: Label(
                                        title: val.title,
                                        fontSize: 18,
                                        color: Colors.black,
                                      ),
                                    );
                                  }).toList(),
                                  onChanged: (newVal) {
                                    _selectTemplate = newVal.title;
                                    templateData = newVal;
                                    templateBool = true;
                                    setState(() {});
                                  }),
                            ),
                      showImageBool
                          ? Container(
                              margin: EdgeInsets.only(top: 40.0),
                              child: Label(
                                title: AppLocalizations.of(context)
                                    .translate("Choose Image"),
                                fontSize: 18,
                                fontWeight: FontWeight.w600,
                                color: Colors.white,
                              ),
                            )
                          : widget.invitationType == "Upload own image"
                              ? Container(
                                  margin: EdgeInsets.only(
                                      top: 40.0, left: 30, right: 30),
                                  child: Button(
                                    title: "Pick Image",
                                    borderColor: Colors.transparent,
                                    textColor: Colors.white,
                                    color: Color(appColors.buttonColor),
                                    onTap: () {
                                      imageChooser
                                          .showImageChooser(context)
                                          .then((picture) {
                                        if (picture != null) {
                                          this.templateImage = picture;
                                          setState(() {});
                                          print(picture);
                                        }
                                      });
                                    },
                                  ),
                                )
                              : Container(),
                      templateImageList != null && templateImageList.length > 0
                          ? showImageBool
                              ? mainMenuItemGrid()
                              : widget.invitationType == "Upload own image"
                                  ? Container(
                                      height: 200,
                                      margin: EdgeInsets.only(
                                          left: 35, right: 35, top: 40),
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              color:
                                                  Color(appColors.buttonColor),
                                              width: 1),
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(15)),
                                      child: Center(
                                        child: templateImage != null
                                            ? Image.file(
                                                File(templateImage.path),
                                                fit: BoxFit.fill,
                                              )
                                            : Container(),
                                      ))
                                  : Container()
                          : Container(),
                      Container(
                        height: 60,
                        margin: EdgeInsets.only(
                            left: 30, top: 50, right: 30, bottom: 40),
                        child: Button(
                          title:
                              AppLocalizations.of(context).translate("Preview"),
                          color: Color(appColors.buttonColor),
                          borderColor: Colors.transparent,
                          textColor: Colors.white,
                          onTap: () {
                            if (!templateBool &&
                                widget.invitationType != "Upload own image") {
                              CommonMethods.showToast(
                                  fToast: fToast,
                                  message: AppLocalizations.of(context)
                                      .translate("Please select a template"),
                                  status: false);
                            } else if (indexValue == -1 && showImageBool) {
                              CommonMethods.showToast(
                                  fToast: fToast,
                                  message: AppLocalizations.of(context)
                                      .translate("Please select an image"),
                                  status: false);
                            } else
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => WeddingInvitePreview(
                                        templateData: templateData,
                                        invitationType: widget.invitationType,
                                        weddingInviteDate: widget.weddingInviteDate,
                                        weddingTime: widget.weddingTime,
                                        mobileNumber: widget.mobileNumber,
                                        groomName: widget.groomName,
                                        weddingType: widget.weddingType,
                                        userSelectedImage: templateImage,
                                        brideName: widget.brideName,
                                        templateImageList: showImageBool
                                            ? templateImageList[indexValue]
                                            : null),
                                  ));
                          },
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          );
        } else {
          return Container();
        }
      },
    );
  }

  Widget mainMenuItemGrid() {
    return GridView.builder(
        padding: EdgeInsets.only(top: 20, left: 20, right: 20),
        shrinkWrap: true,
        physics: ScrollPhysics(),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
        ),
        itemCount: templateImageList.length,
        itemBuilder: (BuildContext context, int index) {
          return menuGridItem(index);
        });
  }

  Widget menuGridItem(int index) {
    return GestureDetector(
      onTap: () {
        indexValue = index;
        if (indexValue == index) setState(() {});
      },
      child: Center(
        child: Container(
            margin: EdgeInsets.only(left: 5, right: 5),
            decoration: BoxDecoration(
                border: Border.all(
                    color: indexValue == index
                        ? Color(appColors.buttonColor)
                        : Colors.transparent,
                    width: 3),
                color: Colors.white,
                borderRadius: BorderRadius.circular(15)),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 20),
                    child: CachedNetworkImage(
                      imageUrl: templateImageList[index].image,
                      fit: BoxFit.cover,
                    ),
                  ),
                ],
              ),
            )),
      ),
    );
  }

  Future<void> templateImageApi() async {
    final _token = await appPreferences.getStringPreference("AccessToken");

    var response = await http.get(Uri.parse(WEDDING_TEMPLATE_IMAGES), headers: {
      'Content-type': 'application/json',
      AUTHORIZATION: BEARER + " " + _token
    });

    if (response.statusCode == 200) {
      final Map<String, dynamic> _responseJson = json.decode(response.body);
      print(
          'TempalteImagesResponse ${response.statusCode} and ${_responseJson.toString()}');
      weddingTemplateImagesModel =
          WeddingTemplateImagesModel.fromJson(_responseJson);
      templateImageList = weddingTemplateImagesModel.templateImages;

      for (int i = 0; i < templateImageList.length; i++) {
        templateImageBoolList.add(false);
      }
      if (widget.invitationType == "With Image") {
        showImageBool = true;
      }

      setState(() {});
    }
  }
}
