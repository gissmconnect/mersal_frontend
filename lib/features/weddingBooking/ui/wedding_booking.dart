import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:mersal/core/network/api_endpoints.dart';
import 'package:mersal/core/network/http_error_handler.dart';
import 'package:mersal/core/widgets/common/Button.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/InputField.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/core/widgets/common/image_chooser.dart';
import 'package:mersal/core/widgets/common/shared_preference.dart';
import 'package:mersal/core/widgets/message.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/info_presentation/ui/info_screen.dart';
import 'package:mersal/features/payment/payment_webview.dart';
import 'package:mersal/features/weddingBooking/bloc/wedding_booking_bloc.dart';
import 'package:mersal/features/weddingBooking/data/model/wedding__contract_place_model.dart';
import 'package:mersal/features/weddingBooking/data/model/wedding_contract_data.dart';
import 'package:mersal/features/weddingBooking/ui/wedding_success.dart';
import 'package:mersal/injection_container.dart';
import 'package:mersal/resources/colors.dart';

class WeddingBooking extends StatefulWidget {
  @override
  _WeddingBookingState createState() => _WeddingBookingState();
}

class _WeddingBookingState extends State<WeddingBooking> {
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  BuildContext mContextLoader;
  List<String> dummyOptions = [
    'Wedding booking with dinner',
    'Wedding without dinner'
  ];
  FToast fToast;

  TimeOfDay selectedTime = TimeOfDay.now();

  PickedFile docOne, docTwo, docThree, docFour;

  String userName = "";

  String interrogationPlaceId = "",
      contractPlaceId = "",
      weddingPlaceId = "",
      nationalityId = "";
  String _selectedLocation = 'Select Interrogation Place',
      contractPlace = 'Select Contract Place',
      weddingPlace = 'Select Wedding Place',
      brideNationality = 'Omani',
      aqidTime = 'Select Aqid Time',
      weddingTime = "Select Time";
  bool inviteTypeBool = false,
      weddingDateBool = false,
      contractDateBool = false,
      questioningBool = false,
      weddingTimeBool = false,
      aqidTimeBool = false,
      dinnerBool = false,
      showEnterWeddingBool = false,
      serveCommitteeBool = false;

  final weddingTitleController = TextEditingController();
  final brideNameController = TextEditingController();
  final groomNameController = TextEditingController();
  final brideGuardianNameController = TextEditingController();
  final brideAddressController = TextEditingController();
  final weddingAddressController = TextEditingController();
  final brideMobileController = TextEditingController();
  final brideIdentityController = TextEditingController();
  final groomMobileController = TextEditingController();
  final brideGuardianMobileController = TextEditingController();
  final brideResidencePlaceController = TextEditingController();
  final brideWorkPlaceController = TextEditingController();
  final weddingPlaceController = TextEditingController();
  final emailController = TextEditingController();
  final contractController = TextEditingController();
  final brideNationalityController = TextEditingController();
  final dateController = TextEditingController();
  final timeController = TextEditingController();
  bool changeBorderBool = false;
  bool obscureText = false;

  String passwordText = "",
      contractDateSend = "",
      weddingDateSend = "",
      questioningDateSend = "",
      questioningDate = "Questioning Date",
      weddingDate = "Wedding Date",
      contractDate = "Contract Date";
  ValueNotifier<DateTime> _dateTimeNotifier =
      ValueNotifier<DateTime>(DateTime.now());

  final FocusNode weddingTitleFocus = new FocusNode();
  final FocusNode brideNameFocus = new FocusNode();
  final FocusNode brideIdentityFocus = new FocusNode();
  final FocusNode groomNameFocus = new FocusNode();
  final FocusNode brideGuardianNameFocus = new FocusNode();
  final FocusNode brideGuardianMobileFocus = new FocusNode();
  final FocusNode brideAddressFocus = new FocusNode();
  final FocusNode weddingAddressFocus = new FocusNode();
  final FocusNode brideMobileFocus = new FocusNode();
  final FocusNode groomMobileFocus = new FocusNode();
  final FocusNode brideResidencePlaceFocus = new FocusNode();
  final FocusNode brideWorkPlaceFocus = new FocusNode();
  final FocusNode weddingPlaceFocus = new FocusNode();
  final FocusNode emailFocus = new FocusNode();
  final FocusNode contractFocus = new FocusNode();
  final FocusNode brideNationalityFocus = new FocusNode();
  final FocusNode dateFocus = new FocusNode();
  final FocusNode timeFocus = new FocusNode();
  final _formKey = GlobalKey<FormState>();

  WeddingContractPlaceModel weddingContractPlaceModel;
  List<WeddingContractData> weddingContractList = [];

  WeddingContractPlaceModel weddingInterrogationPlaceModel;
  List<WeddingContractData> weddingInterrogationList = [];

  WeddingContractPlaceModel nationalityModel;
  List<WeddingContractData> nationalityModelList = [];

  WeddingContractPlaceModel weddingPlaceModel;
  List<WeddingContractData> weddingPlaceModelList = [];

  @override
  void initState() {
    fToast = FToast();
    fToast.init(context);
    super.initState();
    apiContractCall();
    apiWeddingPlaceCall();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => serviceLocator<WeddingBookingBloc>(),
      child: Scaffold(
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/bg_image.jpg"),
              fit: BoxFit.fill,
            ),
          ),
          child: Column(
            children: [
              SizedBox(
                height: 40,
              ),
              Stack(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(bottom: 5, left: 15),
                    alignment: Alignment.bottomLeft,
                    child: GestureDetector(
                      child: Icon(
                        Icons.arrow_back_ios_rounded,
                        color: Colors.white,
                      ),
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 6),
                    alignment: Alignment.bottomCenter,
                    child: Text(
                      "Wedding Booking",
                      textAlign: TextAlign.end,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 15),
                    alignment: Alignment.centerRight,
                    child: GestureDetector(
                      child:  Container(
                        margin: EdgeInsets.only(left: 20),
                        child: SvgPicture.asset(
                          "assets/images/info_icon.svg",
                          width: 30,
                          height: 30,
                        ),
                      ),
                      onTap: () {
                        CommonMethods().animatedNavigation(
                            context: context,
                            currentScreen: WeddingBooking(),
                            landingScreen: InfoScreen());
                      },
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    FocusScope.of(context).unfocus();
                  },
                  child: Form(
                    key: _formKey,
                    child: ListView(
                      shrinkWrap: true,
                      children: [
                        Container(
                          margin: EdgeInsets.only(left: 30, right: 30),
                          child: InputField(
                            controller: brideNameController,
                            focusNode: brideNameFocus,
                            onSubmitted: (term) {
                              CommonMethods.inputFocusChange(
                                  context, brideNameFocus, groomNameFocus);
                            },
                            validator: (text) {
                              return text.isEmpty
                                  ? 'Bride Name cannot be empty'
                                  : null;
                            },
                            hint: "Bride's Name",
                            inputType: TextInputType.name,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 30, top: 25, right: 30),
                          child: InputField(
                            controller: groomNameController,
                            focusNode: groomNameFocus,
                            onSubmitted: (term) {
                              CommonMethods.inputFocusChange(
                                  context, groomNameFocus, weddingTitleFocus);
                            },
                            validator: (text) {
                              return text.isEmpty
                                  ? 'Groom name cannot be empty'
                                  : null;
                            },
                            hint: "Groom Name",
                            inputType: TextInputType.name,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 30, top: 25, right: 30),
                          child: InputField(
                            controller: weddingTitleController,
                            focusNode: weddingTitleFocus,
                            onSubmitted: (term) {
                              CommonMethods.inputFocusChange(context,
                                  weddingTitleFocus, brideAddressFocus);
                            },
                            validator: (text) {
                              return text.isEmpty
                                  ? 'Wedding name cannot be empty'
                                  : null;
                            },
                            hint: "Wedding Name",
                            inputType: TextInputType.name,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 30, top: 25, right: 30),
                          child: InputField(
                            controller: brideAddressController,
                            focusNode: brideAddressFocus,
                            onSubmitted: (term) {
                              CommonMethods.inputFocusChange(context,
                                  brideAddressFocus, weddingAddressFocus);
                            },
                            validator: (text) {
                              return text.isEmpty
                                  ? 'Bride Address cannot be empty'
                                  : null;
                            },
                            hint: "Bride's Address",
                            inputType: TextInputType.streetAddress,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 30, top: 25, right: 30),
                          child: InputField(
                            controller: weddingAddressController,
                            focusNode: weddingAddressFocus,
                            onSubmitted: (term) {
                              CommonMethods.inputFocusChange(context,
                                  weddingAddressFocus, brideMobileFocus);
                            },
                            validator: (text) {
                              return text.isEmpty
                                  ? 'Wedding Address cannot be empty'
                                  : null;
                            },
                            hint: "Wedding Address",
                            inputType: TextInputType.streetAddress,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 30, top: 25, right: 30),
                          child: InputField(
                            controller: brideMobileController,
                            focusNode: brideMobileFocus,
                            onSubmitted: (term) {
                              CommonMethods.inputFocusChange(
                                  context, brideMobileFocus, groomMobileFocus);
                            },
                            validator: (text) {
                              return text.isEmpty
                                  ? 'Bride phone number cannot be empty'
                                  : text.length < 8
                                  ? 'Bride phone number cannot be less than 8'
                                  : null;
                            },
                            hint: "Bride's Phone Number",
                            maxLength: 15,
                            inputType: TextInputType.phone,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 30, top: 25, right: 30),
                          child: InputField(
                            controller: groomMobileController,
                            focusNode: groomMobileFocus,
                            onSubmitted: (term) {
                              CommonMethods.inputFocusChange(context,
                                  groomMobileFocus, brideIdentityFocus);
                            },
                            maxLength: 15,
                            validator: (text) {
                              return text.isEmpty
                                  ? 'Groom mobile cannot be empty'
                                  : text.length < 8
                                  ? 'Groom mobile number cannot be less than 8'
                                  : null;
                            },
                            hint: "Groom's Mobile Number",
                            inputType: TextInputType.phone,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 30, top: 25, right: 30),
                          child: InputField(
                            controller: brideIdentityController,
                            focusNode: brideIdentityFocus,
                            onSubmitted: (term) {
                              CommonMethods.inputFocusChange(context,
                                  brideIdentityFocus, brideResidencePlaceFocus);
                            },
                            validator: (text) {
                              return text.isEmpty
                                  ? 'Bride identity cannot be empty'
                                  : null;
                            },
                            hint: "Bride's Identity",
                            inputType: TextInputType.name,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 30, top: 25, right: 30),
                          child: InputField(
                            controller: brideResidencePlaceController,
                            focusNode: brideResidencePlaceFocus,
                            onSubmitted: (term) {
                              CommonMethods.inputFocusChange(
                                  context,
                                  brideResidencePlaceFocus,
                                  brideWorkPlaceFocus);
                            },
                            validator: (text) {
                              return text.isEmpty
                                  ? 'Bride residence place cannot be empty'
                                  : null;
                            },
                            hint: "Bride's Residence Place",
                            inputType: TextInputType.streetAddress,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 30, top: 25, right: 30),
                          child: InputField(
                            controller: brideWorkPlaceController,
                            focusNode: brideWorkPlaceFocus,
                            onSubmitted: (term) {
                              CommonMethods.inputFocusChange(
                                  context,
                                  brideWorkPlaceFocus,
                                  brideGuardianMobileFocus);
                            },
                            validator: (text) {
                              return text.isEmpty
                                  ? 'Bride work place cannot be empty'
                                  : null;
                            },
                            hint: "Bride WorkPlace",
                            inputType: TextInputType.streetAddress,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 30, top: 25, right: 30),
                          child: InputField(
                            controller: brideGuardianMobileController,
                            focusNode: brideGuardianMobileFocus,
                            onSubmitted: (term) {
                              CommonMethods.inputFocusChange(
                                  context,
                                  brideGuardianMobileFocus,
                                  brideGuardianNameFocus);
                            },
                            validator: (text) {
                              return text.isEmpty
                                  ? 'Bride guardian phone cannot be empty'
                                  : null;
                            },
                            hint: "Bride's Guardian Phone Number",
                            maxLength: 10,
                            inputType: TextInputType.phone,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 30, top: 25, right: 30),
                          child: InputField(
                            controller: brideGuardianNameController,
                            focusNode: brideGuardianNameFocus,
                            onSubmitted: (term) {
                              CommonMethods.inputFocusChange(
                                  context, brideGuardianNameFocus, emailFocus);
                            },
                            validator: (text) {
                              return text.isEmpty
                                  ? 'Bride guardian name cannot be empty'
                                  : null;
                            },
                            hint: "Bride's Guardian",
                            inputType: TextInputType.name,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 30, top: 25, right: 30),
                          child: InputField(
                            controller: emailController,
                            focusNode: emailFocus,
                            onSubmitted: (term) {
                              CommonMethods.inputFocusChange(
                                  context, emailFocus, brideNationalityFocus);
                            },
                            validator: (text) {
                              return text.isEmpty
                                  ? 'E-mail cannot be empty'
                                  : null;
                            },
                            hint: "E-mail",
                            inputType: TextInputType.name,
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            showDatePickerDialog(context).then((date) {
                              if (date != null) {
                                questioningDate =
                                    DateFormat("dd/MM/yyyy").format(date);
                                questioningDateSend =
                                    DateFormat("yyyy-MM-dd").format(date);
                                questioningBool = true;
                                setState(() {});
                              }
                            });
                          },
                          child: Container(
                              height: 50,
                              margin:
                                  EdgeInsets.only(left: 30, top: 25, right: 30),
                              padding: EdgeInsets.only(left: 20, right: 10),
                              width: MediaQuery.of(context).size.width,
                              alignment: Alignment.centerLeft,
                              decoration: new BoxDecoration(
                                shape: BoxShape.rectangle,
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(25),
                                border:
                                    Border.all(color: Colors.white, width: 1),
                              ),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Label(
                                    title: questioningDate,
                                    fontSize: 16,
                                    color: Colors.black,
                                  ),
                                  Icon(
                                    Icons.date_range_rounded,
                                    color: Colors.black,
                                  ),
                                ],
                              )),
                        ),
                        weddingInterrogationList != null &&
                                weddingInterrogationList.length > 0
                            ? Container(
                                alignment: Alignment.center,
                                height: 50,
                                padding: EdgeInsets.only(left: 20, right: 10),
                                margin: EdgeInsets.only(
                                    left: 30, right: 30, top: 20),
                                decoration: new BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(25),
                                  border:
                                      Border.all(color: Colors.white, width: 1),
                                ),
                                child: DropdownButton<WeddingContractData>(
                                    isExpanded: true,
                                    hint: Row(
                                      children: <Widget>[
                                        Label(
                                          title: _selectedLocation ?? "",
                                          color: Colors.black,
                                          fontSize: 16,
                                          textAlign: TextAlign.center,
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: 5),
                                          child: Icon(
                                            Icons.keyboard_arrow_down_sharp,
                                            color: Colors.white,
                                            size: 20,
                                          ),
                                        ),
                                      ],
                                    ),
                                    underline: Container(),
                                    items: weddingInterrogationList
                                        .map((WeddingContractData val) {
                                      return new DropdownMenuItem<
                                          WeddingContractData>(
                                        value: val,
                                        child: Label(
                                          title: val.title,
                                          fontSize: 16,
                                          color: Colors.black,
                                        ),
                                      );
                                    }).toList(),
                                    onChanged: (newVal) {
                                      _selectedLocation = newVal.title;
                                      interrogationPlaceId =
                                          newVal.id.toString();
                                      setState(() {});
                                    }),
                              )
                            : Container(),
                        GestureDetector(
                          onTap: () {
                            showDatePickerDialog(context).then((date) {
                              if (date != null) {
                                weddingDate =
                                    DateFormat("dd/MM/yyyy").format(date);
                                weddingDateSend =
                                    DateFormat("yyyy-MM-dd").format(date);
                                weddingDateBool = true;
                                setState(() {});
                              }
                            });
                          },
                          child: Container(
                              height: 50,
                              margin:
                                  EdgeInsets.only(left: 30, top: 25, right: 30),
                              padding: EdgeInsets.only(left: 20, right: 10),
                              width: MediaQuery.of(context).size.width,
                              alignment: Alignment.centerLeft,
                              decoration: new BoxDecoration(
                                shape: BoxShape.rectangle,
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(25),
                                border:
                                    Border.all(color: Colors.white, width: 1),
                              ),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Label(
                                    title: weddingDate,
                                    fontSize: 16,
                                    color: Colors.black,
                                  ),
                                  Icon(
                                    Icons.date_range_rounded,
                                    color: Colors.black,
                                  ),
                                ],
                              )),
                        ),
                        GestureDetector(
                          onTap: () {
                            showDatePickerDialog(context).then((date) {
                              if (date != null) {
                                contractDate =
                                    DateFormat("dd/MM/yyyy").format(date);
                                contractDateSend =
                                    DateFormat("yyyy-MM-dd").format(date);
                                contractDateBool = true;
                                setState(() {});
                              }
                            });
                          },
                          child: Container(
                              height: 50,
                              margin:
                                  EdgeInsets.only(left: 30, top: 25, right: 30),
                              padding: EdgeInsets.only(left: 20, right: 10),
                              width: MediaQuery.of(context).size.width,
                              alignment: Alignment.centerLeft,
                              decoration: new BoxDecoration(
                                shape: BoxShape.rectangle,
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(25),
                                border:
                                    Border.all(color: Colors.white, width: 1),
                              ),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Label(
                                    title: contractDate,
                                    fontSize: 16,
                                    color: Colors.black,
                                  ),
                                  Icon(
                                    Icons.date_range_rounded,
                                    color: Colors.black,
                                  ),
                                ],
                              )),
                        ),
                        weddingContractList != null &&
                                weddingContractList.length > 0
                            ? Container(
                                alignment: Alignment.center,
                                height: 50,
                                padding: EdgeInsets.only(left: 20, right: 10),
                                margin: EdgeInsets.only(
                                    left: 30, right: 30, top: 20),
                                decoration: new BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(25),
                                  border:
                                      Border.all(color: Colors.white, width: 1),
                                ),
                                child: DropdownButton<WeddingContractData>(
                                    isExpanded: true,
                                    hint: Row(
                                      children: <Widget>[
                                        Label(
                                          title: contractPlace ?? "",
                                          color: Colors.black,
                                          fontSize: 16,
                                          textAlign: TextAlign.center,
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: 5),
                                          child: Icon(
                                            Icons.keyboard_arrow_down_sharp,
                                            color: Colors.white,
                                            size: 20,
                                          ),
                                        ),
                                      ],
                                    ),
                                    underline: Container(),
                                    items: weddingContractList
                                        .map((WeddingContractData val) {
                                      return new DropdownMenuItem<
                                          WeddingContractData>(
                                        value: val,
                                        child: Label(
                                          title: val.title,
                                          fontSize: 16,
                                          color: Colors.black,
                                        ),
                                      );
                                    }).toList(),
                                    onChanged: (newVal) {
                                      contractPlace = newVal.title;
                                      contractPlaceId = newVal.id.toString();
                                      setState(() {});
                                    }),
                              )
                            : Container(),
                        !showEnterWeddingBool? (weddingPlaceModelList != null &&
                                weddingPlaceModelList.length > 0
                            ? Container(
                                alignment: Alignment.center,
                                height: 50,
                                padding: EdgeInsets.only(left: 20, right: 10),
                                margin: EdgeInsets.only(
                                    left: 30, right: 30, top: 20),
                                decoration: new BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(25),
                                  border:
                                      Border.all(color: Colors.white, width: 1),
                                ),
                                child: DropdownButton<WeddingContractData>(
                                    isExpanded: true,
                                    hint: Row(
                                      children: <Widget>[
                                        Label(
                                          title: weddingPlace ?? "",
                                          color: Colors.black,
                                          fontSize: 16,
                                          textAlign: TextAlign.center,
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: 5),
                                          child: Icon(
                                            Icons.keyboard_arrow_down_sharp,
                                            color: Colors.white,
                                            size: 20,
                                          ),
                                        ),
                                      ],
                                    ),
                                    underline: Container(),
                                    items: weddingPlaceModelList
                                        .map((WeddingContractData val) {
                                      return new DropdownMenuItem<
                                          WeddingContractData>(
                                        value: val,
                                        child: Label(
                                          title: val.title,
                                          fontSize: 16,
                                          color: Colors.black,
                                        ),
                                      );
                                    }).toList(),
                                    onChanged: (newVal) {
                                      weddingPlace = newVal.title;
                                      weddingPlaceId = newVal.id.toString();
                                      if(newVal.title=="Others"){
                                        showEnterWeddingBool=true;
                                      }
                                      setState(() {});
                                    }),
                              )
                            : Container()):(
                            Container(
                              height: 50,
                              margin: EdgeInsets.only(left: 30, top: 25, right: 30),
                              child: TextFormField(
                                controller: weddingPlaceController,
                                focusNode: weddingPlaceFocus,
                                obscureText: obscureText,
                                validator: (text) {
                                  return text.isEmpty
                                      ? 'Wedding Place cannot be empty'
                                      : null;
                                },
                                keyboardType: TextInputType.name,
                                decoration: InputDecoration(
                                  filled: true,
                                  counterText: "",
                                  fillColor: Colors.white,
                                  hintText: "Wedding Place",
                                  hintStyle: TextStyle(
                                    color: Colors.black
                                  ),
                                  contentPadding:
                                  const EdgeInsets.only( bottom: 15.0, top: 15.0,left: 20),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.white),
                                    borderRadius: BorderRadius.circular(25.7),
                                  ),
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(color: Colors.white),
                                    borderRadius: BorderRadius.circular(25.7),
                                  ),
                                  focusedErrorBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(25.0),
                                      borderSide: BorderSide(
                                        color: Colors.red,
                                      )),
                                  errorBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(25.0),
                                      borderSide: BorderSide(
                                        color: Colors.red,
                                      )),
                                  floatingLabelBehavior: FloatingLabelBehavior.auto,
                                  suffixIcon: Container(
                                    margin: EdgeInsets.only(right: 10),
                                    child: IconButton(
                                      onPressed: () {
                                        showEnterWeddingBool=false;
                                        setState(() {
                                        });
                                      },
                                      icon: Icon(
                                           Icons.location_on,
                                          size: 25,
                                          color: Colors.black),
                                    ),
                                  ),
                                ),
                                onFieldSubmitted: (term) {
                                  FocusScope.of(context).unfocus();
                                },
                              ),
                            )),
                        nationalityModelList != null &&
                                nationalityModelList.length > 0
                            ? Container(
                                alignment: Alignment.center,
                                height: 50,
                                padding: EdgeInsets.only(left: 20, right: 10),
                                margin: EdgeInsets.only(
                                    left: 30, right: 30, top: 20),
                                decoration: new BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(25),
                                  border:
                                      Border.all(color: Colors.white, width: 1),
                                ),
                                child: DropdownButton<WeddingContractData>(
                                    isExpanded: true,
                                    hint: Row(
                                      children: <Widget>[
                                        Label(
                                          title: brideNationality ?? "",
                                          color: Colors.black,
                                          fontSize: 16,
                                          textAlign: TextAlign.center,
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: 5),
                                          child: Icon(
                                            Icons.keyboard_arrow_down_sharp,
                                            color: Colors.white,
                                            size: 20,
                                          ),
                                        ),
                                      ],
                                    ),
                                    underline: Container(),
                                    items: nationalityModelList
                                        .map((WeddingContractData val) {
                                      return new DropdownMenuItem<
                                          WeddingContractData>(
                                        value: val,
                                        child: Label(
                                          title: val.title,
                                          fontSize: 16,
                                          color: Colors.black,
                                        ),
                                      );
                                    }).toList(),
                                    onChanged: (newVal) {
                                      brideNationality = newVal.title;
                                      nationalityId = newVal.id.toString();
                                      setState(() {});
                                    }),
                              )
                            : Container(),
                        GestureDetector(
                          onTap: () {
                            _selectTime(context).then((time) {
                              if (time != null) {
                                weddingTime = time;
                                weddingTimeBool = true;
                                setState(() {});
                              }
                            });
                          },
                          child: Container(
                              height: 50,
                              margin:
                                  EdgeInsets.only(left: 30, top: 25, right: 30),
                              padding: EdgeInsets.only(left: 20, right: 10),
                              width: MediaQuery.of(context).size.width,
                              alignment: Alignment.centerLeft,
                              decoration: new BoxDecoration(
                                shape: BoxShape.rectangle,
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(25),
                                border:
                                    Border.all(color: Colors.white, width: 1),
                              ),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Label(
                                    title: weddingTime,
                                    fontSize: 16,
                                    color: Colors.black,
                                  ),
                                  Icon(
                                    Icons.alarm,
                                    color: Colors.black,
                                  ),
                                ],
                              )),
                        ),
                        GestureDetector(
                          onTap: () {
                            _selectTime(context).then((time) {
                              if (time != null) {
                                aqidTime = time;
                                aqidTimeBool = true;
                                setState(() {});
                              }
                            });
                          },
                          child: Container(
                              height: 50,
                              margin:
                                  EdgeInsets.only(left: 30, top: 25, right: 30),
                              padding: EdgeInsets.only(left: 20, right: 10),
                              width: MediaQuery.of(context).size.width,
                              alignment: Alignment.centerLeft,
                              decoration: new BoxDecoration(
                                shape: BoxShape.rectangle,
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(25),
                                border:
                                    Border.all(color: Colors.white, width: 1),
                              ),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Label(
                                    title: aqidTime,
                                    fontSize: 16,
                                    color: Colors.black,
                                  ),
                                  Icon(
                                    Icons.alarm,
                                    color: Colors.black,
                                  ),
                                ],
                              )),
                        ),
                        Container(
                          margin:
                              EdgeInsets.only(top: 30, bottom: 10, left: 30),
                          child: Label(
                            title: "With Dinner",
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            color: Colors.white,
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Expanded(
                              child: Container(
                                width: 150,
                                height: 40,
                                margin: EdgeInsets.only(
                                    left: 30, top: 15, right: 30),
                                child: Button(
                                  title: "Yes",
                                  color: !dinnerBool
                                      ? Colors.white
                                      : Colors.transparent,
                                  textColor: !dinnerBool
                                      ? Color(appColors.blue00008B)
                                      : Colors.white,
                                  onTap: () async {
                                    setState(() {
                                      dinnerBool = !dinnerBool;
                                    });
                                  },
                                ),
                              ),
                            ),
                            Expanded(
                              child: Container(
                                width: 150,
                                height: 40,
                                margin: EdgeInsets.only(top: 15, right: 35),
                                child: Button(
                                  title: "No",
                                  color: dinnerBool
                                      ? Colors.white
                                      : Colors.transparent,
                                  textColor: dinnerBool
                                      ? Color(appColors.blue00008B)
                                      : Colors.white,
                                  onTap: () async {
                                    setState(() {
                                      dinnerBool = !dinnerBool;
                                    });
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                        Container(
                          margin:
                              EdgeInsets.only(top: 30, bottom: 10, left: 30),
                          child: Label(
                            title: "Would you like to serve the committee",
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            color: Colors.white,
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Expanded(
                              child: Container(
                                width: 150,
                                height: 40,
                                margin: EdgeInsets.only(
                                    left: 30, top: 15, right: 30),
                                child: Button(
                                  title: "Yes",
                                  color: !serveCommitteeBool
                                      ? Colors.white
                                      : Colors.transparent,
                                  textColor: !serveCommitteeBool
                                      ? Color(appColors.blue00008B)
                                      : Colors.white,
                                  onTap: () async {
                                    setState(() {
                                      serveCommitteeBool = !serveCommitteeBool;
                                    });
                                  },
                                ),
                              ),
                            ),
                            Expanded(
                              child: Container(
                                width: 150,
                                height: 40,
                                margin: EdgeInsets.only(top: 15, right: 35),
                                child: Button(
                                  title: "No",
                                  color: serveCommitteeBool
                                      ? Colors.white
                                      : Colors.transparent,
                                  textColor: serveCommitteeBool
                                      ? Color(appColors.blue00008B)
                                      : Colors.white,
                                  onTap: () async {
                                    setState(() {
                                      serveCommitteeBool = !serveCommitteeBool;
                                    });
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                        Container(
                          margin:
                              EdgeInsets.only(top: 30, bottom: 10, left: 30),
                          child: Label(
                            title: "Attach a Document 1",
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            color: Colors.white,
                          ),
                        ),
                        Container(
                          height: 50,
                          margin: EdgeInsets.only(left: 30, right: 30, top: 10),
                          decoration: new BoxDecoration(
                            shape: BoxShape.rectangle,
                            color: Colors.white.withOpacity(0.4),
                            borderRadius: BorderRadius.circular(25),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 20),
                                child: docOne != null
                                    ? Image.file(
                                        File(docOne.path),
                                        height: 100,
                                        width: 100,
                                        fit: BoxFit.fill,
                                      )
                                    : Label(
                                        title: "No file chosen",
                                        fontSize: 15,
                                        fontWeight: FontWeight.w600,
                                        color: Colors.white,
                                      ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  imageChooser
                                      .showImageChooser(context)
                                      .then((picture) {
                                    if (picture != null) {
                                      this.docOne = picture;
                                      setState(() {});
                                      print(picture);
                                    }
                                  });
                                },
                                child: Container(
                                  width: 150,
                                  alignment: Alignment.center,
                                  margin: EdgeInsets.only(
                                      right: 10, top: 5, bottom: 5),
                                  padding: EdgeInsets.all(8),
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(25),
                                  ),
                                  child: Label(
                                    title: "Choose File",
                                    fontSize: 14,
                                    fontWeight: FontWeight.w600,
                                    color: Color(appColors.buttonColor),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin:
                              EdgeInsets.only(top: 30, bottom: 10, left: 30),
                          child: Label(
                            title: "Attach a Document 2",
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            color: Colors.white,
                          ),
                        ),
                        Container(
                          height: 50,
                          margin: EdgeInsets.only(left: 30, right: 30, top: 10),
                          decoration: new BoxDecoration(
                            shape: BoxShape.rectangle,
                            color: Colors.white.withOpacity(0.4),
                            borderRadius: BorderRadius.circular(25),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 20),
                                child: docTwo != null
                                    ? Image.file(
                                  File(docTwo.path),
                                        height: 100,
                                        width: 100,
                                        fit: BoxFit.fill,
                                      )
                                    : Label(
                                        title: "No file chosen",
                                        fontSize: 15,
                                        fontWeight: FontWeight.w600,
                                        color: Colors.white,
                                      ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  imageChooser
                                      .showImageChooser(context)
                                      .then((picture) {
                                    if (picture != null) {
                                      this.docTwo = picture;
                                      setState(() {});
                                      print(picture);
                                    }
                                  });
                                },
                                child: Container(
                                  width: 150,
                                  alignment: Alignment.center,
                                  margin: EdgeInsets.only(
                                      right: 10, top: 5, bottom: 5),
                                  padding: EdgeInsets.all(8),
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(25),
                                  ),
                                  child: Label(
                                    title: "Choose File",
                                    fontSize: 14,
                                    fontWeight: FontWeight.w600,
                                    color: Color(appColors.buttonColor),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin:
                              EdgeInsets.only(top: 30, bottom: 10, left: 30),
                          child: Label(
                            title: "Attach a Document 3",
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            color: Colors.white,
                          ),
                        ),
                        Container(
                          height: 50,
                          margin: EdgeInsets.only(left: 30, right: 30, top: 10),
                          decoration: new BoxDecoration(
                            shape: BoxShape.rectangle,
                            color: Colors.white.withOpacity(0.4),
                            borderRadius: BorderRadius.circular(25),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 20),
                                child: docThree != null
                                    ? Image.file(
                                  File(docThree.path),
                                        height: 100,
                                        width: 100,
                                        fit: BoxFit.fill,
                                      )
                                    : Label(
                                        title: "No file chosen",
                                        fontSize: 15,
                                        fontWeight: FontWeight.w600,
                                        color: Colors.white,
                                      ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  imageChooser
                                      .showImageChooser(context)
                                      .then((picture) {
                                    if (picture != null) {
                                      this.docThree = picture;
                                      setState(() {});
                                      print(picture);
                                    }
                                  });
                                },
                                child: Container(
                                  width: 150,
                                  alignment: Alignment.center,
                                  margin: EdgeInsets.only(
                                      right: 10, top: 5, bottom: 5),
                                  padding: EdgeInsets.all(8),
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(25),
                                  ),
                                  child: Label(
                                    title: "Choose File",
                                    fontSize: 14,
                                    fontWeight: FontWeight.w600,
                                    color: Color(appColors.buttonColor),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          height: 60,
                          margin: EdgeInsets.only(
                              bottom: 30, left: 30, top: 35, right: 30),
                          child: Button(
                              title: "Check",
                              color: Color(appColors.buttonColor),
                              borderColor: Colors.transparent,
                              textColor: Colors.white,
                              onTap: () {
                                if (_formKey.currentState.validate()) {
                                  weddingBookingBottomSheet();
                                }
                              }),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<DateTime> showDatePickerDialog(_context) {
    Future<DateTime> selectedDate = showDatePicker(
            context: context,
            firstDate: DateTime.now(),
            initialDate: _dateTimeNotifier.value != null
                ? _dateTimeNotifier.value
                : DateTime.now(),
            lastDate: DateTime(2100))
        .then((DateTime dateTime) => _dateTimeNotifier.value = dateTime);
    return selectedDate;
  }

  Future<String> _selectTime(BuildContext context) async {
    var selectedTimeValue = "";
    final TimeOfDay picked_s = await showTimePicker(
        context: context,
        initialTime: selectedTime,
        builder: (BuildContext context, Widget child) {
          return MediaQuery(
            data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: false),
            child: child,
          );
        });

    setState(() {
      if (selectedTime != null) selectedTime = picked_s;
      selectedTimeValue =
          picked_s.hour.toString() + ":" + picked_s.minute.toString();
    });
    return selectedTimeValue;
  }

  Future<Widget> weddingBookingBottomSheet() {
    return showModalBottomSheet<Widget>(
      isScrollControlled: true,
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30.0), topRight: Radius.circular(30.0)),
      ),
      builder: (BuildContext context) {
        return Container(
            height: MediaQuery.of(context).size.height / 2,
            color: Colors.white,
            margin: EdgeInsets.only(top: 40),
            child: bookingSummary());
      },
    );
  }

  Widget bookingSummary() {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            child: Label(
              title: "Wedding Booking Summary",
              textAlign: TextAlign.center,
              fontWeight: FontWeight.w700,
              fontSize: 20,
              color: Color(appColors.buttonColor),
            ),
          ),
          weddingSummary(),
        ],
      ),
    );
  }

  Widget weddingSummary() {
    return Container(
      margin: EdgeInsets.only(left: 25, right: 25, top: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 40),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Label(
                    title: "Name:",
                    textAlign: TextAlign.center,
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                    color: Colors.black,
                  ),
                  SizedBox(height: 10),
                  Label(
                    title: "Date:",
                    textAlign: TextAlign.center,
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                    color: Colors.black,
                  ),
                  SizedBox(height: 10),
                  Label(
                    title: "Time:",
                    textAlign: TextAlign.center,
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                    color: Colors.black,
                  ),
                  SizedBox(height: 10),
                  Label(
                    title: "Amount:",
                    textAlign: TextAlign.center,
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                    color: Colors.black,
                  ),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Label(
                    title: weddingTitleController.text.toString().trim(),
                    textAlign: TextAlign.center,
                    fontSize: 18,
                    color: Colors.black,
                  ),
                  SizedBox(height: 10),
                  Label(
                    title: weddingDate,
                    textAlign: TextAlign.center,
                    fontSize: 18,
                    color: Colors.black,
                  ),
                  SizedBox(height: 10),
                  Label(
                    title: weddingTime,
                    textAlign: TextAlign.center,
                    fontSize: 18,
                    color: Colors.black,
                  ),
                  SizedBox(height: 10),
                  Label(
                    title: "OMR1",
                    textAlign: TextAlign.center,
                    fontSize: 18,
                    color: Colors.black,
                  ),
                ],
              ),
            ],
          ),
          Container(
            height: 60,
            margin: EdgeInsets.only(top: 35, bottom: 40),
            child: Button(
              title: "Confirm",
              color: Color(appColors.buttonColor),
              borderColor: Colors.transparent,
              textColor: Colors.white,
              onTap: () {
                weddingBookingApi();
              },
            ),
          ),
        ],
      ),
    );
  }

  void showSuccessDialog(BuildContext context) {
    showGeneralDialog(
      barrierLabel: "Barrier",
      barrierDismissible: true,
      barrierColor: Colors.black.withOpacity(0.8),
      transitionDuration: Duration(milliseconds: 500),
      context: context,
      pageBuilder: (_, __, ___) {
        return Align(
          alignment: Alignment.center,
          child: Container(
              height: 450,
              margin: EdgeInsets.only(left: 10, right: 10),
              decoration: new BoxDecoration(
                color: const Color(0xFFFFFF),
                borderRadius: new BorderRadius.all(new Radius.circular(32.0)),
              ),
              child: WeddingSuccessStatus()),
        );
      },
      transitionBuilder: (_, anim, __, child) {
        return SlideTransition(
          position: Tween(begin: Offset(0, 1), end: Offset(0, 0)).animate(anim),
          child: child,
        );
      },
    );
  }

  @override
  Future<void> weddingBookingApi() async {
    String base64ImageOne = "",base64ImageTwo = "",base64ImageThree = "";

    if(docOne != null){
      base64ImageOne= base64Method(docOne.path);
    }
    if(docTwo != null){
      base64ImageTwo= base64Method(docTwo.path);
    }
    if(docThree != null){
      base64ImageThree= base64Method(docThree.path);
    }

    final documentOne = docOne != null ? docOne.path : "";
    final documentTwo = docTwo != null ? docTwo.path : "";
    final documentThree = docThree != null ? docThree.path : "";
    final _token = await appPreferences.getStringPreference("AccessToken");

    final _params = <String, String>{
      'bride_nationality': nationalityId,
      'wedding_title': weddingTitleController.text.toString().trim(),
      'bride_name': brideNameController.text.toString().trim(),
      'bride_address': brideAddressController.text.toString().trim(),
      'bride_phone_number': brideMobileController.text.toString().trim(),
      'email': emailController.text.toString().trim(),
      'groom_phone_number': groomMobileController.text.toString().trim(),
      'bride_place_of_residence': brideResidencePlaceController.text.toString().trim(),
      'bride_work_place': brideWorkPlaceController.text.toString().trim(),
      'bride_guardian_phone_number': brideGuardianMobileController.text.toString().trim(),
      'bride_guardian_name': brideGuardianNameController.text.toString().trim(),
      'groom_name': groomNameController.text.toString().trim(),
      'wedding_date': weddingDateSend,
      'questioning_date': questioningDateSend,
      'contract_date': contractDateSend,
      'bride_identity': brideIdentityController.text.toString().trim(),
      'wedding_time': weddingTime,
      'aqid_time': aqidTime,
      'wedding_place': weddingPlaceId?? weddingPlaceController.text.toString().trim(),
      'wedding_type': !dinnerBool ? "1" : "2",
      'interogation_place_id': interrogationPlaceId,
      'contract_place_id': contractPlaceId,
      "document1": base64ImageOne,
      "document2": base64ImageTwo,
      "document3": base64ImageThree
    };

    if (context != null) {
      _showLoader(context);
    }

    var response = await http.post(Uri.parse(WEDDING_BOOKING_CREATE),
        headers: {
          'Content-type': 'application/json',
          AUTHORIZATION: BEARER + " " + _token
        },
        body: json.encode(_params));

    print("Booking Params====${_params.toString()}");

    if (response.statusCode == 200) {
      if (mContextLoader != null) Navigator.pop(mContextLoader);
      final _responseJson = json.decode(response.body);

      print("wedding booking _responseJson====${_responseJson}");
      print("wedding booking ID====${ _responseJson['data']['id']}");

      appPreferences.saveStringPreference("bookingId",_responseJson['data']['id'].toString());

      if (_responseJson['status']) {
        // showSnackBarMessage(context: context, message: _responseJson['message']);
        Navigator.pop(context);
        CommonMethods().animatedNavigation(
            context: context,
            currentScreen:
            WeddingBooking(),
            landingScreen: PaymentWebView(
              type: "wedding-booking",
              id:_responseJson['data']['id'].toString(),
              activity: "",
            ));
        // showSuccessDialog(context);
      } else {
        Navigator.pop(context);
        CommonMethods.showToast(
            fToast: fToast,
            message:  _responseJson['message'],
            status: false);
      }
    } else {
      if (mContextLoader != null) Navigator.pop(mContextLoader);
      handleError(response);
    }
  }

  Future<Null> _showLoader(BuildContext context) async {
    return showDialog<Null>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context1) {
        mContextLoader = context1;
        return Scaffold(
          backgroundColor: Colors.transparent.withOpacity(0.3),
          key: _keyLoader,
          body: Container(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    child: CircularProgressIndicator(),
                    height: 40.0,
                    width: 40.0,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Future<void> apiContractCall() async {

    final _token = await appPreferences.getStringPreference("AccessToken");

    if (context != null) {
      _showLoader(context);
    }

    var response = await http.get(Uri.parse(WEDDING_BOOKING_CONTRACT),
        headers: {
          'Content-type': 'application/json',
          AUTHORIZATION: BEARER + " " + _token
        });

    if (response.statusCode == 200) {
      final Map<String, dynamic> _responseJson = json.decode(response.body);
      if (mContextLoader != null) Navigator.pop(mContextLoader);

      print(
          'ContractPLaceResponse ${response.statusCode} and ${_responseJson.toString()}');
      weddingContractPlaceModel =
          WeddingContractPlaceModel.fromJson(_responseJson);
      weddingContractList = weddingContractPlaceModel.contractDataList;
      setState(() {});

      Future.delayed(Duration(milliseconds: 500), () {
        apiInterrogationCall();
      });
    }
  }

  Future<void> apiInterrogationCall() async {
    final _token = await appPreferences.getStringPreference("AccessToken");

    var response = await http.get(Uri.parse(WEDDING_BOOKING_INTERROGATION),
        headers: {
          'Content-type': 'application/json',
          AUTHORIZATION: BEARER + " " + _token
        });

    if (response.statusCode == 200) {
      final Map<String, dynamic> _responseJson = json.decode(response.body);

      print(
          'InterrogationPLaceResponse ${response.statusCode} and ${_responseJson.toString()}');
      weddingInterrogationPlaceModel =
          WeddingContractPlaceModel.fromJson(_responseJson);
      weddingInterrogationList =
          weddingInterrogationPlaceModel.contractDataList;
      setState(() {});

      Future.delayed(Duration(milliseconds: 500), () {
        apiNationalityCall();
      });
    }
  }

  Future<void> apiNationalityCall() async {
    final _token = await appPreferences.getStringPreference("AccessToken");
    var response = await http.get(Uri.parse(NATIONALITY_ENDPOINT), headers: {
      'Content-type': 'application/json',
      AUTHORIZATION: BEARER + " " + _token
    });

    if (response.statusCode == 200) {
      final Map<String, dynamic> _responseJson = json.decode(response.body);
      print('NationalityResponse ${response.statusCode} and ${_responseJson.toString()}');
      nationalityModel = WeddingContractPlaceModel.fromJson(_responseJson);
      nationalityModelList = nationalityModel.contractDataList;
      nationalityId=nationalityModelList[0].id.toString();

      setState(() {});
    }
  }

  Future<void> apiWeddingPlaceCall() async {
    final _token = await appPreferences.getStringPreference("AccessToken");
    var response = await http.get(Uri.parse(WEDDING_BOOKING_PLACES), headers: {
      'Content-type': 'application/json',
      AUTHORIZATION: BEARER + " " + _token
    });

    if (response.statusCode == 200) {
      final Map<String, dynamic> _responseJson = json.decode(response.body);
      print(
          'WEDDING_BOOKING_PLACES ${response.statusCode} and ${_responseJson.toString()}');
      weddingPlaceModel = WeddingContractPlaceModel.fromJson(_responseJson);
      weddingPlaceModelList = weddingPlaceModel.contractDataList;
      weddingPlaceModelList.add(WeddingContractData(id:99,title:"Others",created_at:DateTime.now().toString()));
      setState(() {});
    }
  }

  String base64Method(String path) {
    final bytes = File(path).readAsBytesSync();
    var base64ImageOne = base64Encode(bytes);
    print(bytes);
    return base64ImageOne;
  }

}
