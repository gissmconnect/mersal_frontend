import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:mersal/core/network/api_endpoints.dart';
import 'package:mersal/core/network/http_call.dart';
import 'package:mersal/core/network/http_error_handler.dart';
import 'package:mersal/features/donation_history/donation-history-data/datasource/donation_history_source.dart';
import 'package:mersal/features/donation_history/donation-history-data/model/donation_history_model.dart';
import 'package:meta/meta.dart';

class DonationHistoryDataSourceImpl implements DonationHistoryDataSource {
  final http.Client httpClient;

  DonationHistoryDataSourceImpl({@required this.httpClient});

  @override
  Future<DonationHistoryModel> getDonationHistory({String token}) async {
    final _response = await httpGetRequest(
        httpClient: httpClient, url: GET_DONATION, token: token);
    if (_response.statusCode == 200) {
      final _donationResponse = json.decode(_response.body);
      print('Donation History response ${_donationResponse}');
      return DonationHistoryModel.fromJson(_donationResponse);
    } else {
      handleError(_response);
    }
  }
}
