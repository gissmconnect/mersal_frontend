import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_countdown_timer/index.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mersal/core/widgets/common/Button.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/CustomAppBar.dart';
import 'package:mersal/core/widgets/common/InputField.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/core/widgets/message.dart';
import 'package:mersal/features/auth/auth_presentation/otp_presentation/bloc/otp_bloc.dart';
import 'package:mersal/features/auth/auth_presentation/doc_upload_presentation/ui/doc_upload_page.dart';
import 'package:mersal/injection_container.dart';
import 'package:mersal/resources/colors.dart';

class OtpScreen extends StatelessWidget {
  final String phoneNumber;

  const OtpScreen({Key key, @required this.phoneNumber}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<OtpBloc>(
      create: (_) => serviceLocator<OtpBloc>(),
      child: Scaffold(
          appBar: CustomAppBar(title: "Otp", isBackButton: true, showText: true),
          body: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/bg_image.jpg"),
                fit: BoxFit.cover,
              ),
            ),
            child: _OtpScreenBody(
              phoneNumber: phoneNumber,
            ),
          )),
    );
  }
}

class _OtpScreenBody extends StatefulWidget {
  final String phoneNumber;

  const _OtpScreenBody({Key key, @required this.phoneNumber}) : super(key: key);

  @override
  _OtpScreenBodyState createState() => _OtpScreenBodyState();
}

class _OtpScreenBodyState extends State<_OtpScreenBody> {
  final otpController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  CountdownTimerController controller;

  int get endTime {
    return DateTime.now().millisecondsSinceEpoch + 1000 * 10;
  }

  FToast fToast;


  @override
  void initState() {
    super.initState();
    controller = CountdownTimerController(
      endTime: endTime,
    );
    fToast = FToast();
    fToast.init(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.only(left: 30, top: 25, right: 30),
              child: InputField(
                controller: otpController,
                onSubmitted: (term) {
                  FocusScope.of(context).unfocus();
                },
                validator: (text) {
                  return text.isEmpty ? 'Enter OTP' : null;
                },
                hint: "Enter OTP",
                inputType: TextInputType.phone,
                maxLength: 6,
              ),
            ),
            BlocListener<OtpBloc, OtpState>(
              listener: (context, state) {
                if (state is OtpError) {
                  CommonMethods.showToast(fToast:fToast,message: state.failure.message,status:false);
                } else if (state is OtpRequestSuccessState) {
                  CommonMethods.showToast(fToast:fToast,message: state.message,status:true);
                  _resetTime();
                }
              },
              child: BlocBuilder<OtpBloc, OtpState>(
                builder: (context, state) {
                  if (state is OtpRequesting) {
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CircularProgressIndicator(),
                      ],
                    );
                  } else {
                    return CountdownTimer(
                      controller: controller,
                      widgetBuilder: (_, CurrentRemainingTime time) {
                        return FlatButton(
                          onPressed: time == null
                              ? () {
                                  _resendOtp(widget.phoneNumber);
                                }
                              : null,
                          child: Label(
                            title: "Resend Otp in " +
                                " ${time != null ? time.sec.toString() : ""}",
                            fontSize: 16,
                            textAlign: TextAlign.center,
                            color: time == null ? Colors.black : Colors.black45,
                          ),
                        );
                      },
                    );
                  }
                },
              ),
            ),
            Container(
              height: 60,
              margin: EdgeInsets.only(left: 30, top: 35, right: 30),
              child: BlocListener<OtpBloc, OtpState>(
                listener: (context, state) {
                  if (state is OtpVerificationFailureState) {
                    CommonMethods.showToast(fToast:fToast,message: state.failure.message,status:false);
                  } else if (state is OtpVerificationSuccessState) {
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(
                          builder: (context) =>
                              DocUploadPage("register"),
                        ),
                        (_) => false);
                  }
                },
                child: BlocBuilder<OtpBloc, OtpState>(
                  builder: (_, state) {
                    if (state is OtpVerifyingState) {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          CircularProgressIndicator(),
                        ],
                      );
                    }
                    return Button(
                      title: "Verify",
                      textColor: Colors.white,
                      borderColor: Colors.transparent,
                      color: Color(appColors.buttonColor),
                      onTap: () {
                        if (_formKey.currentState.validate()) {
                          final _otp = otpController.text.toString().trim();
                          BlocProvider.of<OtpBloc>(context).add(
                            VerifyOtpEvent(
                                phoneNumber: widget.phoneNumber, otp: _otp),
                          );
                        }
                      },
                    );
                  },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void _resendOtp(String phoneNumber) {
    BlocProvider.of<OtpBloc>(context).add(OtpRequestEvent(phoneNumber: phoneNumber));
  }

  void _resetTime() {
    controller.disposeTimer();
    controller.dispose();
    controller = CountdownTimerController(endTime: endTime);
    controller.start();
  }

  @override
  void dispose() {
    controller.dispose();
    otpController.dispose();
    super.dispose();
  }
}
