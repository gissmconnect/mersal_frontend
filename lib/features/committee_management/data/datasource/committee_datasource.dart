import 'package:mersal/features/committee_management/data/model/committee_member_model.dart';
import 'package:mersal/features/committee_management/domain/entity/committe_entity.dart';
import 'package:meta/meta.dart';

abstract class CommitteeDataSource {
  Future<List<CommitteeEntity>> getCommittees({@required String token});

  Future<List<CommitteeMemberModel>> getCommitteeMember(
      {@required String token, @required int committeeId});
}
