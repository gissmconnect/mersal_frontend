import 'package:equatable/equatable.dart';

class CommitteeEntity extends Equatable {
  int id;
  String name, icon, description;

  CommitteeEntity({this.id, this.name,this.description, this.icon});

  @override
  List<Object> get props => [id, name,description, icon];
}
