class WeddingContractData {
  int id;
  String title;
  String created_at;

  WeddingContractData({this.id,this.title,this.created_at,});

  factory WeddingContractData.fromJson(Map<String, dynamic> json) {
    return WeddingContractData(
      id: json['id'],
      title: json['title'],
      created_at: json['created_at'],
    );
  }

}