import 'package:mersal/features/election/election-data/model/election_data_model.dart';
import 'package:mersal/features/election_candidate/election_candidate_data/model/election_candidate_model.dart';
import 'package:meta/meta.dart';

abstract class ElectionDataSource {
  Future<List<ElectionDataModel>> getElections({@required String token});

  Future<ElectionCandidateResponseModel> getCandidates(
      {@required String token, @required int electionId});

  Future<void> vote(
      {@required String token,
      @required int electionId,
      @required int candidateId});
}
