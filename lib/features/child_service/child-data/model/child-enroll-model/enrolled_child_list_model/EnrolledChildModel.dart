class EnrolledChildModel {
  int id;
  String name;
  String dob;
  int relation_type;
  String father_name;
  String grand_father_name;
  String area_of_residence;
  String family_name;
  String email;
  String photo;
  int user_id;

  EnrolledChildModel(
      {this.id,
      this.photo,
      this.email,
      this.area_of_residence,
      this.family_name,
      this.name,
      this.relation_type,
      this.user_id,
      this.father_name,
      this.grand_father_name,
      this.dob});

  factory EnrolledChildModel.fromJson(Map<String, dynamic> json) {
    return EnrolledChildModel(
      id: json['id'],
      email: json['email'],
      photo: json['photo'],
      user_id: json['user_id'],
      family_name: json['family_name'],
      area_of_residence: json['area_of_residence'],
      grand_father_name: json['grand_father_name'],
      father_name: json['father_name'],
      relation_type: json['relation_type'],
      dob: json['dob'],
      name: json['name'],
    );
  }
}
