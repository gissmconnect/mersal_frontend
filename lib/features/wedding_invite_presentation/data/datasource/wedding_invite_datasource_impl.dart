import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:mersal/core/network/api_endpoints.dart';
import 'package:mersal/core/network/http_call.dart';
import 'package:mersal/core/network/http_error_handler.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/survey/data/datasource/survey_datasource.dart';
import 'package:mersal/features/survey/data/model/survey_model.dart';
import 'package:mersal/features/wedding_invite_presentation/data/datasource/wedding_invite_datasource.dart';
import 'package:mersal/features/wedding_invite_presentation/data/model/wedding_template_model.dart';
import 'package:meta/meta.dart';

class WeddingInviteDataSourceImpl implements WeddingInviteDataSource {
  final http.Client client;

  WeddingInviteDataSourceImpl({@required this.client});

  @override
  Future<WeddingTemplateModel> getWeddingTemplates({String token}) async {
    final _response = await httpGetRequest(
        httpClient: client, url: WEDDING_TEMPLATES, token: token);
    if (_response.statusCode == 200) {
      final _weddingResponse = json.decode(_response.body);
      print('WeddingTemplate response============ ${_weddingResponse}');
      return WeddingTemplateModel.fromJson(_weddingResponse);
    } else {
      handleError(_response);
    }
  }
}
