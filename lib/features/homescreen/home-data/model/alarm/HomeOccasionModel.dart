class HomeOccasionModelData {
  int id;
  String title;
  String description;
  String occassion_date;

  HomeOccasionModelData(
      {this.occassion_date, this.description, this.id, this.title});

  factory HomeOccasionModelData.fromJson(Map<String, dynamic> json) {
    return HomeOccasionModelData(
        id: json['id'],
        title: json['title'],
        description: json['description'],
        occassion_date: json['occassion_date']);
  }
}
