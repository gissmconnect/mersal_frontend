part of 'wedding_booking_bloc.dart';

abstract class WeddingBookingEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class WeddingEvent extends WeddingBookingEvent {
  final String name, date,time;

  WeddingEvent({@required this.name, @required this.date, @required this.time});
}
