import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:mersal/core/error/exceptions.dart';
import 'package:mersal/core/network/api_endpoints.dart';
import 'package:mersal/core/network/http_error_handler.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/child_service/child-data/datasource/child_data_source.dart';
import 'package:mersal/features/child_service/child-data/model/child-enroll-model/registered-child-activity/child_activity_model.dart';
import 'package:mersal/features/child_service/child-data/model/child-enroll-model/registered-child-details/child_details_model.dart';
import 'package:mersal/features/election/election-data/datasource/election_data_source.dart';
import 'package:mersal/features/election/election-data/model/election_data_model.dart';
import 'package:mersal/features/election_candidate/election_candidate_data/model/election_candidate_model.dart';
import 'package:meta/meta.dart';
import 'dart:convert';

class ChildDataSourceImpl implements ChildDataSource {
  final http.Client httpClient;

  ChildDataSourceImpl({@required this.httpClient});

  @override
  Future<void> enrollChild({String token, int electionId, int candidateId}) {
    // TODO: implement enrollChild
    throw UnimplementedError();
  }

  @override
  Future<ChildActivityModel> getActivity({String token}) async {
    var response = await httpClient.get(
      Uri.parse(GET_ACTIVITY_LIST),
      headers: {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        AUTHORIZATION: BEARER + " " + token
      },
    );
    if (response.statusCode == 200) {
      final _childActivityResponse = json.decode(response.body);
      print('activity response ${_childActivityResponse}');
      return ChildActivityModel.fromJson(_childActivityResponse);
    }
    handleError(response);
  }

  @override
  Future<ChildDetailsModel> getChildDetails({String token}) async {
    var response = await httpClient.get(
      Uri.parse(GET_REGISTERED_CHILD),
      headers: {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        AUTHORIZATION: BEARER + " " + token
      },
    );
    if (response.statusCode == 200) {
      final _childDetailsResponse = json.decode(response.body);
      print('child details response ${_childDetailsResponse}');
      return ChildDetailsModel.fromJson(_childDetailsResponse);
    }
    handleError(response);
  }


}
