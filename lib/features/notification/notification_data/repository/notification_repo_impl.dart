import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/exceptions.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/network/network_info.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/notification/notification_data/datasource/notification_data_source.dart';
import 'package:mersal/features/notification/notification_domain/entity/notification_entity.dart';
import 'package:mersal/features/notification/notification_domain/repository/notification_repo.dart';
import 'package:meta/meta.dart';

class NotificationRepoImpl implements NotificationRepo {
  final UserDataLocalDataSource userDataLocalDataSource;
  final NetworkInfo networkInfo;
  final NotificationDataSource notificationDataSource;

  NotificationRepoImpl(
      {@required this.userDataLocalDataSource,
      @required this.networkInfo,
      @required this.notificationDataSource});

  @override
  Future<Either<Failure, List<NotificationEntity>>> getNotifications() async {
    if (await networkInfo.isConnected) {
      try {
        final _token = await userDataLocalDataSource.getAuthToken();
        final _notifications =
            await notificationDataSource.getNotifications(token: _token);
        return Right(_notifications);
      } on ServerException catch (ex) {
        return Left(ServerFailure()..message = ex.message);
      } on AuthException catch (ex) {
        return Left(AuthFailure()..message = ex.message);
      } on Exception {
        return Left(ServerFailure());
      }
    } else {
      return Left(NetworkFailure());
    }
  }

  @override
  Future<Either<Failure, String>> archiveNotification({int id}) async {
    if (await networkInfo.isConnected) {
      try {
        final _token = await userDataLocalDataSource.getAuthToken();
        final _response = await notificationDataSource.archiveNotification(
            token: _token, id: id);
        return Right(null);
      } on ServerException catch (ex) {
        return Left(ServerFailure()..message = ex.message);
      } on AuthException catch (ex) {
        return Left(AuthFailure()..message = ex.message);
      } on Exception {
        return Left(ServerFailure());
      }
    } else {
      return Left(NetworkFailure());
    }
  }

  @override
  Future<Either<Failure, String>> deleteNotification({int id}) async {
    if (await networkInfo.isConnected) {
      try {
        final _token = await userDataLocalDataSource.getAuthToken();
        final _response = await notificationDataSource.deleteNotification(
            token: _token, id: id);
        return Right(_response);
      } on ServerException catch (ex) {
        return Left(ServerFailure()..message = ex.message);
      } on AuthException catch (ex) {
        return Left(AuthFailure()..message = ex.message);
      } on Exception {
        return Left(ServerFailure());
      }
    } else {
      return Left(NetworkFailure());
    }
  }
}
