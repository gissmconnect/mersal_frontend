import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/donation/domain/repository/donation_repo.dart';
import 'package:meta/meta.dart';

class GiveDonationUseCase extends UseCase<dynamic, DonationParams> {
  final DonationRepo donationRepo;

  GiveDonationUseCase({@required this.donationRepo});

  @override
  Future<Either<Failure, dynamic>> call(DonationParams params) async {
    return await donationRepo.donation(
        reasonId: params.reasonId,
        donationAmount: params.donationAmount,
        donationDescription: params.donationDescription);
  }
}

class DonationParams {
  final int reasonId;
  final String donationAmount, donationDescription;

  DonationParams(
      {@required this.reasonId,
      @required this.donationAmount,
      @required this.donationDescription});
}
