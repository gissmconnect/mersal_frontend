import 'package:mersal/features/banner/banner-domain/entity/banner_entity.dart';
import 'package:meta/meta.dart';

abstract class HomeDataSource {
  Future<List<BannerEntity>> getBanners({@required String token});
}
