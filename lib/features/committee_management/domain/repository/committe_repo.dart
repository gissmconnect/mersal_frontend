import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/features/committee_management/domain/entity/committe_entity.dart';
import 'package:mersal/features/committee_management/domain/entity/committee_member_entity.dart';
import 'package:meta/meta.dart';

abstract class CommitteeRepo {
  Future<Either<Failure, List<CommitteeEntity>>> getCommittees();

  Future<Either<Failure, List<CommitteeMemberEntity>>> getCommitteeMember(
      {@required int committeeId});
}
