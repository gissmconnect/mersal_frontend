import 'package:mersal/features/donation_history/donation-history-data/model/donation_history_model.dart';
import 'package:meta/meta.dart';

abstract class DonationHistoryDataSource {
  Future<DonationHistoryModel> getDonationHistory({@required String token});
}
