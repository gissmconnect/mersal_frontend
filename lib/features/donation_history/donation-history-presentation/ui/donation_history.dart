import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mersal/core/localization/app_localization.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/features/donation_history/donation-history-data/model/donation_history_model.dart';
import 'package:mersal/features/donation_history/donation-history-presentation/bloc/donation_history_bloc.dart';
import 'package:mersal/injection_container.dart';
import 'package:mersal/resources/colors.dart';

class DonationHistory extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (_) => serviceLocator<DonationHistoryBloc>(),
        child: DonationHistoryBody());
  }
}

class DonationHistoryBody extends StatefulWidget {
  @override
  _DonationHistoryBodyState createState() => _DonationHistoryBodyState();
}

class _DonationHistoryBodyState extends State<DonationHistoryBody> {

  FToast fToast;

  @override
  void initState() {
    super.initState();
    BlocProvider.of<DonationHistoryBloc>(context).add(GetHistoryEvent());
    fToast = FToast();
    fToast.init(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bg_image.jpg"),
            fit: BoxFit.fill,
          ),
        ),
        child: BlocBuilder<DonationHistoryBloc, DonationHistoryState>(
          builder: (context, state) {
            if (state is DonationHistoryInitialising) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            if (state is DonationHistoryLoaded) {
              List<DonationHistoryDataList> donationDataList =
                  state.activity.donationDataList;
              return Container(
                height: MediaQuery.of(context).size.height,
                padding: EdgeInsets.only(left: 20),
                margin: EdgeInsets.only(left: 10, right: 30, top: 20),
                child: Column(
                  children: [
                    SizedBox(
                      height: 25,
                    ),
                    Stack(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(bottom: 5),
                          alignment: Alignment.bottomLeft,
                          child: GestureDetector(
                            child: Icon(
                              Icons.arrow_back_ios_rounded,
                              color: Colors.white,
                            ),
                            onTap: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(bottom: 6),
                          alignment: Alignment.bottomCenter,
                          child: Text(
                            AppLocalizations.of(context).translate("Donation History"),
                            textAlign: TextAlign.end,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.w400),
                          ),
                        ),
                      ],
                    ),
                    donationDataList != null && donationDataList.length > 0
                        ? Expanded(
                          child: Container(
                      margin: const EdgeInsets.only(top: 10.0),
                      child: ListView.builder(
                          shrinkWrap: true,
                          padding:EdgeInsets.zero,
                          itemCount: donationDataList.length,
                          itemBuilder: (context, int index) {
                            return donationHistoryCard(index,donationDataList);
                          },
                      ),
                    ),
                        )
                        : Center(
                          child: Container(
                            margin: EdgeInsets.only(top: MediaQuery.of(context).size.height/2),
                            child: Label(
                              title: "You have not done any donations yet",
                              color: Colors.white,
                              fontSize: 18,
                            ),
                          ),
                        ),
                  ],
                ),
              );
            } else {
              return Container();
            }
          },
        ),
      ),
    );
  }

  Widget donationHistoryCard(int index, List<DonationHistoryDataList> donationDataList) {
    return Container(
      margin: EdgeInsets.only(top: 10, bottom: 10),
      padding: EdgeInsets.only(top: 10, bottom: 10, left: 20),
      decoration: new BoxDecoration(
        borderRadius: BorderRadius.circular(22),
          color: Colors.white),
      child: Row(
        children: [
          Expanded(
            child: Container(
              margin: EdgeInsets.only(right: 25),
              child: Label(
                title: CommonMethods.dateFormatterYMDTime(donationDataList[index].created_at??""),
                fontSize: 13,
                fontWeight: FontWeight.w700,
                color: Color(appColors.buttonColor),
              ),
            ),
          ),
          Expanded(
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(right: 25),
                  child: Label(
                    title: donationDataList[index].reason.reason??"",
                    fontSize: 12,
                    color: Colors.black,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(right: 25),
                  child: Label(
                    title: donationDataList[index].reason.description.toUpperCase(),
                    fontSize: 12,
                    color: Colors.black,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: Container(
              margin: EdgeInsets.only(right: 25),
              child: Label(
                title: "OMR 12",
                fontSize: 16,
                fontWeight: FontWeight.w500,
                color: Colors.blue,
              ),
            ),
          ),
        ],
      ),
    );
  }

}
