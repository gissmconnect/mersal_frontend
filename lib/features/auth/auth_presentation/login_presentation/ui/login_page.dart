import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mersal/core/localization/app_language.dart';
import 'package:mersal/core/localization/app_localization.dart';
import 'package:mersal/core/widgets/common/Button.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/core/widgets/common/shared_preference.dart';
import 'package:mersal/features/auth/auth_presentation/forgot_password_presentation/ui/forgot_password.dart';
import 'package:mersal/features/auth/auth_presentation/login_presentation/bloc/login_bloc.dart';
import 'package:mersal/features/auth/auth_presentation/registration_presentation/ui/register_screen.dart';
import 'package:mersal/features/country_listing/presentation/ui/country_listing.dart';
import 'package:mersal/features/dashboard/dashboard_page.dart';
import 'package:mersal/features/info_presentation/ui/info_screen.dart';
import 'package:mersal/injection_container.dart';
import 'package:mersal/resources/colors.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => serviceLocator<LoginBloc>(),
      child: Scaffold(
        body: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/login_bg.jpg"),
                fit: BoxFit.cover,
              ),
            ),
            child: _LoginPageBody()),
      ),
    );
  }
}

class _LoginPageBody extends StatefulWidget {
  @override
  __LoginPageBodyState createState() => __LoginPageBodyState();
}

class __LoginPageBodyState extends State<_LoginPageBody> {

  FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;

  final phoneController = TextEditingController();
  final passwordController = TextEditingController();
  bool englishSelectBool = true, arabicSelectBool = false;
  bool obscureText = true;
  final FocusNode phoneFocus = new FocusNode();
  final FocusNode passwordFocus = new FocusNode();
  final _formKey = GlobalKey<FormState>();
  String countryCode = "+968";
  String _token = "";

  FToast fToast;

  @override
  void initState() {
    super.initState();
    fToast = FToast();
    fToast.init(context);
  }

  @override
  Widget build(BuildContext context) {
    var appLanguage = Provider.of<AppLanguage>(context);
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: ListView(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.only(top: 20, right: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      GestureDetector(
                        onTap: () {
                          showLanguageDialog(appLanguage);
                        },
                        child: Icon(
                          Icons.language_outlined,
                          color: Colors.white,
                          size: 30,
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => InfoScreen()));
                        },
                        child: Container(
                          margin: EdgeInsets.only(right: 10,left: 10),
                          child: SvgPicture.asset(
                            "assets/images/info_icon.svg",
                            width: 30,
                            height: 30,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                    margin: EdgeInsets.only(top: 30, bottom: 15),
                    height: 180,
                    width: 180,
                    child: Image.asset(
                      "assets/images/logo.png",
                      scale: 2,
                    )),
                Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Container(
                        height: 80,
                        margin: EdgeInsets.only(left: 30, top: 25, right: 30),
                        child: TextFormField(
                          controller: phoneController,
                          focusNode: phoneFocus,
                          validator: (text) {
                            return text.isEmpty
                                ? AppLocalizations.of(context).translate('Phone number cannot be empty')
                                : text.length < 8
                                    ? AppLocalizations.of(context).translate('Phone number should be min 8 digit')
                                    // :text.length>10
                                    // ? 'Phone number should be max 10 digit'
                                    : null;
                          },
                          onChanged: (val) {
                            if (val.length > 0) {}
                          },
                          decoration: InputDecoration(
                            filled: true,
                            counterText: "",
                            fillColor: Colors.white,
                            hintText: AppLocalizations.of(context).translate("Enter Phone number"),
                            contentPadding: EdgeInsets.only(bottom: 15.0, top: 15.0),
                            errorStyle: TextStyle(
                                fontSize: 14.0,
                                color: Colors.red,
                                fontWeight: FontWeight.w600),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(25.7),
                            ),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(25.7),
                            ),
                            focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.red,
                                )),
                            errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.red,
                                )),
                            prefixIcon: Container(
                              margin: EdgeInsets.only(left: 15,right: 10  ),
                              width: 80,
                              child: Row(
                                children: [
                                  GestureDetector(
                                    onTap: () {
                                      _countryCodeButtonTapped();
                                    },
                                    child: Container(
                                      margin: EdgeInsets.only(left: 10),
                                      child: Label(
                                        title: countryCode,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.black,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 5),
                                    width: 1,
                                    height: 30,
                                    color: Colors.black,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          onFieldSubmitted: (term) {
                            CommonMethods.inputFocusChange(
                                context, phoneFocus, passwordFocus);
                          },
                          maxLength: 18,
                          keyboardType: TextInputType.phone,
                        ),
                      ),
                      Container(
                        height: 80,
                        margin: EdgeInsets.only(left: 30, top: 25, right: 30),
                        child: TextFormField(
                          controller: passwordController,
                          focusNode: passwordFocus,
                          obscureText: obscureText,
                          validator: (text) {
                            if (text.isEmpty) {
                              return AppLocalizations.of(context).translate('Password cannot be empty');
                            } else if (text.length < 6) {
                              return AppLocalizations.of(context).translate('Password must be 6 Character');
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            filled: true,
                            counterText: "",
                            fillColor: Colors.white,
                            hintText: AppLocalizations.of(context).translate("Enter Password"),
                            contentPadding:
                                const EdgeInsets.only(bottom: 15.0, top: 15.0),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(25.7),
                            ),
                            errorStyle: TextStyle(
                                fontSize: 14.0,
                                color: Colors.red,
                                fontWeight: FontWeight.w600),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(25.7),
                            ),
                            focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.red,
                                )),
                            errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.red,
                                )),
                            floatingLabelBehavior: FloatingLabelBehavior.auto,
                            prefixIcon: Container(
                              margin: EdgeInsets.only(left: 15),
                              width: 80,
                              child: Row(
                                children: [
                                  IconButton(
                                    onPressed: () {
                                      obscureText = !obscureText;
                                      setState(() {});
                                    },
                                    icon: Icon(Icons.lock_outline,
                                        size: 25, color: Colors.black),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 5),
                                    width: 1,
                                    height: 30,
                                    color: Colors.black,
                                  )
                                ],
                              ),
                            ),
                            suffixIcon: Container(
                              margin: EdgeInsets.only(right: 10),
                              child: IconButton(
                                onPressed: () {
                                  obscureText = !obscureText;
                                  setState(() {});
                                },
                                icon: Icon(
                                    obscureText
                                        ? Icons.visibility_off_outlined
                                        : Icons.remove_red_eye_outlined,
                                    size: 25,
                                    color: Colors.black),
                              ),
                            ),
                          ),
                          onFieldSubmitted: (term) {
                            FocusScope.of(context).unfocus();
                          },
                          maxLength: 20,
                          keyboardType: TextInputType.name,
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  height: 50,
                  margin:
                      EdgeInsets.only(left: 30, top: 35, right: 30, bottom: 20),
                  child: BlocListener<LoginBloc, LoginState>(
                    listener: (context, state) {
                      print('state $state');
                      if (state is LoginError) {
                        CommonMethods.showToast(
                            fToast: fToast,
                            message: state.failure.message,
                            status: false);
                      } else if (state is LoginSuccess) {
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                            builder: (context) => DashboardPage(),
                          ),
                        );
                      }
                    },
                    child: BlocBuilder<LoginBloc, LoginState>(
                      builder: (_, state) {
                        if (state is LoginInitiated) {
                          return Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              CircularProgressIndicator(),
                            ],
                          );
                        }
                        return GestureDetector(
                          onTap: () async {
                            if (_formKey.currentState.validate()) {
                              _firebaseMessaging.getToken().then((token) async {
                                appPreferences.saveStringPreference(
                                    "fcmToken", token.toString());
                                BlocProvider.of<LoginBloc>(context).add(
                                  LoginRequestEvent(
                                      phoneNumber: phoneController.text,
                                      password: passwordController.text,
                                      device_token: token.toString()),
                                );
                              });
                            }
                          },
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                                color: Color(appColors.buttonColor),
                                borderRadius: BorderRadius.circular(25)),
                            child: Stack(
                              children: <Widget>[
                                Align(
                                    alignment: Alignment.center,
                                    child: Label(
                                      title: AppLocalizations.of(context)
                                          .translate('Login'),
                                      color: Colors.white,
                                      fontWeight: FontWeight.w700,
                                      fontSize: 17,
                                      textAlign: TextAlign.center,
                                    ))
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    CommonMethods().animatedNavigation(
                        context: context,
                        currentScreen: LoginPage(),
                        landingScreen: ForgotPassword());
                  },
                  child: Container(
                    padding: EdgeInsets.only(bottom: 40),
                    child: Text(
                      AppLocalizations.of(context)
                          .translate('Forgot Username or Password?'),
                      style: TextStyle(
                        fontSize: 14,
                        fontStyle: FontStyle.italic,
                        color: Color(appColors.buttonColor),
                        decoration: TextDecoration.underline,
                      ),
                    ),
                  ),
                ),
                Container(
                    margin: EdgeInsets.only(top: 30),
                    child: Label(
                        title: AppLocalizations.of(context).translate("If you don't have an account"),
                        color: Colors.white,
                        fontSize: 14)),
                Container(
                  height: 50,
                  margin:
                      EdgeInsets.only(left: 25, top: 20, right: 25, bottom: 20),
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      color: Colors.black87,
                      borderRadius: BorderRadius.circular(25)),
                  child: Button(
                    width: 250,
                    title: AppLocalizations.of(context).translate("Register"),
                    textColor: Colors.white,
                    borderColor: Colors.transparent,
                    color: Color(appColors.buttonColor),
                    onTap: () {
                      registerBottomSheet();
                    },
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Future _countryCodeButtonTapped() async {
    Map results = await Navigator.of(context)
        .push(new MaterialPageRoute(builder: (BuildContext context) {
      return new CountryListing();
    }));
    if (results != null && results.containsKey('country_id')) {
      setState(() {
        countryCode = "+" + results['country_code'].toString();
      });
    }
  }

  Future<Widget> registerBottomSheet() {
    return showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (context) {
          return Container(
              margin: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height / 3.5),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.vertical(top: Radius.circular(30.0)),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black,
                    offset: Offset(0.0, 1.0), //(x,y)
                    blurRadius: 5.0,
                  ),
                ],
              ),
              child: RegisterScreen());
        });
  }

  void showLanguageDialog(AppLanguage appLanguage) {
    showDialog<void>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Label(
              title: AppLocalizations.of(context).translate('Change Language'),
              fontSize: 22,
              textAlign: TextAlign.center,
              fontWeight: FontWeight.w700,
              color: Colors.black,
            ),
            content: StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
                return Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    GestureDetector(
                      onTap: () {
                        englishSelectBool = true;
                        arabicSelectBool = false;
                        appLanguage.changeLanguage(Locale('en'));
                        setState(() {});
                        Navigator.pop(context);
                      },
                      child: Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(right: 20),
                            alignment: Alignment.centerRight,
                            child: Icon(
                              englishSelectBool
                                  ? Icons.check_box
                                  : Icons.check_box_outline_blank,
                              size: 30,
                              color: Color(appColors.blue),
                            ),
                          ),
                          Label(
                            title: AppLocalizations.of(context).translate('English'),
                            fontSize: 18,
                            textAlign: TextAlign.center,
                            color: Colors.black,
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    GestureDetector(
                      onTap: () {
                        englishSelectBool = false;
                        arabicSelectBool = true;
                        appLanguage.changeLanguage(Locale('ar'));
                        setState(() {});
                        Navigator.pop(context);
                      },
                      child: Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(right: 20),
                            alignment: Alignment.centerRight,
                            child: Icon(
                              arabicSelectBool
                                  ? Icons.check_box
                                  : Icons.check_box_outline_blank,
                              size: 30,
                              color: Color(appColors.blue),
                            ),
                          ),
                          Label(
                            title: AppLocalizations.of(context).translate('Arabic'),
                            fontSize: 18,
                            textAlign: TextAlign.center,
                            color: Colors.black,
                          )
                        ],
                      ),
                    ),
                  ],
                );
              },
            ),
          );
        });
  }
}
