import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as http;
import 'package:mersal/core/localization/app_localization.dart';
import 'package:mersal/core/network/api_endpoints.dart';
import 'package:mersal/core/network/vo/Status.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/core/widgets/common/shared_preference.dart';
import 'package:mersal/core/widgets/message.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/lawati_service/domain/entity/sms_subscription_entity.dart';
import 'package:mersal/features/lawati_service/presentation/subscriptions/bloc/sms_subscriptions_bloc.dart';
import 'package:mersal/features/lawati_service/presentation/subscriptions/ui/subscription_details.dart';
import 'package:mersal/features/wedding_invite_presentation/presentation/ui/wedding_invite_preview.dart';
import 'package:mersal/features/wedding_invite_presentation/presentation/wedding_invite_list/model/wedding_invite_list_data.dart';
import 'package:mersal/features/wedding_invite_presentation/presentation/wedding_invite_list/model/wedding_invite_list_model.dart';
import 'package:mersal/features/wedding_invite_presentation/presentation/wedding_invite_list/ui/wedding_invitation_list_preview.dart';
import 'package:mersal/resources/colors.dart';

import '../../../../../injection_container.dart';

class WeddingInvitationListScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<SmsSubscriptionsBloc>(
      create: (_) => serviceLocator<SmsSubscriptionsBloc>(),
      child: Scaffold(
        body: WeddingInvitationListBody(),
      ),
    );
  }
}

class WeddingInvitationListBody extends StatefulWidget {
  @override
  WeddingInviteListState createState() => WeddingInviteListState();
}

class WeddingInviteListState extends State<WeddingInvitationListBody> {
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  BuildContext mContextLoader;

  List<WeddingInviteListData> weddingInviteList = [];
  WeddingInviteDataModel weddingInviteDataModel;

  @override
  void initState() {
    super.initState();
    inviteListApiCall();
    BlocProvider.of<SmsSubscriptionsBloc>(context).add(GetSubscriptionsEvent());
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocListener(
      listeners: [
        BlocListener<SmsSubscriptionsBloc, SmsSubscriptionsState>(
          listenWhen: (oldState, newState) {
            return oldState.subscriptions == null ||
                oldState.subscriptions.status != newState.subscriptions.status;
          },
          listener: (context, state) {
            if (state.subscriptions.status == STATUS.ERROR) {
              showSnackBarMessage(
                  context: context,
                  message: state.subscriptions.failure.message);
            }
          },
        )
      ],
      child: BlocBuilder<SmsSubscriptionsBloc, SmsSubscriptionsState>(
        builder: (context, state) {
          if (state.subscriptions.status == STATUS.LOADING) {
            return Center();
          } else if (state.subscriptions.status == STATUS.SUCCESS) {
            final _subscriptions =
                state.subscriptions.data as List<SmsSubscriptionEntity>;
            return Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/bg_image.jpg"),
                  fit: BoxFit.fill,
                ),
              ),
              child: Column(
                children: [
                  SizedBox(
                    height: 50,
                  ),
                  Stack(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(bottom: 5, left: 15),
                        alignment: Alignment.bottomLeft,
                        child: GestureDetector(
                          child: Icon(
                            Icons.arrow_back_ios_rounded,
                            color: Colors.white,
                          ),
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(bottom: 6),
                        alignment: Alignment.bottomCenter,
                        child: Text(
                          AppLocalizations.of(context)
                              .translate("Wedding Invitations"),
                          textAlign: TextAlign.end,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                    ],
                  ),
                  Expanded(
                    child: weddingInviteList != null &&
                            weddingInviteList.length > 0
                        ? ListView.builder(
                            itemBuilder: (context, index) {
                              return GestureDetector(
                                onTap: () async {
                                  final brideName=weddingInviteList[index].bride_name;
                                  final groomName=weddingInviteList[index].groom_name;
                                  final weddingTime=weddingInviteList[index].time;
                                  final weddingInviteDate=weddingInviteList[index].wedding_date;
                                  final templateImage=weddingInviteList[index].templateModel;
                                  final templateData=weddingInviteList[index].weddingInviteTemplate;
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => WeddingInviteListPreview(
                                            templateData:templateData,
                                            invitationType: "image",
                                            weddingInviteDate: weddingInviteDate,
                                            weddingTime:weddingTime,
                                            groomName:groomName,
                                            weddingType:"Aqid",
                                            brideName: brideName,
                                            templateImage: templateImage!=null?templateImage:null),
                                      ));
                                },
                                child: Container(
                                  padding: EdgeInsets.all(20),
                                  margin: EdgeInsets.only(
                                      bottom: 10, left: 15, right: 15),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(25)),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      RichText(
                                        text: TextSpan(
                                          text: AppLocalizations.of(context)
                                              .translate("Groom Name: "),
                                          style: DefaultTextStyle.of(context)
                                              .style,
                                          children: <TextSpan>[
                                            TextSpan(
                                                text: weddingInviteList[index]
                                                    .groom_name,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 16)),
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      RichText(
                                        text: TextSpan(
                                          text:  AppLocalizations.of(context)
                                              .translate('Bride Name: '),
                                          style: DefaultTextStyle.of(context)
                                              .style,
                                          children: <TextSpan>[
                                            TextSpan(
                                                text: weddingInviteList[index]
                                                    .bride_name,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 16)),
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          RichText(
                                            text: TextSpan(
                                              text: AppLocalizations.of(context)
                                                  .translate('Wedding Date & Time: '),
                                              style: DefaultTextStyle.of(context)
                                                  .style,
                                              children: <TextSpan>[
                                                TextSpan(
                                                    text: weddingInviteList[index]
                                                        .wedding_date+" "+weddingInviteList[index]
                                                        .time,
                                                    style: TextStyle(
                                                        fontWeight: FontWeight.bold,
                                                        fontSize: 16)),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Label(
                                        title: weddingInviteList[index]
                                            .weddingInviteTemplate.title,
                                        fontSize: 13,
                                        textAlign: TextAlign.center,
                                        color: Color(appColors.buttonColor),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            },
                            itemCount: weddingInviteList.length,
                          )
                        : Center(
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 20),
                              child: Text(
                                'No records found',
                                style: TextStyle(
                                    fontSize: 16,
                                    color: Colors.black,),
                              ),
                            ),
                          ),
                  ),
                  // _subscriptions.isNotEmpty
                  //     ? Container(
                  //         height: 60,
                  //         width: 250,
                  //         margin: EdgeInsets.only(top: 35),
                  //         child: Button(
                  //           title: "Renew",
                  //           textColor: Color(appColors.blue00008B),
                  //           onTap: () {
                  //             CommonMethods().animatedNavigation(
                  //                 context: context,
                  //                 currentScreen: LawatiServiceSubscription(),
                  //                 landingScreen: LawatiService("self"));
                  //           },
                  //         ),
                  //       )
                  //     : Container(),
                  // Container(
                  //   height: 60,
                  //   width: 250,
                  //   margin: EdgeInsets.only(top: 35),
                  //   child: Button(
                  //     title: "Add New Subscription",
                  //     textColor: Color(appColors.blue00008B),
                  //     onTap: () {
                  //       CommonMethods().animatedNavigation(
                  //           context: context,
                  //           currentScreen: LawatiServiceSubscription(),
                  //           landingScreen: LawatiService("self"));
                  //     },
                  //   ),
                  // ),
                  // SizedBox(
                  //   height: 40,
                  // ),
                ],
              ),
            );
          }
          return Container();
        },
      ),
    );
  }

  Future<Widget> subscriptionDetailsBottomSheet(_subscriptions) {
    return showModalBottomSheet<Widget>(
      isScrollControlled: true,
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(55.0), topRight: Radius.circular(55.0)),
      ),
      builder: (BuildContext context) {
        return Container(
            height: MediaQuery.of(context).size.height / 2,
            color: Colors.white,
            margin: EdgeInsets.only(top: 40),
            child: SubscriptionDetailsScreen(_subscriptions));
      },
    );
  }

  Future<void> inviteListApiCall() async {
    final _token = await appPreferences.getStringPreference("AccessToken");
    if (context != null) {
      _showLoader(context);
    }
    var response = await http.get(
      Uri.parse(WEDDING_INVITES),
      headers: {
        'Content-type': 'application/json',
        AUTHORIZATION: BEARER + " " + _token
      },
    );

    if (response.statusCode == 200) {
      if (mContextLoader != null) Navigator.pop(mContextLoader);
      final Map<String, dynamic> _responseJson = json.decode(response.body);
      print(
          'InvitationResponse ${response.statusCode} and ${_responseJson.toString()}');
      weddingInviteDataModel = WeddingInviteDataModel.fromJson(_responseJson);
      weddingInviteList = weddingInviteDataModel.weddingInviteList;
      if (mounted) {
        setState(() {});
      }
    }
  }

  Future<Null> _showLoader(BuildContext context) async {
    return showDialog<Null>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context1) {
        mContextLoader = context1;
        return Scaffold(
          backgroundColor: Colors.transparent.withOpacity(0.3),
          key: _keyLoader,
          body: Container(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    child: CircularProgressIndicator(),
                    height: 40.0,
                    width: 40.0,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
