import 'package:equatable/equatable.dart';

class DonationTypeEntity extends Equatable {
  int id;
  String reason;
  String description;
  bool isSelected;

  DonationTypeEntity(
      {this.id, this.reason, this.description, this.isSelected = false});

  @override
  List<Object> get props => [id, reason, description, isSelected];

  DonationTypeEntity copy(
      {int id, String reason, String description, bool isSelected}) {
    return DonationTypeEntity(id: id ?? this.id,
        reason: reason ?? this.reason,
        description: description ?? this.description,
        isSelected: isSelected ?? this.isSelected);
  }

  @override
  String toString() {
    return 'DonationTypeEntity{id: $id, reason: $reason, description: $description, isSelected: $isSelected}';
  }
}

/*class Reason extends Equatable {
  int id;
  String reason;

  Reason({this.id, this.reason});

  @override
  List<Object> get props => [id, reason];

  @override
  String toString() {
    return 'Reason{id: $id, reason: $reason}';
  }
}*/
