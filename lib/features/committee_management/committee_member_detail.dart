import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:mersal/core/widgets/common/CustomAppBar.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/resources/colors.dart';

import 'data/model/committee_member_model.dart';

class CommitteeMemberDetails extends StatefulWidget {
  final CommitteeMemberModel committeeMember;

  const CommitteeMemberDetails({Key key, @required this.committeeMember}) : super(key: key);
  @override
  _CommitteeMemberDetailsState createState() => _CommitteeMemberDetailsState();
}

class _CommitteeMemberDetailsState extends State<CommitteeMemberDetails> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        margin:  EdgeInsets.only(bottom:  MediaQuery.of(context).size.height/4),
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only( top: 20),
              decoration: new BoxDecoration(
                shape: BoxShape.circle,
                border:
                Border.all(color: Colors.black, width: 2),
              ),
              child: Center(
                child: CircleAvatar(
                  backgroundImage:
                  CachedNetworkImageProvider(widget.committeeMember.member.image),
                  radius: 40,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only( top: 10),
              child: Label(
                title: widget.committeeMember.member.name??"User",
                textAlign: TextAlign.center,
                fontSize: 20,
                fontWeight: FontWeight.w700,
                color: Color(appColors.buttonColor),
              ),
            ),
            Container(
              margin: EdgeInsets.only( top: 20),
              child: Label(
                title: widget.committeeMember.member.phone,
                textAlign: TextAlign.center,
                fontSize: 20,
                fontWeight: FontWeight.w600,
                color: Color(appColors.buttonColor),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 20),
              child: Label(
                title: widget.committeeMember.quote,
                textAlign: TextAlign.center,
                fontSize: 20,
                fontWeight: FontWeight.w600,
                color: Color(appColors.buttonColor),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 20),
              child: Label(
                title: widget.committeeMember.otherInfo,
                textAlign: TextAlign.center,
                fontSize: 20,
                fontWeight: FontWeight.w600,
                color: Color(appColors.buttonColor),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
