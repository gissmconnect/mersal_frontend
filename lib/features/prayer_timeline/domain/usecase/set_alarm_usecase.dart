import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/prayer_timeline/domain/repository/prayer_repository.dart';
import 'package:meta/meta.dart';

class SetAlarmUseCase extends UseCase<String,int>{
  final PrayerRepository prayerRepository;

  SetAlarmUseCase({@required this.prayerRepository});

  @override
  Future<Either<Failure, String>> call(int params)async {
    return prayerRepository.setAlarm(id: params);
  }
}