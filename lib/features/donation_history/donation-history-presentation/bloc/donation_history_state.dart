part of 'donation_history_bloc.dart';

abstract class DonationHistoryState extends Equatable {}

class DonationHistoryInitial extends DonationHistoryState {
  @override
  List<Object> get props => [];
}

class DonationHistoryInitialising extends DonationHistoryState {
  @override
  List<Object> get props => [];
}

class DonationHistoryLoaded extends DonationHistoryState {
  final DonationHistoryModel activity;

  DonationHistoryLoaded({@required this.activity});

  @override
  List<Object> get props => [activity];
}

class DonationHistoryError extends DonationHistoryState {
  final Failure failure;

  DonationHistoryError(this.failure);

  @override
  List<Object> get props => [failure];
}
