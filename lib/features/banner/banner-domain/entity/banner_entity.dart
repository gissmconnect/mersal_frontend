import 'package:equatable/equatable.dart';

class BannerEntity extends Equatable {
  int id;
  String image;
  String created_at;
  String updated_at;


  BannerEntity(
      {this.id,
        this.image,
        this.created_at,
        this.updated_at,
      });

  @override
  List<Object> get props =>
      [id, image, created_at, updated_at];
}
