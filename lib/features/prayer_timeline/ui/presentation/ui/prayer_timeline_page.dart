import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mersal/core/localization/app_localization.dart';
import 'package:mersal/core/network/vo/Status.dart';
import 'package:mersal/core/util/date_formatter.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/features/prayer_timeline/domain/entity/prayer_data_entity.dart';
import 'package:mersal/features/prayer_timeline/ui/presentation/bloc/prayer_bloc.dart';
import 'package:mersal/injection_container.dart';
import 'package:mersal/resources/colors.dart';
import 'package:mersal/core/widgets/common/shared_preference.dart';

class PrayerTimeLinePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<PrayerBloc>(
      create: (_) => serviceLocator(),
      child: Container(
        child: _PrayerTimeLinePageBody(),
      ),
    );
  }
}

class _PrayerTimeLinePageBody extends StatefulWidget {
  @override
  __PrayerTimeLinePageBodyState createState() =>
      __PrayerTimeLinePageBodyState();
}

class __PrayerTimeLinePageBodyState extends State<_PrayerTimeLinePageBody> {

  FToast _fToast;
  var prayerName,prayerTime,prayer;

  @override
  void initState() {
    super.initState();
    BlocProvider.of<PrayerBloc>(context).add(GetPrayerTimesEvent());
    _fToast = FToast();
    _fToast.init(context);
    _asyncMethod();
  }

  _asyncMethod() async {
    prayer = await appPreferences.getStringPreference("Prayer");
    print("prayerNamePrint========" + prayer.toString());
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocListener(
      listeners: [
        BlocListener<PrayerBloc, PrayerState>(
          listenWhen: (oldState, newState) {
            return (oldState.prayerTimesResource.status !=
                    newState.prayerTimesResource.status) ||
                oldState.setAlarmResource == null ||
                (oldState.setAlarmResource.status !=
                    newState.setAlarmResource.status);
          },
          listener: (context, state) {
            if (state.prayerTimesResource.status == STATUS.ERROR) {
              CommonMethods.showToast(
                  fToast: _fToast,
                  status: false,
                  message: state.prayerTimesResource.failure.message);
            }
          },
        ),
        BlocListener<PrayerBloc, PrayerState>(
          listenWhen: (oldState, newState) {
            return oldState.setAlarmResource != null &&
                (oldState.setAlarmResource.status !=
                    newState.setAlarmResource.status);
          },
          listener: (context, state) {
            if (state.setAlarmResource.status == STATUS.ERROR) {
              CommonMethods.showToast(
                  fToast: _fToast,
                  status: false,
                  message: state.setAlarmResource.failure.message);
            }else if(state.setAlarmResource.status == STATUS.SUCCESS){
              CommonMethods.showToast(
                  fToast: _fToast,
                  status: true,
                  message: state.setAlarmResource.data);
            }
          },
        ),
      ],
      child: BlocBuilder<PrayerBloc, PrayerState>(
        builder: (context, state) {
          if (state.prayerTimesResource.status == STATUS.LOADING) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          if (state.prayerTimesResource.status == STATUS.SUCCESS) {
            final List<PrayerDataEntity> _prayerTimes = state.prayerTimesResource.data;
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: EdgeInsets.only(left: 20, right: 20),
                  padding: EdgeInsets.only(bottom: 20),
                  decoration: new BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Color(appColors.blue)),
                  child: ListView.builder(
                    padding: EdgeInsets.only(top: 15),
                    shrinkWrap: true,
                    itemCount: _prayerTimes.length,
                    itemBuilder: (context, int index) {
                      if(prayer!=null){
                        if(prayer==_prayerTimes[index].name){
                          prayerName=_prayerTimes[index].name;
                          prayerTime=CommonMethods.dateFormatterTime(
                              _prayerTimes[index].prayerTime);
                          Future.delayed(Duration(milliseconds: 1000), () {
                            setState(() {

                            });
                          });
                      }
                      }
                      return GestureDetector(
                        onTap: () {
                          BlocProvider.of<PrayerBloc>(context).add(
                            AddPrayerEvent(id: _prayerTimes[index].id),
                          );
                        },
                        child: Container(
                          padding: EdgeInsets.only(bottom: 10),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(
                                    child: Container(
                                      margin: EdgeInsets.only(left: 10),
                                      child: Label(
                                        title: _prayerTimes[index].name,
                                        fontSize: 18,
                                        color: _prayerTimes[index].isPrayerOver
                                            ? Colors.grey.shade700
                                            : Colors.white,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(right: 20),
                                    child: Label(
                                        title: CommonMethods.dateFormatterTime(
                                            _prayerTimes[index].prayerTime),
                                        fontSize: 14,
                                        color: _prayerTimes[index].isPrayerOver
                                            ? Colors.grey.shade700
                                            : Colors.white),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(right: 20),
                                    child: _prayerTimes[index].settingAlarm
                                        ? Padding(
                                      padding: const EdgeInsets.all(4),
                                          child: SizedBox(
                                              width: 20,
                                              height: 20,
                                              child: CircularProgressIndicator()),
                                        )
                                        : SvgPicture.asset(
                                            "assets/images/alarm_clock.svg",
                                            width: 30,
                                            height: 30,
                                            color:
                                                _prayerTimes[index].isPrayerOver
                                                    ? Colors.grey.shade700
                                                    : Colors.white,
                                          ),
                                  ),
                                ],
                              ),
                              Divider(
                                thickness: 1,
                                color: Colors.white,
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  // child: state.upcomingPrayer != null
                  child: prayer != null && prayerName!=null
                      ? Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              AppLocalizations.of(context)
                                  .translate('Alarm set for'),
                              style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                            // Expanded(
                            //   child: Text(
                            //     state.upcomingPrayer.name +
                            //         " " +
                            //         formatPrayerTimeToHuman(
                            //             dateTime:
                            //                 state.upcomingPrayer.prayerTime),
                            //     style: TextStyle(
                            //         fontSize: 16,
                            //         color: Colors.white,
                            //         fontWeight: FontWeight.bold),
                            //   ),
                            // )
                            Expanded(
                              child: Text(
                                prayerName+ " " + prayerTime,
                                // prayerName??"",
                                style: TextStyle(
                                    fontSize: 16,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            )
                          ],
                        )
                      : Text(
                          AppLocalizations.of(context)
                              .translate('No alarm set'),
                          style: TextStyle(
                              fontSize: 16,
                              color: Colors.white,
                              fontWeight: FontWeight.bold),
                        ),
                )
              ],
            );
          }
          return Container();
        },
      ),
    );
  }
}
