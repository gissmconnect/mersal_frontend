
import 'package:mersal/features/homescreen/home-domain/entity/BannerDataEntity.dart';

class BannerDataModel {
  List<BannerDataList> bannerList;
  String message;
  bool status;

  BannerDataModel({this.bannerList,this.status});

  factory BannerDataModel.fromJson(Map<String, dynamic> json) {
    return BannerDataModel(
      bannerList: json['data'] != null ? (json['data'] as List).map((i) => BannerDataList.fromJson(i)).toList() : null,
      status: json['status'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.bannerList != null) {
      data['data'] = this.bannerList.map((v) => v.toJson()).toList();
    }
    return data;
  }
}