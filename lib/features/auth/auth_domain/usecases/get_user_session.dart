import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:meta/meta.dart';

import '../entity/user_data.dart';
import '../repository/auth_repository.dart';

class GetUserSession implements UseCase<UserData, NoParams> {
  final AuthRepository authRepository;

  GetUserSession({@required this.authRepository});

  @override
  Future<Either<Failure, UserData>> call(NoParams params) async {
    return await authRepository.getUserSession();
  }
}
