import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/exceptions.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/network/network_info.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/committee_management/data/datasource/committee_datasource.dart';
import 'package:mersal/features/committee_management/domain/entity/committe_entity.dart';
import 'package:mersal/features/committee_management/domain/entity/committee_member_entity.dart';
import 'package:mersal/features/committee_management/domain/repository/committe_repo.dart';
import 'package:meta/meta.dart';

class CommitteeRepoImpl implements CommitteeRepo {
  final CommitteeDataSource committeeDataSource;
  final UserDataLocalDataSource userDataLocalDataSource;
  final NetworkInfo networkInfo;

  CommitteeRepoImpl(
      {@required this.networkInfo,
      @required this.userDataLocalDataSource,
      @required this.committeeDataSource});

  @override
  Future<Either<Failure, List<CommitteeEntity>>> getCommittees() async {
    if (await networkInfo.isConnected) {
      final _token = await userDataLocalDataSource.getAuthToken();
      try {
        final _service = await committeeDataSource.getCommittees(token: _token);
        return Right(_service);
      } on ServerException catch (ex) {
        return Left(ServerFailure()..message = ex.message);
      } on AuthException catch (ex) {
        return Left(AuthFailure()..message = ex.message);
      } on Exception {
        return Left(ServerFailure());
      }
    } else {
      return Left(NetworkFailure());
    }
  }

  @override
  Future<Either<Failure,List< CommitteeMemberEntity>>> getCommitteeMember(
      {int committeeId}) async {
    if (await networkInfo.isConnected) {
      final _token = await userDataLocalDataSource.getAuthToken();
      try {
        final _service = await committeeDataSource.getCommitteeMember(
            token: _token, committeeId: committeeId);
        return Right(_service);
      } on ServerException catch (ex) {
        return Left(ServerFailure()..message = ex.message);
      } on AuthException catch (ex) {
        return Left(AuthFailure()..message = ex.message);
      } on Exception {
        return Left(ServerFailure());
      }
    } else {
      return Left(NetworkFailure());
    }
  }
}
