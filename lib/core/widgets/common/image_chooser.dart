import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import 'Label.dart';

class ImageChooser{

  Future<dynamic> _openCamera() async{
    var picture = await ImagePicker.platform.pickImage(source: ImageSource.camera
    ,imageQuality: 60).catchError((e){print("Some error: ${e.toString()}");});
    return picture;
  }

  Future<dynamic> _openGallery() async{
    var picture = await ImagePicker.platform.pickImage(source: ImageSource.gallery,
        imageQuality: 60).catchError((e){print("Some error: ${e.toString()}");});
    return picture;
  }

  Future<dynamic> showImageChooser(BuildContext context){
    return showModalBottomSheet(context: context,
        isScrollControlled: false,
        backgroundColor: Colors.transparent,
        builder: (BuildContext builder){
          return Container(
            height: 100,
            color: Colors.white,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                GestureDetector(
                  onTap: (){
                    _openCamera()
                        .then((onValue)=>Navigator.of(context).pop(onValue))
                        .catchError((e) {
                      print("Some error: ${e.error}");     // Finally, callback fires.
                    });
                  },
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(12),
                        child: Icon(Icons.camera_alt_outlined),
                      ),
                      Label(title: "Camera")
                    ],
                  ),
                ),
                Container(
                  height: 70,
                  width: 1,
                  color: Colors.grey,
                ),
                GestureDetector(
                  onTap: (){
                    _openGallery().then((onValue)=>Navigator.of(context).pop(onValue));
                  },
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(12),
                        child: Icon(Icons.photo_size_select_actual_outlined),
                      ),
                      Label(title: "Gallery")
                    ],
                  ),
                )
              ],
            ),
          );
        });
  }
}
ImageChooser imageChooser=new ImageChooser();