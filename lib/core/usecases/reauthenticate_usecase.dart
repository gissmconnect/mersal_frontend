import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/auth/auth_domain/repository/auth_repository.dart';
import 'package:meta/meta.dart';

class ReAuthenticateUseCase extends UseCase<void, NoParams> {
  final AuthRepository authRepository;

  ReAuthenticateUseCase({@required this.authRepository});

  @override
  Future<Either<Failure, void>> call(NoParams params) async {
    final _loginDataResponse = await authRepository.getLoginData();
    _loginDataResponse.fold(
      (l) => Left(l),
      (r) async {
        return await authRepository.login(
            phoneNumber: r.loginId, password: r.password, device_token: r.device_token);
      },
    );
  }
}

class Params {
  final String loginId, password;

  Params({@required this.loginId, @required this.password});
}
