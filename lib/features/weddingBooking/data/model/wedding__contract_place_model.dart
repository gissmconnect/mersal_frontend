
import 'package:mersal/features/weddingBooking/data/model/wedding_contract_data.dart';

class WeddingContractPlaceModel {
  List<WeddingContractData> contractDataList;
  String message;
  bool status;

  WeddingContractPlaceModel({this.contractDataList,this.status});

  factory WeddingContractPlaceModel.fromJson(Map<String, dynamic> json) {
    return WeddingContractPlaceModel(
      contractDataList: json['data'] != null ? (json['data'] as List).map((i) => WeddingContractData.fromJson(i)).toList() : null,
      status: json['status'],
    );
  }

}