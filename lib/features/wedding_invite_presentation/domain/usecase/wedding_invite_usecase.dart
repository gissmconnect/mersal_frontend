import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/wedding_invite_presentation/domain/entity/wedding_template_entity.dart';
import 'package:mersal/features/wedding_invite_presentation/domain/repository/wedding_template_repo.dart';
import 'package:meta/meta.dart';

class WeddingTemplateUseCase extends UseCase<WeddingTemplateEntity, NoParams> {
  final WeddingTemplateRepo weddingTemplateRepo;

  WeddingTemplateUseCase({@required this.weddingTemplateRepo});

  @override
  Future<Either<Failure, WeddingTemplateEntity>> call(NoParams params) async {
    return await weddingTemplateRepo.getWeddingTemplate();
  }
}
