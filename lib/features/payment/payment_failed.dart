import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mersal/core/widgets/common/Button.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/core/widgets/message.dart';
import 'package:mersal/features/auth/auth_presentation/doc_upload_presentation/bloc/doc_upload_bloc.dart';
import 'package:mersal/features/dashboard/dashboard_page.dart';
import 'package:mersal/features/profile/profile_presentation/ui/upload_id_screen.dart';
import 'package:mersal/injection_container.dart';
import 'package:mersal/resources/colors.dart';

class PaymentFailedScreen extends StatelessWidget {
  final String from;

  PaymentFailedScreen(this.from);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/bg_image.jpg"),
              fit: BoxFit.fill,
            ),
          ),
          child: _PaymentFailedPageBody(from)),
    );
  }
}

class _PaymentFailedPageBody extends StatefulWidget {
  final String from;

  _PaymentFailedPageBody(this.from);

  @override
  _PaymentFailedPageBodyState createState() => _PaymentFailedPageBodyState();
}

class _PaymentFailedPageBodyState extends State<_PaymentFailedPageBody> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: ListView(
        shrinkWrap: true,
        children: [
          Container(
            height: 400,
            padding: const EdgeInsets.all(8.0),
            margin: EdgeInsets.only(top: 60, left: 20, right: 20),
            decoration: new BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white,
              borderRadius: BorderRadius.circular(25.0),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: 80,
                  height:80,
                  child: Image.asset(
                    "assets/images/warning.png",
                  ),
                ),
                 successLayout(
                    "The transaction with reference id 1744 has failed due to one of the following reasons: Your phone was unreachable due to lack of signal or temporary problems with your mobile phone network. ")
              ],
            ),
          ),
          Container(
            height: 60,
            width: 250,
            margin: EdgeInsets.only(left: 30, top: 35, right: 30, bottom: 30),
            child: Button(
              title: "Go to Homepage",
              textColor: Color(appColors.blue00008B),
              onTap: () {
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget successLayout(String message) {
    return Container(
      margin: EdgeInsets.only(top: 20,left: 10,right: 10),
      child: Label(
        textAlign: TextAlign.center,
        title: message,
        fontSize: 18,
        color: Colors.black,
      ),
    );
  }
}
