import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:image_downloader/image_downloader.dart';
import 'package:intl/intl.dart';
import 'package:mersal/core/localization/app_localization.dart';
import 'package:mersal/core/widgets/common/Button.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/features/wedding_invite_presentation/presentation/ui/wedding_invitation_summary.dart';
import 'package:mersal/features/wedding_invite_presentation/presentation/ui/wedding_invite_contact_import.dart';
import 'package:mersal/features/wedding_invite_presentation/presentation/wedding_invite_list/model/wedding_invite_tempate_data.dart';
import 'package:mersal/features/wedding_invite_presentation/presentation/wedding_invite_list/model/wedding_invite_template_model.dart';
import 'package:mersal/resources/colors.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WeddingInviteListPreview extends StatefulWidget {
  final WeddingInviteTemplateData templateData;
  final WeddingInviteTemplateModel templateImage;
  final String invitationType,
      weddingInviteDate,
      weddingTime,
      groomName,
      weddingType,
      brideName;

  WeddingInviteListPreview(
      {this.invitationType,
      this.templateData,
      this.weddingInviteDate,
      this.weddingType,
      this.groomName,
      this.brideName,
      this.weddingTime,
      this.templateImage});

  @override
  _WeddingInviteListPreviewState createState() =>
      _WeddingInviteListPreviewState();
}

class _WeddingInviteListPreviewState extends State<WeddingInviteListPreview> {
  bool showImageBool = false;
  var outputDate;

  @override
  void initState() {
    super.initState();
    if (widget.invitationType != "Text") {
      showImageBool = true;
    }
    DateTime parseDate =
        new DateFormat("yyyy-MM-dd").parse(widget.weddingInviteDate);
    var inputDate = DateTime.parse(parseDate.toString());
    var outputFormat = DateFormat('dd-MM-yyyy');
    outputDate = outputFormat.format(inputDate);
    print(outputDate);
    setState(() {});
  }

  String selectedUrl = 'https://flutter.io';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/bg_image.jpg"),
              fit: BoxFit.fill,
            ),
          ),
          child: Column(
            children: [
              SizedBox(
                height: 50,
              ),
              Stack(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(bottom: 5, left: 15),
                    alignment: Alignment.bottomLeft,
                    child: GestureDetector(
                      child: Icon(
                        Icons.arrow_back_ios_rounded,
                        color: Colors.white,
                      ),
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 6),
                    alignment: Alignment.bottomCenter,
                    child: Text(
                      AppLocalizations.of(context)
                          .translate("Wedding Invitation"),
                      textAlign: TextAlign.end,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                ],
              ),
              Expanded(
                child: ListView(
                  shrinkWrap: true,
                  children: [
                    widget.templateImage!=null
                        ? Container(
                            margin:
                                EdgeInsets.only(left: 20, top: 20, right: 20),
                            padding: EdgeInsets.only(left: 10, right: 10),
                            decoration: new BoxDecoration(
                              borderRadius: BorderRadius.circular(25),
                              color: Colors.white,
                            ),
                            child: Column(
                              children: [
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  height: MediaQuery.of(context).size.height / 2.5,
                                  child: WebView(
                                    gestureNavigationEnabled: true,
                                    javascriptMode: JavascriptMode.unrestricted,
                                    initialUrl:
                                    // "http://93.115.18.236/public/wedding-card/preview/4?brideName=Neha&groomName=Mohit&weddingDate=06-06-2020&weddingType=Aqid",
                                    'http://93.115.18.236/public/wedding-card/preview/4?brideName=${widget.brideName}&groomName=${widget.groomName}&weddingDate=$outputDate&weddingType=${widget.weddingType}',
                                    onPageFinished: (value) {
                                      setState(() {
                                        print("====your page is load");
                                      });
                                    },
                                    onProgress: (int progress) {
                                      print("WebView is loading (progress : $progress%)");
                                    },
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only( bottom: 20, left: 5, right: 5),
                                  child: Label(
                                    title: widget.templateData.template ?? "",
                                    fontSize: 16,
                                    textAlign: TextAlign.center,
                                    fontWeight: FontWeight.bold,
                                    color: Color(appColors.buttonColor),
                                  ),
                                ),
                              ],
                            ),
                          )
                        : Container(
                            margin:
                                EdgeInsets.only(left: 20, top: 20, right: 20),
                            padding: EdgeInsets.only(left: 20, right: 10),
                            height: 250,
                            alignment: Alignment.center,
                            width: MediaQuery.of(context).size.width,
                            decoration: new BoxDecoration(
                              borderRadius: BorderRadius.circular(25),
                              color: Colors.white,
                            ),
                            child: Label(
                              title: widget.templateData.template ?? "",
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                              color: Color(appColors.navyBlue),
                            ),
                          ),
                     showImageBool
                        ? Container(
                            height: 60,
                            margin:
                                EdgeInsets.only(top: 45, left: 20, right: 20),
                            child: Button(
                              title:  AppLocalizations.of(context)
                                  .translate("Download Image") ,
                              color: Color(appColors.buttonColor),
                              borderColor: Colors.transparent,
                              textColor: Colors.white,
                              onTap: () async {
                                try {
                                  _launchURL(
                                    "http://93.115.18.236/public/wedding-card/preview/4?brideName=${widget.brideName}&groomName=${widget.groomName}&weddingDate=$outputDate&weddingType=${widget.weddingType}"
                                  );
                                } on PlatformException catch (error) {
                                  print(error);
                                }
                              },
                            ),
                          )
                        : Container(),
                    Container(
                      height: 60,
                      margin: EdgeInsets.only(
                          top: 45, left: 20, right: 20, bottom: 30),
                      child: Button(
                        title: AppLocalizations.of(context)
                            .translate("Select Contacts"),
                        color: Color(appColors.buttonColor),
                        borderColor: Colors.transparent,
                        textColor: Colors.white,
                        onTap: () async {
                          // weddingInvitationBottomSheet();
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      WeddingInviteContactsPage(
                                          invitationType:
                                          widget.invitationType,
                                          templateId: widget.templateData.id
                                              .toString(),
                                          weddingInviteDate:
                                          widget.weddingInviteDate,
                                          weddingTime: widget.weddingTime,
                                          groomName: widget.groomName,
                                          weddingType: widget.weddingType,
                                          brideName: widget.brideName,
                                          // mobileNumber: widget.mobileNumber,
                                          templateImageList:
                                          widget.templateImage)));
                          // final PermissionStatus permissionStatus =
                          //     await _getPermission();
                          // if (permissionStatus == PermissionStatus.granted) {
                          // }
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  //Check contacts permission
  Future<PermissionStatus> _getPermission() async {
    PermissionStatus permission = await Permission.contacts.status;
    if (permission != PermissionStatus.granted &&
        permission != PermissionStatus.permanentlyDenied) {
      PermissionStatus permissionStatus = await Permission.contacts.request();
      return permissionStatus;
    } else {
      return permission;
    }
  }

  Future<Widget> weddingInvitationBottomSheet() {
    String sendPhone = "";

    print("pffff" + sendPhone);
    return showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (context) {
          return Container(
              margin: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height / 3.5),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.vertical(top: Radius.circular(30.0)),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black,
                    offset: Offset(0.0, 1.0), //(x,y)
                    blurRadius: 5.0,
                  ),
                ],
              ),
              child: WeddingInvitationSummary(
                  sendPhone: "",
                  templateId: widget.templateData.id.toString(),
                  invitationType: widget.invitationType,
                  weddingInviteDate: widget.weddingInviteDate,
                  weddingTime: widget.weddingTime,
                  groomName: widget.groomName,
                  brideName: widget.brideName));
        });
  }

  void _launchURL(String _url) async =>
      await canLaunch(_url)
          ? await launch(_url)
          : throw 'Could not launch $_url';
}
