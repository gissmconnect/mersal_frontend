part of 'event_bloc.dart';

abstract class EventEvent extends Equatable {
  const EventEvent();
}

class GetEventEvent extends EventEvent {
  final DateTime date;

  GetEventEvent({@required this.date});

  @override
  List<Object> get props => [date];
}
