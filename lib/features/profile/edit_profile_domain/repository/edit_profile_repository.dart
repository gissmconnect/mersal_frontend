import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/features/auth/auth_data/model/user_data_model.dart';
import 'package:meta/meta.dart';

abstract class EditProfileRepository {
  Future<Either<Failure,UserDataModel>> updateProfile(
      {@required String name,
      @required String image,
      @required String dob,
      @required int gender,
      @required String bloodGroup});
}
