import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/exceptions.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/network/network_info.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/election/election-data/datasource/election_data_source.dart';
import 'package:mersal/features/election/election-domain/entity/election_entity.dart';
import 'package:mersal/features/election/election-domain/repository/election_repository.dart';
import 'package:mersal/features/election_candidate/election_candidate_data/model/election_candidate_model.dart';
import 'package:mersal/features/election_candidate/election_candidate_domain/entity/election_candidate_entity.dart';
import 'package:meta/meta.dart';

class ElectionRepositoryImpl implements ElectionRepository {
  final NetworkInfo networkInfo;
  final ElectionDataSource electionDataSource;
  final UserDataLocalDataSource userDataLocalDataSource;

  ElectionRepositoryImpl(
      {@required this.networkInfo,
      @required this.electionDataSource,
      @required this.userDataLocalDataSource});

  @override
  Future<Either<Failure, List<ElectionDataEntity>>> getElections() async {
    if (await networkInfo.isConnected) {
      try {
        final _token = await userDataLocalDataSource.getAuthToken();
        final _elections = await electionDataSource.getElections(token: _token);
        return Right(_elections);
      } on ServerException catch (ex) {
        return Left(ServerFailure()..message = ex.message);
      } on AuthException catch (ex) {
        return Left(AuthFailure()..message = ex.message);
      } on Exception {
        return Left(ServerFailure());
      }
    } else {
      return Left(NetworkFailure());
    }
  }

  @override
  Future<Either<Failure, ElectionCandidateResponseModel>> getCandidates(
      {int electionId}) async {
    if (await networkInfo.isConnected) {
      try {
        final _token = await userDataLocalDataSource.getAuthToken();
        final _candidates = await electionDataSource.getCandidates(
            token: _token, electionId: electionId);
        return Right(_candidates);
      } on ServerException catch (ex) {
        return Left(ServerFailure()..message = ex.message);
      } on Exception {
        return Left(ServerFailure());
      }
    } else {
      return Left(NetworkFailure());
    }
  }

  @override
  Future<Either<Failure, void>> vote({int electionId, int candidateId}) async {
    if (await networkInfo.isConnected) {
      try {
        final _token = await userDataLocalDataSource.getAuthToken();
        await electionDataSource.vote(
            token: _token, electionId: electionId, candidateId: candidateId);
      } on ServerException catch (ex) {
        return Left(ServerFailure()..message = ex.message);
      } on Exception {
        return Left(ServerFailure());
      }
    }
  }
}
