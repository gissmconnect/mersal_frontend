import 'package:intl/intl.dart';
import 'package:meta/meta.dart';

String formatDDMMYYY({@required DateTime date}) {
  return DateFormat('dd/MM/yyyy').format(date);
}

String formatYYYYMMDD({@required DateTime date}) {
  return DateFormat('yyyy-MM-dd').format(date);
}

String formatToTime({@required String dateTime}){
  print('date $dateTime');
   final DateTime date = DateFormat("yyyy-MM-dd HH:mm:ss").parse(dateTime);
   return DateFormat('hh:mm a').format(date);
}
int formatToMillis({@required String dateTime}){
  final DateTime date = DateFormat("dd-MM-yyyy HH:mm a").parse(dateTime);
  return date.millisecondsSinceEpoch;
}

DateTime formatPrayerTimeToMillis({@required String dateTime}){
  DateTime _prayerTime = DateFormat('yyyy-MM-dd HH:mm:ss').parse(dateTime);
  final _date = DateTime.now();
  _prayerTime = DateTime(_date.year,_date.month,_date.day,_prayerTime.hour,_prayerTime.minute);
  return _prayerTime;
}

String formatPrayerTimeToHuman({@required String dateTime}){
  final DateTime date = DateFormat("yyyy-MM-dd HH:mm:ss").parse(dateTime);
  return DateFormat('hh:mm a').format(date);
}