import 'package:mersal/features/child_service/child-domain/entity/child-enrollment-entity/registered-child-data-entity/child_details_model_entity.dart';
import 'package:meta/meta.dart';

class ChildDetailsModel extends ChildDetailsModelEntity {
  ChildDetailsModel(
      {@required bool status,
      @required List<ChildDetailsDataModel> childNewDataList})
      : super(status: status, childDetailsList: childNewDataList);

  ChildDetailsModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['data'] != null) {
      childDetailsList = List<ChildDetailsDataModel>();
      json['data'].forEach((v) {
        childDetailsList.add(ChildDetailsDataModel.fromJson(v));
      });
    }
  }
}

class ChildDetailsDataModel extends ChildDetailsData {
  ChildDetailsDataModel({
    int id,
    String name,
    ChildRelationData childRelationData,
  }) : super(id: id, childRelationData: childRelationData, name: name);

  ChildDetailsDataModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    if (json['childRelationData'] != null) {
      ChildRelationDataModel.fromJson(json['childRelationData']);
    }
  }
}

class ChildRelationDataModel extends ChildRelationData {
  ChildRelationDataModel(
      {int id, String relation_type, String title, String created_at})
      : super(
            id: id,
            relation_type: relation_type,
            title: title,
            created_at: created_at);

  ChildRelationDataModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    relation_type = json['relation_type'];
    title = json['title'];
    created_at = json['created_at'];
  }
}
