import 'dart:io';

import 'package:camera/camera.dart';
import 'package:dart_date/dart_date.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:mersal/core/bloc/user_bloc.dart';
import 'package:mersal/core/localization/app_localization.dart';
import 'package:mersal/core/network/vo/Status.dart';
import 'package:mersal/core/util/constants.dart';
import 'package:mersal/core/widgets/common/Button.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/CustomAppBar.dart';
import 'package:mersal/core/widgets/common/InputField.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/core/widgets/message.dart';
import 'package:mersal/core/widgets/widgets.dart';
import 'package:mersal/features/auth/auth_domain/entity/user_data.dart';
import 'package:mersal/features/auth/auth_presentation/doc_upload_presentation/bloc/doc_upload_bloc.dart';
import 'package:mersal/features/auth/auth_presentation/doc_upload_presentation/ui/doc_upload_page.dart';
import 'package:mersal/features/profile/profile_presentation/ui/image_capture_page.dart';
import 'package:mersal/injection_container.dart';
import 'package:mersal/resources/colors.dart';

class UploadIdScreen extends StatefulWidget {
  final bool isNewAccount;

  const UploadIdScreen({Key key, @required this.isNewAccount})
      : super(key: key);

  @override
  _UploadIdScreenState createState() => _UploadIdScreenState();
}

class _UploadIdScreenState extends State<UploadIdScreen> {
  final _formKey = GlobalKey<FormState>();
  final nameController = TextEditingController();
  final expiryDateController = TextEditingController();
  final idController = TextEditingController();
  final dobController = TextEditingController();

  final FocusNode nameFocus = new FocusNode();
  final FocusNode expiryDateFocus = new FocusNode();
  final FocusNode idFocus = new FocusNode();
  final FocusNode dobFocus = new FocusNode();
  var dob;
  ValueNotifier<DateTime> _dateTimeNotifier = ValueNotifier<DateTime>(DateTime.now());

  bool dateBool = false, expiryBool = false;

  String frontImagePath,
      backImagePath,
      dateOfBirth = "Enter DOB",
      expiryDateValue = "Enter Expiry Date",
      firstImageTitle = "Click to Upload front side of ID",
      secondImageTitle = "Click to Upload back side of ID";

  @override
  Widget build(BuildContext context) {
    return BlocProvider<DocUploadBloc>(
      create: (_) => serviceLocator<DocUploadBloc>(),
      child: Scaffold(
        backgroundColor: Color(appColors.blue),
        appBar: CustomAppBar(
          title: AppLocalizations.of(context)
              .translate("Upload ID"),
          isBackButton: true,
          showText: true,
          rightIconTwo: true,
        ),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/bg_image.jpg"),
              fit: BoxFit.fill,
            ),
          ),
          child: BlocBuilder<UserBloc, UserState>(
            builder: (context, state) {
              if (state.userDataLoadingResource != null) {
                if (state.userDataLoadingResource.status == STATUS.LOADING) {
                  return circularProgressIndicator();
                } else if (state.userDataLoadingResource.status ==
                    STATUS.SUCCESS) {
                  UserData _userData = state.userDataLoadingResource.data;

                  if(_userData.document!=null){
                    if (_userData.document.documentStatus ==
                        DOCUMENT_STATUS_NOT_APPROVED) {
                      return Center(
                        child: Text(
                          AppLocalizations.of(context)
                              .translate('Your ID is under approval') ,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 22,
                              fontWeight: FontWeight.bold),
                        ),
                      );
                    } else if (_userData.document.documentStatus ==
                        DOCUMENT_STATUS_APPROVED &&
                        _doumentNotExpired(_userData.document.idExpiryDate)) {
                      return Center(
                        child: Text(
                          AppLocalizations.of(context)
                              .translate('Your ID is already approved') ,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 22,
                              fontWeight: FontWeight.bold),
                        ),
                      );
                    }
                  }

                }
              }
              return SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(
                      height: 50,
                    ),
                    Container(
                      child: Form(
                        key: _formKey,
                        child: Column(
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 30, top: 25, right: 30, bottom: 15),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Expanded(
                                    child: InputField(
                                      controller: nameController,
                                      focusNode: nameFocus,
                                      onSubmitted: (term) {
                                        CommonMethods.inputFocusChange(context,
                                            nameFocus, expiryDateFocus);
                                      },
                                      validator: (text) {
                                        return text.isEmpty
                                            ? AppLocalizations.of(context)
                                            .translate('Name cannot be null')
                                            : null;
                                      },
                                      hint: AppLocalizations.of(context)
                                          .translate("Enter Your Name"),
                                      inputType: TextInputType.name,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 8,
                                  ),
                                  GestureDetector(
                                    onTap: () async {
                                      _getMrzData();
                                    },
                                    child: Image.asset(
                                      'assets/images/scan.png',
                                      width: 32,
                                      height: 32,
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Align(
                              alignment: Alignment.centerRight,
                              child: Container(
                                margin: EdgeInsets.only(right: 40),
                                child: Text(
                                  AppLocalizations.of(context)
                                      .translate('Scan back side of the id'),
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                showDatePickerDialog(context).then((date) {
                                  if (date != null) {
                                    dateOfBirth =
                                        DateFormat("dd/MM/yyyy").format(date);
                                    dateBool = true;
                                    dob = DateFormat("yyyy/MM/dd").format(date);
                                    setState(() {});
                                  }
                                });
                              },
                              child: Container(
                                  height: 60,
                                  margin: EdgeInsets.only(
                                      left: 30, top: 25, right: 30),
                                  padding: EdgeInsets.only(left: 20, right: 10),
                                  width: MediaQuery.of(context).size.width,
                                  alignment: Alignment.centerLeft,
                                  decoration: new BoxDecoration(
                                      shape: BoxShape.rectangle,
                                      borderRadius: BorderRadius.circular(25),
                                      color: Colors.white),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Label(
                                        title: dateOfBirth,
                                        fontSize: 16,
                                        color: Colors.black,
                                      ),
                                      Icon(
                                        Icons.date_range_rounded,
                                        color: Colors.black,
                                      ),
                                    ],
                                  )),
                            ),
                            GestureDetector(
                              onTap: () {
                                showDatePickerDialog(context).then((date) {
                                  if (date != null) {
                                    expiryDateValue =
                                        DateFormat("dd/MM/yyyy").format(date);
                                    expiryBool = true;
                                    dob = DateFormat("yyyy/MM/dd").format(date);
                                    setState(() {});
                                  }
                                });
                              },
                              child: Container(
                                  height: 60,
                                  margin: EdgeInsets.only(
                                      left: 30, top: 25, right: 30),
                                  padding: EdgeInsets.only(left: 20, right: 10),
                                  width: MediaQuery.of(context).size.width,
                                  alignment: Alignment.centerLeft,
                                  decoration: new BoxDecoration(
                                      shape: BoxShape.rectangle,
                                      borderRadius: BorderRadius.circular(25),
                                      color: Colors.white),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Label(
                                        title: expiryDateValue,
                                        fontSize: 16,
                                        color: Colors.black,
                                      ),
                                      Icon(
                                        Icons.date_range_rounded,
                                        color: Colors.black,
                                      ),
                                    ],
                                  )),
                            ),
                            Container(
                              margin:
                                  EdgeInsets.only(left: 30, top: 25, right: 30),
                              child: InputField(
                                controller: idController,
                                focusNode: idFocus,
                                validator: (text) {
                                  if (text.isEmpty) {
                                    return AppLocalizations.of(context)
                                        .translate('Id number cannot be empty');
                                  }
                                  return null;
                                },
                                onSubmitted: (term) {
                                  FocusScope.of(context).unfocus();
                                },
                                hint: AppLocalizations.of(context)
                                    .translate("Enter ID Number"),
                                inputType: TextInputType.visiblePassword,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 30, vertical: 20),
                      child: BlocBuilder<UserBloc, UserState>(
                        builder: (context, state) {
                          if (state.userDataLoadingResource != null) {
                            if (state.userDataLoadingResource.status ==
                                STATUS.LOADING) {
                              return circularProgressIndicator();
                            } else if (state.userDataLoadingResource.status ==
                                STATUS.SUCCESS) {
                              UserData _userData = state.userDataLoadingResource.data;
                             if(_userData.document!=null){
                               if (_userData.document.documentStatus ==
                                   DOCUMENT_STATUS_NOT_APPROVED) {
                                 return Text(
                                   AppLocalizations.of(context)
                                       .translate('Your ID is under approval'),
                                   style: TextStyle(
                                       color: Colors.white,
                                       fontSize: 16,
                                       fontWeight: FontWeight.bold),
                                 );
                               } else if (_userData.document.documentStatus ==
                                   DOCUMENT_STATUS_APPROVED &&
                                   _doumentNotExpired( _userData.document.idExpiryDate)) {
                                 return Text(
                                   AppLocalizations.of(context)
                                       .translate('Your ID is already approved'),
                                   style: TextStyle(
                                       color: Colors.white,
                                       fontSize: 16,
                                       fontWeight: FontWeight.bold),
                                 );
                               }
                             }
                            }
                          }
                          return Button(
                            title: AppLocalizations.of(context)
                                .translate("Click here to upload ID"),
                            color: Color(appColors.buttonColor),
                            textColor: Colors.white,
                            borderColor: Colors.transparent,
                            onTap: () async {
                              frontImagePath = '';
                              backImagePath = '';
                              List<XFile> images = await Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => ImageCapturePage(),
                                ),
                              );
                              if (images != null && images.length == 2) {
                                frontImagePath = images[0].path;
                                backImagePath = images[1].path;
                                setState(() {});
                              }
                            },
                          );
                        },
                      ),
                    ),
                    frontImagePath == null || backImagePath == null
                        ? Container()
                        : Container(
                            padding: EdgeInsets.symmetric(horizontal: 20),
                            height: 150,
                            child: ListView(
                              scrollDirection: Axis.horizontal,
                              children: [
                                Image.file(File(frontImagePath)),
                                SizedBox(
                                  width: 10,
                                ),
                                Image.file(File(backImagePath)),
                              ],
                            ),
                          ),
                    BlocListener<DocUploadBloc, DocUploadState>(
                      listener: (context, state) {
                        if (state is UploadFailedState) {
                          showSnackBarMessage(
                              context: context, message: state.failure.message);
                        } else if (state is UploadSuccessState) {
                          Navigator.of(context).pushAndRemoveUntil(
                              MaterialPageRoute(
                                builder: (context) => DocUploadPage("uploadID"),
                              ),
                              (route) => false);
                        }
                      },
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 30, vertical: 20),
                        child: BlocBuilder<DocUploadBloc, DocUploadState>(
                            builder: (context, state) {
                          if (state is UploadInitiatedState) {
                            return circularProgressIndicator();
                          }
                          return Button(
                            title:AppLocalizations.of(context)
                                .translate("Submit") ,
                            color: Color(appColors.buttonColor),
                            textColor: Colors.white,
                            borderColor: Colors.transparent,
                            onTap: () {
                              if (_formKey.currentState.validate()) {
                                final name =
                                    nameController.text.toString().trim();
                                final expDate = expiryDateValue;
                                final idNumber =
                                idController.text.toString().trim();
                                dob = dateOfBirth;
                                BlocProvider.of<DocUploadBloc>(context).add(
                                  UploadDocEvent(
                                      frontImage: frontImagePath,
                                      backImage: backImagePath,
                                      expDate: expDate,
                                      dob: dob,
                                      name: name,
                                      idNumber: idNumber,
                                      isNewAccount: false),
                                );
                              }
                            },
                          );
                        }),
                      ),
                    ),
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  Future<DateTime> showDatePickerDialog(_context) {
    Future<DateTime> selectedDate = showDatePicker(
      context: context,
      firstDate: DateTime(1900),
      initialDate: _dateTimeNotifier.value != null
          ? _dateTimeNotifier.value
          : DateTime.now(),
      lastDate: DateTime(2100),
    ).then((DateTime dateTime) => _dateTimeNotifier.value = dateTime);
    return selectedDate;
  }

  static const platform = const MethodChannel('insync.flutter.dev/mrz');
  String filePath = "";
  String fullName = "";
  String idNumber = "";
  String expiryDate = "";
  String nationality = "";
  String selectedService = "";
  String isWfbbServiceSelect = '';
  String imagepath = "";
  String isOtherCustomerWidget = 'no';

  Future<void> _getMrzData() async {
    try {
      final Map<dynamic, dynamic> mrzData =
          await platform.invokeMethod('getMrzData');
      print('mrzdata $mrzData' "");
      print(mrzData['ImagePath'].toString());
      setState(() {
        filePath = mrzData['ImagePath'].toString();
        String firstName = mrzData['FirstName'].toString();
        String middleName = mrzData['MiddleName'].toString();
        String lastName = mrzData['LastName'].toString();

        if (firstName.isNotEmpty && middleName.isNotEmpty) {
          fullName = firstName + " " + middleName;
        } else if (lastName.isNotEmpty) {
          fullName = lastName;
        }

        if (mrzData['DocumentNumber'].toString().isNotEmpty) {
          idNumber = mrzData['DocumentNumber'].toString();
        }

        if (mrzData['DateOfExpiry'].toString().isNotEmpty) {
          expiryDate = mrzData['DateOfExpiry'].toString();
          expiryDateValue = mrzData['DateOfExpiry'].toString();
        }

        if (mrzData['DateOfBirth'].toString().isNotEmpty) {
          nationality = mrzData['DateOfBirth'].toString();
        }

        nameController.text = fullName;
        idController.text = idNumber;
        expiryDateController.text = expiryDate;
        dobController.text = nationality;
        dateOfBirth = nationality;
      });
      //print(mrzData['ImagePath'].toString());
    } on PlatformException catch (e) {
      print("Failed to get mrz data : '${e.message}'");
    }
  }

  void showPictureDialog(
      BuildContext context, String imageFile, String imageSide) {
    showGeneralDialog(
      barrierLabel: "Barrier",
      barrierDismissible: true,
      barrierColor: Colors.black.withOpacity(0.8),
      transitionDuration: Duration(milliseconds: 500),
      context: context,
      pageBuilder: (_, __, ___) {
        return Align(
          alignment: Alignment.center,
          child: Container(
            height: 400,
            margin: EdgeInsets.only(left: 10, right: 10),
            child: Card(
              elevation: 20,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                      width: 250,
                      height: 250,
                      child: Image.file(
                        File(imageFile),
                        fit: BoxFit.fill,
                      )),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        width: 120,
                        height: 40,
                        margin: EdgeInsets.only(left: 10, top: 35, right: 30),
                        child: Button(
                          title: AppLocalizations.of(context)
                              .translate("Upload"),
                          textColor: Color(appColors.blue00008B),
                          borderColor: Color(appColors.blue00008B),
                          onTap: () {
                            if (imageSide == "front") {
                              frontImagePath = imageFile;
                              showSnackBarMessage(
                                  context: context,
                                  message:
                                  AppLocalizations.of(context)
                                      .translate("Front Side of Id Captured Successfully."));
                              print("FrontImagePath====$frontImagePath");
                            } else {
                              backImagePath = imageFile;
                              showSnackBarMessage(
                                  context: context,
                                  message:
                                  AppLocalizations.of(context).translate(
                                      "Back Side of Id Captured Successfully."));
                              print("backImagePath====$backImagePath");
                            }
                            setState(() {});
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        );
      },
      transitionBuilder: (_, anim, __, child) {
        return SlideTransition(
          position: Tween(begin: Offset(0, 1), end: Offset(0, 0)).animate(anim),
          child: child,
        );
      },
    );
  }

  bool _doumentNotExpired(String idExpiryDate) {
    DateTime parseDate;
    if(idExpiryDate.contains("-")){
       parseDate = new DateFormat("dd-MM-yyyy").parse(idExpiryDate);
    }
    else{
      parseDate = new DateFormat("dd/MM/yyyy").parse(idExpiryDate);
    }

    print("parseDatePrint========" + parseDate.toString());
    final notExpired = Date.today < Date.parse(parseDate.toString());
    print('is doc expired $notExpired');
    return notExpired;
  }
}
