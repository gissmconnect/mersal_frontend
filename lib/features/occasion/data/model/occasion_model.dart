import 'package:mersal/features/occasion/domain/entity/occasion_entity.dart';
import 'package:meta/meta.dart';

class OccasionModel extends OccasionEntity {

  OccasionModel(
      {@required bool status,
        @required List<OccasionDataListEntity> occasionDataList})
      : super(status: status, occasionDataList: occasionDataList);

  OccasionModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['data'] != null) {
      occasionDataList = new List<OccasionDataListEntity>();
      json['data'].forEach((v) {
        occasionDataList.add(new OccasionDataList.fromJson(v));
      });
    }
  }
}

class OccasionDataList extends OccasionDataListEntity {
  OccasionDataList(
      {int id,
        String description,
        String title,
        String link,
        String occasionDate,
        String created_at})
      : super(id: id, created_at: created_at,description: description);

  OccasionDataList.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    created_at = json['created_at'];
    description = json['description'];
    title = json['title'];
    link = json['link'];
    occasionDate = json['occassion_date'];
  }
}

