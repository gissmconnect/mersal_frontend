import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/auth/auth_domain/entity/user_data.dart';
import 'package:mersal/features/auth/auth_domain/repository/auth_repository.dart';
import 'package:mersal/features/auth/auth_domain/usecases/get_user_session.dart';
import 'package:mockito/mockito.dart';

class MockAuthRepository extends Mock implements AuthRepository {}

void main() {
  GetUserSession usecase;
  MockAuthRepository mockAuthRepository;
  setUp(() {
    mockAuthRepository = MockAuthRepository();
    usecase = GetUserSession(authRepository: mockAuthRepository);
  });


  final userData = UserData(userId: 1, name: 'test user');
  test('should get user data from the repository', () async {
    //arrange
    when(mockAuthRepository.getUserSession()).thenAnswer((_) async => Right(userData));

    //act
    final result  = await usecase(NoParams());

    //assert
    expect(result, Right(userData));
    verify(mockAuthRepository.getUserSession());
    verifyNoMoreInteractions(mockAuthRepository);
  });
}