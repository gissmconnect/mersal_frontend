import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/exceptions.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/network/network_info.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/lawati_service/data/datasource/sms_service_datasource.dart';
import 'package:mersal/features/lawati_service/domain/entity/sms_serviceInfo_entity.dart';
import 'package:mersal/features/lawati_service/domain/entity/sms_subscription_entity.dart';
import 'package:mersal/features/lawati_service/domain/repository/sms_service_repo.dart';
import 'package:mersal/features/lawati_service/domain/usecase/subscribe_sms_subscription_usecase.dart';
import 'package:meta/meta.dart';

class SMSServiceRepoImpl implements SMSServiceRepo {
  final SMSServiceDataSource smsServiceDataSource;
  final UserDataLocalDataSource userDataLocalDataSource;
  final NetworkInfo networkInfo;

  const SMSServiceRepoImpl(
      {@required this.smsServiceDataSource,
      @required this.userDataLocalDataSource,
      @required this.networkInfo});

  @override
  Future<Either<Failure, SMSServiceInfoEntity>> getSMSServiceInfo() async {
    if (await networkInfo.isConnected) {
      final _token = await userDataLocalDataSource.getAuthToken();
      try {
        final _service =
            await smsServiceDataSource.getServiceInfo(token: _token);
        return Right(_service);
      } on ServerException catch (ex) {
        return Left(ServerFailure()..message = ex.message);
      } on AuthException catch (ex) {
        return Left(AuthFailure()..message = ex.message);
      } on Exception {
        return Left(ServerFailure());
      }
    } else {
      return Left(NetworkFailure());
    }
  }

  @override
  Future<Either<Failure, List<SmsSubscriptionEntity>>>
      getSubscriptions() async {
    if (await networkInfo.isConnected) {
      final _token = await userDataLocalDataSource.getAuthToken();
      try {
        final _service =
            await smsServiceDataSource.getSubscriptions(token: _token);
        return Right(_service);
      } on ServerException catch (ex) {
        return Left(ServerFailure()..message = ex.message);
      } on AuthException catch (ex) {
        return Left(AuthFailure()..message = ex.message);
      } on Exception {
        return Left(ServerFailure());
      }
    } else {
      return Left(NetworkFailure());
    }
  }

  @override
  Future<Either<Failure, dynamic>> subscribe(
      {SubscriptionRequestParams params}) async {
    if (await networkInfo.isConnected) {
      final _token = await userDataLocalDataSource.getAuthToken();
      try {
        final _service = await smsServiceDataSource.subscribe(token: _token, params: params);
        return Right(_service);
      } on ServerException catch (ex) {
        return Left(ServerFailure()..message = ex.message);
      } on AuthException catch (ex) {
        return Left(AuthFailure()..message = ex.message);
      } on Exception {
        return Left(ServerFailure());
      }
    } else {
      return Left(NetworkFailure());
    }
  }

  @override
  Future<Either<Failure, SmsSubscriptionEntity>> getMySubscription() async {
    if (await networkInfo.isConnected) {
      final _token = await userDataLocalDataSource.getAuthToken();
      try {
        final _service =
            await smsServiceDataSource.getMySubscription(token: _token);
        return Right(_service);
      } on ServerException catch (ex) {
        return Left(ServerFailure()..message = ex.message);
      } on AuthException catch (ex) {
        return Left(AuthFailure()..message = ex.message);
      } on Exception {
        return Left(ServerFailure());
      }
    } else {
      return Left(NetworkFailure());
    }
  }

  @override
  Future<Either<Failure, String>> renewSubscription(
      {int subscriptionId}) async {
    if (await networkInfo.isConnected) {
      final _token = await userDataLocalDataSource.getAuthToken();
      try {
        final _service = await smsServiceDataSource.renewSubscription(
            token: _token, subscriptionId: subscriptionId);
        return Right(_service);
      } on ServerException catch (ex) {
        return Left(ServerFailure()..message = ex.message);
      } on AuthException catch (ex) {
        return Left(AuthFailure()..message = ex.message);
      } on Exception {
        return Left(ServerFailure());
      }
    } else {
      return Left(NetworkFailure());
    }
  }
}
