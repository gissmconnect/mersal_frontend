import 'package:mersal/features/child_service/child-data/model/child-enroll-model/registered-child-activity/child_activity_model.dart';
import 'package:mersal/features/child_service/child-data/model/child-enroll-model/registered-child-details/child_details_model.dart';
import 'package:meta/meta.dart';

abstract class ChildDataSource {
  Future<ChildDetailsModel> getChildDetails({@required String token});

  Future<ChildActivityModel> getActivity({@required String token});

  Future<void> enrollChild(
      {@required String token,
      @required int electionId,
      @required int candidateId});
}
