import 'package:mersal/features/committee_management/domain/entity/committe_entity.dart';
import 'package:meta/meta.dart';

class CommitteeModel extends CommitteeEntity {
  CommitteeModel(
      {@required int id, @required String name, @required String icon, @required String description})
      : super(id: id, name: name, icon: icon, description: description);

  CommitteeModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    icon = json['icon'];
    description = json['description'];
  }

  Map<String, dynamic> toJSON() {
    return {'id': id, 'name': name, 'icon': icon, 'description': description};
  }
}
