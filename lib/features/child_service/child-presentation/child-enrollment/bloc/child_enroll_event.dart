part of 'child_enroll_bloc.dart';

abstract class ChildEnrollmentEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class GetActivityEvent extends ChildEnrollmentEvent {}

class GetChildDetailsEvent extends ChildEnrollmentEvent {}

class ChildEvent extends ChildEnrollmentEvent {
  final String name, age;

  ChildEvent({@required this.name, @required this.age});
}
