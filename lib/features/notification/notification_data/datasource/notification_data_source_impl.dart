import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:mersal/core/network/api_endpoints.dart';
import 'package:mersal/core/network/http_call.dart';
import 'package:mersal/core/network/http_error_handler.dart';
import 'package:mersal/features/notification/notification_data/datasource/notification_data_source.dart';
import 'package:mersal/features/notification/notification_data/model/notification_model.dart';
import 'package:meta/meta.dart';

class NotificationDataSourceImpl implements NotificationDataSource {
  final http.Client httpClient;

  NotificationDataSourceImpl({@required this.httpClient});

  @override
  Future<List<NotificationModel>> getNotifications({String token}) async {
    final _params = <String, String>{'service_id': '0'};
    final response = await httpPostRequest(
        httpClient: httpClient,
        url: NOTIFICATION_END_POINT,
        token: token,
        params: _params);
    if (response.statusCode == 200) {
      final _responseJsonList = json.decode(response.body)['data']['notifications'] as List<dynamic>;
      final _notifications =    _responseJsonList.map((e) => NotificationModel.fromJson(e)).toList();
      return _notifications;
    } else {
      handleError(response);
    }
  }

  @override
  Future<String> archiveNotification({String token, int id}) async {
    final _params = <String, String>{'id': "$id"};
    print('params $_params');
    final response = await httpPostRequest(
        httpClient: httpClient,
        url: NOTIFICATION_ARCHIVE_ENDPOINT,
        token: token,
        params: _params);
    print('archive response ${response.body}');
    if (response.statusCode == 200) {
      final Map<String, dynamic> _responseJson = json.decode(response.body);
      return _responseJson['message'];
    } else {
      handleError(response);
    }
  }

  @override
  Future<String> deleteNotification({String token, int id}) async {
    final _params = <String, String>{'id': "$id"};
    print('params $_params');
    final response = await httpPostRequest(
        httpClient: httpClient,
        url: NOTIFICATION_DELETE_ENDPOINT,
        token: token,
        params: _params);
    print('delete response ${response.body}');
    if (response.statusCode == 200) {
      final Map<String, dynamic> _responseJson = json.decode(response.body);
      return _responseJson['message'];
    } else {
      handleError(response);
    }
  }
}
