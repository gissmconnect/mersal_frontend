import 'package:mersal/features/calendar_screen/calendar-data/model/PrayerDataModel.dart';
import 'package:meta/meta.dart';

abstract class CalendarDataSource {
  Future<List<PrayerDataModel>> getPrayer({@required String token});

  Future<void> createAlarm({@required String token, @required int prayerId});
}
