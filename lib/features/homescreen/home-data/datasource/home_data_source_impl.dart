import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:mersal/core/network/api_endpoints.dart';
import 'package:mersal/core/network/http_error_handler.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/banner/banner-domain/entity/banner_entity.dart';
import 'package:mersal/features/homescreen/home-data/datasource/home_data_source.dart';
import 'package:mersal/features/homescreen/home-data/model/banner/BannerListDataModel.dart';
import 'package:meta/meta.dart';

class HomeDataSourceImpl implements HomeDataSource {
  final http.Client httpClient;

  HomeDataSourceImpl({@required this.httpClient});

  @override
  Future<List<BannerEntity>> getBanners({String token}) async {
    var response = await httpClient.get(
      Uri.parse(GET_BANNER_LIST),
      headers: {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        AUTHORIZATION: BEARER + " " + token
      },
    );
    if (response.statusCode == 200) {
      return ((json.decode(response.body)['data']) as List<dynamic>)
          .map((e) => BannerListDataModel.fromJson(e))
          .toList();
    }
    handleError(response);
  }

}
