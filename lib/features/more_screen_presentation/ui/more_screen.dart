import 'package:flutter/material.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/features/child_service/child-presentation/child-registration/ui/child_registration.dart';
import 'package:mersal/features/committee_management/presentation/management/ui/committe_management.dart';
import 'package:mersal/features/profile/profile_presentation/ui/profile_screen.dart';
import 'package:mersal/features/settings_presentation/presentation/ui/settings_screen.dart';

class MoreScreen extends StatefulWidget {
  @override
  _MoreScreenState createState() => _MoreScreenState();
}

class _MoreScreenState extends State<MoreScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/images/bg_image.jpg"),
          fit: BoxFit.fill,
        ),
      ),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GestureDetector(
              onTap: () {
                CommonMethods().animatedNavigation(
                    context: context,
                    currentScreen: MoreScreen(),
                    landingScreen: ProfileScreen());
              },
              child: Container(
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.only(left: 20, top: 20, bottom: 10),
                child: Label(
                  title: "Profile",
                  color: Colors.white,
                  fontWeight: FontWeight.w600,
                  fontSize: 18,
                ),
              ),
            ),
            Divider(
              thickness: 1,
              color: Colors.black,
            ),
            GestureDetector(
              onTap: () {
                CommonMethods().animatedNavigation(
                    context: context,
                    currentScreen: MoreScreen(),
                    landingScreen: CommitteeManagement());
              },
              child: Container(
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.only(left: 20, top: 10, bottom: 10),
                child: Label(
                  title: "Committee Management",
                  color: Colors.white,
                  fontWeight: FontWeight.w600,
                  fontSize: 18,
                ),
              ),
            ),
            Divider(
              thickness: 1,
              color: Colors.black,
            ),
            GestureDetector(
              onTap: () {
                CommonMethods().animatedNavigation(
                    context: context,
                    currentScreen: MoreScreen(),
                    landingScreen: SettingsScreen());
              },
              child: Container(
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.only(left: 20, top: 10, bottom: 10),
                child: Label(
                  title: "Settings",
                  color: Colors.white,
                  fontWeight: FontWeight.w600,
                  fontSize: 18,
                ),
              ),
            ),
            Divider(
              thickness: 1,
              color: Colors.black,
            ),
            GestureDetector(
              onTap: () {
                CommonMethods().animatedNavigation(
                    context: context,
                    currentScreen: MoreScreen(),
                    landingScreen: ChildRegistration());
              },
              child: Container(
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.only(left: 20, top: 10, bottom: 10),
                child: Label(
                  title: "Child Registration",
                  color: Colors.white,
                  fontWeight: FontWeight.w600,
                  fontSize: 18,
                ),
              ),
            ),
            Divider(
              thickness: 1,
              color: Colors.black,
            ),
          ],
        ),
      ),
    );
  }
}
