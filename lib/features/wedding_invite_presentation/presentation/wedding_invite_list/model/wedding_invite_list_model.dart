
import 'package:mersal/features/homescreen/home-domain/entity/BannerDataEntity.dart';
import 'package:mersal/features/wedding_invite_presentation/presentation/wedding_invite_list/model/wedding_invite_list_data.dart';

class WeddingInviteDataModel {
  List<WeddingInviteListData> weddingInviteList;
  bool status;

  WeddingInviteDataModel({this.weddingInviteList,this.status});

  factory WeddingInviteDataModel.fromJson(Map<String, dynamic> json) {
    return WeddingInviteDataModel(
      weddingInviteList: json['data'] != null ? (json['data'] as List).map((i) => WeddingInviteListData.fromJson(i)).toList() : null,
      status: json['status'],
    );
  }
}