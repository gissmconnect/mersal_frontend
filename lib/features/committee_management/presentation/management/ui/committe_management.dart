import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mersal/core/localization/app_localization.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/core/widgets/message.dart';
import 'package:mersal/features/committee_management/domain/entity/committe_entity.dart';
import 'package:mersal/features/committee_management/presentation/management/bloc/committe_management_bloc.dart';
import 'package:mersal/features/committee_management/presentation/member/ui/committee_member.dart';
import 'package:mersal/injection_container.dart';
import 'package:mersal/resources/colors.dart';

class CommitteeManagement extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<CommitteManagementBloc>(
      create: (_) => serviceLocator<CommitteManagementBloc>(),
      child: Scaffold(
        body: _CommitteeManagementBody(),
      ),
    );
  }
}

class _CommitteeManagementBody extends StatefulWidget {
  @override
  _CommitteeManagementBodyState createState() =>
      _CommitteeManagementBodyState();
}

class _CommitteeManagementBodyState extends State<_CommitteeManagementBody> {
  @override
  void initState() {
    BlocProvider.of<CommitteManagementBloc>(context).add(GetCommitteesEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/images/bg_image.jpg"),
          fit: BoxFit.fill,
        ),
      ),
      child: BlocListener<CommitteManagementBloc, CommitteManagementState>(
        listener: (context, state) {
          if (state is CommitteeLoadingFailure) {
            showSnackBarMessage(
                context: context, message: state.failure.message);
          }
        },
        listenWhen: (oldState, newState) {
          return oldState != newState;
        },
        child: BlocBuilder<CommitteManagementBloc, CommitteManagementState>(
          builder: (context, state) {
            if (state is CommitteeLoadedState) {
              final _committees = state.committees;
              return Column(
                children: [
                  SizedBox(
                    height: 60,
                  ),
                  Stack(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(bottom: 5, left: 15),
                        alignment: Alignment.bottomLeft,
                        child: GestureDetector(
                          child: Icon(
                            Icons.arrow_back_ios_rounded,
                            color: Colors.white,
                          ),
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(bottom: 6),
                        alignment: Alignment.bottomCenter,
                        child: Text(
                          AppLocalizations.of(context)
                              .translate("Allawati Committee"),
                          textAlign: TextAlign.end,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 20, top: 30),
                    child: Label(
                      title: AppLocalizations.of(context)
                          .translate("Click on Icon to Select Committee"),
                      textAlign: TextAlign.center,
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                      color: Color(appColors.buttonColor),
                    ),
                  ),
                  Expanded(
                    child: GridView.builder(
                      padding: EdgeInsets.only(top: 7),
                      shrinkWrap: true,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                      ),
                      itemCount: _committees.length,
                      itemBuilder: (BuildContext context, int index) {
                        return menuGridItem(_committees[index]);
                      },
                    ),
                  ),
                ],
              );
            } else if (state is CommitteeLoadingState) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            return Container();
          },
        ),
      ),
    );
  }

  Widget menuGridItem(CommitteeEntity committeeEntity) {
    return GestureDetector(
      onTap: () {
        CommonMethods().animatedNavigation(
            context: context,
            landingScreen: CommitteeMember(
              committeeName: committeeEntity.name,
              id: committeeEntity.id,
              description: committeeEntity.description,
            ),
            currentScreen: CommitteeManagement());
      },
      child: Container(
          margin: EdgeInsets.only(bottom: 10, top: 10, left: 10, right: 10),
          decoration: new BoxDecoration(
            shape: BoxShape.rectangle,
            color: Colors.white,
            borderRadius: BorderRadius.circular(25.0),
            border: Border.all(color: Colors.black, width: 2),
          ),
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CircleAvatar(
                backgroundImage:
                    CachedNetworkImageProvider(committeeEntity.icon),
                radius: 60,
              ),
              SizedBox(height: 10,),
              Label(
                title: committeeEntity.name,
                textAlign: TextAlign.center,
                fontSize: 15,
                fontWeight: FontWeight.w600,
                color: Color(appColors.buttonColor),
              ),
            ],
          )),
    );
  }
}
