class CommitteeMemberEntity {
  Member member;
  String quote, otherInfo;

  CommitteeMemberEntity({this.member, this.quote, this.otherInfo});
}

class Member {
  int id, userId, documentStatus;
  String name,
      userName,
      email,
      phone,
      gender,
      bloodGroup,
      dob,
      address,
      image;
  Document document;
  bool addService,isVendor;

  Member(
      {this.id,
      this.userId,
      this.documentStatus,
      this.name,
      this.userName,
      this.email,
      this.phone,
      this.gender,
      this.bloodGroup,
      this.dob,
      this.address,
      this.image,
      this.isVendor,
      this.document,
      this.addService});
}

class Document {
  int id, documentStatus;
  String name, number, dob, expDate, frontImage, backImage;

  Document(
      {this.id,
      this.documentStatus,
      this.name,
      this.number,
      this.dob,
      this.expDate,
      this.frontImage,
      this.backImage});
}
