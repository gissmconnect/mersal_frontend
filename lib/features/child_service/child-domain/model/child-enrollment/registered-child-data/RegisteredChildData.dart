
import 'package:mersal/features/child_service/child-domain/model/child-enrollment/registered-child-data/RegisteredChildRelation.dart';

class RegisteredChildData {
  RegisteredChildRelation registeredChildRelation;
  int id;
  String name;
  String dob;

  RegisteredChildData({this.registeredChildRelation, this.id, this.name, this.dob});

  factory RegisteredChildData.fromJson(Map<String, dynamic> json) {
    return RegisteredChildData(
      registeredChildRelation: json['relation'] != null
          ? RegisteredChildRelation.fromJson(json['relation'])
          : null,
      id: json['id'],
      dob: json['dob'],
      name: json['name'],
    );
  }
}
