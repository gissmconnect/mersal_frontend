import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:mersal/core/network/api_endpoints.dart';
import 'package:mersal/core/network/http_error_handler.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/auth/auth_data/model/user_data_model.dart';
import 'package:mersal/features/profile/edit_profile_data/data_source/edit_profile_data_source.dart';
import 'package:meta/meta.dart';

class EditProfileDataSourceImpl implements EditProfileDataSource {
  final http.Client httpClient;

  EditProfileDataSourceImpl({@required this.httpClient});

  @override
  Future<UserDataModel> editProfile(
      {String token,
      String name,
      String dob,
      String image,
      int gender,
      String bloodGroup}) async {
    final _params = <String, String>{
      'name': name,
      'dob': dob,
      'blood_group': bloodGroup,
      'image': image,
      'gender': gender.toString()
    };
    var response = await httpClient.post(Uri.parse(UPDATE_PROFILE_END_POINT),
        headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json',
          AUTHORIZATION: BEARER + " " + token
        },
        body: json.encode(_params));
    print('edit profile response ${response.body}');
    if (response.statusCode == 200) {
      var responseBody = jsonDecode(response.body);
      return UserDataModel.fromJson(responseBody['data']);
    }
    handleError(response);
  }
}
