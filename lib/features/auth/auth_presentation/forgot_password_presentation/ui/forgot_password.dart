import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mersal/core/localization/app_localization.dart';
import 'package:mersal/core/widgets/common/Button.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/CustomAppBar.dart';
import 'package:mersal/core/widgets/common/InputField.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/features/auth/auth_presentation/otp_presentation/bloc/otp_bloc.dart';
import 'package:mersal/features/country_listing/presentation/ui/country_listing.dart';
import 'package:mersal/features/reset_password/presentation/reset_password.dart';

import '../../../../../injection_container.dart';

class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<OtpBloc>(
      create: (_) => serviceLocator<OtpBloc>(),
      child: Scaffold(
          appBar: CustomAppBar(
            title: AppLocalizations.of(context).translate("Forgot Password"),
            isBackButton: true,
            showText: true,
            rightIconTwo: true,
            rightIconOne: true,
          ),
          body: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/bg_image.jpg"),
                fit: BoxFit.cover,
              ),
            ),
            child: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("assets/images/login_bg.jpg"),
                    fit: BoxFit.cover,
                  ),
                ),
                child: ForgotPasswordBody()),
          )),
    );
  }
}

class ForgotPasswordBody extends StatefulWidget {
  @override
  _ForgotPasswordBodyState createState() => _ForgotPasswordBodyState();
}

class _ForgotPasswordBodyState extends State<ForgotPasswordBody> {
  final phoneController = TextEditingController();
  final otpController = TextEditingController();
  bool changeBorderBool = false;
  bool obscureText = true;
  final FocusNode phoneFocus = new FocusNode();
  final FocusNode otpFocus = new FocusNode();
  final _formKey = GlobalKey<FormState>();
  String countryCode = "+968";
  FToast fToast;

  @override
  void initState() {
    super.initState();
    fToast = FToast();
    fToast.init(context);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: ListView(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Form(
                  key: _formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 30, top: 25, right: 30),
                        child: TextFormField(
                          controller: phoneController,
                          focusNode: phoneFocus,
                          maxLength: 20,
                          onFieldSubmitted: (term) {
                            CommonMethods.inputFocusChange(
                                context, phoneFocus, otpFocus);
                          },
                          validator: (text) {
                            return text.isEmpty
                                ?AppLocalizations.of(context).translate( 'Phone number cannot be empty')
                                : text.length < 8
                                ?
                            AppLocalizations.of(context).translate('Phone number should be min 8 digit')
                            // :text.length>10
                            // ? 'Phone number should be max 10 digit'
                                : null;
                          },
                          decoration: InputDecoration(
                            fillColor: Colors.white,
                            counterText: "",
                            filled: true,
                            hintText: AppLocalizations.of(context).translate("Enter Phone Number"),
                            contentPadding: const EdgeInsets.only( bottom: 15.0, top: 15.0,right: 10),
                            hintStyle: TextStyle(color: Colors.black),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(25.7),
                            ),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(25.7),
                            ),
                            focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.red,
                                )),
                            errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.red,
                                )),
                            prefixIcon: Container(
                              width: 100,
                              child: Row(
                                children: [
                                  GestureDetector(
                                    onTap: () {
                                      _countryCodeButtonTapped();
                                    },
                                    child: Container(
                                      margin: EdgeInsets.only(left: 10),
                                      child: Label(
                                        title: countryCode,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.black,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 10),
                                    width: 1,
                                    height: 30,
                                    color: Colors.black,
                                  )
                                ],
                              ),
                            ),
                          ),
                          keyboardType: TextInputType.phone,
                        ),
                      ),
                      /*BlocListener<OtpBloc, OtpState>(
                        listener: (context, state) {
                          if (state is OtpError) {
                            CommonMethods.showToast(
                                fToast: fToast,
                                message: state.failure.message,
                                status: false);
                          } else if (state is OtpRequestSuccessState) {
                            CommonMethods.showToast(
                                fToast: fToast,
                                message: state.message,
                                status: true);
                          }
                        },
                        child: BlocBuilder<OtpBloc, OtpState>(
                          builder: (context, state) {
                            if (state is OtpRequesting) {
                              return Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  CircularProgressIndicator(),
                                ],
                              );
                            } else {
                              return GestureDetector(
                                onTap: () {
                                  final mobileNumber =
                                      phoneController.text.toString().trim();
                                  if (mobileNumber.isNotEmpty) {
                                    _sendOtp(mobileNumber);
                                  } else {
                                    CommonMethods.showToast(
                                        fToast: fToast,
                                        message: "Phone number is required",
                                        status: false);
                                  }
                                },
                                child: Container(
                                    margin: EdgeInsets.only(right: 30, top: 10),
                                    alignment: Alignment.centerRight,
                                    child: Label(
                                        title:
                                        AppLocalizations.of(context).translate("Send OTP")
                                        ,
                                        color: Colors.black,
                                        fontSize: 14)),
                              );
                            }
                          },
                        ),
                      ),*/
                    ],
                  ),
                ),
                BlocListener<OtpBloc, OtpState>(
                  listener: (context, state) {
                    if (state is OtpError) {
                      CommonMethods.showToast(
                          fToast: fToast,
                          message: state.failure.message,
                          status: false);
                    } else if (state is OtpRequestSuccessState) {
                      final phone = phoneController.text.toString().trim();
                      CommonMethods.showToast(
                          fToast: fToast,
                          message: state.message,
                          status: true);
                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                          builder: (context) =>
                              ResetPassword(phoneValue: phone),
                        ),
                      );
                    }
                  },
                  child: BlocBuilder<OtpBloc, OtpState>(
                    builder: (context, state) {
                      if (state is OtpRequesting) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            CircularProgressIndicator(),
                          ],
                        );
                      } else {
                        return
                          Container(
                            height: 60,
                            margin: EdgeInsets.only(left: 30, top: 35, right: 30),
                            child: Button(
                              title: AppLocalizations.of(context).translate("Submit"),
                              textColor: Colors.white,
                              borderColor: Colors.transparent,
                              color: Colors.black87,
                              onTap: () {
                                if (_formKey.currentState.validate()) {
                                  final mobileNumber =  phoneController.text.toString().trim();
                                  if (mobileNumber.isNotEmpty) {
                                    _sendOtp(mobileNumber);
                                  } else {
                                    CommonMethods.showToast(
                                        fToast: fToast,
                                        message: "Phone number is required",
                                        status: false);
                                  }
                                  // final phone = phoneController.text.toString().trim();
                                  // // final otp = otpController.text.toString().trim();
                                  // Navigator.pushReplacement(
                                  //   context,
                                  //   MaterialPageRoute(
                                  //     builder: (context) =>
                                  //         ResetPassword(phoneValue: phone),
                                  //   ),
                                  // );
                                }
                              },
                            ),
                          );
                      }
                    },
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Future _countryCodeButtonTapped() async {
    Map results = await Navigator.of(context)
        .push(new MaterialPageRoute(builder: (BuildContext context) {
      return new CountryListing();
    }));
    if (results != null && results.containsKey('country_id')) {
      setState(() {
        countryCode = "+" + results['country_code'].toString();
      });
    }
  }

  void _sendOtp(String phoneNumber) {
    BlocProvider.of<OtpBloc>(context)
        .add(OtpRequestEvent(phoneNumber: phoneNumber));
  }
}
