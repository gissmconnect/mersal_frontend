
part of 'wedding_invite_template_bloc.dart';

abstract class WeddingInviteTemplateEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class GetTemplateEvent extends WeddingInviteTemplateEvent {}
