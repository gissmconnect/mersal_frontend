import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:mersal/core/error/exceptions.dart';
import 'package:mersal/core/network/api_endpoints.dart';
import 'package:mersal/core/network/http_call.dart';
import 'package:mersal/core/network/http_error_handler.dart';
import 'package:mersal/features/committee_management/data/datasource/committee_datasource.dart';
import 'package:mersal/features/committee_management/data/model/committee_member_model.dart';
import 'package:mersal/features/committee_management/data/model/committee_model.dart';
import 'package:mersal/features/committee_management/domain/entity/committe_entity.dart';
import 'package:meta/meta.dart';

class CommitteeDataSourceImpl implements CommitteeDataSource {
  final http.Client client;

  CommitteeDataSourceImpl({@required this.client});

  @override
  Future<List<CommitteeEntity>> getCommittees({String token}) async {
    final _response = await httpGetRequest(
        httpClient: client, url: COMMITTEES_ENDPOINT, token: token);

    if (_response.statusCode == 200) {
      final Map<String, dynamic> _responseJson = json.decode(_response.body);
      if (_responseJson.containsKey('data')) {
        return ((json.decode(_response.body)['data']) as List<dynamic>)
            .map((e) => CommitteeModel.fromJson(e))
            .toList();
      } else {
        throw ServerException()..message = _responseJson['message'];
      }
    } else {
      handleError(_response);
    }
  }

  @override
  Future<List<CommitteeMemberModel>> getCommitteeMember(
      {String token, int committeeId}) async {
    final _response = await httpGetRequest(
        httpClient: client,
        url: COMMITTEE_MEMBER_ENDPOINT + "?committee_id=$committeeId",
        token: token);

    print('url ${COMMITTEE_MEMBER_ENDPOINT + "?committee_id=$committeeId"}');
    print('getCommitteeMember response ${_response.body}');
    if (_response.statusCode == 200) {
      final Map<String, dynamic> _responseJson = json.decode(_response.body);
      if (_responseJson.containsKey('data')) {
        final _res = json.decode(_response.body);
        print('getCommitteeMember response ${_res}');
        return ((json.decode(_response.body)['data']) as List<dynamic>)
            .map((e) => CommitteeMemberModel.fromJson(e))
            .toList();
      } else {
        throw ServerException()..message = _responseJson['message'];
      }
    } else {
      handleError(_response);
    }
  }
}
