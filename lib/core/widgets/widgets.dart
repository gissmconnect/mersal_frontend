import 'package:flutter/material.dart';

Widget circularProgressIndicator() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      CircularProgressIndicator(),
    ],
  );
}

void showProgressDialog(BuildContext context) async {
  await showDialog(
    context: context,
    builder: (context) => AlertDialog(
      content: Row(
        children: [
          CircularProgressIndicator(),
          SizedBox(
            width: 40,
          ),
          Text('Please wait'),
        ],
      ),
    ),
  );
}
