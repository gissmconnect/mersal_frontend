import 'package:flutter/material.dart';
import 'package:mersal/resources/colors.dart';
import 'package:mersal/resources/fonts.dart';

class BigLabel extends StatelessWidget {

  final String title;
  final TextAlign textAlign;

  BigLabel({ this.title,this.textAlign=TextAlign.left});

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      textAlign:textAlign,
      style: TextStyle(
          color: Color(appColors.blue00008B),
          fontSize:25,
          fontFamily: fonts.segoeui,
          fontWeight: FontWeight.w600
      ),
    );
  }
}

