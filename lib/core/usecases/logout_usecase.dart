import 'package:meta/meta.dart';
import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/auth/auth_domain/repository/auth_repository.dart';

class LogoutUseCase extends UseCase<void,NoParams>{
  final AuthRepository authRepository;

  LogoutUseCase({@required this.authRepository});
  @override
  Future<Either<Failure, void>> call(NoParams params) async {
    return await authRepository.logout();
  }
}