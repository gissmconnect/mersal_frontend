part of 'election_candidate_bloc.dart';

abstract class ElectionCandidateEvent extends Equatable {
  const ElectionCandidateEvent();
}

class GetElectionCandidatesEvent extends ElectionCandidateEvent {
  final int electionId;

  GetElectionCandidatesEvent({@required this.electionId});

  @override
  List<Object> get props => [electionId];
}

class VoteEvent extends ElectionCandidateEvent {
  final int electionId, candidateId;

  VoteEvent({@required this.electionId, @required this.candidateId});

  @override
  List<Object> get props => [electionId, candidateId];
}
