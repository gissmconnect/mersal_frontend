import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:mersal/core/localization/app_localization.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/core/widgets/common/shared_preference.dart';
import 'package:mersal/features/child_service/child-presentation/child-enrollment/ui/child_enrollment.dart';
import 'package:mersal/features/election/election-presentation/ui/election_page.dart';
import 'package:mersal/features/lawati_service/lawati_service_option.dart';
import 'package:mersal/features/survey/presentation/ui/survey_main_page.dart';
import 'package:mersal/features/weddingBooking/ui/wedding_booking.dart';
import 'package:mersal/features/wedding_invite_presentation/presentation/ui/wedding_invite.dart';
import 'package:mersal/resources/colors.dart';

class ServicesScreen extends StatefulWidget {
  @override
  _ServicesScreenState createState() => _ServicesScreenState();
}

class _ServicesScreenState extends State<ServicesScreen> {

  String gender;
  var documentStatus, dob,userAge;

  @override
  void initState() {
    super.initState();
    _asyncMethod();

  }

  _asyncMethod() async {
    gender = await appPreferences.getStringPreference("Gender");
    print("genderPrint========" + gender.toString());
    dob = await appPreferences.getStringPreference("UserDOB");
    documentStatus = await appPreferences.getStringPreference("DocumentStatus");
    print("documentStatusPrint========" + documentStatus.toString());
    print("dobPrint========" + dob.toString());


    DateTime parseDate = new DateFormat("dd/MM/yyyy").parse(dob);
    print("userAgePrint========" + parseDate.toString());

    var inputDate = DateTime.parse(parseDate.toString());
    userAge = calculateAge(inputDate);
    print("userAgePrint========" + userAge.toString());
    if(userAge==null){
      userAge=0;
    }
    setState(() {

    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/images/bg_image.jpg"),
          fit: BoxFit.fill,
        ),
      ),
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 20, bottom: 10),
            child: Label(
              title: AppLocalizations.of(context).translate("Services"),
              textAlign: TextAlign.center,
              fontSize: 18,
              fontWeight: FontWeight.w600,
              color: Colors.white,
            ),
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.only(bottom: 20),
              margin: EdgeInsets.only(left: 10, right: 10),
              child: ListView(
                shrinkWrap: true,
                children: [
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        flex: 1,
                        child: GestureDetector(
                          onTap: () {
                            CommonMethods().animatedNavigation(
                                context: context,
                                currentScreen: ServicesScreen(),
                                landingScreen: ElectionPage());
                          },
                          child: Container(
                            padding: EdgeInsets.only(bottom: 15),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(15)),
                            alignment: Alignment.center,
                            margin: EdgeInsets.only(
                              right: 20,
                              left: 10,
                              bottom: 20,
                            ),
                            child: Column(
                              children: [
                                SvgPicture.asset(
                                  "assets/images/vote_icon.svg",
                                  width: 100,
                                  height: 100,
                                ),
                                Label(
                                  textAlign: TextAlign.center,
                                  title: AppLocalizations.of(context).translate("Poll"),
                                  fontSize: 15,
                                  color: Color(appColors.blue00008B),
                                  fontWeight: FontWeight.bold,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: GestureDetector(
                          onTap: () {
                            CommonMethods().animatedNavigation(
                                context: context,
                                currentScreen: ServicesScreen(),
                                landingScreen: LawatiServiceOption());
                          },
                          child: Container(
                            padding: EdgeInsets.only(bottom: 15),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(15)),
                            alignment: Alignment.center,
                            margin: EdgeInsets.only(
                              right: 20,
                              left: 10,
                              bottom: 20,
                            ),
                            child: Column(
                              children: [
                                SvgPicture.asset(
                                  "assets/images/sms_service_icon.svg",
                                  width: 100,
                                  height: 100,
                                ),
                                Label(
                                  textAlign: TextAlign.center,
                                  title: AppLocalizations.of(context).translate("Lawati SMS Service"),
                                  fontSize: 15,
                                  color: Color(appColors.blue00008B),
                                  fontWeight: FontWeight.bold,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        flex: 1,
                        child: GestureDetector(
                          onTap: () {
                            CommonMethods().animatedNavigation(
                                context: context,
                                currentScreen: ServicesScreen(),
                                landingScreen: WeddingBooking());
                          },
                          child: Container(
                            padding: EdgeInsets.only(bottom: 15),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(15)),
                            alignment: Alignment.center,
                            margin: EdgeInsets.only(
                              right: 20,
                              left: 10,
                              bottom: 20,
                            ),
                            child: Column(
                              children: [
                                SvgPicture.asset(
                                  "assets/images/wedding_booking.svg",
                                  width: 100,
                                  height: 100,
                                ),
                                Label(
                                  textAlign: TextAlign.center,
                                  title: AppLocalizations.of(context).translate("Wedding Booking"),
                                  fontSize: 15,
                                  color: Color(appColors.blue00008B),
                                  fontWeight: FontWeight.bold,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: gender=="Male"?GestureDetector(
                          onTap: () {
                            CommonMethods().animatedNavigation(
                                context: context,
                                currentScreen: ServicesScreen(),
                                landingScreen: WeddingInvite());
                          },
                          child: Container(
                            padding: EdgeInsets.only(bottom: 15),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(15)),
                            alignment: Alignment.center,
                            margin: EdgeInsets.only(
                              right: 20,
                              left: 10,
                              bottom: 20,
                            ),
                            child: Column(
                              children: [
                                SvgPicture.asset(
                                  "assets/images/wedding_invitation.svg",
                                  width: 100,
                                  height: 100,
                                ),
                                Label(
                                  textAlign: TextAlign.center,
                                  title: AppLocalizations.of(context).translate("Wedding Invitation"),
                                   fontSize: 15,
                                  color: Color(appColors.blue00008B),
                                  fontWeight: FontWeight.bold,
                                ),
                              ],
                            ),
                          ),
                        ):Container(),
                      ),
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        flex: 1,
                        child: GestureDetector(
                          onTap: () {
                            CommonMethods().animatedNavigation(
                                context: context,
                                currentScreen: ServicesScreen(),
                                landingScreen: SurveyMainPage());
                          },
                          child: Container(
                            padding: EdgeInsets.only(bottom: 15),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(15)),
                            alignment: Alignment.center,
                            margin: EdgeInsets.only(
                              right: 20,
                              left: 10,
                              bottom: 20,
                            ),
                            child: Column(
                              children: [
                                SvgPicture.asset(
                                  "assets/images/survey_icon.svg",
                                  width: 100,
                                  height: 100,
                                ),
                                Label(
                                  textAlign: TextAlign.center,
                                  title: AppLocalizations.of(context).translate("Participate in Survey"),
                                  fontSize: 15,
                                  color: Color(appColors.blue00008B),
                                  fontWeight: FontWeight.bold,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child:(userAge!=null && userAge>20) && documentStatus=="2"? GestureDetector(
                          onTap: () {
                            CommonMethods().animatedNavigation(
                                context: context,
                                currentScreen: ServicesScreen(),
                                landingScreen: ChildEnrollment());
                          },
                          child: Container(
                            padding: EdgeInsets.only(bottom: 15),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(15)),
                            alignment: Alignment.center,
                            margin: EdgeInsets.only(
                              right: 20,
                              left: 10,
                              bottom: 20,
                            ),
                            child: Column(
                              children: [
                                SvgPicture.asset(
                                  "assets/images/child_enrollment.svg",
                                  width: 100,
                                  height: 100,
                                ),
                                Label(
                                  textAlign: TextAlign.center,
                                  title: AppLocalizations.of(context).translate("Child Enrollment"),
                                  fontSize: 15,
                                  color: Color(appColors.blue00008B),
                                  fontWeight: FontWeight.bold,
                                ),
                              ],
                            ),
                          ),
                        ):Container(),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  calculateAge(DateTime birthDate) {
    DateTime currentDate = DateTime.now();
    int age = currentDate.year - birthDate.year;
    int month1 = currentDate.month;
    int month2 = birthDate.month;
    if (month2 > month1) {
      age--;
    } else if (month1 == month2) {
      int day1 = currentDate.day;
      int day2 = birthDate.day;
      if (day2 > day1) {
        age--;
      }
    }
    return age;
  }

}
