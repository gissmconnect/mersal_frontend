package com.gis.mersal

import io.flutter.embedding.android.FlutterActivity

import android.app.Activity
import android.content.Context
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel
import com.trm.mrzreaderengine.MrzReaderEngineSDK

import com.trm.mrzreaderengine.MrzReaderEngineSDK.KEY_RESULT_JSON
import com.trm.mrzreaderengine.MrzReaderResult
import android.content.Intent

import android.graphics.Bitmap
import android.content.ContextWrapper

import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

import io.flutter.embedding.android.FlutterFragmentActivity
import io.flutter.plugins.GeneratedPluginRegistrant

class MainActivity: FlutterActivity() {

    var sdk: MrzReaderEngineSDK? = null
    var pendingResult: MethodChannel.Result? = null

    private val platformChannel = "insync.flutter.dev/mrz"

    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        GeneratedPluginRegistrant.registerWith(flutterEngine);

        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, platformChannel)
                .setMethodCallHandler { call, result ->
                    if (call.method == "getMrzData") {
                        pendingResult = result
                        getMrzData()
                    } else {
                        result.notImplemented()
                    }
                }
    }

    private fun getMrzData() {
        startScan()
    }

    private fun startScan() {
        sdk = MrzReaderEngineSDK(this)
        sdk!!.start(MrzReaderEngineSDK.SCAN_REQUEST_CODE)
    }


    override fun onPause() {
        super.onPause()

    }


    /*override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

    }*/
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == MrzReaderEngineSDK.SCAN_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                val result = data!!.getParcelableExtra<MrzReaderResult>(KEY_RESULT_JSON)
                if (result !=null && result.statusCode.equals("200", ignoreCase = true)) {

                    val responseData = HashMap<String,String>()

                    responseData["CardType"] = result.cardType.value
                    responseData["DocumentType"] = result.documentType.value
                    responseData["CountryOfIssue"] = result.countryOfIssue.value
                    responseData["LastName"] = result.lastName.value
                    responseData["FirstName"] =  result.firstName.value
                    responseData["MiddleName"] = result.middleName.value
                    responseData["DocumentNumber"]  =  result.documentNumber.value
                    responseData["Nationality"] = result.nationality.value
                    responseData["DateOfBirth"]  =  result.dateOfBirth.value
                    responseData["Sex"] = result.sex.value
                    responseData["DateOfExpiry"] = result.dateOfExpiry.value
                    responseData["PersonalNumber"] = result.personalNumber.value

                    //val imagePath = saveToInternalStorage(sdk!!.imageFront)
                    //  responseData["ImagePath"] = imagePath
                    pendingResult!!.success(responseData)
                    pendingResult = null
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                pendingResult!!.error("CANCELLED", "User Cancelled the mrz scan.", null)
                pendingResult = null
            }
        }
    }

    private fun saveToInternalStorage(bitmapImage: Bitmap): String {
        val cw = ContextWrapper(applicationContext)

        val directory = cw.getDir("imageDir", Context.MODE_PRIVATE)

        val timeStamp = SimpleDateFormat("ddMMyyyy_HHmm").format(Date())

        val fileName = "$timeStamp.jpg"

        // Create imageDir
        val imagePath = File(directory, fileName)

        var fos: FileOutputStream? = null
        try {
            fos = FileOutputStream(imagePath)
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos)
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            try {
                fos!!.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
        return imagePath.absolutePath
    }
}
