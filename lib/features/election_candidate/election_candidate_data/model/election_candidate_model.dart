import 'package:mersal/features/election_candidate/election_candidate_domain/entity/election_candidate_entity.dart';

class ElectionCandidateResponseModel extends ElectionCandidateEntityResponse {
  ElectionCandidateResponseModel(
      List<ElectionCandidateModel> candidates, int hasVoted)
      : super(hasVoted: hasVoted, candidates: candidates);

  ElectionCandidateResponseModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      candidates = [];
      json['data'].forEach((v) {
        candidates.add(new ElectionCandidateModel.fromJson(v));
      });
    }
    hasVoted = json['hasVoted'];
  }
}

class ElectionCandidateModel extends ElectionCandidateEntity {
  ElectionCandidateModel(int candidateId, int electionId, String candidateName,
      String candidateDescription, String candidateImage, bool isSelected)
      : super(
            candidateId: candidateId,
            electionId: electionId,
            candidateName: candidateName,
            candidateDescription: candidateDescription,
            candidateImage: candidateImage,
            isSelected: isSelected);

  ElectionCandidateModel.fromJson(Map<String, dynamic> json) {
    electionId = json['election_id'];
    candidateId = json['elector_id'];
    candidateName = json['elector_name'];
    candidateDescription = json['elector_description'];
    candidateImage = json['elector_image'];
    isSelected = json['isselect'];
  }

}
