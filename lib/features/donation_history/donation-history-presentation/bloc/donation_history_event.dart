part of 'donation_history_bloc.dart';

abstract class DonationHistoryEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class GetHistoryEvent extends DonationHistoryEvent {}
