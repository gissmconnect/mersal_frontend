import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:mersal/core/localization/app_localization.dart';
import 'package:mersal/core/network/api_endpoints.dart';
import 'package:mersal/core/widgets/common/shared_preference.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/country_listing/data/model/country_data_list.dart';
import 'package:mersal/features/country_listing/data/model/country_data_model.dart';

class CountryListing extends StatefulWidget {
  @override
  _CountryListingState createState() => _CountryListingState();
}

final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

class _CountryListingState extends State<CountryListing> {
  List<dynamic> list = [];
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  BuildContext mContextLoader;

  List<CountryDataList> countryDataList = [];
  CountryDataModel countryDataModel;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        builder: (context, child) {
          return MediaQuery(
            child: child,
            data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
          );
        },
        home: Scaffold(
            key: _scaffoldKey,
            body: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/bg_image.jpg"),
                  fit: BoxFit.fill,
                ),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  SizedBox(
                    height: 60,
                  ),
                  Stack(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(bottom: 5, left: 15),
                        alignment: Alignment.bottomLeft,
                        child: GestureDetector(
                          child: Icon(
                            Icons.arrow_back_ios_rounded,
                            color: Colors.white,
                          ),
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(bottom: 6),
                        alignment: Alignment.bottomCenter,
                        child: Text(
                          AppLocalizations.of(context).translate("Country List"),
                          textAlign: TextAlign.end,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                    ],
                  ),
                  list != null && list.length > 0
                      ? Expanded(
                          child: Container(
                              padding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 10.0),
                              margin: EdgeInsets.all(0.0),
                              child: ListView.builder(
                                  itemCount: list.length,
                                  itemBuilder: (BuildContext ctxt, int index) {
                                    return GestureDetector(
                                        onTap: () {
                                          // print("onTap called.");
                                          // _scaffoldKey.currentState
                                          //     .showSnackBar(new SnackBar(
                                          //   content: new Text(list[index]),
                                          // ));
                                        },
                                        child: rowLayout(index));
                                  })))
                      : Container(),
                ],
              ),
            )));
  }

  Widget rowLayout(int index) {
    return GestureDetector(
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(10.0),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Container(
                      child: Text(
                    "+" +
                        list[index]["phonecode"].toString() +
                        " " +
                        list[index]["nicename"].toString(),
                    style: TextStyle(fontSize: 16.0,color: Colors.white),
                  )
                      /*  Text(
                      "+" +
                          countryDataList[index].phonecode.toString() +
                          " " +
                          countryDataList[index].nicename.toString(),
                      style: TextStyle(fontSize: 16.0),
                    ),*/
                      ),
                ),
              ],
            ),
          ),
          Container(
            color: Colors.white,
            width: MediaQuery.of(context).size.width,
            height: 1.0,
          )
        ],
      ),
      onTap: () {
        Navigator.of(context).pop({
          'country_id': list[index]["id"].toString(),
          'country_code': list[index]["phonecode"].toString()
          // 'country_id': countryDataList[index].id.toString(),
          // 'country_code': countryDataList[index].phonecode.toString()
        });
      },
    );
  }

  @override
  void initState() {
    super.initState();
    apiCountryListCall();
  }

  Future<void> apiCountryListCall() async {
    final _token = await appPreferences.getStringPreference("AccessToken");

    if (context != null) {
      _showLoader(context);
    }

    var response = await http.get(Uri.parse(COUNTRY_LIST), headers: {
      'Content-type': 'application/json',
      AUTHORIZATION: BEARER + " " + _token
    });

    if (response.statusCode == 200) {
      try {
        final Map<String, dynamic> _responseJson = json.decode(response.body);
        if (mContextLoader != null) Navigator.pop(mContextLoader);
        print(
            'CountryResponse ${response.statusCode} and ${_responseJson.toString()}');

        list = json.decode(response.body)['data'];
        //
        // ((json.decode(response.body)['data']) as List<dynamic>)
        //     .map((e) => CountryDataList.fromJson(e))
        //     .toList();
        // countryDataModel = CountryDataModel.fromJson(_responseJson);
        // countryDataList = countryDataModel.countryList;
        setState(() {});
      } catch (ex) {}
    }
  }

  Future<Null> _showLoader(BuildContext context) async {
    return showDialog<Null>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context1) {
        mContextLoader = context1;
        return Scaffold(
          backgroundColor: Colors.transparent.withOpacity(0.3),
          key: _keyLoader,
          body: Container(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    child: CircularProgressIndicator(),
                    height: 40.0,
                    width: 40.0,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
