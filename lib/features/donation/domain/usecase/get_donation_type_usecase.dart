import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/donation/domain/entity/donation_type_entity.dart';
import 'package:mersal/features/donation/domain/repository/donation_repo.dart';

class GetDonationTypeUseCase
    extends UseCase<List<DonationTypeEntity>, NoParams> {
  final DonationRepo donationRepo;

  GetDonationTypeUseCase({this.donationRepo});

  @override
  Future<Either<Failure, List<DonationTypeEntity>>> call(
      NoParams params) async {
    return await donationRepo.getDonationType();
  }
}
