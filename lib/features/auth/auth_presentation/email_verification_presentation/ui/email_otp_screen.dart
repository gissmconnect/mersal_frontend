import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/index.dart';
import 'package:mersal/core/widgets/common/Button.dart';
import 'package:mersal/core/widgets/common/InputField.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/features/auth/auth_presentation/change_password_presentation/ui/change_password.dart';

class EmailOtpScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // appBar: CustomAppBar(
        //     title: "Change Password", isBackButton: true, showText: true),
        body: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/bg_image.jpg"),
                fit: BoxFit.fill,
              ),
            ),
            child: _EmailOtpScreenBody()));
  }
}

class _EmailOtpScreenBody extends StatefulWidget {
  @override
  _EmailOtpScreenBodyState createState() => _EmailOtpScreenBodyState();
}

class _EmailOtpScreenBodyState extends State<_EmailOtpScreenBody> {
  final otpController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  CountdownTimerController controller;

  int get endTime {
    return DateTime.now().millisecondsSinceEpoch + 1000 * 10;
  }

  @override
  void initState() {
    super.initState();
    controller = CountdownTimerController(
      endTime: endTime,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            Container(
                margin: EdgeInsets.only(top: 30),
                child: Label(
                    title: "Verify Code",
                    fontWeight: FontWeight.w600,
                    color: Colors.black,
                    fontSize: 18)),
            Container(
                margin: EdgeInsets.only(top: 30, left: 25, right: 25),
                child: Label(
                    textAlign: TextAlign.center,
                    title:
                        "A Verify Code has been sent to your registered E-mail ID",
                    color: Colors.black45,
                    fontSize: 14)),
            Container(
              margin: EdgeInsets.only(left: 30, top: 30, right: 30, bottom: 10),
              child: InputField(
                controller: otpController,
                onSubmitted: (term) {
                  FocusScope.of(context).unfocus();
                },
                maxLength: 6,
                hint: "Enter OTP",
                validator: (text) {
                  return text.isEmpty ? 'Enter OTP' : null;
                },
                inputType: TextInputType.phone,
              ),
            ),
            CountdownTimer(
              controller: controller,
              widgetBuilder: (_, CurrentRemainingTime time) {
                return FlatButton(
                  onPressed: time == null ? () {} : null,
                  child: Label(
                    title: "Resend Otp in " +
                        " ${time != null ? time.sec.toString() : ""}",
                    fontSize: 16,
                    textAlign: TextAlign.center,
                    color: time == null ? Colors.white : Colors.black45,
                  ),
                );
              },
            ),
            Container(
              height: 60,
              width: 250,
              margin: EdgeInsets.only(left: 30, top: 35, right: 30),
              child: Button(
                title: "Verify",
                textColor: Colors.white,
                borderColor: Colors.transparent,
                color: Colors.black,
                onTap: () {
                  if (_formKey.currentState.validate()) {
                    final _otp = otpController.text.toString().trim();
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ChangePassword(),
                      ),
                    );
                  }
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  void _resetTime() {
    controller.disposeTimer();
    controller.dispose();
    controller = CountdownTimerController(endTime: endTime);
    controller.start();
  }

  @override
  void dispose() {
    controller.dispose();
    otpController.dispose();
    super.dispose();
  }
}
