class Fonts{
   get segoeui=> "segoeui";
   get segoeBold=> "segoeuib";
   get segoeItalic=> "segoeuii";
   get segoeLight=> "segoeuil";
   get segoeSemiLight=> "segoeuisl";
   get segoeBoldItalic=> "segoeuiz";
   get seguiBlack=> "seguibl";
   get seguiBlackItalic=> "seguibli";
   get seguiLightItalic=> "seguili";
   get seguiSemiBold=> "seguisb";
   get  seguiSemiBoldItalic=> "seguisbi";
   get seguiSemiLightItalic=> "seguisli";

   static final Fonts _fonts = new Fonts._internal();
   factory Fonts() {
      return _fonts;
   }
   Fonts._internal();
}
Fonts fonts = new Fonts();
