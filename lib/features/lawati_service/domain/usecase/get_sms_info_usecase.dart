import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/lawati_service/domain/entity/sms_serviceInfo_entity.dart';
import 'package:mersal/features/lawati_service/domain/repository/sms_service_repo.dart';
import 'package:meta/meta.dart';

class GetSMSServiceInfoUseCase extends UseCase<SMSServiceInfoEntity, NoParams> {
  final SMSServiceRepo smsServiceRepo;

  GetSMSServiceInfoUseCase({@required this.smsServiceRepo});

  @override
  Future<Either<Failure, SMSServiceInfoEntity>> call(NoParams params) async {
    return await smsServiceRepo.getSMSServiceInfo();
  }
}
