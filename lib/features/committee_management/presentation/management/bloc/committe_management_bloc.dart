import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/reauthenticate_usecase.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/committee_management/domain/entity/committe_entity.dart';
import 'package:mersal/features/committee_management/domain/usecases/get_committes_usecase.dart';
import 'package:meta/meta.dart';

part 'committe_management_event.dart';

part 'committe_management_state.dart';

class CommitteManagementBloc
    extends Bloc<CommitteManagementEvent, CommitteManagementState> {
  final GetCommitteesUseCase getCommitteesUseCase;
  final ReAuthenticateUseCase reAuthenticateUseCase;

  CommitteManagementBloc(
      {@required this.getCommitteesUseCase,
      @required this.reAuthenticateUseCase})
      : super(CommitteManagementInitial());

  @override
  Stream<CommitteManagementState> mapEventToState(
    CommitteManagementEvent event,
  ) async* {
    if (event is GetCommitteesEvent) {
      yield CommitteeLoadingState();
      final _response = await getCommitteesUseCase(NoParams());
      yield* _response.fold((l) async* {
        if (l is AuthFailure) {
          final _reauthenticateResponse =
              await reAuthenticateUseCase(NoParams());
          _reauthenticateResponse.fold((l) async* {
            yield CommitteeLoadingFailure(failure: l);
          }, (r) => add(GetCommitteesEvent()));
        } else {
          yield CommitteeLoadingFailure(failure: l);
        }
      }, (r) async* {
        yield CommitteeLoadedState(committees: r);
      });
    }
  }
}
