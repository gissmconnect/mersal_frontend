import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/auth/auth_domain/repository/auth_repository.dart';
import 'package:meta/meta.dart';

class ChildRegistrationUseCase extends UseCase<void, ChildRegistrationParams> {

  final AuthRepository authRepo;

  ChildRegistrationUseCase({@required this.authRepo});

  @override
  Future<Either<Failure, void>> call(ChildRegistrationParams params) async {
    return await authRepo.childRegistration(
        name: params.name, age: params.age);
  }
}

class ChildRegistrationParams {
  final String name, age;

  ChildRegistrationParams({@required this.name, @required this.age});
}
