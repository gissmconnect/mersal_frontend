import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:meta/meta.dart';

Future<http.Response> httpPostRequest(
    {@required http.Client httpClient,
    @required String url,
    Map<String, String> params,
    String token = ''}) async {
  try {
    return await httpClient.post(
      Uri.parse(url),
      body: json.encode(params),
      headers: {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        AUTHORIZATION: BEARER + " " + token
      },
    );
  } catch (ex) {
    return http.Response("", 500);
  }
}

Future<http.Response> httpGetRequest(
    {@required http.Client httpClient,
      @required String url,
      String token = ''}) async {
  try {
    return await httpClient.get(
      Uri.parse(url),
      headers: {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        AUTHORIZATION: BEARER + " " + token
      },
    );
  } catch (ex) {
    print('httpGetRequest exception $ex');
    return http.Response("", 500);
  }
}
