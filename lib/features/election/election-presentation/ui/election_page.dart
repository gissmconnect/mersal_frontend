import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mersal/core/bloc/user_bloc.dart';
import 'package:mersal/core/localization/app_localization.dart';
import 'package:mersal/core/network/vo/Status.dart';
import 'package:mersal/core/util/constants.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/core/widgets/message.dart';
import 'package:mersal/features/election/election-domain/entity/election_entity.dart';
import 'package:mersal/features/election/election-presentation/bloc/election_bloc.dart';
import 'package:mersal/features/election_candidate/election_candidate_presentation/ui/candidate_screen.dart';
import 'package:mersal/features/election_details/election_details_presentation/ui/election_details.dart';
import 'package:mersal/features/info_presentation/ui/info_screen.dart';
import 'package:mersal/injection_container.dart';
import 'package:mersal/resources/colors.dart';

class ElectionPage extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (_) => serviceLocator<ElectionBloc>(),
        )
      ],
      child: Scaffold(
        body: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/bg_image.jpg"),
                fit: BoxFit.fill,
              ),
            ),
            child: _ElectionPageBody()),
      ),
    );
  }
}

class _ElectionPageBody extends StatefulWidget {
  @override
  _ElectionPageBodyState createState() => _ElectionPageBodyState();
}

class _ElectionPageBodyState extends State<_ElectionPageBody> {
  ElectionDataEntity _clickedElection;

  FToast fToast;

  @override
  void initState() {
    BlocProvider.of<ElectionBloc>(context).add(GetElections());
    super.initState();
    fToast = FToast();
    fToast.init(context);
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocListener(
      listeners: [
        BlocListener<ElectionBloc, ElectionState>(
          listener: (context, state) {
            if (state is ElectionLoadFiled) {
              CommonMethods.showToast(
                  fToast: fToast,
                  message: state.failure.message,
                  status: false);
            }
          },
        ),
        BlocListener<UserBloc, UserState>(
          listenWhen: (oldState, newState) {
            return oldState.documentVerificationResource == null ||
                oldState.documentVerificationResource.status !=
                    newState.documentVerificationResource.status;
          },
          listener: (context, state) {
            print(
                'doc verification status ${state.documentVerificationResource.status}\n${state.documentVerificationResource.data}');
            if (state.documentVerificationResource.status == STATUS.SUCCESS) {
              if (state.documentVerificationResource.data ==
                  DOCUMENT_STATUS_APPROVED) {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => CandidateScreen(
                      electionId: _clickedElection.id,
                    ),
                  ),
                );
              } else {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => ElectionDetails(
                      election: _clickedElection,
                      documentStatus: state.documentVerificationResource.data,
                    ),
                  ),
                );
              }
            } else if (state.documentVerificationResource.status ==
                STATUS.ERROR) {
              CommonMethods.showToast(
                  fToast: fToast,
                  message:  state.documentVerificationResource.failure.message,
                  status: false);
            }
          },
        ),
      ],
      child: BlocBuilder<ElectionBloc, ElectionState>(
        builder: (context, state) {
          if (state is ElectionLoading) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          if (state is ElectionLoaded) {
            return Column(
              children: [
                SizedBox(
                  height: 50,
                ),
                Stack(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(bottom: 5, left: 15),
                      alignment: Alignment.bottomLeft,
                      child: GestureDetector(
                        child: Icon(
                          Icons.arrow_back_ios_rounded,
                          color: Colors.white,
                        ),
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(bottom: 6),
                      alignment: Alignment.bottomCenter,
                      child: Text(
                        AppLocalizations.of(context)
                            .translate("Poll"),
                        textAlign: TextAlign.end,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(right: 15),
                      alignment: Alignment.centerRight,
                      child: GestureDetector(
                        child:  Container(
                          margin: EdgeInsets.only(left: 20),
                          child: SvgPicture.asset(
                            "assets/images/info_icon.svg",
                            width: 30,
                            height: 30,
                          ),
                        ),
                        onTap: () {
                          CommonMethods().animatedNavigation(
                              context: context,
                              currentScreen: ElectionPage(),
                              landingScreen: InfoScreen());
                        },
                      ),
                    ),
                  ],
                ),
                Container(
                  margin: EdgeInsets.only(top: 20, bottom: 20),
                  child: Label(
                    title: AppLocalizations.of(context)
                        .translate("Live Elections"),
                    fontWeight: FontWeight.w500,
                    color: Colors.white,
                    fontSize: 18,
                  ),
                ),
                state.elections.length>0?Container(
                  margin: EdgeInsets.only(bottom: 40, top: 20),
                  child: Label(
                    title:AppLocalizations.of(context)
                        .translate("Click on Elections to vote"),
                    fontWeight: FontWeight.w700,
                    color: Colors.white,
                    fontSize: 20,
                  ),
                ):Container(
                  margin: EdgeInsets.only( top: 70,left: 20,right: 20),
                  child: Label(
                    title:"Sorry, we don’t have any polls available at the moment, stay tuned and we will inform you…",
                    fontWeight: FontWeight.w600,
                    textAlign: TextAlign.center,
                    color: Colors.white,
                    fontSize: 20,
                  ),
                ),
                Expanded(
                  child: ListView.builder(
                    padding: EdgeInsets.only(bottom: 20),
                    shrinkWrap: true,
                    itemCount: state.elections.length,
                    itemBuilder: (context, int index) {
                      return liveElectionItem(state.elections[index], index);
                    },
                  ),
                ),
              ],
            );
          } else {
            return Container();
          }
        },
      ),
    );
  }

  Widget liveElectionItem(ElectionDataEntity election, int index) {
    return GestureDetector(
      onTap: () {
        _clickedElection = election;
        BlocProvider.of<UserBloc>(context).add(VerifyDocumentStatusEvent());
      },
      child: Container(
        margin: EdgeInsets.only(top: 20, left: 20, right: 20),
        padding: EdgeInsets.only(top: 40, bottom: 40),
        decoration: new BoxDecoration(
            image: DecorationImage(
          image: AssetImage(index == 0
              ? "assets/images/vote_card_one.png"
              : index == 1 || index == 3
                  ? "assets/images/vote_card_two.png"
                  : "assets/images/vote_card_one.png"),
          fit: BoxFit.cover,
        )),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              child: SvgPicture.asset(
                "assets/images/vote_icon.svg",
                width: 60,
                height: 60,
                color: Colors.white,
              ),
            ),
            SizedBox(
              height: 6,
            ),
            Label(
              title: election.title,
              color: Colors.white,
              fontSize: 18,
            ),
            SizedBox(
              height: 20,
            ),
            Label(
              title: election.endDate,
              color: Color(appColors.buttonColor),
              fontWeight: FontWeight.w600,
              fontSize: 18,
            ),
          ],
        ),
      ),
    );
  }
}
