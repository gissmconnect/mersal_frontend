import 'package:mersal/features/wedding_invite_presentation/data/model/wedding_template_image/wedding_template_image_data.dart';

class WeddingTemplateImagesModel {
  List<WeddingTemplateImageData> templateImages;
  bool status;

  WeddingTemplateImagesModel({this.templateImages, this.status});

  factory WeddingTemplateImagesModel.fromJson(Map<String, dynamic> json) {
    return WeddingTemplateImagesModel(
      templateImages: json['data'] != null
          ? (json['data'] as List)
              .map((i) => WeddingTemplateImageData.fromJson(i))
              .toList()
          : null,
      status: json['status'],
    );
  }
}
