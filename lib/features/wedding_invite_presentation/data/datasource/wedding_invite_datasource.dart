import 'package:mersal/features/wedding_invite_presentation/data/model/wedding_template_model.dart';
import 'package:meta/meta.dart';

abstract class WeddingInviteDataSource {
  Future<WeddingTemplateModel> getWeddingTemplates({@required String token});
}
