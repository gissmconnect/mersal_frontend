import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:mersal/core/util/date_formatter.dart';
import 'package:mersal/core/widgets/common/Button.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/features/auth/auth_presentation/registration_presentation/ui/register_screen.dart';
import 'package:mersal/features/election/election-domain/entity/election_entity.dart';
import 'package:mersal/features/profile/profile_presentation/ui/upload_id_screen.dart';
import 'package:mersal/resources/colors.dart';
import 'package:meta/meta.dart';

import '../../../election_candidate/election_candidate_presentation/ui/candidate_screen.dart';

class ElectionDetails extends StatelessWidget {
  final ElectionDataEntity election;
  final int documentStatus;

  const ElectionDetails({Key key, @required this.election, this.documentStatus})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _ElectionDetailsBody(
        election: election,
        documentStatus: documentStatus,
      ),
    );
  }
}

class _ElectionDetailsBody extends StatefulWidget {
  final ElectionDataEntity election;
  final int documentStatus;

  const _ElectionDetailsBody(
      {Key key, @required this.election, @required this.documentStatus})
      : super(key: key);

  @override
  _VoteDetailsState createState() => _VoteDetailsState();
}

class _VoteDetailsState extends State<_ElectionDetailsBody> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bg_image.jpg"),
            fit: BoxFit.fill,
          ),
        ),
        child: Column(
          children: [
            SizedBox(
              height: 60,
            ),
            Stack(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(bottom: 5, left: 15),
                  alignment: Alignment.bottomLeft,
                  child: GestureDetector(
                    child: Icon(
                      Icons.arrow_back_ios_rounded,
                      color: Colors.white,
                    ),
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 6),
                  alignment: Alignment.bottomCenter,
                  child: Text(
                    "Poll",
                    textAlign: TextAlign.end,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.w400),
                  ),
                ),
              ],
            ),
            Expanded(
              child: ListView(
                children: [
                  Container(
                    alignment: Alignment.center,
                    margin: EdgeInsets.only(top: 40, bottom: 50),
                    child: Label(
                      title: "Get Ready to Vote",
                      fontWeight: FontWeight.w700,
                      color: Colors.white,
                      fontSize: 20,
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 40),
                        padding: EdgeInsets.only(top: 30, right: 20),
                        height: 180,
                        alignment: Alignment.center,
                        decoration: new BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage(
                                "assets/images/election_red_card.png"),
                            fit: BoxFit.fill,
                          ),
                        ),
                        child: Column(
                          children: [
                            Container(
                              alignment: Alignment.center,
                              child: Label(
                                title: "Voter Registration",
                                color: Colors.black,
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                              ),
                            ),
                            Container(
                              margin:
                                  EdgeInsets.only(top: 30, right: 10, left: 10),
                              child: Label(
                                title: widget.election.date,
                                color: Colors.white,
                                textAlign: TextAlign.center,
                                fontWeight: FontWeight.w700,
                                fontSize: 16,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                  bottom: 10, right: 10, left: 10),
                              child: Label(
                                title: "Deadline",
                                color: Colors.white,
                                fontSize: 16,
                              ),
                            ),
                          ],
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          CommonMethods().animatedNavigation(
                              context: context,
                              landingScreen: CandidateScreen(
                                electionId: widget.election.id,
                              ),
                              currentScreen: ElectionDetails(
                                election: widget.election,
                              ));
                        },
                        child: Container(
                          margin: EdgeInsets.only(left: 40),
                          padding: EdgeInsets.only(top: 30, right: 20),
                          height: 180,
                          alignment: Alignment.center,
                          decoration: new BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage(
                                  "assets/images/election_blue_card.png"),
                              fit: BoxFit.fill,
                            ),
                          ),
                          child: Column(
                            children: [
                              Container(
                                height: 30,
                                alignment: Alignment.center,
                                child: Label(
                                  title: "General Election",
                                  color: Colors.black,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 16,
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                    top: 30, right: 10, left: 10),
                                child: Label(
                                  title: widget.election.date,
                                  color: Colors.white,
                                  textAlign: TextAlign.center,
                                  fontWeight: FontWeight.w700,
                                  fontSize: 16,
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                    bottom: 10, right: 10, left: 10),
                                child: Label(
                                  title: "Election Day",
                                  color: Colors.white,
                                  fontSize: 16,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  Center(
                    child: CountdownTimer(
                      endTime: _getEndTimeStamp(widget.election.date),
                      textStyle: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                      endWidget: Center(
                        child: Text(
                          'Registration Period Over',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ),
                  isTimeOver(widget.election.date)
                      ? Container()
                      : Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.only(bottom: 20, top: 20),
                          child: Label(
                            title: "Remaining Until last days to register",
                            color: Colors.white,
                            fontSize: 18,
                          ),
                        ),
                  isTimeOver(widget.election.date)
                      ? Container()
                      : Container(
                          height: 50,
                          margin: EdgeInsets.only(left: 30, top: 35, right: 30),
                          child: Button(
                            title: "Register To Vote",
                            color: Color(appColors.buttonColor),
                            borderColor: Colors.transparent,
                            textColor: Colors.white,
                            onTap: () {
                              CommonMethods().animatedNavigation(
                                context: context,
                                landingScreen: UploadIdScreen(
                                  isNewAccount: false,
                                ),
                                currentScreen: ElectionDetails(
                                  election: widget.election,
                                ),
                              );
                            },
                          ),
                        ),
                  SizedBox(
                    height: 40,
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<Widget> registerBottomSheet() {
    return showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      backgroundColor: Colors.transparent,
      builder: (context) {
        return Container(
            margin:
                EdgeInsets.only(top: MediaQuery.of(context).size.height / 3.5),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.vertical(top: Radius.circular(30.0)),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.black,
                  offset: Offset(0.0, 1.0), //(x,y)
                  blurRadius: 5.0,
                ),
              ],
            ),
            child: RegisterScreen());
      },
    );
  }

  bool isTimeOver(String date){
    int _electionEndTime = formatToMillis(dateTime: date);
    int _currentTime = DateTime.now().millisecondsSinceEpoch;
    print("_election $_electionEndTime && $_currentTime and ${_electionEndTime<_currentTime}");
    return _electionEndTime<_currentTime;
  }

  int _getEndTimeStamp(String date) {
    int _electionEndTime = formatToMillis(dateTime: date);
    print('end time ${_electionEndTime}');
    return _electionEndTime;
  }
}
