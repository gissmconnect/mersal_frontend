import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mersal/features/auth/auth_domain/entity/user_data.dart';
import 'package:mersal/features/auth/auth_domain/repository/auth_repository.dart';
import 'package:mersal/features/auth/auth_domain/usecases/login_usecase.dart';
import 'package:mockito/mockito.dart';

class MockAuthRepository extends Mock implements AuthRepository {}

void main() {
  MockAuthRepository mockAuthRepository;
  LoginUseCase useCase;
  setUp(() {
    mockAuthRepository = MockAuthRepository();
    useCase = LoginUseCase(authRepository: mockAuthRepository);
  });

  test('should return UserData Entity when login successful', () async {
    final _tUserData = UserData(id: 1, userName: 'test');
    final _phoneNumber = '123456';
    final _password = 'abcd';
    //arrange
    when(mockAuthRepository.login(
            phoneNumber: _phoneNumber, password: _password))
        .thenAnswer((_) async => Right(_tUserData));

    //act
    final result =
        await useCase(Params(phoneNumber: _phoneNumber, password: _password));

    //assert
    expect(result, Right(_tUserData));
    verify(mockAuthRepository.login(
        phoneNumber: _phoneNumber, password: _password));
    verifyNoMoreInteractions(mockAuthRepository);
  });
}
