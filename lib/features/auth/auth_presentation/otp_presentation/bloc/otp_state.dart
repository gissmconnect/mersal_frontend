part of 'otp_bloc.dart';


abstract class OtpState extends Equatable{
  @override
  List<Object> get props => [];
}

class OtpInitial extends OtpState{}
class OtpRequesting extends OtpState{}
class OtpRequestSuccessState extends OtpState{
  final String message;

  OtpRequestSuccessState({@required this.message});
}
class OtpError extends OtpState{
  final Failure failure;

  OtpError({@required this.failure});
}

class OtpVerifyingState extends OtpState{}

class OtpVerificationSuccessState extends OtpState{}

class OtpVerificationFailureState extends OtpState{
  final Failure failure;

  OtpVerificationFailureState({@required this.failure});
}