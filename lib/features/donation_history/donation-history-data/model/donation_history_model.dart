import 'package:mersal/features/donation_history/donation-history-domain/entity/donation_history_entity.dart';
import 'package:meta/meta.dart';

class DonationHistoryModel extends DonationHistoryEntity {
  DonationHistoryModel(
      {@required bool status,
      @required List<DonationHistoryDataList> donationDataList})
      : super(status: status, donationDataList: donationDataList);

  DonationHistoryModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['data'] != null) {
      donationDataList = new List<DonationHistoryDataList>();
      json['data'].forEach((v) {
        donationDataList.add(new DonationHistoryDataList.fromJson(v));
      });
    }
  }
}

class DonationHistoryDataList extends DonationDataListEntity {
  DonationHistoryDataList(
      {int id,
      String description,
      String created_at,
      DonationReasonData donationReasonData})
      : super(id: id, created_at: created_at,description: description, reason: donationReasonData);

  DonationHistoryDataList.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    created_at = json['created_at'];
    description = json['description'];
    reason  = json['reason'] != null
        ? new DonationReasonData.fromJson(json['reason'])
        : null;
  }
}

class DonationReasonData extends DonationReasonDataEntity {
  DonationReasonData({int id, String reason, String description})
      : super(
          id: id,
          reason: reason,
    description: description,
        );

  DonationReasonData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    reason = json['reason'];
    description = json['description'];
  }
}
