class PrayerDataList {
  String name;
  int id;
  int isNext;
  String prayer_time;

  PrayerDataList({this.name,this.isNext,this.prayer_time,this.id,});

  factory PrayerDataList.fromJson(Map<String, dynamic> json) {
    return PrayerDataList(
      name: json['name'],
      id: json['id'],
      prayer_time: json['prayer_time'],
      isNext: json['isNext'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['prayer_time'] = this.prayer_time;
    data['id'] = this.id;
    return data;
  }
}