import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/occasion/domain/entity/occasion_entity.dart';
import 'package:mersal/features/occasion/domain/repository/occasion_repo.dart';
import 'package:meta/meta.dart';

class GetOccasionUseCase extends UseCase<OccasionEntity, String> {
  final OccasionRepo occasionRepo;

  GetOccasionUseCase({@required this.occasionRepo});

  @override
  Future<Either<Failure, OccasionEntity>> call(String date) async {
    return await occasionRepo.getOccasion(date: date);
  }
}
