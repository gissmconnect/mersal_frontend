part of 'child_enroll_bloc.dart';

abstract class ChildEnrollmentState extends Equatable {}

class ChildEnrollmentInitial extends ChildEnrollmentState {
  @override
  List<Object> get props => [];
}

class ChildEnrolling extends ChildEnrollmentState {
  @override
  List<Object> get props => [];
}

class ChildActivityLoaded extends ChildEnrollmentState {
  final ChildActivityModel activity;

  ChildActivityLoaded({@required this.activity});

  @override
  List<Object> get props => [activity];
}

class ChildDetailsFetch extends ChildEnrollmentState {
  @override
  List<Object> get props => [];
}

class ChildDetailsLoaded extends ChildEnrollmentState {
  final ChildDetailsModel detailsDataModel;

  ChildDetailsLoaded({@required this.detailsDataModel});

  @override
  List<Object> get props => [detailsDataModel];
}

class ChildEnrollmentError extends ChildEnrollmentState {
  final Failure failure;

  ChildEnrollmentError(this.failure);

  @override
  List<Object> get props => [failure];
}
