import 'package:mersal/core/network/network_info.dart';
import 'package:mersal/core/usecases/get_user_login_data_usecase.dart';
import 'package:mersal/core/usecases/reauthenticate_usecase.dart';
import 'package:mersal/features/auth/auth_data/datasource/auth_data_source.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/auth/auth_domain/repository/auth_repository.dart';
import 'package:mersal/features/auth/auth_domain/usecases/get_user_session.dart';
import 'package:mersal/core/usecases/save_user_login_data_usecase.dart';
import 'package:mersal/features/auth/auth_domain/usecases/send_otp_usecase.dart';
import 'package:mersal/features/auth/auth_domain/usecases/verify_otp_usecase.dart';
import 'package:mersal/features/election/election-data/datasource/election_data_source.dart';
import 'package:mersal/features/election/election-domain/repository/election_repository.dart';
import 'package:mersal/features/election/election-domain/usecases/get_elections_usecase.dart';
import 'package:mockito/mockito.dart';

class MockAuthRepository extends Mock implements AuthRepository {}

class MockNetworkInfo extends Mock implements NetworkInfo {}

class MockUserDataLocalDataSource extends Mock
    implements UserDataLocalDataSource {}

class MockAuthDataSource extends Mock implements AuthDataSource {}

class MockGetUserSession extends Mock implements GetUserSession {}

class MockSendOTPUseCase extends Mock implements SendOtpUseCase {}

class MockVerifyOTPUseCase extends Mock implements VerifyOtpUseCase {}

class MockElectionRepo extends Mock implements ElectionRepository{}

class MockGetElectionsUseCase extends Mock implements GetElectionUseCase{}

class MockElectionDataSource extends Mock implements ElectionDataSource{}

class MockReAuthenticateUseCase extends Mock implements ReAuthenticateUseCase{}

class MockSaveLoginDataUseCase extends Mock implements SaveUserLoginDataUseCase{}

class MockGetUserLoginDataUseCase extends Mock implements GetUserLoginDataUseCase{}
