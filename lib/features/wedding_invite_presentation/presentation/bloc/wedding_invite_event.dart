part of 'wedding_invite_bloc.dart';

abstract class WeddingInviteEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class WeddingInvitationEvent extends WeddingInviteEvent {
  final String invitationType,date,time;

  WeddingInvitationEvent({ @required this.invitationType,@required this.date, @required this.time});
}
