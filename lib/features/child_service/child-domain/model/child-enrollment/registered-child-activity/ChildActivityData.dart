class ChildActivityData {
  int id;
  String title;
  String created_at;

  ChildActivityData({this.id, this.title, this.created_at});

  factory ChildActivityData.fromJson(Map<String, dynamic> json) {
    return ChildActivityData(
      id: json['id'],
      created_at: json['created_at'],
      title: json['title'],
    );
  }
}
