class SurveyEntity {
  int id;
  String title;
  String description;
  List<Questions> questions;

  SurveyEntity({this.id, this.title, this.description, this.questions});

  SurveyEntity copy({int id,
    String title,
    String description,
    List<Questions> questions,}) {
    return SurveyEntity(id: id ?? this.id,
        title: title ?? this.title,
        description: description ?? this.description,
        questions: questions ?? this.questions);
  }
}

class Questions {
  int id;
  String question;
  List<Option> options;
  int answerId;

  Questions({this.id, this.question, this.options, this.answerId = -1});

  Questions copy({int id,
    String question,
    List<Option> options,
    int answerId}) {
    return Questions(id: id ?? this.id,
        question: question ?? this.question,
        options: options ?? this.options,
        answerId: answerId ?? this.answerId);
  }
}

class Option {
  int id;
  int surveyId;
  int questionId;
  int optionType;
  String option;

  Option({
    this.id,
    this.surveyId,
    this.questionId,
    this.optionType,
    this.option,
  });
}
