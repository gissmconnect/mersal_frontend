import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/banner/banner-domain/entity/banner_entity.dart';
import 'package:mersal/features/homescreen/home-domain/repository/home_repository.dart';
import 'package:meta/meta.dart';

class GetBannerUseCase extends UseCase<List<BannerEntity>, NoParams> {
  final HomeRepository homeRepo;

  GetBannerUseCase({@required this.homeRepo});

  @override
  Future<Either<Failure, List<BannerEntity>>> call(NoParams params) async {
    return await homeRepo.getBanners();
  }
}
