class ArchivedDataList {
  String title;
  String user_id;
  String created_at;
  String text;

  ArchivedDataList({this.user_id,this.title,this.text,this.created_at});

  factory ArchivedDataList.fromJson(Map<String, dynamic> json) {
    return ArchivedDataList(
      user_id: json['user_id'].toString(),
      text: json['text'],
      title: json['title'],
      created_at: json['created_at'],
    );
  }
}