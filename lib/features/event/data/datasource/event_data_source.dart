import 'package:mersal/features/event/data/model/event_model.dart';
import 'package:meta/meta.dart';

abstract class EventDataSource {
  Future<EventModel> getEvent({@required String token, @required String date});
}
