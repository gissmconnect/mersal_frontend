class EnrolledChildActivityModel {
  int id;
  String description;
  String title;
  String text;
  String created_at;

  EnrolledChildActivityModel({this.description,this.id,this.text,this.title,this.created_at});

  factory EnrolledChildActivityModel.fromJson(Map<String, dynamic> json) {
    return EnrolledChildActivityModel(
      title: json['title'],
      text: json['text'],
      description: json['description'],
      id: json['id'],
      created_at: json['created_at'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    return data;
  }
}