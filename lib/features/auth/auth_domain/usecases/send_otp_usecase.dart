import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/auth/auth_domain/repository/auth_repository.dart';

import 'package:meta/meta.dart';

class SendOtpUseCase extends UseCase<String,Params>{

  final AuthRepository authRepository;

  SendOtpUseCase({@required this.authRepository});

  @override
  Future<Either<Failure, String>> call(Params params) async {
    return await authRepository.sendOtp(
        phoneNumber: params.phoneNumber);
  }

}

class Params {
  final String phoneNumber;

  Params({@required this.phoneNumber});
}