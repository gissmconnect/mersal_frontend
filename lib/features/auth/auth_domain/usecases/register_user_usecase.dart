import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/auth/auth_domain/repository/auth_repository.dart';
import 'package:meta/meta.dart';

class RegisterUserUseCase extends UseCase<void, Params> {
  final AuthRepository authRepo;

  RegisterUserUseCase({@required this.authRepo});

  @override
  Future<Either<Failure, void>> call(Params params) async {
    return await authRepo.register(
        phoneNumber: params.phoneNumber,
        password: params.password,
        userName: params.userName,
        gender: params.gender);
  }
}

class Params {
  final String phoneNumber, password, userName, gender;

  Params(
      {@required this.phoneNumber,
      @required this.password,
      @required this.userName,
      @required this.gender});
}
