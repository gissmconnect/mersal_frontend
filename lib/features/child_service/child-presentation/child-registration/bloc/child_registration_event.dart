part of 'child_registration_bloc.dart';

abstract class ChildRegistrationEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class GetActivityEvent extends ChildRegistrationEvent {}

class GetChildDetailsEvent extends ChildRegistrationEvent {}

class ChildEvent extends ChildRegistrationEvent {
  final String name, age;

  ChildEvent({@required this.name, @required this.age});
}
