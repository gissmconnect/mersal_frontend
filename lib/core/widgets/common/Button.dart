import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mersal/resources/fonts.dart';

class Button extends StatefulWidget {
  final String title;
  final GestureTapCallback onTap;
  final bool enabled;
  final double width;
  final Color color;
  final Color borderColor;
  final Color textColor;
  final String image;

  @override
  _ButtonState createState() => _ButtonState();

  Button(
      {this.enabled,
      this.onTap,
      this.title,
      this.width,
      this.textColor,
      this.borderColor,
      this.image,
      this.color});
}

class _ButtonState extends State<Button> {
  @override
  Widget build(BuildContext context) {
    return new GestureDetector(
      onTap: widget.onTap,
      child: new Container(
        height: 44.0,
        width: widget.width != null
            ? widget.width
            : MediaQuery.of(context).size.width,
        decoration: new BoxDecoration(
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(30.0),
          border: Border.all(
              color: widget.borderColor != null
                  ? widget.borderColor
                  : Colors.white,
              width: 1.5),
          color: widget.color != null ? widget.color : Colors.white,
        ),
        child: new Center(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              new Text(
                widget.title,
                textAlign: TextAlign.center,
                style: new TextStyle(
                    fontSize: 17.0,
                    color: widget.textColor,
                    fontFamily: fonts.segoeui,
                    fontWeight: FontWeight.w700),
              ),
              widget.image != null
                  ? Container(
                      margin: EdgeInsets.only(left: 20),
                      child: SvgPicture.asset(
                        widget.image,
                        height: 30,
                        width: 30,
                      ),
                    )
                  : Container()
            ],
          ),
        ),
      ),
    );
  }
}
