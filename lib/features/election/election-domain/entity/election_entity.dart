import 'package:equatable/equatable.dart';

class ElectionDataEntity extends Equatable {
  int id;
  String title;
  String date;
  String endDate;
  String endTime;
  String shortDescription;
  int noOfElectorsSelect;

  ElectionDataEntity(
      {this.id,
      this.title,
      this.date,
      this.endDate,
      this.endTime,
      this.shortDescription,
      this.noOfElectorsSelect});

  @override
  List<Object> get props =>
      [id, title, date, endDate, endTime, shortDescription, noOfElectorsSelect];

  @override
  String toString() {
    return 'ElectionDataEntity{id: $id, title: $title, date: $date, endDate: $endDate, endTime: $endTime, shortDescription: $shortDescription, noOfElectorsSelect: $noOfElectorsSelect}';
  }
}
