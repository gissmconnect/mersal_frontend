import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/network/vo/Resource.dart';
import 'package:mersal/core/usecases/reauthenticate_usecase.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/lawati_service/domain/usecase/get_sms_info_usecase.dart';
import 'package:mersal/features/lawati_service/domain/usecase/subscribe_sms_subscription_usecase.dart';
import 'package:meta/meta.dart';

part 'sms_service_info_event.dart';

part 'sms_service_info_state.dart';

class SmsServiceInfoBloc
    extends Bloc<SmsServiceInfoEvent, SmsServiceInfoState> {
  final GetSMSServiceInfoUseCase smsServiceInfoUseCase;
  final ReAuthenticateUseCase reAuthenticateUseCase;
  final SubscribeSmsServiceUseCase subscribeSmsServiceUseCase;

  SmsServiceInfoBloc(
      {@required this.smsServiceInfoUseCase,
      @required this.reAuthenticateUseCase,
      @required this.subscribeSmsServiceUseCase})
      : super(SmsServiceInfoState.initial());

  @override
  Stream<SmsServiceInfoState> mapEventToState(
    SmsServiceInfoEvent event,
  ) async* {
    if (event is GetSmsServiceInfoEvent) {
      yield state.copy(serviceInfoResource: Resource.loading());
      final _response = await smsServiceInfoUseCase(NoParams());
      yield* _response.fold((l) async* {
        if (l is AuthFailure) {
          final _reauthenticateResponse =
              await reAuthenticateUseCase(NoParams());
          _reauthenticateResponse.fold((l) async* {
            yield state.copy(serviceInfoResource: Resource.error(failure: l));
          }, (r) => GetSmsServiceInfoEvent());
        } else {
          yield state.copy(serviceInfoResource: Resource.error(failure: l));
        }
      }, (r) async* {
        yield state.copy(serviceInfoResource: Resource.success(data: r));
      });
    }

    if (event is SubscribeEvent) {
      yield state.copy(subscribeService: Resource.loading());
      final _response = await subscribeSmsServiceUseCase(event.params);
      yield* _response.fold((l) async* {
        if (l is AuthFailure) {
          final _reauthenticateResponse =
              await reAuthenticateUseCase(NoParams());
          _reauthenticateResponse.fold((l) async* {
            yield state.copy(subscribeService: Resource.error(failure: l));
          }, (r) => SubscribeEvent(params: event.params));
        } else {
          yield state.copy(subscribeService: Resource.error(failure: l));
        }
      }, (r) async* {
        yield state.copy(subscribeService: Resource.success(data: r));
      });
    }
  }
}
