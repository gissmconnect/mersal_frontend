import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:matcher/matcher.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/save_user_login_data_usecase.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source_impl.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/auth/auth_data/model/user_data_model.dart';
import 'package:mockito/mockito.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../fixtures/fixture_reader.dart';

class MockSharedPreference extends Mock implements SharedPreferences {}

void main() {
  UserDataLocalDataSourceImpl dataSource;
  MockSharedPreference mockSharedPreference;

  setUp(() {
    mockSharedPreference = MockSharedPreference();
    dataSource =
        UserDataLocalDataSourceImpl(sharedPreferences: mockSharedPreference);
  });

  group('user_cache_exists', () {
    Map<String, dynamic> _userDataMap;
    UserDataModel tUserDataModel;
    setUp(() {
      _userDataMap = json.decode(fixture('login_response.json'));
      tUserDataModel = UserLoginModel.fromJson(_userDataMap).data;
    });

    test(
        'should return UserData entity from SharedPreference when userdata is cached',
        () async {
      //arrange
      when(mockSharedPreference.getString(any))
          .thenReturn(json.encode(_userDataMap['data']));
      //act
      final _result = await dataSource.getUserData();

      //assert
      verify(mockSharedPreference.getString(USER_DATA));
      expect(_result, equals(tUserDataModel));
    });

    test('should return AuthToken from sharedPreference when token is cached',
        () async {
      //arrange
      when(mockSharedPreference.getString(any)).thenReturn('test_token');

      //act
      final _result = await dataSource.getAuthToken();
      //assert
      verify(mockSharedPreference.getString(AUTH_TOKEN));
      expect(_result, equals('test_token'));
    });

    test(
        'should throw UserSessionFailure when UserData is not cached in SharedPreference',
        () async {
      //arrange
      when(mockSharedPreference.getString(any)).thenReturn(null);

      //act
      final _call = dataSource.getUserData;

      //assert
      expect(() => _call(), throwsA(TypeMatcher<UserSessionFailure>()));
    });

    final Map<String, dynamic> _tLoginDataMap =
        json.decode(fixture('login_data.json'));
    test('should return LoginParams when from sharedPreference', () async {
      when(mockSharedPreference.getString(any))
          .thenReturn(json.encode(_tLoginDataMap));

      //act
      final _result = await dataSource.getLoginData();
      //assert
      verify(mockSharedPreference.getString(LOGIN_DATA));
      expect(_result, equals(LoginParams.fromJson(_tLoginDataMap)));
    });
  });

  group('cache_user_data', () {
    final UserLoginModel _tUserLoginMode = UserLoginModel(
        accessToken: 'abcd',
        data: UserDataModel(userName: 'abcd', userId: 1, id: 1));
    test('should call SharedPreferences to cache data locally', () async {
      await dataSource.saveUserData(userDataModel: _tUserLoginMode.data);
      final _expectedString = json.encode(_tUserLoginMode.data);
      verify(mockSharedPreference.setString(USER_DATA, _expectedString));
    });

    test('should call sharedPreference to cache auth_token', () async {
      await dataSource.saveAuthToken(authToken: _tUserLoginMode.accessToken);
      verify(mockSharedPreference.setString(
          AUTH_TOKEN, _tUserLoginMode.accessToken));
    });

    final _loginId = 'test';
    final _password = 'test';
    final _loginParams = LoginParams(loginId: _loginId, password: _password);
    test('should call sharedPreference to cache login data', () async {
      //act
      await dataSource.saveLoginData(loginParams: _loginParams);

      //assert

      verify(mockSharedPreference.setString(
          LOGIN_DATA, json.encode(_loginParams)));
    });
  });
}
