import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/auth/auth_data/model/user_data_model.dart';
import 'package:mersal/features/auth/auth_domain/repository/auth_repository.dart';
import 'package:meta/meta.dart';

class SaveUserDataUseCase extends UseCase<void,UserDataModel>{
  final AuthRepository authRepository;

  SaveUserDataUseCase({@required this.authRepository});
  @override
  Future<Either<Failure, void>> call(UserDataModel params)async {
     await authRepository.saveUserDate(userDataModel: params);
     return Right(null);
  }
}