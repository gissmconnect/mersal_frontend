import 'package:meta/meta.dart';

class SMSServiceInfoEntity {
  int id;
  String title;
  int status;
  String price;
  int duration;

  SMSServiceInfoEntity(
      {@required this.id,
      @required this.title,
      @required this.status,
      @required this.price,
      @required this.duration});
}
