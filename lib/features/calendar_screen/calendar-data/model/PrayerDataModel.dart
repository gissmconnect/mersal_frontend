import 'package:mersal/features/calendar_screen/calendar-data/model/PrayerDataList.dart';
import 'package:mersal/features/calendar_screen/calendar-domain/entity/PrayerDataEntity.dart';

class PrayerDataModel extends PrayerDataEntity {
  List<PrayerDataList> prayerList;
  String message;
  bool status;

  PrayerDataModel({this.prayerList, this.status});

  factory PrayerDataModel.fromJson(Map<String, dynamic> json) {
    return PrayerDataModel(
      prayerList: json['data'] != null
          ? (json['data'] as List)
              .map((i) => PrayerDataList.fromJson(i))
              .toList()
          : null,
      status: json['status'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.prayerList != null) {
      data['data'] = this.prayerList.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
