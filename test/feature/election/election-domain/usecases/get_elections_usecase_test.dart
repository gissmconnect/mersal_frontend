import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/election/election-domain/entity/election_entity.dart';
import 'package:mersal/features/election/election-domain/usecases/get_elections_usecase.dart';
import 'package:mockito/mockito.dart';

import '../../../../mocked_classes.dart';

void main() {
  MockElectionRepo _mockElectionRepo;
  GetElectionUseCase _getElectionUseCase;

  setUp(() {
    _mockElectionRepo = MockElectionRepo();
    _getElectionUseCase = GetElectionUseCase(electionRepo: _mockElectionRepo);
  });

  final _elections = [
    ElectionDataEntity(id: 1, date: 'test date', endDate: 'test end date'),
    ElectionDataEntity(id: 2, date: 'test date2', endDate: 'test end date2'),
  ];
  test('should return list of election', () async {
    //arrange
    when(_mockElectionRepo.getElections())
        .thenAnswer((_) async => Right(_elections));

    //act
    final _result = await _getElectionUseCase(NoParams());

    //assert
    verify(_mockElectionRepo.getElections());
    expect(_result, equals(Right(_elections)));
  });
}
