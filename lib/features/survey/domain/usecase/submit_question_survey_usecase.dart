import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/survey/domain/repository/survey_repository.dart';
import 'package:meta/meta.dart';

class SubmitQuestionSurveyUseCase extends UseCase<void, SurveySubmitParams> {

  final SurveyRepository surveyRepository;

  SubmitQuestionSurveyUseCase({@required this.surveyRepository});

  @override
  Future<Either<Failure, void>> call(SurveySubmitParams params) {
    return surveyRepository.submitSurveyQuestion(
        surveyId: params.surveyId, surveyQuestionId: params.surveyQuestionId, surveyOptionId: params.surveyOptionId);
  }

}

class SurveySubmitParams {

  final int surveyId, surveyQuestionId, surveyOptionId;

  SurveySubmitParams({@required this.surveyId, @required this.surveyQuestionId, @required this.surveyOptionId});
}

