import 'dart:async';
import 'dart:ffi';
import 'dart:math';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/network/vo/Resource.dart';
import 'package:mersal/core/usecases/reauthenticate_usecase.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/donation/data/model/donation_type_model.dart';
import 'package:mersal/features/donation/domain/entity/donation_type_entity.dart';
import 'package:mersal/features/donation/domain/usecase/get_donation_type_usecase.dart';
import 'package:mersal/features/donation/domain/usecase/give_donation_usecase.dart';
import 'package:meta/meta.dart';

part 'donation_event.dart';

part 'donation_state.dart';

class DonationBloc extends Bloc<DonationEvent, DonationState> {
  final GiveDonationUseCase donationUseCase;
  final ReAuthenticateUseCase reAuthenticateUseCase;
  final GetDonationTypeUseCase getDonationTypeUseCase;

  DonationBloc(
      {@required this.donationUseCase,
      @required this.reAuthenticateUseCase,
      @required this.getDonationTypeUseCase})
      : super(DonationState.initial());

  @override
  Stream<DonationState> mapEventToState(
    DonationEvent event,
  ) async* {
    if (event is DonateEvent) {
      yield state.copy(donateResource: Resource.loading());
      final response = await donationUseCase(DonationParams(
          reasonId: event.reasonId,
          donationAmount: event.donationAmount,
          donationDescription: event.donationDescription));
      yield* response.fold(
        (l) async* {
          if (l is AuthFailure) {
            final _reauthenticateResponse =
                await reAuthenticateUseCase(NoParams());
            _reauthenticateResponse.fold(
              (l) async* {
                yield Left(l);
              },
              (r) => add(
                DonateEvent(
                    reasonId: event.reasonId,
                    donationAmount: event.donationAmount,
                    donationDescription: event.donationAmount),
              ),
            );
          } else {
            yield state.copy(donateResource: Resource.error(failure: l));
          }
        },
        (r) async* {
          try {
            yield state.copy(donateResource: Resource.success(data: r),selectedDonationType: null);
          } catch (ex) {
            print('error ${ex}');
          }
        },
      );
    }
    if (event is GetDonationTypeEvent) {
      print('GetDonationTypeEvent event ');
      yield state.copy(donationTypeResource: Resource.loading(),selectedDonationType: null);
      final _surveysCall = await getDonationTypeUseCase(NoParams());
      yield* _surveysCall.fold(
        (l) async* {
          if (l is AuthFailure) {
            final _reauthenticateResponse =
                await reAuthenticateUseCase(NoParams());
            _reauthenticateResponse.fold((l) async* {
              yield Left(l);
            }, (r) => add(GetDonationTypeEvent()));
          } else {
            yield state.copy(donationTypeResource: Resource.error(failure: l));
          }
        },
        (r) async* {
          try {
            print('type ${r}');

            yield state.copy(
                donationTypeResource:
                Resource.success(data: r));
          } catch (ex) {
            print('error ${ex}');
          }
        },
      );
    }
    if (event is SelectDonationType) {
      yield state.copy(selectedDonationType: event.donationType,donationTypeResource: state.donationTypeResource);
    }
  }
}
