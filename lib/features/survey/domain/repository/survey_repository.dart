import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/features/survey/domain/entity/survey_entity.dart';

abstract class SurveyRepository {

  Future<Either<Failure, SurveyEntity>> getSurvey();

  Future<Either<Failure, void>> submitSurveyQuestion(
      {@required int surveyId,@required int surveyQuestionId, @required int surveyOptionId});
}
