import 'package:cached_network_image/cached_network_image.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:mersal/core/bloc/user_bloc.dart';
import 'package:mersal/core/localization/app_localization.dart';
import 'package:mersal/core/network/vo/Status.dart';
import 'package:mersal/core/util/helper_functions.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/core/widgets/common/shared_preference.dart';
import 'package:mersal/core/widgets/message.dart';
import 'package:mersal/core/widgets/widgets.dart';
import 'package:mersal/features/auth/auth_domain/entity/user_data.dart';
import 'package:mersal/features/auth/auth_presentation/login_presentation/ui/login_page.dart';
import 'package:mersal/features/calendar_screen/ui/calendar_page.dart';
import 'package:mersal/features/child_service/child-presentation/child-registration/ui/child_registration.dart';
import 'package:mersal/features/committee_management/presentation/management/ui/committe_management.dart';
import 'package:mersal/features/donation/donation-presentation/ui/donation_screen.dart';
import 'package:mersal/features/donation_history/donation-history-presentation/ui/donation_history.dart';
import 'package:mersal/features/election/election-presentation/ui/election_page.dart';
import 'package:mersal/features/homescreen/home-presentation/ui/home_screen.dart';
import 'package:mersal/features/info_presentation/ui/info_screen.dart';
import 'package:mersal/features/more_screen_presentation/ui/more_screen.dart';
import 'package:mersal/features/notification/notification_presentation/ui/notification.dart';
import 'package:mersal/features/profile/profile_presentation/ui/profile_screen.dart';
import 'package:mersal/features/services_presentation/ui/services_screen.dart';
import 'package:mersal/features/settings_presentation/presentation/ui/settings_screen.dart';
import 'package:mersal/features/survey/presentation/ui/survey_main_page.dart';
import 'package:mersal/features/weddingBooking/ui/wedding_booking.dart';
import 'package:mersal/features/wedding_invite_presentation/presentation/ui/wedding_invite.dart';
import 'package:mersal/resources/colors.dart';

class DashboardPage extends StatefulWidget {
  @override
  DashboardPageState createState() => DashboardPageState();
}

class DashboardPageState extends State<DashboardPage> {
  String screenTitle = "";

  bool rightIconOneBool = true,
      rightIconTwoBool = true,
      showAppBar = false,
      moreScreenBool = false;

  GlobalKey _bottomNavigationKey = GlobalKey();

  GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  Widget _currentScreen = HomeScreen();

  var documentStatus, dob,userAge;

  @override
  void initState() {
    super.initState();
    _asyncMethod();
    if(userAge==null){
      userAge=0;
    }
  }

  _asyncMethod() async {
    dob = await appPreferences.getStringPreference("UserDOB");
    documentStatus = await appPreferences.getStringPreference("DocumentStatus");
    print("documentStatusPrint========" + documentStatus.toString());
    print("dobPrint========" + dob.toString());

    DateTime parseDate = new DateFormat("dd/MM/yyyy").parse(dob);
    print("userAgePrint========" + parseDate.toString());

    var inputDate = DateTime.parse(parseDate.toString());
     userAge = calculateAge(inputDate);
    print("userAgePrint========" + userAge.toString());
  }

  @override
  Widget build(BuildContext context) {
    screenTitle = AppLocalizations.of(context).translate("Home");

    List<String> homeMenuTitle = [
      AppLocalizations.of(context).translate("Profile"),
      AppLocalizations.of(context).translate("Wedding Booking"),
      AppLocalizations.of(context).translate("Wedding Invite"),
      AppLocalizations.of(context).translate("Child Registration"),
      AppLocalizations.of(context).translate("Participate In Survey"),
      AppLocalizations.of(context).translate("Vote"),
    ];
    return Scaffold(
      backgroundColor: Color(appColors.buttonColor),
      key: scaffoldKey,
      // drawer: navigationDrawer(),
      bottomNavigationBar: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          CurvedNavigationBar(
            key: _bottomNavigationKey,
            index: 0,
            height: 50.0,
            items: <Widget>[
              SvgPicture.asset(
                "assets/images/home_icon.svg",
                fit: BoxFit.cover,
                width: 30,
                height: 30,
              ),
              SvgPicture.asset(
                "assets/images/donation.svg",
                fit: BoxFit.cover,
                width: 40,
                height: 40,
              ),
              SvgPicture.asset(
                "assets/images/calender.svg",
                fit: BoxFit.cover,
                width: 40,
                height: 40,
              ),
              SvgPicture.asset(
                "assets/images/service_icon.svg",
                fit: BoxFit.cover,
                width: 40,
                height: 40,
              ),
              !moreScreenBool?SvgPicture.asset(
                "assets/images/more_icon.svg",
                fit: BoxFit.cover,
                width: 40,
                height: 40,
              ):Icon(Icons.cancel_outlined,color: Colors.white,),
              // SvgPicture.asset(
              //   "assets/images/more_icon.svg",
              //   fit: BoxFit.cover,
              //   width: 40,
              //   height: 40,
              // )
            ],
            color: Color(appColors.buttonColor),
            buttonBackgroundColor: Color(appColors.buttonColor),
            backgroundColor: Color(appColors.blue),
            animationCurve: Curves.easeInOut,
            animationDuration: Duration(milliseconds: 600),
            onTap: (index) {
              if (index == 4) {
                onSelectBottomMenu(4);
              } else if (index == 3) {
                onSelectBottomMenu(3);
              } else if (index == 0) {
                onSelectBottomMenu(0);
              } else if (index == 1) {
                onSelectBottomMenu(1);
              } else if (index == 2) {
                onSelectBottomMenu(2);
              }
            },
            letIndexChange: (index) => true,
          ),
          Container(
            padding: EdgeInsets.only(bottom: 15),
            color: Color(appColors.buttonColor),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Container(
                  margin: EdgeInsets.only(left: 10),
                  child: Label(
                    title: AppLocalizations.of(context).translate("Home"),
                    fontSize: 12,
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 5),
                  child: Label(
                    title: AppLocalizations.of(context).translate("Donation"),
                    fontSize: 12,
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                  ),
                ),
                Container(
                  child: Label(
                    title: AppLocalizations.of(context).translate("Calendar"),
                    fontSize: 12,
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(right: 15),
                  child: Label(
                    title: AppLocalizations.of(context).translate("Service"),
                    fontSize: 12,
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(right: 10),
                  child: Label(
                    title: AppLocalizations.of(context).translate("More"),
                    fontSize: 12,
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/bg_image.jpg"),
              fit: BoxFit.fill,
            ),
          ),
          padding: EdgeInsets.only(top: 20),
          child: Stack(
            children: [
              /* showAppBar?Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("assets/images/bg_image.jpg"),
                    fit: BoxFit.fill,
                  ),
                ),
                padding: EdgeInsets.only(
                  left: 7,
                  right: 5,
                  bottom: 10,
                  top: 10,
                ),
                alignment: Alignment.center,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    BlocListener<UserBloc, UserState>(
                      listenWhen: (oldState, newSate) {
                        return oldState.logoutResource == null ||
                            oldState.logoutResource.status !=
                                newSate.logoutResource.status;
                      },
                      listener: (context, state) {
                        if (state.logoutResource != null &&
                            state.logoutResource.status == STATUS.ERROR) {
                          Navigator.of(context).pop();
                          showSnackBarMessage(
                              context: context,
                              message:
                                  state.logoutResource.failure.message);
                        } else if (state.logoutResource!=null && state.logoutResource.status ==
                            STATUS.SUCCESS) {
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                              builder: (context) => LoginPage(),
                            ),
                          );
                        }
                      },
                      child: BlocBuilder<UserBloc, UserState>(
                        buildWhen: (oldState, newState) {
                          return oldState.logoutResource == null ||
                              oldState.logoutResource.status !=
                                  newState.logoutResource.status;
                        },
                        builder: (context, state) {
                          if (state.logoutResource != null &&
                              state.logoutResource.status ==
                                  STATUS.LOADING) {
                            return circularProgressIndicator();
                          }
                          return GestureDetector(
                            onTap: () {
                              print('logout called');
                              try {
                                BlocProvider.of<UserBloc>(context)
                                    .add(LogoutEvent());
                              } catch (ex) {
                                print('logout exception $ex');
                              }
                            },
                            child: SvgPicture.asset(
                              "assets/images/logout_icon.svg",
                              fit: BoxFit.cover,
                              width: 40,
                              height: 40,
                            ),
                          );
                        },
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: Container(
                        margin: EdgeInsets.only(left: 20),
                        child: Label(
                            title: screenTitle,
                            fontSize: 20,
                            color: Colors.white,
                            textAlign: TextAlign.center),
                      ),
                    ),
                    actionIconsRight()
                  ],
                ),
              ):Container(),*/
              Container(
                  height: MediaQuery.of(context).size.height,
                  decoration: BoxDecoration(
                    color: Colors.white,
                  ),
                  child: _currentScreen),
              moreScreenBool
                  ? Container(
                      height: MediaQuery.of(context).size.height,
                      decoration:
                          BoxDecoration(color: Colors.white.withOpacity(0.8)),
                    )
                  : Container(),
              moreScreenBool
                  ? Positioned(
                      right: 20,
                      bottom: 30,
                      child: Row(
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              GestureDetector(
                                onTap: () {
                                  CommonMethods().animatedNavigation(
                                      context: context,
                                      currentScreen: MoreScreen(),
                                      landingScreen: ProfileScreen());
                                },
                                child: Row(
                                  children: [
                                    Container(
                                      margin:
                                          EdgeInsets.only(left: 10, right: 10),
                                      child: Label(
                                        title: AppLocalizations.of(context).translate("Profile"),
                                        fontSize: 14,
                                        fontWeight: FontWeight.w600,
                                        color: Color(appColors.buttonColor),
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.all(4),
                                      decoration: BoxDecoration(
                                          color: Color(appColors.buttonColor),
                                          borderRadius:
                                              BorderRadius.circular(30)),
                                      child: SvgPicture.asset(
                                        "assets/images/profile_icon.svg",
                                        fit: BoxFit.cover,
                                        color: Colors.white,
                                        width: 35,
                                        height: 35,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              GestureDetector(
                                onTap: () {
                                  CommonMethods().animatedNavigation(
                                      context: context,
                                      currentScreen: MoreScreen(),
                                      landingScreen: CommitteeManagement());
                                },
                                child: Row(
                                  children: [
                                    Container(
                                      margin:
                                          EdgeInsets.only(left: 10, right: 10),
                                      child: Label(
                                        title: AppLocalizations.of(context).translate("Allawati Committee"),
                                        fontSize: 14,
                                        fontWeight: FontWeight.w600,
                                        color: Color(appColors.buttonColor),
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.all(4),
                                      decoration: BoxDecoration(
                                          color: Color(appColors.buttonColor),
                                          borderRadius:
                                              BorderRadius.circular(30)),
                                      child: SvgPicture.asset(
                                        "assets/images/commitee_icon.svg",
                                        fit: BoxFit.cover,
                                        color: Colors.white,
                                        width: 35,
                                        height: 35,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              userAge>20 && documentStatus=="2"?GestureDetector(
                                onTap: () {
                                  CommonMethods().animatedNavigation(
                                      context: context,
                                      currentScreen: MoreScreen(),
                                      landingScreen: ChildRegistration());
                                },
                                child: Row(
                                  children: [
                                    Container(
                                      margin:
                                          EdgeInsets.only(left: 10, right: 10),
                                      child: Label(
                                        title: AppLocalizations.of(context).translate("Child Registration"),
                                         fontSize: 14,
                                        fontWeight: FontWeight.w600,
                                        color: Color(appColors.buttonColor),
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.all(4),
                                      decoration: BoxDecoration(
                                          color: Color(appColors.buttonColor),
                                          borderRadius:
                                              BorderRadius.circular(30)),
                                      child: SvgPicture.asset(
                                        "assets/images/child_enrollment.svg",
                                        fit: BoxFit.cover,
                                        color: Colors.white,
                                        width: 35,
                                        height: 35,
                                      ),
                                    ),
                                  ],
                                ),
                              ):Container(),
                              SizedBox(
                                height: 10,
                              ),
                              GestureDetector(
                                onTap: () {
                                  CommonMethods().animatedNavigation(
                                      context: context,
                                      currentScreen: MoreScreen(),
                                      landingScreen: SettingsScreen());
                                },
                                child: Row(
                                  children: [
                                    Container(
                                      margin:
                                          EdgeInsets.only(left: 10, right: 10),
                                      child: Label(
                                        title: AppLocalizations.of(context).translate("Settings"),
                                        fontSize: 14,
                                        fontWeight: FontWeight.w600,
                                        color: Color(appColors.buttonColor),
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.all(4),
                                      decoration: BoxDecoration(
                                          color: Color(appColors.buttonColor),
                                          borderRadius:
                                              BorderRadius.circular(30)),
                                      child: SvgPicture.asset(
                                        "assets/images/setting_icon.svg",
                                        fit: BoxFit.cover,
                                        color: Colors.white,
                                        width: 35,
                                        height: 35,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              BlocListener<UserBloc, UserState>(
                                listenWhen: (oldState, newSate) {
                                  return oldState.logoutResource == null ||
                                      oldState.logoutResource.status !=
                                          newSate.logoutResource.status;
                                },
                                listener: (context, state) {
                                  if (state.logoutResource != null &&
                                      state.logoutResource.status ==
                                          STATUS.ERROR) {
                                    Navigator.of(context).pop();
                                    showSnackBarMessage(
                                        context: context,
                                        message: state
                                            .logoutResource.failure.message);
                                  } else if (state.logoutResource != null &&
                                      state.logoutResource.status ==
                                          STATUS.SUCCESS) {
                                    Navigator.pushReplacement(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => LoginPage(),
                                      ),
                                    );
                                  }
                                },
                                child: BlocBuilder<UserBloc, UserState>(
                                  buildWhen: (oldState, newState) {
                                    return oldState.logoutResource == null ||
                                        oldState.logoutResource.status !=
                                            newState.logoutResource.status;
                                  },
                                  builder: (context, state) {
                                    if (state.logoutResource != null &&
                                        state.logoutResource.status ==
                                            STATUS.LOADING) {
                                      return circularProgressIndicator();
                                    }
                                    return GestureDetector(
                                      onTap: () {
                                        print('logout called');
                                        try {
                                          BlocProvider.of<UserBloc>(context)
                                              .add(LogoutEvent());
                                        } catch (ex) {
                                          print('logout exception $ex');
                                        }
                                      },
                                      child: Row(
                                        children: [
                                          Container(
                                            margin: EdgeInsets.only(
                                                left: 10, right: 10),
                                            child: Label(
                                              title: AppLocalizations.of(context).translate("Log Out"),
                                              fontSize: 14,
                                              fontWeight: FontWeight.w600,
                                              color:
                                                  Color(appColors.buttonColor),
                                            ),
                                          ),
                                          Container(
                                            padding: EdgeInsets.all(4),
                                            decoration: BoxDecoration(
                                                color: Color(
                                                    appColors.buttonColor),
                                                borderRadius:
                                                    BorderRadius.circular(30)),
                                            child: SvgPicture.asset(
                                              "assets/images/new_log_out.svg",
                                              fit: BoxFit.cover,
                                              color: Colors.white,
                                              width: 35,
                                              height: 35,
                                            ),
                                          ),
                                        ],
                                      ),
                                    );
                                  },
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )
                  : Container(),
            ],
          ),
        ),
      ),
    );
  }

  void onSelectBottomMenu(int index) {
    switch (index) {
      case 0: //Home menu tiles
        if (_currentScreen is! HomeScreen) {
          screenTitle = "";
          moreScreenBool = false;
          setState(() {});
          updateScreen(HomeScreen());
        }
        break;
      case 1:
        if (_currentScreen is! DonationScreen) {
          screenTitle = "Donation";
          updateScreen(DonationScreen());
        }
        break;
      case 2:
        if (_currentScreen is! CalendarPage) {
          screenTitle = "Calendar";
          updateScreen(CalendarPage());
        }
        break;
      case 3:
        if (_currentScreen is! ServicesScreen) {
          screenTitle = "Services";
          updateScreen(ServicesScreen());
        }
        break;
      case 4:
        if (_currentScreen is! MoreScreen) {
          screenTitle = "More";
          moreScreenBool = !moreScreenBool;
          setState(() {});
          // updateScreen(MoreScreen());
        }
        break;
    }
  }

  void onSelectSideMenu(int index) {
    if (scaffoldKey.currentState.isDrawerOpen) {
      Navigator.pop(context);
    }
    switch (index) {
      case 0: //Home menu tiles
        if (_currentScreen is! ProfileScreen) {
          CommonMethods().animatedNavigation(
              context: context,
              currentScreen: DashboardPage(),
              landingScreen: ProfileScreen());
        }
        break;
      case 1: //Home menu tiles
        if (_currentScreen is! WeddingBooking) {
          CommonMethods().animatedNavigation(
              context: context,
              currentScreen: DashboardPage(),
              landingScreen: WeddingBooking());
        }
        break;
      case 2:
        if (_currentScreen is! WeddingInvite) {
          CommonMethods().animatedNavigation(
              context: context,
              currentScreen: DashboardPage(),
              landingScreen: WeddingInvite());
        }
        break;
      case 3:
        // if (_currentScreen is! ChildrenRegistration) {
        //   CommonMethods().animatedNavigation(
        //       context: context,
        //       currentScreen: DashboardPage(),
        //       landingScreen: ChildrenRegistration());
        // }
        break;
      case 4:
        if (_currentScreen is! SurveyMainPage) {
          CommonMethods().animatedNavigation(
              context: context,
              currentScreen: DashboardPage(),
              landingScreen: SurveyMainPage());
        }
        break;
      case 5:
        if (_currentScreen is! ElectionPage) {
          CommonMethods().animatedNavigation(
              context: context,
              currentScreen: DashboardPage(),
              landingScreen: ElectionPage());
        }

        break;
    }
  }

  void updateScreen(Widget widget) {
    _currentScreen = widget;
    moreScreenBool = false;
    setState(() {});
  }

  navigationDrawer() {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width * 0.75,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
            topRight: Radius.circular(20), bottomRight: Radius.circular(20)),
        boxShadow: [
          new BoxShadow(
            color: Color(appColors.greyC2C2C2),
            blurRadius: 5,
            offset: new Offset(2.0, 0.0),
          )
        ],
      ),
      child: Column(
        children: <Widget>[
          Expanded(
            child: ListView(
              shrinkWrap: true,
              physics: ScrollPhysics(),
              children: <Widget>[
                BlocBuilder<UserBloc, UserState>(
                  buildWhen: (oldState, newState) {
                    return userDataUpdated(oldState, newState);
                  },
                  builder: (context, state) {
                    if (state.userDataLoadingResource.status ==
                        STATUS.SUCCESS) {
                      final UserData _userData =
                          state.userDataLoadingResource.data;
                      return Column(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 30),
                            decoration: new BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(color: Colors.black, width: 2),
                            ),
                            child: state
                                    .userDataLoadingResource.data.image.isEmpty
                                ? Icon(
                                    Icons.person_outline,
                                    size: 90,
                                  )
                                : CircleAvatar(
                                    radius: 30,
                                    backgroundImage: CachedNetworkImageProvider(
                                      _userData.image,
                                    ),
                                  ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 20, bottom: 10),
                            child: Label(
                              title: _userData.name,
                              textAlign: TextAlign.center,
                              fontSize: 16,
                              color: Color(appColors.blue00008B),
                            ),
                          ),
                        ],
                      );
                    }
                    return Container();
                  },
                ),
                Divider(
                  thickness: 3,
                  color: Colors.grey,
                ),
                ListView.builder(
                    padding: EdgeInsets.only(top: 20),
                    itemCount: 6,
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    scrollDirection: Axis.vertical,
                    itemBuilder: (BuildContext ctxt, int index) {
                      return GestureDetector(
                          onTap: () {
                            onSelectSideMenu(index);
                          },
                          child: menuListItem(index));
                    }),
                BlocListener<UserBloc, UserState>(
                  listenWhen: (oldState, newSate) {
                    return oldState.logoutResource == null ||
                        oldState.logoutResource.status !=
                            newSate.logoutResource.status;
                  },
                  listener: (context, state) {
                    if (state.logoutResource != null &&
                        state.logoutResource.status == STATUS.ERROR) {
                      Navigator.of(context).pop();
                      showSnackBarMessage(
                          context: context,
                          message: state.logoutResource.failure.message);
                    } else if (state.logoutResource.status == STATUS.SUCCESS) {
                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                          builder: (context) => LoginPage(),
                        ),
                      );
                    }
                  },
                  child: BlocBuilder<UserBloc, UserState>(
                    buildWhen: (oldState, newState) {
                      return oldState.logoutResource == null ||
                          oldState.logoutResource.status !=
                              newState.logoutResource.status;
                    },
                    builder: (context, state) {
                      if (state.logoutResource != null &&
                          state.logoutResource.status == STATUS.LOADING) {
                        return circularProgressIndicator();
                      }
                      return GestureDetector(
                        onTap: () {
                          print('logout called');
                          try {
                            BlocProvider.of<UserBloc>(context)
                                .add(LogoutEvent());
                          } catch (ex) {
                            print('logout exception $ex');
                          }
                        },
                        child: Container(
                          alignment: Alignment.center,
                          width: 150,
                          height: 50,
                          margin: EdgeInsets.fromLTRB(25, 10, 25, 25),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25),
                            color: Colors.red,
                          ),
                          child: Label(
                            title: "Log Out",
                            fontWeight: FontWeight.w700,
                            fontSize: 15,
                            color: Colors.white,
                          ),
                        ),
                      );
                    },
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget menuListItem(int index) {
    return Container(
      margin: EdgeInsets.only(bottom: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(left: 15, bottom: 15),
            child: Label(
              // title: homeMenuTitle[index],
              fontSize: 16,
            ),
          ),
          Divider(
            thickness: 0.5,
            color: Color(appColors.greyC2C2C2),
          ),
        ],
      ),
    );
  }

  actionIconsRight() {
    return Row(
      children: <Widget>[
        _currentScreen is HomeScreen || _currentScreen is DonationScreen
            ? Container(
                padding: const EdgeInsets.all(5),
                child: Row(
                  children: [
                    rightIconOneBool
                        ? GestureDetector(
                            onTap: () {
                              handleNavigation();
                            },
                            child: Container(
                              child: screenTitle == "Home"
                                  ? SvgPicture.asset(
                                      "assets/images/notification_icon.svg",
                                      width: 30,
                                      height: 30,
                                    )
                                  : SvgPicture.asset(
                                      "assets/images/donation_history.svg",
                                      width: 30,
                                      height: 30,
                                    ),
                            ),
                          )
                        : Container(),
                    SizedBox(
                      width: 15,
                    ),
                    rightIconTwoBool
                        ? GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => InfoScreen()));
                            },
                            child: Container(
                              margin: EdgeInsets.only(right: 10),
                              child: SvgPicture.asset(
                                "assets/images/info_icon.svg",
                                width: 30,
                                height: 30,
                              ),
                            ),
                          )
                        : Container(),
                  ],
                ),
              )
            : Container(width: 38),
      ],
    );
  }

  void handleNavigation() {
    if (screenTitle == "Home") {
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => NotificationScreen()));
    } else {
      donationHistoryBottomSheet();
    }
  }

  Future<Widget> donationHistoryBottomSheet() {
    return showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (context) {
          return Container(
              margin: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height / 3.5),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.vertical(top: Radius.circular(30.0)),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black,
                    offset: Offset(0.0, 1.0), //(x,y)
                    blurRadius: 5.0,
                  ),
                ],
              ),
              child: DonationHistory());
        });
  }

  calculateAge(DateTime birthDate) {
    DateTime currentDate = DateTime.now();
    int age = currentDate.year - birthDate.year;
    int month1 = currentDate.month;
    int month2 = birthDate.month;
    if (month2 > month1) {
      age--;
    } else if (month1 == month2) {
      int day1 = currentDate.day;
      int day2 = birthDate.day;
      if (day2 > day1) {
        age--;
      }
    }
    return age;
  }

}
