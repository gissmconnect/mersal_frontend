
import 'package:flutter/material.dart';
import 'package:mersal/core/widgets/common/CustomAppBar.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/features/committee_management/committee_member_detail.dart';
import 'package:mersal/resources/colors.dart';

class CommitteeMember extends StatefulWidget {
  @override
  _CommitteeMemberState createState() => _CommitteeMemberState();
}

class _CommitteeMemberState extends State<CommitteeMember> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
          title: "Committee Management", isBackButton: true, showText: true),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bg_image.jpg"),
            fit: BoxFit.fill,
          ),
        ),
        child: Column(
          children: [
            Expanded(
                child: ListView(
              shrinkWrap: true,
              children: [
                mainMenuItemGrid(),
              ],
            )),
          ],
        ),
      ),
    );
  }

  Widget mainMenuItemGrid() {
    return GridView.builder(
        padding: EdgeInsets.only(top: 7),
        shrinkWrap: true,
        physics: ScrollPhysics(),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
        ),
        itemCount: 10,
        itemBuilder: (BuildContext context, int index) {
          return menuGridItem(index);
        });
  }

  Widget menuGridItem(int index) {
    return GestureDetector(
      onTap: () {
        committeeDetailBottomSheet();
      },
      child: Container(
        margin: EdgeInsets.all(10),
        decoration: new BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
          border: Border.all(color: Colors.black, width: 2),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.only(top: 10),
              decoration: new BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(color: Colors.black, width: 2),
              ),
              child: Center(
                child: Icon(
                  Icons.person_outline,
                  size: 30,
                ),
              ),
            ),
            Container(
                margin: EdgeInsets.only(bottom: 10, top: 10),
                child: Label(
                  title: "Candidate Name",
                  textAlign: TextAlign.center,
                  fontSize: 14,
                  color: Color(appColors.blue00008B),
                ))
          ],
        ),
      ),
    );
  }

  Future<Widget> committeeDetailBottomSheet() {
    return showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (context) {
          return Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.vertical(top: Radius.circular(30.0)),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black,
                    offset: Offset(0.0, 1.0), //(x,y)
                    blurRadius: 5.0,
                  ),
                ],
                image: DecorationImage(
                  image: AssetImage("assets/images/bg_image.jpg"),
                  fit: BoxFit.fill,
                ),
              ),
              child: CommitteeMemberDetails());
        });
  }
}