import 'package:mersal/core/usecases/save_user_login_data_usecase.dart';
import 'package:mersal/features/auth/auth_domain/entity/user_data.dart';
import 'package:meta/meta.dart';

import '../model/user_data_model.dart';

const USER_DATA = "user_data";
const AUTH_TOKEN = "auth_token";
const AUTHORIZATION = "Authorization";
const BEARER = "Bearer";
const LOGIN_DATA = "login_data";

abstract class UserDataLocalDataSource {
  Future<UserDataModel> getUserData();

  Future<void> saveUserData({@required UserDataModel userDataModel});

  Future<void> saveAuthToken({@required String authToken});

  Future<String> getAuthToken();

  Future<void> saveLoginData({@required LoginParams loginParams});

  Future<LoginParams> getLoginData();

  Future<void> clear();

  Future<int>getDocumentStatus();

  Future<void>updatePassword({@required String password});
}
