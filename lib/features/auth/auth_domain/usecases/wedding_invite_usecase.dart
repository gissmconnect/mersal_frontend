import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/auth/auth_domain/repository/auth_repository.dart';
import 'package:meta/meta.dart';

class WeddingInviteUseCase extends UseCase<void, WeddingInviteParams> {
  final AuthRepository authRepo;

  WeddingInviteUseCase({@required this.authRepo});

  @override
  Future<Either<Failure, void>> call(WeddingInviteParams params) async {
    return await authRepo.weddingInvitation(
        invitationType: params.invitationType,
        date: params.date,
        time: params.time);
  }
}

class WeddingInviteParams {
  final String invitationType, date, time;

  WeddingInviteParams(
      {@required this.invitationType,
      @required this.date,
      @required this.time});
}
