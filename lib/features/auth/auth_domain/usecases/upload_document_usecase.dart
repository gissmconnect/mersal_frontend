import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/auth/auth_domain/repository/auth_repository.dart';
import 'package:meta/meta.dart';

class UploadDocumentUseCase extends UseCase<void, UploadDocParams> {
  final AuthRepository authRepository;

  UploadDocumentUseCase({@required this.authRepository});

  @override
  Future<Either<Failure, void>> call(UploadDocParams params) async {
    return await authRepository.uploadDocuments(
        frontImage: params.frontImage,
        backImage: params.backImage,
        name: params.name,
        dob: params.dob,
        expDate: params.expDate,
        idNumber: params.idNumber);
  }
}

class UploadDocParams {
  final String frontImage, backImage, name, dob, expDate, idNumber;

  UploadDocParams(
      {@required this.frontImage,
      @required this.backImage,
      @required this.name,
      @required this.dob,
      @required this.expDate,
      @required this.idNumber});
}
