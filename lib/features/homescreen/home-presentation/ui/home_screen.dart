import 'dart:async';
import 'dart:convert';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:mersal/core/bloc/user_bloc.dart';
import 'package:mersal/core/localization/app_localization.dart';
import 'package:mersal/core/network/api_endpoints.dart';
import 'package:mersal/core/network/http_error_handler.dart';
import 'package:mersal/core/network/vo/Status.dart';
import 'package:mersal/core/util/helper_functions.dart';
import 'package:mersal/core/widgets/common/CirclePageIndicator.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/core/widgets/common/shared_preference.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/homescreen/home-data/model/ads/AdDataList.dart';
import 'package:mersal/features/homescreen/home-data/model/ads/AdDataModel.dart';
import 'package:mersal/features/homescreen/home-data/model/alarm/AlarmData.dart';
import 'package:mersal/features/homescreen/home-data/model/alarm/AlarmDataModel.dart';
import 'package:mersal/features/homescreen/home-data/model/alarm/HomeOccasionModel.dart';
import 'package:mersal/features/homescreen/home-data/model/alarm/NextPrayerModel.dart';
import 'package:mersal/features/homescreen/home-data/model/banner/BannerDataModel.dart';
import 'package:mersal/features/homescreen/home-data/model/banner/BannerListDataModel.dart';
import 'package:mersal/features/homescreen/home-domain/entity/BannerDataEntity.dart';
import 'package:mersal/features/homescreen/home-presentation/bloc/home_bloc.dart';
import 'package:mersal/features/info_presentation/ui/info_screen.dart';
import 'package:mersal/features/notification/notification_presentation/ui/notification.dart';
import 'package:mersal/resources/colors.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../../injection_container.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<HomeBloc>(
      create: (_) => serviceLocator<HomeBloc>(),
      child: Scaffold(
        backgroundColor: Colors.grey.shade200,
        body: _HomScreenBody(),
      ),
    );
  }
}

class _HomScreenBody extends StatefulWidget {
  const _HomScreenBody({
    Key key,
  }) : super(key: key);

  @override
  __HomScreenBodyState createState() => __HomScreenBodyState();
}

class __HomScreenBodyState extends State<_HomScreenBody> {
  PageController _pageController = new PageController();
  final _currentPageNotifier = ValueNotifier<int>(0);
  int _currentPage = 0;
  var _userData;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  BuildContext mContextLoader;
  List<BannerDataList> bannerList = [];
  BannerDataModel bannerDataModel;
  List<AdDataList> adDataList = [];
  AdDataModel adDataModel;
  AlarmDataModel alarmDataModel;
  AlarmData alarmData;
  HomeOccasionModelData occasionModelData;
  NextPrayerModel nextPrayerModel;
  String occasionName = "";
  var outputDate;
  int currentPos = 0;

  @override
  void initState() {
    super.initState();

    // BlocProvider.of<BannerBloc>(context).add(GetBanners());
    BlocProvider.of<UserBloc>(context).add(GetUserSessionEvent());

    Future.delayed(Duration(milliseconds: 1000), () {
      apiBannerCall(context);
    });
    DateFormat('hh:mm a').format(DateTime.now());
  }

  String greeting() {
    var hour = DateTime.now().hour;
    if (hour < 12) {
      return AppLocalizations.of(context).translate("Good Morning");
    }
    if (hour < 17) {
      return AppLocalizations.of(context).translate('Good Afternoon');
    }
    return AppLocalizations.of(context).translate( 'Good Evening');
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/images/bg_image.jpg"),
          fit: BoxFit.fill,
        ),
      ),
      child: SingleChildScrollView(
        child: BlocBuilder<UserBloc, UserState>(
          buildWhen: (oldState, newState) {
            return userDataUpdated(oldState, newState);
          },
          builder: (context, state) {
            if (state.userDataLoadingResource.status == STATUS.SUCCESS) {
              _userData = state.userDataLoadingResource.data;
              return Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(top: 30),
                        padding: const EdgeInsets.all(5),
                        child: Row(
                          children: [
                            GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            NotificationScreen()));
                              },
                              child: Container(
                                child: SvgPicture.asset(
                                  "assets/images/notification_icon.svg",
                                  width: 25,
                                  height: 25,
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 15,
                            ),
                            GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => InfoScreen()));
                              },
                              child: Container(
                                margin: EdgeInsets.only(right: 10),
                                child: SvgPicture.asset(
                                  "assets/images/info_icon.svg",
                                  width: 25,
                                  height: 25,
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 20,right: 20),
                        child: Label(
                          title: greeting() + ",",
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                          color: Colors.white,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 20,right: 20),
                        child: Label(
                          title: _userData.name ?? "N/A",
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                          color: Colors.white,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 20, top: 8,right: 20),
                        child: Label(
                          title: AppLocalizations.of(context).translate("Get a new experience"),
                          fontSize: 25,
                          fontWeight: FontWeight.w600,
                          color: Colors.white,
                        ),
                      ),
                      occasionModelData != null || nextPrayerModel != null
                          ? Container(
                              width: MediaQuery.of(context).size.width,
                              margin:
                                  EdgeInsets.only(top: 16, left: 5, right: 10),
                              child: Stack(
                                children: [
                                  Container(
                                    height: MediaQuery.of(context).size.height /
                                        3.2,
                                    width: MediaQuery.of(context).size.width,
                                    child: Image.asset(
                                      "assets/images/alarm_bg.png",
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 25, top: 65,right: 25),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        nextPrayerModel != null
                                            ? Row(
                                                children: [
                                                  Icon(
                                                    Icons
                                                        .notifications_active_outlined,
                                                    color: Color(
                                                        appColors.buttonColor),
                                                  ),
                                                  SizedBox(
                                                    width: 5,
                                                  ),
                                                  Label(
                                                    title:AppLocalizations.of(context).translate( "Alarm"),
                                                    fontSize: 13,
                                                    fontWeight: FontWeight.w600,
                                                    color: Color(
                                                        appColors.buttonColor),
                                                  ),
                                                ],
                                              )
                                            : Container(),
                                        SizedBox(
                                          height: alarmData != null ? 10 : 0,
                                        ),
                                        nextPrayerModel != null
                                            ? Label(
                                                title: AppLocalizations.of(context).translate("Next Prayer"),
                                                fontSize: 11,
                                                fontWeight: FontWeight.w600,
                                                color: Color(
                                                    appColors.buttonColor),
                                              )
                                            : Container(),
                                        nextPrayerModel != null
                                            ? Label(
                                                title: CommonMethods
                                                    .dateFormatterTime(
                                                        nextPrayerModel
                                                                .prayer_time ??
                                                            ""),
                                                fontSize: 16,
                                                fontWeight: FontWeight.w600,
                                                color: Color(
                                                    appColors.buttonColor),
                                              )
                                            : Container(),
                                        nextPrayerModel != null
                                            ? Label(
                                                title:
                                                    nextPrayerModel.name ?? "",
                                                fontSize: 14,
                                                fontWeight: FontWeight.w600,
                                                color: Color(
                                                    appColors.buttonColor),
                                              )
                                            : Container(),
                                        SizedBox(
                                          height: alarmData != null ? 5 : 0,
                                        ),
                                        occasionModelData != null
                                            ? Label(
                                          title: AppLocalizations.of(context).translate("Next Occasion"),
                                                fontSize: 11,
                                                fontWeight: FontWeight.w600,
                                                color: Color(
                                                    appColors.buttonColor),
                                              )
                                            : Container(),
                                        occasionModelData != null
                                            ? Label(
                                                title: occasionModelData.title,
                                                fontSize: 13,
                                                fontWeight: FontWeight.w600,
                                                color: Color(
                                                    appColors.buttonColor),
                                              )
                                            : Container(),
                                        occasionModelData != null
                                            ? Label(
                                                title: outputDate,
                                                fontSize: 13,
                                                fontWeight: FontWeight.w600,
                                                color: Color(
                                                    appColors.buttonColor),
                                              )
                                            : Container(),
                                        SizedBox(
                                          height: 25,
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            )
                          : Container(),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        child: CarouselSlider(
                          options: CarouselOptions(
                            height: 220.0,
                            onPageChanged: (index, reason) {
                              setState(() {
                                currentPos = index;
                              });
                            },
                            viewportFraction: 1,
                            autoPlayInterval: Duration(seconds: 10),
                            autoPlayAnimationDuration: Duration(milliseconds: 1000),
                            autoPlay: true,
                          ),
                          items: bannerList.map((item) {
                            return GestureDetector(
                              onTap: () {
                                _launchURL(item.link);
                              },
                              child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  margin: EdgeInsets.only(
                                      top: 6, left: 10, right: 10),
                                  child: Card(
                                      semanticContainer: true,
                                      clipBehavior: Clip.antiAliasWithSaveLayer,
                                      shape: RoundedRectangleBorder(
                                        side: BorderSide(
                                            color: Colors.cyan, width: 1),
                                        borderRadius: BorderRadius.circular(25),
                                      ),
                                      elevation: 5,
                                      child: Image.network(
                                        item.image,
                                        fit: BoxFit.fill,
                                      ))),
                            );
                          }).toList(),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: bannerList.map((url) {
                          int index = bannerList.indexOf(url);
                          return Container(
                            width: 12.0,
                            height: 12.0,
                            margin: EdgeInsets.symmetric(vertical: 20.0, horizontal: 5.0),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: currentPos == index
                                  ? Colors.cyan
                                  : Colors.white,
                            ),
                          );
                        }).toList(),
                      ),
                     /* bannerList != null && bannerList.length > 0
                          ? _buildPageView()
                          : Container(),*/
                    /*  Center(
                        child: Container(
                            padding: EdgeInsets.all(25),
                            child: _buildPageIndicator()),
                      ),*/
                      adDataList != null && adDataList.length > 0
                          ? Container(
                              margin: EdgeInsets.only(bottom: 40),
                              child: ListView.builder(
                                shrinkWrap: true,
                                physics: NeverScrollableScrollPhysics(),
                                itemCount: adDataList.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return Container(
                                    margin: EdgeInsets.only(
                                        top: 6, left: 10, right: 10),
                                    child: Card(
                                      semanticContainer: true,
                                      clipBehavior: Clip.antiAliasWithSaveLayer,
                                      shape: RoundedRectangleBorder(
                                        side: BorderSide(
                                            color: Colors.cyan, width: 1),
                                        borderRadius: BorderRadius.circular(25),
                                      ),
                                      elevation: 5,
                                      child: Container(
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(25),
                                          color: Colors.white,
                                        ),
                                        alignment: Alignment.center,
                                        margin: EdgeInsets.only(
                                            left: 20,
                                            top: 20,
                                            right: 20,
                                            bottom: 20),
                                        child: Label(
                                          title: adDataList[index].title,
                                          fontSize: 18,
                                          textAlign: TextAlign.center,
                                          fontWeight: FontWeight.w600,
                                          color: Color(appColors.navyBlue),
                                        ),
                                      ),
                                    ),
                                  );
                                },
                              ),
                            )
                          : Container()
                    ],
                  ),
                ],
              );
            } else {
              return Container();
            }
          },
        ),
      ),
    );
  }

  Widget _buildPageView() {
    return Container(
        margin: EdgeInsets.only(top: 15),
        height: 220,
        child: PageView.builder(
            itemCount: bannerList.length,
            controller: _pageController,
            itemBuilder: (BuildContext context, int index) {
              return GestureDetector(
                onTap: () {
                  _launchURL(bannerList[index].link);
                },
                child: Container(
                    margin: EdgeInsets.only(top: 6, left: 10, right: 10),
                    child: Card(
                        semanticContainer: true,
                        clipBehavior: Clip.antiAliasWithSaveLayer,
                        shape: RoundedRectangleBorder(
                          side: BorderSide(color: Colors.cyan, width: 1),
                          borderRadius: BorderRadius.circular(25),
                        ),
                        elevation: 5,
                        child: Image.network(
                          bannerList[index].image,
                          fit: BoxFit.fill,
                        ))),
              );
            },
            onPageChanged: (int index) {
              _currentPageNotifier.value = index;
              setState(() {});
            }));
  }

  Widget _buildPageIndicator() {
    return CirclePageIndicator(
      itemCount: bannerList.length,
      dotColor: Colors.white,
      selectedDotColor: Colors.cyan,
      currentPageNotifier: _currentPageNotifier,
      size: 10.0,
      selectedSize: 15.0,
    );
  }

  Future<void> alarmApiCall() async {
    final _token = await appPreferences.getStringPreference("AccessToken");
    var response = await http.get(
      Uri.parse(GET_ALARM),
      headers: {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        AUTHORIZATION: BEARER + " " + _token
      },
    );
    if (response.statusCode == 200) {
      final Map<String, dynamic> _responseJson = json.decode(response.body);
      print(
          'AlarmResponse ${response.statusCode} and ${_responseJson.toString()}');
      alarmDataModel = AlarmDataModel.fromJson(_responseJson);
      if (alarmDataModel.alarmData != null) {
        alarmData = alarmDataModel.alarmData.alarmData;
        occasionModelData = alarmDataModel.alarmData.occasionModel;
        nextPrayerModel = alarmDataModel.alarmData.nextPrayerModel;
        if (alarmDataModel.alarmData.alarmData != null) {
          appPreferences.saveStringPreference(
              "Prayer", alarmDataModel.alarmData.alarmData.name);
        }

        if (occasionModelData != null) {
          DateTime parseDate = new DateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
              .parse(occasionModelData.occassion_date);
          var inputDate = DateTime.parse(parseDate.toString());
          var outputFormat = DateFormat('dd-MM-yyyy hh:mm a');
          outputDate = outputFormat.format(inputDate);
          print(outputDate);
        }
      }
      if (mounted) {
        setState(() {});
      }
    }
  }

  Future<void> occasionApiCall() async {
    final _token = await appPreferences.getStringPreference("AccessToken");
    var response = await http.get(
      Uri.parse(PAYMENT_GENERATE_SESSION + "?type=lawati-sms&id=1"),
      headers: {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        AUTHORIZATION: BEARER + " " + _token
      },
    );

    if (response.statusCode == 200) {
      final Map<String, dynamic> _responseJson = json.decode(response.body);
      print('OccasionResponse ${response.statusCode} and ${_responseJson.toString()}');
      setState(() {});
    }
  }

  showAlertDialog(BuildContext context) {

  // set up the button
  Widget okButton = TextButton(
    child: Text("OK"),
    onPressed: () => Navigator.pop(context, 'OK'),
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text("API Error"),
    content: Text("Getting Error in API"),
    actions: [
      okButton,
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

  Future<void> apiBannerCall(context) async {
    final _token = await appPreferences.getStringPreference("AccessToken");

    if (context != null) {
      _showLoader(context);
    }

    var response = await http.get(Uri.parse(GET_BANNER_LIST), headers: {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      AUTHORIZATION: BEARER + " " + _token
    });

    if (response.statusCode == 200) {
      try {
        final Map<String, dynamic> _responseJson = json.decode(response.body);
        if (mContextLoader != null) Navigator.pop(mContextLoader);

        print(
            'BANNERFinalResponse ${response.statusCode} and ${_responseJson.toString()}');
        ((json.decode(response.body)['data']) as List<dynamic>)
            .map((e) => BannerListDataModel.fromJson(e))
            .toList();

        bannerDataModel = BannerDataModel.fromJson(_responseJson);
        bannerList = bannerDataModel.bannerList;
        setState(() {});

        // Future.delayed(Duration(milliseconds: 1000), () {
        //   addAlarmCall();
        // });
        Future.delayed(Duration(milliseconds: 1000), () {
          alarmApiCall();
        });

        Timer.periodic(Duration(seconds: 10), (Timer timer) {
          if (_currentPage < 2) {
            _currentPage++;
          } else {
            _currentPage = 0;
          }
          if (_pageController.hasClients) {
            _pageController.animateToPage(
              _currentPage,
              duration: Duration(milliseconds: 200),
              curve: Curves.easeIn,
            );
          }
        });
      } catch (ex) {
        // showAlertDialog(context);
  //       AlertDialog(  
  //   title: Text("Simple Alert"),  
  //   content: Text("This is an alert message."),  
  // );  
      }
    } else {
      handleError(response, () => showAlertDialog(context));
    }
  }

  Future<Null> _showLoader(BuildContext context) async {
    return showDialog<Null>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context1) {
        mContextLoader = context1;
        return Scaffold(
          backgroundColor: Colors.transparent.withOpacity(0.3),
          key: _keyLoader,
          body: Container(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    child: CircularProgressIndicator(),
                    height: 40.0,
                    width: 40.0,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  void _launchURL(String _url) async =>
      await canLaunch("https://www.google.com/")
          ? await launch("https://www.google.com/")
          : throw 'Could not launch $_url';
}
