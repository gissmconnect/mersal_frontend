import 'package:mersal/features/donation/data/model/donation_type_model.dart';
import 'package:meta/meta.dart';
abstract class DonationDataSource{
  Future<List<DonationTypeModel>> getDonationType({@required String token});
  Future<dynamic> donation(
      {@required String token,@required int reasonId,
        @required String donationAmount,
        @required String donationDescription});
}