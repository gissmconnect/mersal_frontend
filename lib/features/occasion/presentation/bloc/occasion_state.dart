part of 'occasion_bloc.dart';

abstract class OccasionState extends Equatable {
  @override
  List<Object> get props => [];
}

class OccasionInitial extends OccasionState {}

class OccasionLoadingState extends OccasionState{}
class OccasionLoadedState extends OccasionState{
  final OccasionEntity occasion;

  OccasionLoadedState({@required this.occasion});
}
class OccasionFailureState extends OccasionState{
  final Failure failure;

  OccasionFailureState({@required this.failure});
}