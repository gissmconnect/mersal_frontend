import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/exceptions.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/network/network_info.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/auth/auth_data/model/user_data_model.dart';
import 'package:mersal/features/profile/edit_profile_data/data_source/edit_profile_data_source.dart';
import 'package:mersal/features/profile/edit_profile_domain/repository/edit_profile_repository.dart';
import 'package:meta/meta.dart';

class EditProfileRepositoryImpl implements EditProfileRepository {
  final NetworkInfo networkInfo;
  final EditProfileDataSource editProfileDataSource;
  final UserDataLocalDataSource userDataLocalDataSource;

  EditProfileRepositoryImpl(
      {@required this.networkInfo,
      @required this.editProfileDataSource,
      @required this.userDataLocalDataSource});

  @override
  Future<Either<Failure, UserDataModel>> updateProfile(
      {String name, String dob, int gender, String image, String bloodGroup}) async {
    if (await networkInfo.isConnected) {
      try {
        final _token = await userDataLocalDataSource.getAuthToken();
        final _response = await editProfileDataSource.editProfile(
            token: _token,
            name: name,
            dob: dob,
            image: image,
            gender: gender,
            bloodGroup: bloodGroup);
        print('image $image');
        await userDataLocalDataSource.saveUserData(
            userDataModel: _response);
        return Right(_response);
      } on ServerException catch (ex) {
        return Left(ServerFailure()..message = ex.message);
      }
    } else {
      return Left(NetworkFailure());
    }
  }
}
