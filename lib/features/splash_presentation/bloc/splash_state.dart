part of 'splash_bloc.dart';

abstract class SplashState extends Equatable {
  const SplashState();

  @override
  List<Object> get props => [];
}

class Empty extends SplashState {}

class Loading extends SplashState {}

class Loaded extends SplashState {}

class Error extends SplashState {
  final Failure failure;

  Error({@required this.failure});
}
