import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/child_service/child-data/model/child-enroll-model/registered-child-activity/child_activity_model.dart';
import 'package:mersal/features/child_service/child-data/model/child-enroll-model/registered-child-details/child_details_model.dart';
import 'package:mersal/features/child_service/child-domain/repository/child_repository.dart';
import 'package:meta/meta.dart';

class ChildDetailsUseCase extends UseCase<ChildDetailsModel, NoParams> {
  final ChildRepository childRepository;

  ChildDetailsUseCase({@required this.childRepository});

  @override
  Future<Either<Failure, ChildDetailsModel>> call(NoParams params) {
    return childRepository.getChildDetails();
  }
}
