import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:mersal/core/error/exceptions.dart';
import 'package:mersal/core/network/api_endpoints.dart';
import 'package:mersal/core/network/http_call.dart';
import 'package:mersal/core/network/http_error_handler.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/survey/data/datasource/survey_datasource.dart';
import 'package:mersal/features/survey/data/model/survey_model.dart';
import 'package:meta/meta.dart';

class SurveyDataSourceImpl implements SurveyDataSource {
  final http.Client client;

  SurveyDataSourceImpl({@required this.client});

  @override
  Future<SurveyModel> getSurvey({String token}) async {
    final _response = await httpGetRequest(
        httpClient: client, url: GET_SURVEY_ENDPOINT, token: token);
    if (_response.statusCode == 200) {
      final _surveyResponse = json.decode(_response.body);
      print('survey response ${_surveyResponse}');
      return SurveyModel.fromJson(_surveyResponse['data']);
    } else {
      handleError(_response);
    }
  }

  @override
  Future<SurveyModel> submitSurveyQuestion({String token}) async {
    final _response = await httpGetRequest(
        httpClient: client, url: SUBMIT_SURVEY_QUESTION_ENDPOINT, token: token);
    if (_response.statusCode == 200) {
      final _surveyResponse = json.decode(_response.body);
      print('survey question response ${_surveyResponse}');
      // return SurveyModel.fromJson(_surveyResponse['data']);
    } else {
      handleError(_response);
    }
  }

  @override
  Future<void> surveyQuestion({String token, int surveyId, int surveyQuestionId, int surveyOptionId}) async {
    final _params = <String, String>{
      'survey_id': surveyId.toString(),
      'survey_question_id': surveyQuestionId.toString(),
      'survey_option_id': surveyOptionId.toString()
    };

    var response = await client.post(Uri.parse(SUBMIT_SURVEY_QUESTION_ENDPOINT),
        headers: {
          'Content-type': 'application/json',
          AUTHORIZATION: BEARER + " " + token
        },
        body: json.encode(_params));

    if (response.statusCode == 200) {
      final Map<String, dynamic> _responseJson = json.decode(response.body);
    }
    handleError(response);
  }
}
