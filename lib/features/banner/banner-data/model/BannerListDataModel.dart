import 'package:mersal/features/homescreen/home-domain/entity/BannerListDataEntity.dart';

class BannerListDataModel extends BannerListDataEntity {
  BannerListDataModel({
    int id,
    String image,
    String created_at,
    String updated_at,
  }) : super(
          id: id,
          image: image,
          created_at: created_at,
          updated_at: updated_at,
        );

  BannerListDataModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    image = json['image'];
    created_at = json['created_at'];
    updated_at = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['image'] = this.image;
    data['created_at'] = this.created_at;
    data['updated_at'] = this.updated_at;

    return data;
  }
}
