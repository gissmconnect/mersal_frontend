part of 'child_registration_bloc.dart';

abstract class ChildRegistrationState extends Equatable {}

class ChildEnrollmentInitial extends ChildRegistrationState {
  @override
  List<Object> get props => [];
}

class ChildRegistering extends ChildRegistrationState {
  @override
  List<Object> get props => [];
}

class ChildActivityLoaded extends ChildRegistrationState {
  final ChildActivityModel activity;

  ChildActivityLoaded({@required this.activity});

  @override
  List<Object> get props => [activity];
}

class ChildDetailsFetch extends ChildRegistrationState {
  @override
  List<Object> get props => [];
}

class ChildDetailsLoaded extends ChildRegistrationState {
  final ChildDetailsModel detailsDataModel;

  ChildDetailsLoaded({@required this.detailsDataModel});

  @override
  List<Object> get props => [detailsDataModel];
}

class ChildEnrollmentError extends ChildRegistrationState {
  final Failure failure;

  ChildEnrollmentError(this.failure);

  @override
  List<Object> get props => [failure];
}
