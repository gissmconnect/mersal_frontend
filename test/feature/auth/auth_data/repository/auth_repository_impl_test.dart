import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:matcher/matcher.dart';
import 'package:mersal/core/error/exceptions.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/save_user_login_data_usecase.dart';
import 'package:mersal/features/auth/auth_data/model/user_data_model.dart';
import 'package:mersal/features/auth/auth_data/repository/auth_repository_impl.dart';
import 'package:mersal/features/auth/auth_domain/entity/user_data.dart';
import 'package:mockito/mockito.dart';

import '../../../../fixtures/fixture_reader.dart';
import '../../../../mocked_classes.dart';

void main() {
  MockUserDataLocalDataSource mockUserDataLocalDataSource;
  MockAuthDataSource mockAuthDataSource;
  MockNetworkInfo mockNetworkInfo;
  AuthRepositoryImpl authRepository;
  setUp(() {
    mockUserDataLocalDataSource = MockUserDataLocalDataSource();
    mockNetworkInfo = MockNetworkInfo();
    mockAuthDataSource = MockAuthDataSource();
    authRepository = AuthRepositoryImpl(
      userDataLocalDataSource: mockUserDataLocalDataSource,
      authDataSource: mockAuthDataSource,
      networkInfo: mockNetworkInfo,
    );
  });

  test('should check if the device is offline', () {
    when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);

    authRepository.getUserSession();

    verify(mockNetworkInfo.isConnected);
  });

  test('should return network failure when network connection failed',
      () async {
    when(mockNetworkInfo.isConnected).thenAnswer((_) async => false);
    final result = await authRepository.getUserSession();
    verifyNoMoreInteractions(mockUserDataLocalDataSource);
    expect(result, equals(Left(NetworkFailure())));
  });

  final String _phoneNumber = '1234567';
  final String _password = 'abcd';
  final String _otp = '1234567';

  group('device_offline', () {
    setUp(() {
      when(mockNetworkInfo.isConnected)
          .thenAnswer((realInvocation) async => false);
    });
    test('should return NetworkFailure when login is attempted', () async {
      //arrange

      //act
      final _result = await authRepository.login(
          phoneNumber: _phoneNumber, password: _password);

      //assert
      verifyZeroInteractions(mockAuthDataSource);
      expect(_result, equals(Left(NetworkFailure())));
    });

    test('should return network failure when registration is attempted',
        () async {
      //arrange

      //act
      final _result = await authRepository.register(
          phoneNumber: _phoneNumber, password: _password);

      //assert
      verifyZeroInteractions(mockAuthDataSource);
      expect(_result, equals(Left(NetworkFailure())));
    });

    //Re-Send OTP
    test('should return NetworkFailure when internet is not available',
        () async {
      //arrange

      //act
      final _result = await authRepository.sendOtp(phoneNumber: _phoneNumber);

      //assert
      verifyZeroInteractions(mockAuthDataSource);
      expect(_result, equals(Left(NetworkFailure())));
    });

    //Verify OTP
    test('should return network failure when network is not available',
        () async {
      //arrange

      //act
      final _result =
          await authRepository.verifyOtp(phoneNumber: _phoneNumber, otp: _otp);

      //assert
      verifyZeroInteractions(mockAuthDataSource);
      expect(_result, equals(Left(NetworkFailure())));
    });
  });

  group('device_online', () {
    Map<String, dynamic> _userDataMap;
    UserLoginModel _tUserLoginModel;
    UserDataModel _tUserDataModel;
    UserData _expectedTUserData;
    setUp(() {
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      _userDataMap = json.decode(fixture('login_response.json'));
      _tUserLoginModel = UserLoginModel.fromJson(_userDataMap);
      _tUserDataModel = _tUserLoginModel.data;
      _expectedTUserData = _tUserDataModel;
    });

    group('login', () {
      void _mockLoginException(Exception exception) {
        return when(mockAuthDataSource.login(
                phoneNumber: anyNamed('phoneNumber'),
                password: anyNamed('password')))
            .thenThrow(exception);
      }

      test('should return UserSessionFailure when there is no cached user data',
          () async {
        when(mockUserDataLocalDataSource.getUserData())
            .thenThrow(UserSessionFailure());
        final result = await authRepository.getUserSession();
        verify(mockUserDataLocalDataSource.getUserData());
        expect(result, equals(Left(UserSessionFailure())));
      });

      test('should return UserData entity when there is cached used data',
          () async {
        when(mockUserDataLocalDataSource.getUserData())
            .thenAnswer((_) async => _tUserDataModel);

        final result = await authRepository.getUserSession();
        verify(mockUserDataLocalDataSource.getUserData());
        expect(result, equals(Right(_expectedTUserData)));
      });

      test('should return UserData entity when login successful', () async {
        //arrange
        when(mockAuthDataSource.login(
                phoneNumber: _phoneNumber, password: _password))
            .thenAnswer((realInvocation) async => _tUserLoginModel);

        //act
        final _result = await authRepository.login(
            phoneNumber: _phoneNumber, password: _password);

        //assert
        verify(mockAuthDataSource.login(
            phoneNumber: _phoneNumber, password: _password));
        expect(_result, equals(Right(_expectedTUserData)));
      });

      test(
          'should cache UserData model and AuthToken of the UserData  json and auth token when login response successful',
          () async {
        //arrange
        when(mockAuthDataSource.login(
                phoneNumber: anyNamed('phoneNumber'),
                password: anyNamed('password')))
            .thenAnswer((_) async => _tUserLoginModel);
        //act
        await authRepository.login(
            phoneNumber: _phoneNumber, password: _password);

        //assert
        verify(mockAuthDataSource.login(
            phoneNumber: _phoneNumber, password: _password));
        verify(mockUserDataLocalDataSource.saveUserData(
            userDataModel: _tUserLoginModel.data));
        verify(mockUserDataLocalDataSource.saveAuthToken(
            authToken: _tUserDataModel.authToken));
      });

      test('should return AuthFailure when AuthException happens', () async {
        //arrange
        _mockLoginException(AuthException());

        //act
        final _result = await authRepository.login(
            phoneNumber: _phoneNumber, password: _password);

        //assert
        verify(mockAuthDataSource.login(
            phoneNumber: _phoneNumber, password: _password));
        expect(_result, equals(Left(AuthFailure())));
      });

      test('should return ServerFailure when ServerException happens',
          () async {
        _mockLoginException(ServerException());
        //act
        final _result = await authRepository.login(
            phoneNumber: _phoneNumber, password: _password);

        //assert
        verify(mockAuthDataSource.login(
            phoneNumber: _phoneNumber, password: _password));
        expect(_result, equals(Left(ServerFailure())));
      });
    });

    group('registration', () {
      test('should return void when registration is successful', () async {
        //arrange
        when(mockAuthDataSource.register(
                phoneNumber: anyNamed('phoneNumber'),
                password: anyNamed('password')))
            .thenAnswer((realInvocation) => null);

        //act
        final _result = await authRepository.register(
            phoneNumber: _phoneNumber, password: _password);

        //assert
        verify(mockAuthDataSource.register(
            phoneNumber: _phoneNumber, password: _password));
        expect(_result, Right(null));
      });

      test(
          'should return ServerFailure with message when registration is unsuccessful',
          () async {
        //arrange
        when(mockAuthDataSource.register(
                phoneNumber: anyNamed('phoneNumber'),
                password: anyNamed('password')))
            .thenThrow(ServerException());

        //act
        final _result = await authRepository.register(
            phoneNumber: _phoneNumber, password: _password);

        //assert
        verify(mockAuthDataSource.register(
            phoneNumber: _phoneNumber, password: _password));

        expect(_result, equals(Left(ServerFailure())));
      });
    });

    final _otp_sent_msg = "Otp has been sent succesfully";
    group('otp', () {
      test('should return OTP sent success message when otp sent successfully',
          () async {
        //arrange
        when(
          mockAuthDataSource.sendOtp(
            phoneNumber: anyNamed('phoneNumber'),
          ),
        ).thenAnswer((realInvocation) async => _otp_sent_msg);

        //act
        final _result = await authRepository.sendOtp(phoneNumber: _phoneNumber);

        //assert
        verify(mockAuthDataSource.sendOtp(phoneNumber: _phoneNumber));
        expect(_result, equals(Right(_otp_sent_msg)));
      });

      test('should return ServerFailure with message when otp send fails',
          () async {
        //arrange
        when(
          mockAuthDataSource.sendOtp(
            phoneNumber: anyNamed('phoneNumber'),
          ),
        ).thenThrow(ServerException());

        //act
        final _result = await authRepository.sendOtp(phoneNumber: _phoneNumber);

        //assert
        verify(mockAuthDataSource.sendOtp(phoneNumber: _phoneNumber));

        expect(_result, equals(Left(ServerFailure())));
      });
    });

    group('otp_verification', () {
      test('should return null when OTP verification is successful', () async {
        //arrange
        when(mockAuthDataSource.verifyOtp(
                phoneNumber: anyNamed('phoneNumber'), otp: anyNamed('otp')))
            .thenAnswer((realInvocation) async => Right(null));

        //act
        final _result = await authRepository.verifyOtp(
            phoneNumber: _phoneNumber, otp: _otp);

        //assert
        verify(
            mockAuthDataSource.verifyOtp(phoneNumber: _phoneNumber, otp: _otp));
        expect(_result, equals(Right(null)));
      });

      test('should return ServerFailure when there is a ServerException',
          () async {
        //arrange
        when(mockAuthDataSource.verifyOtp(
                phoneNumber: anyNamed('phoneNumber'), otp: anyNamed('otp')))
            .thenThrow(ServerException());

        //act
        final _result = await authRepository.verifyOtp(
            phoneNumber: _phoneNumber, otp: _otp);
        //assert
        verify(
            mockAuthDataSource.verifyOtp(phoneNumber: _phoneNumber, otp: _otp));
        expect(_result, equals(Left(ServerFailure())));
      });
    });

    group('login_data', () {
      final _loginId = 'test';
      final _loginPassword = 'test';
      final _tLoginParasm =
          LoginParams(loginId: _loginId, password: _loginPassword);

      test('should return LoginParams when data is cached', () async {
        when(mockUserDataLocalDataSource.getLoginData())
            .thenAnswer((realInvocation) async => _tLoginParasm);

        //act
        final _result = await authRepository.getLoginData();

        //assert
        verify(mockUserDataLocalDataSource.getLoginData());
        expect(_result, equals(Right(_tLoginParasm)));
      });
    });
  });
}
