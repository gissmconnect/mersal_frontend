class CountryDataList {
  int id;
  String iso;
  String nicename;
  String iso3;
  String numcode;
  String phonecode;

  CountryDataList(
      {this.id,
      this.iso,
      this.nicename,
      this.iso3,
      this.numcode,
      this.phonecode});

  factory CountryDataList.fromJson(Map<String, dynamic> json) {
    return CountryDataList(
      nicename: json['nicename']!=null?json['nicename']:"",
      id: json['id']!=null ?json['id']:"",
      iso: json['iso']!=null?json['iso']:"",
      iso3: json['iso3']!=null?json['iso3']:"",
      numcode: json['numcode']!=null?json['numcode']:"",
      phonecode: json['phonecode']!=null ?json['phonecode']:"",
    );
  }
}
