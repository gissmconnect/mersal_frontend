import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/exceptions.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/network/network_info.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/wedding_invite_presentation/data/datasource/wedding_invite_datasource.dart';
import 'package:mersal/features/wedding_invite_presentation/domain/entity/wedding_template_entity.dart';
import 'package:mersal/features/wedding_invite_presentation/domain/repository/wedding_template_repo.dart';
import 'package:meta/meta.dart';

class WeddingInviteRepoImpl implements WeddingTemplateRepo {
  final WeddingInviteDataSource weddingTemplateDataSource;
  final NetworkInfo networkInfo;
  final UserDataLocalDataSource userDataLocalDataSource;

  WeddingInviteRepoImpl(
      {@required this.weddingTemplateDataSource,
      @required this.networkInfo,
      @required this.userDataLocalDataSource});

  @override
  Future<Either<Failure, WeddingTemplateEntity>> getWeddingTemplate() async {
    if (await networkInfo.isConnected) {
      final _token = await userDataLocalDataSource.getAuthToken();
      try {
        final _service =
            await weddingTemplateDataSource.getWeddingTemplates(token: _token);
        return Right(_service);
      } on ServerException catch (ex) {
        return Left(ServerFailure()..message = ex.message);
      } on AuthException catch (ex) {
        return Left(AuthFailure()..message = ex.message);
      } on Exception {
        return Left(ServerFailure());
      }
    } else {
      return Left(NetworkFailure());
    }
  }
}
