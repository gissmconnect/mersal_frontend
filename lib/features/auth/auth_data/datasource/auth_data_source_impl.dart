import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:http/http.dart' as http;
import 'package:mersal/core/error/exceptions.dart';
import 'package:mersal/core/network/api_endpoints.dart';
import 'package:mersal/core/network/http_call.dart';
import 'package:mersal/core/network/http_error_handler.dart';
import 'package:mersal/features/auth/auth_data/datasource/auth_data_source.dart';
import 'package:mersal/features/auth/auth_data/model/user_data_model.dart';
import 'package:meta/meta.dart';

class AuthDataSourceImpl implements AuthDataSource {
  final http.Client httpClient;

  AuthDataSourceImpl({@required this.httpClient});

  @override
  Future<UserLoginModel> login({String phoneNumber, String password, String device_token}) async {
    var params = <String, String>{
      "phone": phoneNumber,
      "password": password,
      "device_token": device_token,
    };

    var response = await httpPostRequest(
        httpClient: httpClient, url: LOGIN_API_ENDPOINT, params: params);
    if (response.statusCode == 200) {
      return UserLoginModel.fromJson(json.decode(response.body));
    }
    handleError(response);
  }

  @override
  Future<void> register({String phoneNumber, String password, String userName, String gender}) async {
    var params = <String, String>{
      "phone": phoneNumber,
      "name": userName,
      "password": password,
      "gender": gender,
    };

    var response = await httpPostRequest(
        httpClient: httpClient,
        url: USER_REGISTRATION_ENDPOINT,
        params: params);
    if (response.statusCode == 200) {
      return null;
    }
    handleError(response);
  }

  @override
  Future<String> sendOtp({@required String phoneNumber}) async {
    var _params = <String, String>{
      "phone": phoneNumber,
    };

    var response = await httpPostRequest(
        httpClient: httpClient, url: SEND_OTP_ENDPOINT, params: _params);
    if (response.statusCode == 200) {
      return json.decode(response.body)['message'];
    }
    handleError(response);
  }

  @override
  Future<void> weddingBooking({String name, String date, String time}) async {
    var params = <String, String>{
      "name": name,
      "date": date,
    };
  }

  @override
  Future<void> childRegistration({String name, String age}) async {
    var params = <String, String>{
      "name": name,
      "age": age,
    };
  }

  @override
  Future<void> weddingInvite(
      {String invitationType, String date, String time}) async {
    var params = <String, String>{
      "type": invitationType,
      "date": date,
      "time": time
    };
  }

  @override
  Future<void> verifyOtp({
    String phoneNumber,
    String otp,
  }) async {
    final _params = <String, String>{'phone': phoneNumber, 'otp': otp};

    final response = await httpPostRequest(
        httpClient: httpClient, url: VERIFY_OTP_ENDPOINT, params: _params);
    if (response.statusCode == 200) {
      return Right(null);
    } else {
      handleError(response);
    }
  }

  @override
  Future<void> notification({String service_id}) async {
    var params = <String, String>{"service_id": service_id};
  }

  @override
  Future<void> logout({String token}) async {
    final response = await httpPostRequest(
        httpClient: httpClient, url: LOGOUT_ENDPOINT, token: token);
    print('response ${response.statusCode}');
    if (response.statusCode == 200) {
      return Right(null);
    } else {
      handleError(response);
    }
  }

  @override
  Future<void> changePassword(
      {String token, String oldPassword, String newPassword}) async {
    var _params = <String, String>{
      "old_password": oldPassword,
      "new_password": newPassword,
    };
    var response = await httpPostRequest(
        httpClient: httpClient,
        url: CHANGE_PASSWORD_ENDPOINT,
        params: _params,
        token: token);
    if (response.statusCode == 200) {
      final _responseMap = json.decode(response.body);
      if (_responseMap['status'] == false) {
        throw ServerException()..message = _responseMap['message'];
      }
      return null;
    }
    handleError(response);
  }

  @override
  Future<void> uploadDocument(
      {String token,
      String frontImage,
      String backImage,
      String name,
      String dob,
      String expDate,
      String idNumber}) async {
    var _params = <String, String>{
      "id_front": frontImage,
      "id_back": backImage,
      "id_name": name,
      "id_dob": dob,
      "id_expiry_date": expDate,
      "id_number": idNumber,
    };
    var response = await httpPostRequest(
        httpClient: httpClient,
        url: UPLOAD_DOC_ENDPOINT,
        params: _params,
        token: token);
    if (response.statusCode == 200) {
      final _responseMap = json.decode(response.body);
      if (_responseMap['status'] == false) {
        throw ServerException()..message = _responseMap['message'];
      }
      return null;
    }
    handleError(response);
  }


}
