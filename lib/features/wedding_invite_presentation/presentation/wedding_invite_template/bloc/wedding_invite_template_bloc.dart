import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/wedding_invite_presentation/data/model/wedding_template_model.dart';
import 'package:mersal/features/wedding_invite_presentation/domain/usecase/wedding_invite_usecase.dart';
import 'package:meta/meta.dart';

part 'wedding_invite_template_event.dart';

part 'wedding_invite_template_state.dart';

class WeddingInviteTemplateBloc
    extends Bloc<WeddingInviteTemplateEvent, WeddingInviteTemplateState> {
  final WeddingTemplateUseCase weddingInviteTemplateUseCase;

  WeddingInviteTemplateBloc({@required this.weddingInviteTemplateUseCase})
      : super(WeddingInviteTemplateInitial());

  @override
  Stream<WeddingInviteTemplateState> mapEventToState(
    WeddingInviteTemplateEvent event,
  ) async* {
    if (event is GetTemplateEvent) {
      yield WeddingInviteTemplateInitialising();
      final result = await weddingInviteTemplateUseCase(NoParams());
      yield* result.fold(
        (l) async* {
          yield WeddingInviteTemplateError(l);
        },
        (data) async* {
          yield WeddingInviteTemplateLoaded(activity: data);
        },
      );
    }
  }
}
