part of 'change_password_bloc.dart';

abstract class ChangePasswordState extends Equatable {
  const ChangePasswordState();
}

class ChangePasswordInitial extends ChangePasswordState {
  @override
  List<Object> get props => [];
}

class ChangePasswordInitiated extends ChangePasswordState{
  @override
  List<Object> get props => [];
}
class ChangePasswordSuccess extends ChangePasswordState{
  @override
  List<Object> get props => [];
}

class ChangePasswordFailed extends ChangePasswordState{
  final Failure failure;

  ChangePasswordFailed({@required this.failure});
  @override
  List<Object> get props => [];
}