import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:mersal/core/localization/app_localization.dart';
import 'package:mersal/core/network/api_endpoints.dart';
import 'package:mersal/core/util/date_formatter.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/core/widgets/common/shared_preference.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/calendar_screen/calendar-data/model/PrayerDataList.dart';
import 'package:mersal/features/calendar_screen/calendar-data/model/PrayerDataModel.dart';
import 'package:mersal/features/event/presentation/ui/event_page.dart';
import 'package:mersal/features/occasion/presentation/ui/occasion_page.dart';
import 'package:mersal/features/prayer_timeline/ui/presentation/ui/prayer_timeline_page.dart';
import 'package:mersal/resources/colors.dart';
import 'package:mersal/resources/fonts.dart';

class CalendarEventsOptions extends StatefulWidget {
  final List<dynamic> selectedEvents;
  final DateTime date;

  CalendarEventsOptions(this.selectedEvents, this.date);

  @override
  _CalendarEventsOptionsState createState() => _CalendarEventsOptionsState();
}

class _CalendarEventsOptionsState extends State<CalendarEventsOptions>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  List<PrayerDataList> prayerDataList = [];
  PrayerDataModel prayerDataModel;
  int currentIndex = 0;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  BuildContext mContextLoader;
  FToast fToast;
  var prayerName;

  @override
  void initState() {
    _tabController = TabController(length: 3, vsync: this);
    super.initState();
   // prayerApiCall();
    fToast = FToast();
    fToast.init(context);
    _asyncMethod();
  }

  _asyncMethod() async {
    prayerName = await appPreferences.getStringPreference("Prayer");
    print("prayerNamePrint========" + prayerName.toString());
    setState(() {});
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(appColors.bgBlueColor),
        body: Container(
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/bg_image.jpg"),
              fit: BoxFit.fill,
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 20,
              ),
              Stack(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(bottom: 5, left: 15),
                    alignment: Alignment.bottomLeft,
                    child: GestureDetector(
                      child: Icon(
                        Icons.arrow_back_ios_rounded,
                        color: Colors.white,
                      ),
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 6),
                    alignment: Alignment.bottomCenter,
                    child: Text(
                      AppLocalizations.of(context).translate("Calendar"),
                      textAlign: TextAlign.end,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontFamily: fonts.segoeui,
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              // Row(
              //   children: [
              //     Container(
              //       margin: EdgeInsets.only(left: 20),
              //       child: SvgPicture.asset(
              //         "assets/images/notification_icon.svg",
              //         width: 60,
              //         height: 60,
              //       ),
              //     ),
              //     Expanded(
              //       child: Container(
              //         margin: EdgeInsets.only(left: 15, right: 20),
              //         child: Label(
              //           title: "How would you like Alarm to notify you?",
              //           color: Colors.white,
              //           fontSize: 15,
              //         ),
              //       ),
              //     ),
              //   ],
              // ),
              Center(
                child: Container(
                  margin: EdgeInsets.only(top: 25),
                  child: Label(
                    title: formatDDMMYYY(date: widget.date),
                    color: Colors.white,
                    fontWeight: FontWeight.w600,
                    fontSize: 20,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 20, right: 20, top: 20),
                height: 45,
                decoration: BoxDecoration(
                  color: Colors.grey[300],
                  borderRadius: BorderRadius.circular(
                    25.0,
                  ),
                ),
                child: TabBar(
                  controller: _tabController,
                  // give the indicator a decoration (color and border radius)
                  indicator: BoxDecoration(
                    borderRadius: BorderRadius.circular(
                      25.0,
                    ),
                    color: Color(appColors.buttonColor),
                  ),
                  labelColor: Colors.white,
                  unselectedLabelColor: Colors.black,
                  tabs: [
                    // first tab [you can add an icon using the icon property]
                    Tab(
                      text:AppLocalizations.of(context).translate('Prayer Time'),
                    ),

                    // second tab [you can add an icon using the icon property]
                    Tab(
                      text:AppLocalizations.of(context).translate('Occasions'),
                    ),
                    Tab(
                      text:AppLocalizations.of(context).translate('Events'),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(bottom: 20),
                  child: TabBarView(
                    controller: _tabController,
                    children: [
                      PrayerTimeLinePage(),
                      OccasionPage(date: widget.date),
                      EventPage(date: widget.date),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> addAlarmCall(int id) async {
    final _params = <String, int>{'prayer_id': id};

    final _token = await appPreferences.getStringPreference("AccessToken");

    if (context != null) {
      _showLoader(context);
    }
    var response = await http.post(Uri.parse(CREATE_ALARM_END_POINT),
        headers: {
          'Content-type': 'application/json',
          AUTHORIZATION: BEARER + " " + _token
        },
        body: json.encode(_params));

    if (response.statusCode == 200) {
      final Map<String, dynamic> _responseJson = json.decode(response.body);
      if (mContextLoader != null) Navigator.pop(mContextLoader);
      CommonMethods.showToast(fToast: fToast, message: _responseJson["message"], status: true);
    }
  }

  Future<Null> _showLoader(BuildContext context) async {
    return showDialog<Null>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context1) {
        mContextLoader = context1;
        return Scaffold(
          backgroundColor: Colors.transparent.withOpacity(0.3),
          key: _keyLoader,
          body: Container(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    child: CircularProgressIndicator(),
                    height: 40.0,
                    width: 40.0,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
