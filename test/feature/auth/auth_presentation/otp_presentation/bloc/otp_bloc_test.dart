import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/features/auth/auth_domain/usecases/send_otp_usecase.dart';
import 'package:mersal/features/auth/auth_domain/usecases/verify_otp_usecase.dart';
import 'package:mersal/features/auth/auth_presentation/otp_presentation/bloc/otp_bloc.dart';
import 'package:mockito/mockito.dart';

import '../../../../../mocked_classes.dart';

void main() {
  MockSendOTPUseCase mockSendOTPUseCase;
  MockVerifyOTPUseCase mockVerifyOTPUseCase;
  OtpBloc _bloc;

  setUp(() {
    mockSendOTPUseCase = MockSendOTPUseCase();
    mockVerifyOTPUseCase = MockVerifyOTPUseCase();
    _bloc = OtpBloc(
        verifyOtpUseCase: mockVerifyOTPUseCase, otpUseCase: mockSendOTPUseCase);
  });

  test('should be the initial state as OtpInitial', () async {
    expect(_bloc.state, equals(OtpInitial()));
  });

  final _phoneNumber = '1234567890';
  final _otp = '123456';
  group('otp-verification', () {
    test('should otp verification successful from VerifyOtpUseCase', () async {
      //arrange
      when(mockVerifyOTPUseCase(
              VerifyOtpParams(phoneNumber: _phoneNumber, otp: _otp)))
          .thenAnswer((_) async => Right(null));

      //act
      _bloc.add(
        VerifyOtpEvent(phoneNumber: _phoneNumber, otp: _otp),
      );
      await untilCalled(
        mockVerifyOTPUseCase(
          any,
        ),
      );

      //assert
      verify(mockVerifyOTPUseCase(
          VerifyOtpParams(phoneNumber: _phoneNumber, otp: _otp)));
    });

    test(
        'should emit [OtpVerifyingState, OtpVerificationSuccessState] when opt verification is successful',
        () {
      //arrange
      when(mockVerifyOTPUseCase(any))
          .thenAnswer((realInvocation) async => Right(null));

      //act
      _bloc.add(VerifyOtpEvent(phoneNumber: _phoneNumber, otp: _otp));
      //assert
      final _expectedStates = [
        OtpVerifyingState(),
        OtpVerificationSuccessState()
      ];
      expectLater(_bloc.stream, emitsInOrder(_expectedStates));
    });

    test(
      'should emit [OtpVerifyingState, OtpVerificationFailureState] when opt verification fails',
      () {
        //arrange
        when(mockVerifyOTPUseCase(any))
            .thenAnswer((realInvocation) async => Left(ServerFailure()));

        //act
        _bloc.add(VerifyOtpEvent(phoneNumber: _phoneNumber, otp: _otp));
        //assert
        final _expectedStates = [
          OtpVerifyingState(),
          OtpVerificationFailureState(failure: ServerFailure())
        ];
        expectLater(_bloc.stream, emitsInOrder(_expectedStates));
      },
    );
  });

  group('resend_otp', () {
    test('should otp resent from SendOtpUseCase', () async {
      //arrange
      when(mockSendOTPUseCase(
              Params(phoneNumber: _phoneNumber)))
          .thenAnswer((_) async => Right('Otp Sent successfully'));

      //act
      _bloc.add(
        OtpRequestEvent(phoneNumber: _phoneNumber),
      );
      await untilCalled(
        mockSendOTPUseCase(
          any,
        ),
      );

      //assert
      verify(mockSendOTPUseCase(
          Params(phoneNumber: _phoneNumber)));
    });

    final _message = 'OTP send successylly';
    test(
        'should emit [OtpRequesting, OtpRequestSuccessState] when opt verification is successful',
        () {
      //arrange
      when(mockSendOTPUseCase(any))
          .thenAnswer((_) async => Right(_message));

      //act
      _bloc.add(OtpRequestEvent(phoneNumber: _phoneNumber));
      //assert
      final _expectedStates = [
        OtpRequesting(),
        OtpRequestSuccessState(message: '_message')
      ];
      expectLater(_bloc.stream, emitsInOrder(_expectedStates));
    });

    test(
      'should emit [OtpRequesting, OtpError] when opt verification fails',
      () {
        //arrange
        when(mockSendOTPUseCase(any))
            .thenAnswer((realInvocation) async => Left(ServerFailure()));

        //act
        _bloc.add(OtpRequestEvent(phoneNumber: _phoneNumber));
        //assert
        final _expectedStates = [
          OtpRequesting(),
          OtpError(failure: ServerFailure())
        ];
        expectLater(_bloc.stream, emitsInOrder(_expectedStates));
      },
    );
  });
}
