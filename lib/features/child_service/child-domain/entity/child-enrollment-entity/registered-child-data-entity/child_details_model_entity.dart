class ChildDetailsModelEntity {
  bool status;
  List<ChildDetailsData> childDetailsList;

  ChildDetailsModelEntity({this.status, this.childDetailsList});

  ChildDetailsModelEntity copy({
    int status,
    List<ChildDetailsData> childDataList,
  }) {
    return ChildDetailsModelEntity(
        status: status ?? this.status,
        childDetailsList: childDataList ?? this.childDetailsList);
  }
}

class ChildDetailsData {
  int id;
  String name;
  ChildRelationData childRelationData;
  int dob;

  ChildDetailsData(
      {this.id, this.name, this.childRelationData, this.dob });

  ChildDetailsData copy(
      {int id,
      String name,
      ChildRelationData childRelationData,
      int answerId}) {
    return ChildDetailsData(
        id: id ?? this.id,
        name: name ?? this.name,
        childRelationData: childRelationData ?? this.childRelationData,
        dob: dob ?? this.dob);
  }
}

class ChildRelationData {
  int id;
  String relation_type;
  String created_at;
  String title;

  ChildRelationData({this.id, this.relation_type, this.created_at, this.title});

  ChildRelationData copy(
      {int id, String relation_type, String created_at, String title}) {
    return ChildRelationData(
        id: id ?? this.id,
        relation_type: relation_type ?? this.relation_type,
        created_at: created_at ?? this.created_at,
        title: title ?? this.title);
  }
}
