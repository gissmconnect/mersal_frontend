import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mersal/core/util/helper_functions.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/core/widgets/message.dart';
import 'package:mersal/features/occasion/presentation/bloc/occasion_bloc.dart';
import 'package:mersal/injection_container.dart';
import 'package:mersal/resources/colors.dart';

class OccasionPage extends StatelessWidget {
  final DateTime date;

  const OccasionPage({Key key, @required this.date}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<OccasionBloc>(
      create: (_) => serviceLocator<OccasionBloc>(),
      child: _OccasionPageBody(
        date: date,
      ),
    );
  }
}

class _OccasionPageBody extends StatefulWidget {
  final DateTime date;

  const _OccasionPageBody({Key key, @required this.date}) : super(key: key);

  @override
  __OccasionPageBodyState createState() => __OccasionPageBodyState();
}

class __OccasionPageBodyState extends State<_OccasionPageBody> {

  FToast fToast;


  @override
  void initState() {
    print('date ${widget.date}');
    BlocProvider.of<OccasionBloc>(context).add(GetOccasionEvent(date: widget.date));
    super.initState();

    fToast = FToast();
    fToast.init(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<OccasionBloc, OccasionState>(
      listener: (context, state) {
        if (state is OccasionFailureState) {
          CommonMethods.showToast(fToast:fToast,message: state.failure.message,status:false);
        }
      },
      listenWhen: (oldState, newState) {
        return oldState != newState;
      },
      child: BlocBuilder<OccasionBloc, OccasionState>(
        buildWhen: (oldState, newState) {
          return oldState != newState;
        },
        builder: (context, state) {
          if (state is OccasionLoadingState) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          if (state is OccasionLoadedState) {
            final occasion = state.occasion;
            return SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 20, right: 20),
                    padding: EdgeInsets.only(bottom: 20),
                    decoration: new BoxDecoration(
                        color: Color(appColors.blue).withOpacity(0.6),
                        borderRadius: BorderRadius.circular(25)),
                    child: Column(
                      children: [
                        Container(
                          height: 50,
                          margin: EdgeInsets.only(top: 20, left: 10, right: 10),
                          alignment: Alignment.center,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                              color: Color(appColors.blue),
                              borderRadius: BorderRadius.circular(25)),
                          child: Label(
                            title: occasion.occasionDataList[0].title,
                            color: Colors.white,
                            fontSize: 18,
                          ),
                        ),
                        Container(
                          height: 250,
                          margin: EdgeInsets.only(top: 20, left: 10, right: 10),
                          alignment: Alignment.center,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                              color: Color(appColors.blue),
                              border: Border.all(color: Colors.white),
                              borderRadius: BorderRadius.circular(10)),
                          child: Label(
                            title: occasion.occasionDataList[0].description,
                            color: Colors.white,
                            fontSize: 18,
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            launchURL(url: occasion.occasionDataList[0].link);
                          },
                          child: Container(
                            padding: EdgeInsets.only(bottom: 20,top: 30),
                            child: Text(
                          occasion.occasionDataList[0].link,
                              style: TextStyle(
                                fontSize: 18,
                                fontStyle: FontStyle.italic,
                                fontWeight: FontWeight.w700,
                                color: Color(appColors.buttonColor),
                                decoration:TextDecoration.underline,

                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            );
          }
          return Container();
        },
      ),
    );
  }
}
