import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:mersal/core/error/exceptions.dart';
import 'package:mersal/core/network/api_endpoints.dart';
import 'package:mersal/core/network/http_error_handler.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/calendar_screen/calendar-data/datasource/calendar_data_source.dart';
import 'package:mersal/features/calendar_screen/calendar-data/model/PrayerDataModel.dart';
import 'package:mersal/features/election_candidate/election_candidate_data/model/election_candidate_model.dart';
import 'package:meta/meta.dart';

class CalendarDataSourceImpl implements CalendarDataSource {
  final http.Client httpClient;

  CalendarDataSourceImpl({@required this.httpClient});

  @override
  Future<List<PrayerDataModel>> getPrayer({@required String token}) async {
    var response = await httpClient.get(
      Uri.parse(GET_PRAYER_TIMES_ENDPOINT),
      headers: {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        AUTHORIZATION: BEARER + " " + token
      },
    );
    if (response.statusCode == 200) {
      return ((json.decode(response.body)['data']) as List<dynamic>)
          .map((e) => PrayerDataModel.fromJson(e))
          .toList();
    }
    handleError(response);
  }

  @override
  Future<void> createAlarm(
      {String token, int prayerId}) async {
    final _params = <String, dynamic>{
      'prayer_id': prayerId
    };
    var response = await httpClient.post(Uri.parse(CREATE_ALARM_END_POINT),
        headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json',
          AUTHORIZATION: BEARER + " " + token
        },
        body: json.encode(_params));

    print('response ${response.statusCode} and ${response.body}');
    if (response.statusCode == 200) {
      final Map<String, dynamic> _responseJson = json.decode(response.body);
      if (_responseJson.containsKey('data')) {
        return ((json.decode(response.body)['data']) as List<dynamic>)
            .map((e) => ElectionCandidateModel.fromJson(e))
            .toList();
      } else {
        throw ServerException()..message = _responseJson['message'];
      }
    }
    handleError(response);
  }
}
