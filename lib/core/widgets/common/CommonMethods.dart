import 'package:awesome_page_transitions/awesome_page_transitions.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/resources/colors.dart';

class CommonMethods {
  static FToast fToast;
  bool changeBorderBool = false;

  static void moveCursorTolastPos(TextEditingController textField) {
    var cursorPos = new TextSelection.fromPosition(
        new TextPosition(offset: textField.text.length));
    textField.selection = cursorPos;
  }

  static bool validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

    RegExp regex = new RegExp(pattern.toString());
    if (!regex.hasMatch(value))
      return false;
    else
      return true;
  }

  static void inputFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  bool isConnect = false;

  static void showSnackBar(
      GlobalKey<ScaffoldState> scaffoldKey, String message, bool status) {
    scaffoldKey.currentState?.showSnackBar(new SnackBar(
        content: new Text(message.toUpperCase()),
        backgroundColor: status ? Colors.green : Colors.red,
        duration: Duration(milliseconds: 2000)));
  }

  static String dateFormatterYMDTime(String date) {
    DateFormat df = new DateFormat("dd-MM-yyyy 'at' hh:mm a");
    return df.format(DateFormat("yyyy-MM-dd HH:mm").parse(date));
  }

  static String dateFormatterDate(String date) {
    DateFormat df = new DateFormat("dd MMM");
    return df.format(DateFormat("yyyy-MM-dd HH:mm").parse(date));
  }

  static String dateFormatterTime(String date) {
    DateFormat df = new DateFormat("hh:mm a");
    return df.format(DateFormat("yyyy-MM-dd HH:mm").parse(date));
  }

  static String dateFormatterYMD(String date, {String inputFormat}) {
    DateFormat df = new DateFormat("dd-MM-yyyy");
    return df.format(DateFormat(inputFormat ?? "yyyy-MM-dd HH:mm").parse(date));
  }

  void animatedNavigation(
      {BuildContext context, Widget currentScreen, Widget landingScreen}) {
    Navigator.push(
      context,
      AwesomePageRoute(
        transitionDuration: Duration(milliseconds: 800),
        exitPage: currentScreen,
        enterPage: landingScreen,
        transition: StackTransition(),
      ),
    );
  }

  static Future<bool> checkInternetConnectivity() async {
    String connectionStatus;
    bool isConnected = false;
    final Connectivity _connectivity = Connectivity();

    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      connectionStatus = (await _connectivity.checkConnectivity()).toString();
      if (await _connectivity.checkConnectivity() ==
          ConnectivityResult.mobile) {
        print("===internetconnected==Mobile" + connectionStatus);
        isConnected = true;
        // I am connected to a mobile network.
      } else if (await _connectivity.checkConnectivity() ==
          ConnectivityResult.wifi) {
        isConnected = true;
        print("===internetconnected==wifi" + connectionStatus);
        // I am connected to a wifi network.
      } else if (await _connectivity.checkConnectivity() ==
          ConnectivityResult.none) {
        isConnected = false;
        print("===internetconnected==not" + connectionStatus);
      }
    } on PlatformException catch (e) {
      print("===internet==not connected" + e.toString());
      connectionStatus = 'Failed to get connectivity.';
    }
    return isConnected;
    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
  }

   static void showToast({FToast fToast,String message,bool status}) {
    Widget toast = Container(
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color:Colors.grey.shade200.withOpacity(0.7)
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          status? Icon(Icons.check,color: Colors.green):
          Icon(Icons.cancel_outlined,color: Colors.red,),
          SizedBox(
            width: 12.0,
          ),
          Label(
              title: message??"",
              color: Colors.black,
              fontSize: 14)
        ],
      ),
    );
    // Custom Toast Position
    fToast.showToast(
        child: toast,
        toastDuration: Duration(seconds: 6),
        positionedToastBuilder: (context, child) {
          return Positioned(
            child: child,
            top: 50.0,
            right: 16.0,
          );
        });
  }

}
