import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/features/auth/auth_domain/usecases/get_user_session.dart';
import 'package:mersal/features/banner/banner-domain/entity/banner_entity.dart';
import 'package:meta/meta.dart';

part 'home_event.dart';

part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {

  final GetUserSession getUserSessionUseCase;

  HomeBloc({@required this.getUserSessionUseCase}) : super(HomeLoading());

  @override
  Stream<HomeState> mapEventToState(HomeEvent event) {
    throw UnimplementedError();
  }
}
