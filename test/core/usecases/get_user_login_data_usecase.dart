import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mersal/core/usecases/get_user_login_data_usecase.dart';
import 'package:mersal/core/usecases/save_user_login_data_usecase.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mockito/mockito.dart';

import '../../feature/auth/auth_domain/usecases/get_user_session_test.dart';

void main() {
  MockAuthRepository _mockAuthRepository;
  GetUserLoginDataUseCase _useCase;

  setUp(() {
    _mockAuthRepository = MockAuthRepository();
    _useCase = GetUserLoginDataUseCase(authRepository: _mockAuthRepository);
  });

  final _tLoginParams = LoginParams(loginId: 'test', password: 'test');
  test('should return LoginParams', () async {
    //arrange
    when(_mockAuthRepository.getLoginData())
        .thenAnswer((realInvocation) async => Right(_tLoginParams));

    //act
    final _result = await _useCase.call(NoParams());

    //assert
    verify(_mockAuthRepository.getLoginData());
    expect(_result, equals(Right(_tLoginParams)));
  });
}
