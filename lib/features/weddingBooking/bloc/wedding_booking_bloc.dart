import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/features/auth/auth_domain/usecases/wedding_booking_usecase.dart';
import 'package:meta/meta.dart';

part 'wedding_booking_event.dart';

part 'wedding_booking_state.dart';

class WeddingBookingBloc extends Bloc<WeddingBookingEvent, WeddingBookingState> {
  final WeddingBookingUseCase weddingBookingUseCase;

  WeddingBookingBloc({@required this.weddingBookingUseCase})
      : super(WeddingBookingInitial());

  @override
  Stream<WeddingBookingState> mapEventToState(
    WeddingBookingEvent event,
  ) async* {
    if (event is WeddingEvent) {
      yield WeddingRegistering();

      final result = await weddingBookingUseCase(
          WeddingParams(name: event.name, date: event.date, time: event.time));

      yield* result.fold((l) async* {
        yield WeddingError(l);
      }, (r) async* {
        yield WeddingSuccess();
      });
    }
  }
}
