import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:matcher/matcher.dart';
import 'package:mersal/core/error/exceptions.dart';
import 'package:mersal/core/network/api_endpoints.dart';
import 'package:mersal/features/auth/auth_data/datasource/auth_data_source_impl.dart';
import 'package:mersal/features/auth/auth_data/model/user_data_model.dart';
import 'package:meta/meta.dart';
import 'package:mockito/mockito.dart';

import '../../../../fixtures/fixture_reader.dart';

class MockHttpClient extends Mock implements http.Client {}

void main() {
  AuthDataSourceImpl dataSource;
  MockHttpClient mockHttpClient;

  setUp(() {
    mockHttpClient = MockHttpClient();
    dataSource = AuthDataSourceImpl(httpClient: mockHttpClient);
  });

  void setUpMockHttpClientFailure404() {
    when(mockHttpClient.get(any, headers: anyNamed('headers')))
        .thenAnswer((_) async => http.Response('Something went wrong', 404));
  }

  void setUpMockHttpClientFailure422() {
    when(mockHttpClient.post(any,
            body: anyNamed('body'), headers: anyNamed('headers')))
        .thenAnswer((_) async => http.Response(
            '{"status": false,"message": "The password field is required."}',
            422));
  }

  void setUpMockHttpClientFailure500() {
    when(mockHttpClient.post(any,
            body: anyNamed('body'), headers: anyNamed('headers')))
        .thenAnswer((_) async => http.Response(
            '{"status": false,"message": "Something went wrong"}', 500));
  }

  void setUpMockHttpClientFailure401() {
    when(mockHttpClient.post(any,
            body: anyNamed('body'), headers: anyNamed('headers')))
        .thenAnswer((_) async =>
            http.Response('{"status": false,"error": "Unauthorized"}', 401));
  }

  void testApiFailures({@required Function() call}) {
    test('should throw  Unauthorized failure when the response code is 401',
        () async {
      //arrange
      setUpMockHttpClientFailure401();

      //act

      //assert
      expect(() => call(), throwsA(TypeMatcher<AuthException>()));
    });

    test('should throw ServerFailure when the response code is 422', () {
      //arrange
      setUpMockHttpClientFailure500();

      //act

      //assert
      expect(() => call(), throwsA(TypeMatcher<ServerException>()));
    });

    test('should throw ServerFailure when the response code is 500', () {
      //arrange
      setUpMockHttpClientFailure422();

      //act

      //assert
      expect(() => call(), throwsA(TypeMatcher<ServerException>()));
    });
  }

  final String _phoneNumber = '1234556';
  final String _password = 'abcd1234';
  final _otp = '123456';
  var _params = <String, String>{
    "phone": _phoneNumber,
    "password": _password,
  };

  group('login', () {
    UserLoginModel _tUserLoginModel;
    setUp(() {
      _tUserLoginModel = UserLoginModel.fromJson(
        json.decode(
          fixture('login_response.json'),
        ),
      );
    });

    void setUpLoginMockHttpClientSuccess200() {
      when(
        mockHttpClient.post(
          any,
          body: anyNamed('body'),
          headers: anyNamed('headers'),
        ),
      ).thenAnswer(
          (_) async => http.Response(fixture('login_response.json'), 200));
    }

    test(
      '''should perform a POST request on a URL with number
       being the endpoint and with application/json header''',
      () async {
        // arrange
        setUpLoginMockHttpClientSuccess200();
        // act
        await dataSource.login(phoneNumber: _phoneNumber, password: _password);
        // assert
        verify(
          mockHttpClient.post(
            Uri.parse(LOGIN_API_ENDPOINT),
            body: json.encode(_params),
            headers: {
              'Content-Type': 'application/json',
            },
          ),
        );
      },
    );

    testApiFailures(
        call: () async =>
            dataSource.login(phoneNumber: _phoneNumber, password: _password));
  });

  group('registration', () {
    void setUpRegistrationMockHttpClientSuccess200() {
      when(
        mockHttpClient.post(
          any,
          body: anyNamed('body'),
          headers: anyNamed('headers'),
        ),
      ).thenAnswer((_) async => http.Response(
          "{\"status\": \true\,\"message\": \"Registered  succesfully\"}",
          200));
    }

    test(
      '''should perform a POST request on a URL with phone number and password
       being the endpoint and with application/json header''',
      () async {
        // arrange
        setUpRegistrationMockHttpClientSuccess200();
        // act
        await dataSource.register(
            phoneNumber: _phoneNumber, password: _password);
        // assert
        verify(
          mockHttpClient.post(
            Uri.parse(USER_REGISTRATION_ENDPOINT),
            body: json.encode(_params),
            headers: {
              'Content-Type': 'application/json',
            },
          ),
        );
      },
    );

    testApiFailures(
        call: () async => dataSource.register(
            phoneNumber: _phoneNumber, password: _password));
  });

  group('otp', () {
    test('''should perform a POST request on a URL with phone number
       on the endpoint and with application/json header''', () async {
      //arrange

      when(
        mockHttpClient.post(
          any,
          body: anyNamed('body'),
          headers: anyNamed('headers'),
        ),
      ).thenAnswer((_) async => http.Response(
          '{"status": true,"message": "Otp has been sent succesfully"}', 200));
      //act
      await dataSource.sendOtp(phoneNumber: _phoneNumber);

      //assert
      verify(
        mockHttpClient.post(
          Uri.parse(SEND_OTP_ENDPOINT),
          body: json.encode({'phone': _phoneNumber}),
          headers: {
            'Content-type': 'application/json',
          },
        ),
      );
    });
    testApiFailures(
        call: () async => dataSource.sendOtp(phoneNumber: _phoneNumber));
  });

  group('otp_verification', () {
    test(
        '''should perform a POST request on a URL with phoneNumber and otp on the 
    endpoint and with the application/json header''', () async {
      //arrange
      when(
        mockHttpClient.post(
          any,
          body: anyNamed('body'),
          headers: anyNamed('headers'),
        ),
      ).thenAnswer(
        (_) async =>
            http.Response('{"status": true,"message": "Otp verified"}', 200),
      );
      //act
      await dataSource.verifyOtp(phoneNumber: _phoneNumber, otp: _otp);

      //assert
      verify(
        mockHttpClient.post(
          Uri.parse(VERIFY_OTP_ENDPOINT),
          body: json.encode(
            {'phone': _phoneNumber, 'otp': _otp},
          ),
          headers: {'Content-type': 'application/json'},
        ),
      );
    });

    testApiFailures(
        call: () async => dataSource.verifyOtp(phoneNumber: _phoneNumber,otp: _otp));
  });
}
