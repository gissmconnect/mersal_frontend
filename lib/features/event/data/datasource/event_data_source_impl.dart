import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:mersal/core/error/exceptions.dart';
import 'package:mersal/core/network/api_endpoints.dart';
import 'package:mersal/core/network/http_call.dart';
import 'package:mersal/core/network/http_error_handler.dart';
import 'package:mersal/features/event/data/datasource/event_data_source.dart';
import 'package:mersal/features/event/data/model/event_model.dart';
import 'package:meta/meta.dart';

class EventDataSourceImpl implements EventDataSource {
  final http.Client client;

  EventDataSourceImpl({@required this.client});

  @override
  Future<EventModel> getEvent({String token, String date}) async {
    final _response = await httpGetRequest(
        httpClient: client,
        url: EVENT_ENDPOINT + "?date=" + date,
        token: token);
    print('response ${_response.body}');
    if (_response.statusCode == 200) {
      final _serviceResponse = json.decode(_response.body);
      if (_serviceResponse['data'] != null && _serviceResponse['data'].length>0) {
        return EventModel.fromJson(_serviceResponse);
      } else {
        throw ServerException()
          ..message = _serviceResponse['message'] ?? 'No Event';
      }
    } else {
      handleError(_response);
    }
  }
}
