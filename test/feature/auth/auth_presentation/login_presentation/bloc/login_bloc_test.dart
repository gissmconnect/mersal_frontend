import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/save_user_login_data_usecase.dart';
import 'package:mersal/features/auth/auth_domain/entity/user_data.dart';
import 'package:mersal/features/auth/auth_domain/usecases/login_usecase.dart';
import 'package:mersal/features/auth/auth_presentation/login_presentation/bloc/login_bloc.dart';
import 'package:mockito/mockito.dart';
import '../../../../../mocked_classes.dart';

class MockLoginUseCase extends Mock implements LoginUseCase {}

void main() {
  LoginBloc _loginBloc;
  MockLoginUseCase mockLoginUseCase;
  MockSaveLoginDataUseCase _mockSaveLoginDataUseCase;
  setUp(() {
    mockLoginUseCase = MockLoginUseCase();
    _mockSaveLoginDataUseCase = MockSaveLoginDataUseCase();
    _loginBloc = LoginBloc(
        loginUseCase: mockLoginUseCase,
        saveUserLoginDataUseCase: _mockSaveLoginDataUseCase);
  });

  test('should initial state be Initial LoginInitial', () async {
    expect(_loginBloc.state, LoginInitial());
  });

  final _tUserData = UserData(id: 1, userId: 11, userName: 'test');
  final _phoneNumber = '234567890';
  final _password = 'abcd1234';

  group('LoginRequested', () {
    test('should login successful from the LoginUseCase', () async {
      //arrange
      when(
        mockLoginUseCase(
          Params(phoneNumber: _phoneNumber, password: _password),
        ),
      ).thenAnswer((realInvocation) async => Right(_tUserData));
      //act
      _loginBloc.add(
          LoginRequestEvent(phoneNumber: _phoneNumber, password: _password));
      await untilCalled(mockLoginUseCase(any));
      await untilCalled(_mockSaveLoginDataUseCase(any));

      //assert
      verify(_mockSaveLoginDataUseCase(
          LoginParams(loginId: _phoneNumber, password: _password)));
      verify(mockLoginUseCase(
          Params(phoneNumber: _phoneNumber, password: _password)));
    });

    test('should emit [LoginInitiated, LoginSuccess] when login is success',
        () async {
      //arrange
      when(mockLoginUseCase(any)).thenAnswer((_) async => Right(_tUserData));

      //asset later
      final _expectedStates = [LoginInitiated(), LoginSuccess()];

      //act
      _loginBloc.add(
          LoginRequestEvent(phoneNumber: _phoneNumber, password: _password));

      //assert
      await expectLater(_loginBloc.stream, emitsInOrder(_expectedStates));
    });

    final _failure = AuthFailure();
    test(
        'should emit [LoginInitiated, LoginError] when login credential is invalid',
        () {
      //arrange
      when(mockLoginUseCase(any)).thenAnswer((_) async => Left(_failure));
      //assert later
      final _expectedStates = [LoginInitiated(), LoginError(failure: _failure)];
      //act
      _loginBloc.add(
          LoginRequestEvent(phoneNumber: _phoneNumber, password: _password));

      //assert
      expectLater(_loginBloc.stream, emitsInOrder(_expectedStates));
    });
  });
}
