class NextPrayerModel {
  int id;
  String name;
  String prayer_time;

  NextPrayerModel(
      {this.prayer_time, this.id, this.name});

  factory NextPrayerModel.fromJson(Map<String, dynamic> json) {
    return NextPrayerModel(
        id: json['id'],
        name: json['name'],
        prayer_time: json['prayer_time']);
  }
}
