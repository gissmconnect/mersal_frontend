class WeddingTemplateImageData {
  int id;
  String image;
  String created_at;

  WeddingTemplateImageData({this.id,this.image,this.created_at});

  factory WeddingTemplateImageData.fromJson(Map<String, dynamic> json) {
    return WeddingTemplateImageData(
      image: json['image'],
      id: json['id'],
      created_at: json['created_at'],
    );
  }

}