import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:mersal/core/error/exceptions.dart';
import 'package:mersal/core/network/api_endpoints.dart';
import 'package:mersal/core/network/http_call.dart';
import 'package:mersal/core/network/http_error_handler.dart';
import 'package:mersal/features/donation/data/datasource/donation_datasource.dart';
import 'package:mersal/features/donation/data/model/donation_type_model.dart';

class DonationDataSourceImpl implements DonationDataSource {
  final http.Client client;

  DonationDataSourceImpl({@required this.client});

  @override
  Future<List<DonationTypeModel>> getDonationType({String token}) async {
    print('call ');
    final _response = await httpGetRequest(
        httpClient: client, url: GET_DONATION_TYPE_ENDPOINT, token: token);
    if (_response.statusCode == 200) {
      final Map<String, dynamic> _responseJson = json.decode(_response.body);
      print('response ${_response.body}');
      if (_responseJson.containsKey('data')) {
        var _list =  (json.decode(_response.body)['data']as List<dynamic>)
            .map((e) => DonationTypeModel.fromJson(e))
            .toList();
        print('list size ${_list.length}');
        return _list;
      } else {
        throw ServerException()..message = _responseJson['message'];
      }
    } else {
      handleError(_response);
    }
  }

  @override
  Future<dynamic> donation(
      {String token,int reasonId,
        String donationAmount,
        String donationDescription}) async {
    var params = <String, String>{
      "reason_id": reasonId.toString(),
      "description": donationDescription,
      "amount": donationAmount
    };

    final _response = await httpPostRequest(
        httpClient: client, url: DONATE_ENDPOINT, token: token,params: params);
    print('donate response $params \n${_response.body}');
    if (_response.statusCode == 200) {
      final Map<String, dynamic> _responseJson = json.decode(_response.body);
      if (_responseJson.containsKey('status') && _responseJson['status'].toString() == "true") {
        return _responseJson['data'];
      } else {
        throw ServerException()..message = _responseJson['message'];
      }
    } else {
      handleError(_response);
    }
  }
}
