import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:mersal/features/auth/auth_data/model/user_data_model.dart';
import 'package:mersal/features/auth/auth_domain/entity/user_data.dart';

import '../../../../fixtures/fixture_reader.dart';

void main() {
  final tUserDataModel = UserDataModel(
      id: 1,
      userName: 'test',
      userId: 1,
      name: 'test',
      email: 'test',
      phone: 'test',
      gender: 'test',
      authToken: 'test',
      bloodGroup: 'test',
      dob: 'test',
      address: 'test',
      image: 'test',
      isVender: false,
      document: Doc(
          id: 1,
          documentStatus: 1,
          idBack: 'test',
          idFront: 'test',
          idExpiryDate: 'test',
          idDob: 'test',
          idNumber: 'test',
          idName: 'test'),
      addServices: false);

  test('should be subclass of UserData entity', () async {
    expect(tUserDataModel, isA<UserData>());
  });

  group('fromJson', () {
    test('should return a valid model', () async {
      final Map<String, dynamic> jsonMap =
          jsonDecode(fixture('login_response.json'))['data'];
      final result = UserDataModel.fromJson(jsonMap);
      expect(result, isA<UserDataModel>());
    });
  });

  group('toJson', () {
    test('should return a JSON map containing the proper data', () async {
      final result = tUserDataModel.toJson();
      final expectedJsonMap = {
        'id': 1,
        'user_id': 1,
        'name': 'test',
        'user_name': 'test',
        'email': 'test',
        'phone': 'test',
        'gender': 'test',
        'auth_token': 'test',
        'blood_group': 'test',
        'dob': 'test',
        'address': 'test',
        'image': 'test',
        'is_vender': false,
        'document_status': 'test',
        'add_services': false,
        'document': {
          'id': 1,
          'id_name': 'test',
          'id_number': 'test',
          'id_dob': 'test',
          'id_expiry_date': 'test',
          'id_front': 'test',
          'id_back': 'test',
          'document_status': 1
        }
      };
      expect(result, expectedJsonMap);
    });
  });
}
