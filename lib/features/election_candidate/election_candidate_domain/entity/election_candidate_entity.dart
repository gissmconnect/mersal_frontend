class ElectionCandidateEntityResponse{
  int hasVoted;
  List<ElectionCandidateEntity> candidates;

  ElectionCandidateEntityResponse({this.hasVoted, this.candidates});
}

class ElectionCandidateEntity {
  int candidateId;
  int electionId;
  String candidateName;
  String candidateDescription;
  String candidateImage;
  bool isSelected;

  ElectionCandidateEntity(
      {this.candidateId,
      this.electionId,
      this.candidateName,
      this.candidateDescription,
      this.candidateImage,
      this.isSelected});
}
