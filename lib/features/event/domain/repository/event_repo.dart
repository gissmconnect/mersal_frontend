import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/features/event/domain/enity/event_entity.dart';
import 'package:meta/meta.dart';
abstract class EventRepo{
  Future<Either<Failure,EventEntity>> getEvent({@required String date});
}