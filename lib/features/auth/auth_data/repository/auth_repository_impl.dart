import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/exceptions.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/save_user_login_data_usecase.dart';
import 'package:mersal/core/widgets/common/shared_preference.dart';
import 'package:mersal/features/auth/auth_data/datasource/auth_data_source.dart';
import 'package:mersal/features/auth/auth_data/model/user_data_model.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../core/network/network_info.dart';
import '../../auth_domain/entity/user_data.dart';
import '../../auth_domain/repository/auth_repository.dart';
import '../datasource/user_data_local_data_source.dart';

class AuthRepositoryImpl implements AuthRepository {
  final NetworkInfo networkInfo;
  final UserDataLocalDataSource userDataLocalDataSource;
  final AuthDataSource authDataSource;

  AuthRepositoryImpl({
    @required this.networkInfo,
    @required this.userDataLocalDataSource,
    @required this.authDataSource,
  });

  @override
  Future<Either<Failure, UserData>> getUserSession() async {
    if (await networkInfo.isConnected) {
      try {
        final userData = await userDataLocalDataSource.getUserData();
        return Right(userData);
      } on UserSessionFailure {
        return Left(UserSessionFailure());
      }
    } else {
      return Left(NetworkFailure());
    }
  }

  @override
  Future<Either<Failure, UserData>> login(
      {String phoneNumber, String password, String device_token}) async {
    if (await networkInfo.isConnected) {
      try {
        final result = await authDataSource.login(
            phoneNumber: phoneNumber, password: password, device_token: device_token);
        await userDataLocalDataSource.saveUserData(userDataModel: result.data);
        appPreferences.saveStringPreference("AccessToken",result.data.authToken);
        appPreferences.saveStringPreference("UserPhone", result.data.phone);
        appPreferences.saveStringPreference("UserName", result.data.name);
        appPreferences.saveStringPreference("Gender", result.data.gender);
        if(result.data.document!=null){
          appPreferences.saveStringPreference("DocumentStatus",result.data.document.documentStatus.toString());
          appPreferences.saveStringPreference("UserDOB",result.data.document.idDob.toString());
        }

        await userDataLocalDataSource.saveAuthToken(
            authToken: result.data.authToken);
        return Right(result.data);
      } on AuthException catch (ex) {
        return Left(AuthFailure()..message = ex.message);
      } on ServerException catch (ex) {
        return Left(ServerFailure()..message = ex.message);
      }
    } else {
      return Left(NetworkFailure());
    }
  }

  @override
  Future<Either<Failure, void>> register(
      {String phoneNumber, String password, String userName, String gender}) async {
    if (await networkInfo.isConnected) {
      try {
        await authDataSource.register(
            phoneNumber: phoneNumber, password: password, userName: userName, gender: gender);
        return Right(null);
      } on ServerException catch (ex) {
        return Left(ServerFailure()..message = ex.message);
      }
    } else {
      return Left(NetworkFailure());
    }
  }

  @override
  Future<Either<Failure, String>> sendOtp({String phoneNumber}) async {
    if (await networkInfo.isConnected) {
      try {
        final _result = await authDataSource.sendOtp(phoneNumber: phoneNumber);
        return Right(_result);
      } on ServerException catch (ex) {
        return Left(ServerFailure()..message = ex.message);
      }
    } else {
      return Left(NetworkFailure());
    }
  }

  @override
  Future<Either<Failure, void>> weddingBooking(
      {String name, String date, String time}) async {
    if (await networkInfo.isConnected) {
      try {
        await authDataSource.weddingBooking(
          name: name,
          date: date,
          time: time,
        );
        return Right(null);
      } on AuthException {
        return Left(AuthFailure());
      } on ServerException catch (ex) {
        return Left(ServerFailure()..message = ex.message);
      }
    } else {
      return Left(NetworkFailure());
    }
  }

  @override
  Future<Either<Failure, void>> childRegistration(
      {String name, String age}) async {
    if (await networkInfo.isConnected) {
      try {
        await authDataSource.childRegistration(name: name, age: age);
        return Right(null);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(NetworkFailure());
    }
  }

  @override
  Future<Either<Failure, void>> weddingInvitation(
      {String invitationType, String date, String time}) async {
    if (await networkInfo.isConnected) {
      try {
        await authDataSource.weddingInvite(
            invitationType: invitationType, date: date, time: time);
        return Right(null);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(NetworkFailure());
    }
  }

  @override
  Future<Either<Failure, void>> verifyOtp({phoneNumber, String otp}) async {
    if (await networkInfo.isConnected) {
      try {
        await authDataSource.verifyOtp(phoneNumber: phoneNumber, otp: otp);
        return Right(null);
      } on ServerException catch (ex) {
        return Left(ServerFailure()..message = ex.message);
      }
    } else {
      return Left(NetworkFailure());
    }
  }

  @override
  Future<Either<Failure, String>> notification({String service_id}) async {
    if (await networkInfo.isConnected) {
      try {
        await authDataSource.notification(service_id: service_id);
        return Right(null);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(NetworkFailure());
    }
  }

  @override
  Future<void> saveUserLoginData({LoginParams loginParams}) async {
    await userDataLocalDataSource.saveLoginData(loginParams: loginParams);
  }

  @override
  Future<Either<Failure, LoginParams>> getLoginData() async {
    final _loginData = await userDataLocalDataSource.getLoginData();
    try {
      return Right(_loginData);
    } on Exception {
      return Left(CacheFailure());
    }
  }

  @override
  Future<Either<Failure, void>> logout() async {
    final _token = await userDataLocalDataSource.getAuthToken();

    if (await networkInfo.isConnected) {
      try {
        await authDataSource.logout(token: _token);
        await userDataLocalDataSource.clear();
        return Right(null);
      } on ServerException catch (ex) {
        return Left(ServerFailure()..message = ex.message);
      }
    } else {
      return Left(NetworkFailure());
    }
  }

  @override
  Future<Either<Failure, int>> getDocumentStatus() async {
    try {
      final _documentStatus = await userDataLocalDataSource.getDocumentStatus();
      return Right(_documentStatus);
    } on CacheException {
      return Left(CacheFailure());
    }
  }

  @override
  Future<Either<Failure, void>> changePassword(
      {String oldPassword, String newPassword}) async {
    if (await networkInfo.isConnected) {
      try {
        final _token = await userDataLocalDataSource.getAuthToken();
        await authDataSource.changePassword(
            token: _token, oldPassword: oldPassword, newPassword: newPassword);
        userDataLocalDataSource.updatePassword(password: newPassword);
        return Right(null);
      } on AuthException {
        return Left(AuthFailure());
      } on ServerException catch (ex) {
        return Left(ServerFailure()..message = ex.message);
      }
    } else {
      return Left(NetworkFailure());
    }
  }

  @override
  Future<Either<Failure, void>> uploadDocuments(
      {String frontImage,
      String backImage,
      String name,
      String dob,
      String expDate,
      String idNumber}) async {
    if (await networkInfo.isConnected) {
      try {
        final _token = await userDataLocalDataSource.getAuthToken();
        await authDataSource.uploadDocument(
            token: _token,
            frontImage: frontImage,
            backImage: backImage,
            name: name,
            dob: dob,
            expDate: expDate,
            idNumber: idNumber);
        return Right(null);
      } on AuthException {
        return Left(AuthFailure());
      } on ServerException catch (ex) {
        return Left(ServerFailure()..message = ex.message);
      }
    } else {
      return Left(NetworkFailure());
    }
  }

  @override
  Future<void> saveUserDate({UserDataModel userDataModel})async {
    await userDataLocalDataSource.saveUserData(userDataModel: userDataModel);
  }
}
