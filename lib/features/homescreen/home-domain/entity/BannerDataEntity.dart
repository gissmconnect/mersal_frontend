class BannerDataList {
  String image;
  String link;

  BannerDataList({this.image,this.link});

  factory BannerDataList.fromJson(Map<String, dynamic> json) {
    return BannerDataList(
      image: json['image'],
      link: json['link'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['image'] = this.image;
    return data;
  }
}