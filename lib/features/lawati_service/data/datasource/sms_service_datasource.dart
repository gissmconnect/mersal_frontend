import 'package:mersal/features/lawati_service/data/model/sms_service_info_model.dart';
import 'package:mersal/features/lawati_service/data/model/sms_subscriptions_model.dart';
import 'package:mersal/features/lawati_service/domain/entity/sms_subscription_entity.dart';
import 'package:mersal/features/lawati_service/domain/usecase/subscribe_sms_subscription_usecase.dart';
import 'package:meta/meta.dart';

abstract class SMSServiceDataSource {
  Future<SMSServiceInfoModel> getServiceInfo({@required String token});

  Future<List<SMSSubscriptionModel>> getSubscriptions({@required String token});

  Future<SMSSubscriptionModel> getMySubscription({@required String token});

  Future<dynamic> subscribe(
      {@required String token, @required SubscriptionRequestParams params});


  Future<String> renewSubscription(
      {@required String token, @required int subscriptionId});
}
