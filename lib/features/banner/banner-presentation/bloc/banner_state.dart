part of 'banner_bloc.dart';

abstract class BannerState extends Equatable {}

class BannerInitial extends BannerState{
  @override
  List<Object> get props => [];
}

//Banner Loading
class BannerLoading extends BannerState {
  @override
  List<Object> get props => [];
}

class BannerLoaded extends BannerState {
  final List<BannerEntity> banners;

  BannerLoaded({@required this.banners});

  @override
  List<Object> get props => [banners];
}

class BannerFailed extends BannerState {
  final Failure failure;

  BannerFailed({this.failure});

  @override
  List<Object> get props => [failure];
}