import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/features/notification/notification_domain/entity/notification_entity.dart';
import 'package:meta/meta.dart';

abstract class NotificationRepo {
  Future<Either<Failure, List<NotificationEntity>>> getNotifications();

  Future<Either<Failure, String>> deleteNotification({@required int id});

  Future<Either<Failure, String>> archiveNotification({@required int id});
}
