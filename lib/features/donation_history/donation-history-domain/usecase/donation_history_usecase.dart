import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/donation_history/donation-history-domain/entity/donation_history_entity.dart';
import 'package:mersal/features/donation_history/donation-history-domain/repository/donation_history_repo.dart';
import 'package:meta/meta.dart';

class DonationHistoryUseCase extends UseCase<DonationHistoryEntity, NoParams> {
  final DonationHistoryRepo donationHistoryRepo;

  DonationHistoryUseCase({@required this.donationHistoryRepo});

  @override
  Future<Either<Failure, DonationHistoryEntity>> call(NoParams params) async {
    return await donationHistoryRepo.getDonationHistory();
  }
}
