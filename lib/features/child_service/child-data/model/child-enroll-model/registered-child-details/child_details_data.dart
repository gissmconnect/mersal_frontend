class ChildDetailsData {
  int id;
  String title;
  String created_at;

  ChildDetailsData({this.id, this.title, this.created_at});

  factory ChildDetailsData.fromJson(Map<String, dynamic> json) {
    return ChildDetailsData(
      id: json['id'],
      created_at: json['created_at'],
      title: json['title'],
    );
  }
}
