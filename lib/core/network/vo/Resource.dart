import 'package:equatable/equatable.dart';
import 'package:mersal/core/error/failures.dart';
import 'Status.dart';

class Resource<T> extends Equatable{
  STATUS status;
  T data;
  Failure failure;

  Resource({this.status, this.data, this.failure});

  static Resource success({data}) => Resource(status: STATUS.SUCCESS, data: data, failure: null);
  static Resource error({data,failure}) => Resource(status: STATUS.ERROR, data: data, failure: failure);
  static Resource loading({data}) => Resource(status: STATUS.LOADING, data: data, failure: null);


  @override
  String toString() {
    return 'Resource{status: $status, data: $data, message: $failure}';
  }

  Resource copyWith({STATUS status, T data, String messege}){
    return Resource(status: status??this.status,data: data??this.data,failure: messege??this.failure);
  }
  @override
  List<Object> get props => [status,data,failure];
}

class Stack<T>extends Equatable {
  List<T> _stack = [];

  void push(T item) => _stack.add(item);

  T pop() => _stack.removeLast();
  @override
  List<Object> get props => [_stack];
}
