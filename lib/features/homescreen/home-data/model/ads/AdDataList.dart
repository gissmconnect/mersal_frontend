class AdDataList {
  String title;

  AdDataList({this.title});

  factory AdDataList.fromJson(Map<String, dynamic> json) {
    return AdDataList(
      title: json['title'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    return data;
  }
}