part of 'change_password_bloc.dart';

class ChangePasswordEvent extends Equatable {
  final String oldPassword, newPassword;

  ChangePasswordEvent({@required this.oldPassword, @required this.newPassword});

  @override
  List<Object> get props => [];
}
