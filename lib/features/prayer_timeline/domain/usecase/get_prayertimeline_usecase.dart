import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/prayer_timeline/domain/entity/prayer_data_entity.dart';
import 'package:mersal/features/prayer_timeline/domain/repository/prayer_repository.dart';
import 'package:meta/meta.dart';

class GetPrayerTimeLineUseCase
    extends UseCase<List<PrayerDataEntity>, NoParams> {
  final PrayerRepository prayerRepository;

  GetPrayerTimeLineUseCase({@required this.prayerRepository});

  @override
  Future<Either<Failure, List<PrayerDataEntity>>> call(NoParams params) async {
    return await prayerRepository.getPrayerTimes();
  }
}
