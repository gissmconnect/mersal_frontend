import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/survey/domain/entity/survey_entity.dart';
import 'package:mersal/features/survey/domain/repository/survey_repository.dart';
import 'package:meta/meta.dart';

class GetSurveyUseCase extends UseCase<SurveyEntity, NoParams> {

  final SurveyRepository surveyRepository;

  GetSurveyUseCase({@required this.surveyRepository});

  @override
  Future<Either<Failure, SurveyEntity>> call(NoParams params) {
    return surveyRepository.getSurvey();
  }

}
