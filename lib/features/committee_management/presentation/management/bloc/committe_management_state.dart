part of 'committe_management_bloc.dart';

abstract class CommitteManagementState extends Equatable {
  @override
  List<Object> get props => [];
}

class CommitteManagementInitial extends CommitteManagementState {}

class CommitteeLoadingState extends CommitteManagementState {}

class CommitteeLoadedState extends CommitteManagementState {
  final List<CommitteeEntity> committees;

  CommitteeLoadedState({@required this.committees});
}

class CommitteeLoadingFailure extends CommitteManagementState {
  final Failure failure;

  CommitteeLoadingFailure({@required this.failure});
}
