import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/child_service/child-data/model/child-enroll-model/registered-child-activity/child_activity_model.dart';
import 'package:mersal/features/child_service/child-data/model/child-enroll-model/registered-child-details/child_details_model.dart';
import 'package:mersal/features/child_service/child-domain/usecase/child_activity_usecase.dart';
import 'package:mersal/features/child_service/child-domain/usecase/child_details_usecase.dart';
import 'package:meta/meta.dart';

part 'child_registration_event.dart';

part 'child_registration_state.dart';

class ChildRegistrationBloc
    extends Bloc<ChildRegistrationEvent, ChildRegistrationState> {
  final ChildActivityUseCase childActivityUseCase;
  final ChildDetailsUseCase childDetailsUseCase;

  ChildRegistrationBloc(
      {@required this.childActivityUseCase, @required this.childDetailsUseCase})
      : super(ChildEnrollmentInitial());

  @override
  Stream<ChildRegistrationState> mapEventToState(
      ChildRegistrationEvent event,
      ) async* {
    if (event is GetActivityEvent) {
      yield ChildRegistering();
      final result = await childActivityUseCase(NoParams());
      yield* result.fold(
            (l) async* {
          yield ChildEnrollmentError(l);
        },
            (data) async* {
          yield ChildActivityLoaded(activity: data);
        },
      );
    }
    if (event is GetChildDetailsEvent) {
      yield ChildDetailsFetch();
      final result = await childDetailsUseCase(NoParams());
      yield* result.fold(
            (l) async* {
          yield ChildEnrollmentError(l);
        },
            (data) async* {
          yield ChildDetailsLoaded(detailsDataModel: data);
        },
      );
    }
  }
}
