import 'package:equatable/equatable.dart';

import '../../auth_domain/entity/user_data.dart';

class UserLoginModel extends Equatable {
  String accessToken;
  UserDataModel data;

  UserLoginModel({this.accessToken, this.data});

  UserLoginModel.fromJson(Map<String, dynamic> json) {
    accessToken = json['access_token'];
    data =json['data'] != null ? new UserDataModel.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['access_token'] = this.accessToken;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }

  @override
  List<Object> get props => [accessToken, data];
}

class UserDataModel extends UserData {
  UserDataModel(
      {int id,
      int userId,
      String name,
      String userName,
      String email,
      String phone,
      String gender,
      String authToken,
      String bloodGroup,
      String dob,
      String address,
      String image,
      bool isVender,
      Doc document,
      bool addServices})
      : super(
            id: id,
            userId: userId,
            name: name,
            userName: userName,
            email: email,
            phone: phone,
            gender: gender,
            authToken: authToken,
            bloodGroup: bloodGroup,
            dob: dob,
            address: address,
            image: image,
            isVender: isVender,
            document: document,
            addServices: addServices);

  UserDataModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    name = json['name']??"";
    userName = json['user_name'];
    email = json['email']??"";
    phone = json['phone']??"";
    gender = json['gender']??"";
    authToken = json['auth_token']??"";
    bloodGroup = json['blood_group']??"";
    dob = json['dob']??"";
    address = json['address']??"";
    image = json['image']??"";
    isVender = json['is_vender']??"";
    document = json['document'] != null ? new Doc.fromJson(json['document']) : null;
    addServices = json['add_services'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['name'] = this.name;
    data['user_name'] = this.userName;
    data['email'] = this.email;
    data['phone'] = this.phone;
    data['gender'] = this.gender;
    data['auth_token'] = this.authToken;
    data['blood_group'] = this.bloodGroup;
    data['dob'] = this.dob;
    data['address'] = this.address;
    data['image'] = this.image;
    data['is_vender'] = this.isVender;
    if (this.document != null) {
      data['document'] = (this.document as Doc).toJson();
    }
    data['document_status'] = this.documentStatus;
    data['add_services'] = this.addServices;
    return data;
  }

  UserDataModel copy(
      {int id,
      int userId,
      String name,
      String userName,
      String email,
      String phone,
      String gender,
      String authToken,
      String bloodGroup,
      String dob,
      String address,
      String image,
      bool isVender,
      Document document,
      String documentStatus,
      bool addServices}) {
    return UserDataModel(
        id: id ?? this.id,
        userId: userId ?? this.userId,
        name: name ?? this.name,
        email: email ?? this.email,
        phone: phone ?? this.phone,
        gender: gender ?? this.gender,
        authToken: authToken ?? this.authToken,
        bloodGroup: bloodGroup ?? this.bloodGroup,
        dob: dob ?? this.dob,
        address: address ?? this.address,
        image: image ?? this.image);
  }
}

class Doc extends Document {
  Doc({
    int id,
    String idName,
    String idNumber,
    String idDob,
    String idExpiryDate,
    String idFront,
    String idBack,
    int documentStatus,
  }) : super(
            id: id,
            idName: idName,
            idNumber: idNumber,
            idDob: idDob,
            idExpiryDate: idExpiryDate,
            idFront: idFront,
            idBack: idBack,
            documentStatus: documentStatus);

  Doc.fromJson(Map<String, dynamic> json) {
    if(json!=null){
    id = json['id'];
    idName = json['id_name'];
    idNumber = json['id_number'];
    idDob = json['id_dob'];
    idExpiryDate = json['id_expiry_date'];
    idFront = json['id_front'];
    idBack = json['id_back'];
    documentStatus = json['document_status'];
    }

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['id_name'] = this.idName;
    data['id_number'] = this.idNumber;
    data['id_dob'] = this.idDob;
    data['id_expiry_date'] = this.idExpiryDate;
    data['id_front'] = this.idFront;
    data['id_back'] = this.idBack;
    data['document_status'] = this.documentStatus;
    return data;
  }
}
