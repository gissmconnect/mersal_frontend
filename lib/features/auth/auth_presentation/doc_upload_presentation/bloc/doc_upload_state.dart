part of 'doc_upload_bloc.dart';

abstract class DocUploadState extends Equatable {
  const DocUploadState();
}

class DocUploadInitial extends DocUploadState {
  @override
  List<Object> get props => [];
}

class SoftLoginInitiated extends DocUploadState {
  @override
  List<Object> get props => [];
}

class SoftLoginFailed extends DocUploadState {
  final Failure failure;

  SoftLoginFailed({@required this.failure});

  @override
  List<Object> get props => [failure];
}

class SoftLoginSuccess extends DocUploadState {
  @override
  List<Object> get props => [];
}

class UploadInitiatedState extends DocUploadState {
  @override
  List<Object> get props => [];
}

class UploadFailedState extends DocUploadState {
  final Failure failure;

  UploadFailedState({@required this.failure});

  @override
  List<Object> get props => [failure];
}

class UploadSuccessState extends DocUploadState {
  @override
  List<Object> get props => [];
}
