part of 'user_bloc.dart';

class UserState extends Equatable {
  final Resource logoutResource;
  final Resource documentVerificationResource;
  final Resource userDataLoadingResource;
  final Resource profileUpdatingResource;

  UserState(
      {this.logoutResource,
      this.documentVerificationResource,
      this.userDataLoadingResource,
      this.profileUpdatingResource});

  @override
  List<Object> get props => [
        logoutResource,
        documentVerificationResource,
        userDataLoadingResource,
        profileUpdatingResource
      ];

  UserState copy(
      {Resource logoutResource,
      Resource documentVerificationResource,
      Resource userDataResource,
      Resource profileResource}) {
    return UserState(
        logoutResource: logoutResource ?? this.logoutResource,
        documentVerificationResource:
            documentVerificationResource ?? this.documentVerificationResource,
        userDataLoadingResource:
            userDataResource ?? this.userDataLoadingResource,
        profileUpdatingResource:
            profileResource ?? this.profileUpdatingResource);
  }

  factory UserState.initial() {
    return UserState(userDataLoadingResource: Resource.loading());
  }
}

/*
class UserInitial extends UserState {
  @override
  List<Object> get props => [];
}

class LogoutState extends UserState {
  @override
  List<Object> get props => [];
}

class LogoutInitiated extends LogoutState {}

class LogoutSuccess extends LogoutState {}

class LogoutFailure extends LogoutState {
  final Failure failure;

  LogoutFailure({@required this.failure});
}

//UserDocumentVerification
class DocumentVerificationState extends UserState {
  @override
  List<Object> get props => [];
}

class DocumentVerificationInitiated extends DocumentVerificationState {}

class DocumentVerificationFailure extends DocumentVerificationState {
  final Failure failure;

  DocumentVerificationFailure({@required this.failure});
}

class DocumentVerificationSuccess extends DocumentVerificationState {
  final int documentStatus;

  DocumentVerificationSuccess({@required this.documentStatus});
}

//UserBloc
class UserDataLoading extends UserState {}

class UserDataLoaded extends UserState {
  final UserData userData;

  UserDataLoaded({@required this.userData});
}

class UserDataLoadFailed extends UserState {
  final Failure failure;

  UserDataLoadFailed({@required this.failure});
}

//UpdateProfile
class ProfileUpdating extends UserState {}

class ProfileUpdateFailed extends UserState {
  final Failure failure;

  ProfileUpdateFailed({@required this.failure});
}

class ProfileUpdated extends UserState {}
*/
