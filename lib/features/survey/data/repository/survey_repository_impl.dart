import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/exceptions.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/network/network_info.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/survey/data/datasource/survey_datasource.dart';
import 'package:mersal/features/survey/domain/entity/survey_entity.dart';
import 'package:mersal/features/survey/domain/repository/survey_repository.dart';
import 'package:meta/meta.dart';

class SurveyRepositoryImpl implements SurveyRepository {
  final SurveyDataSource surveyDataSource;
  final NetworkInfo networkInfo;
  final UserDataLocalDataSource userDataLocalDataSource;

  SurveyRepositoryImpl(
      {@required this.surveyDataSource,
      @required this.networkInfo,
      @required this.userDataLocalDataSource});

  @override
  Future<Either<Failure, SurveyEntity>> getSurvey() async {
    print('getSurvey called');
    if (await networkInfo.isConnected) {
      try {
        final _token = await userDataLocalDataSource.getAuthToken();
        final _surveys = await surveyDataSource.getSurvey(token: _token);
        return Right(_surveys);
      } on ServerException catch (ex) {
        return Left(ServerFailure()..message = ex.message);
      } on AuthException catch (ex) {
        return Left(AuthFailure()..message = ex.message);
      } on Exception {
        return Left(ServerFailure());
      }
    } else {
      return Left(NetworkFailure());
    }
  }

  @override
  Future<Either<Failure, void>> submitSurveyQuestion({int surveyId, int surveyQuestionId, int surveyOptionId}) async {
    print('submitSurveyQuestion called');
    if (await networkInfo.isConnected) {
      try {
        final _token = await userDataLocalDataSource.getAuthToken();
        final _surveyQuestionSubmit = await surveyDataSource.surveyQuestion(
            token: _token, surveyId: surveyId, surveyQuestionId: surveyQuestionId, surveyOptionId: surveyOptionId);
        return Right(_surveyQuestionSubmit);
      } on ServerException catch (ex) {
        return Left(ServerFailure()..message = ex.message);
      } on Exception {
        return Left(ServerFailure());
      }
    } else {
      return Left(NetworkFailure());
    }
  }
}
