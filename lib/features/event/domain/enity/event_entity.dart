class EventEntity {
  bool status;
  List<EventDataListEntity> eventDataList;

  EventEntity({this.status, this.eventDataList});

  EventEntity copy({
    int id,
    String title,
    String description,
    List<EventDataListEntity> eventDataList,
  }) {
    return EventEntity(
        status: status ?? this.status,
        eventDataList: eventDataList ?? this.eventDataList);
  }
}

class EventDataListEntity {
  int id;
  String description;
  String title;
  String link;
  String eventDate;
  String endDate;
  String startDate;
  String occasionDate;
  String created_at;

  EventDataListEntity({this.id, this.title, this.startDate, this.endDate, this.eventDate,  this.link, this.created_at,  this.description});

  EventDataListEntity copy(
      {int id,
        String title,
        String link,
        String eventDate,
        String startDate,
        String endDate,
        String created_at,
        int answerId}) {
    return EventDataListEntity(
        id: id ?? this.id,
        title: title ?? this.title,
        startDate: startDate ?? this.startDate,
        endDate: endDate ?? this.endDate,
        link: link ?? this.link,
        eventDate: eventDate ?? this.eventDate,
        created_at: created_at ?? this.created_at,
        description: description ?? this.description);
  }
}
