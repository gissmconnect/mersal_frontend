import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:mersal/core/localization/app_localization.dart';
import 'package:mersal/core/widgets/common/Button.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/InputField.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/core/widgets/common/shared_preference.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/info_presentation/ui/info_screen.dart';
import 'package:mersal/features/weddingBooking/ui/summary_screen.dart';
import 'package:mersal/features/wedding_invite_presentation/data/model/ServicePriceModel.dart';
import 'package:mersal/features/wedding_invite_presentation/presentation/bloc/wedding_invite_bloc.dart';
import 'package:mersal/features/wedding_invite_presentation/presentation/wedding_invite_list/ui/wedding_invite_list.dart';
import 'package:mersal/features/wedding_invite_presentation/presentation/wedding_invite_template/ui/wedding_invitation_template.dart';
import 'package:mersal/injection_container.dart';
import 'package:mersal/resources/colors.dart';

class WeddingInvite extends StatefulWidget {
  @override
  _WeddingInviteState createState() => _WeddingInviteState();
}

class _WeddingInviteState extends State<WeddingInvite> {

  ValueNotifier<DateTime> _dateTimeNotifier = ValueNotifier<DateTime>(DateTime.now());

  final timeController = TextEditingController();
  TimeOfDay selectedTime = TimeOfDay.now();

  final FocusNode timeFocus = new FocusNode();
  final _formKey = GlobalKey<FormState>();

  List<String> dummyOptions = ['Text', 'With Image', 'Upload own image'];
  List<String> weddingTypeOptions = ['Aqid', 'Zifaf'];
  ServicePriceModel servicePriceModel;
  ServicePriceData servicePriceData;

  bool inviteTypeBool = false,
      weddingTypeBool = false,
      dateBool = false,
      timeBool = false;

  final mobileController = TextEditingController();
  final brideNameController = TextEditingController();
  final groomNameController = TextEditingController();
  final FocusNode mobileFocus = new FocusNode();
  final FocusNode brideNameFocus = new FocusNode();
  final FocusNode groomNameFocus = new FocusNode();

  String weddingInviteDate = "Select Date", weddingTime = "Select Time";
  FToast fToast;
  String _invitationType = 'Select Invitation Type',
      _weddingType ='Select Wedding Type',
      weddingDate = "";

  @override
  void initState() {
    super.initState();
    fToast = FToast();
    fToast.init(context);
    apiServicePriceCall();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => serviceLocator<WeddingInviteBloc>(),
      child: Scaffold(
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/bg_image.jpg"),
                fit: BoxFit.fill,
              ),
            ),
            child: Column(
              children: [
                SizedBox(
                  height: 50,
                ),
                Stack(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(bottom: 5, left: 15),
                      alignment: Alignment.bottomLeft,
                      child: GestureDetector(
                        child: Icon(
                          Icons.arrow_back_ios_rounded,
                          color: Colors.white,
                        ),
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(bottom: 6),
                      alignment: Alignment.bottomCenter,
                      child: Text(
                        AppLocalizations.of(context)
                            .translate("Wedding Invitation"),
                        textAlign: TextAlign.end,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(right: 50),
                      alignment: Alignment.centerRight,
                      child: GestureDetector(
                        child: Icon(
                          Icons.view_list,
                          color: Colors.white,
                          size: 35,
                        ),
                        onTap: () {
                          CommonMethods().animatedNavigation(
                              context: context,
                              currentScreen: WeddingInvite(),
                              landingScreen: WeddingInvitationListScreen());
                        },
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(right: 15),
                      alignment: Alignment.centerRight,
                      child: GestureDetector(
                        child: Container(
                          margin: EdgeInsets.only(left: 20),
                          child: SvgPicture.asset(
                            "assets/images/info_icon.svg",
                            width: 30,
                            height: 30,
                          ),
                        ),
                        onTap: () {
                          CommonMethods().animatedNavigation(
                              context: context,
                              currentScreen: WeddingInvite(),
                              landingScreen: InfoScreen());
                        },
                      ),
                    ),
                  ],
                ),
                Expanded(
                  child: ListView(
                    shrinkWrap: true,
                    children: [
                      Form(
                        key: _formKey,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              alignment: Alignment.center,
                              padding: EdgeInsets.only(left: 20, right: 10),
                              margin:
                                  EdgeInsets.only(left: 30, right: 30, top: 65),
                              decoration: new BoxDecoration(
                                shape: BoxShape.rectangle,
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(25),
                              ),
                              child: DropdownButton<String>(
                                  underline: Container(),
                                  isExpanded: true,
                                  hint: Row(
                                    children: <Widget>[
                                      Label(
                                        title: _weddingType ?? "",
                                        color: Colors.black,
                                        fontSize: 16,
                                        textAlign: TextAlign.center,
                                      ),
                                      Icon(
                                        Icons.keyboard_arrow_down_sharp,
                                        color: Colors.white,
                                        size: 20,
                                      ),
                                    ],
                                  ),
                                  items: weddingTypeOptions.map((String val) {
                                    return new DropdownMenuItem<String>(
                                      value: val,
                                      child: new Text(
                                        val,
                                        style: TextStyle(
                                            color: Colors.black54,
                                            fontSize: 16),
                                      ),
                                    );
                                  }).toList(),
                                  onChanged: (newVal) {
                                    _weddingType = newVal;
                                    weddingTypeBool = true;
                                    setState(() {});
                                  }),
                            ),
                            Container(
                              alignment: Alignment.center,
                              padding: EdgeInsets.only(left: 20, right: 10),
                              margin: EdgeInsets.only(left: 30, right: 30, top: 20),
                              decoration: new BoxDecoration(
                                shape: BoxShape.rectangle,
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(25),
                              ),
                              child: DropdownButton<String>(
                                  underline: Container(),
                                  isExpanded: true,
                                  hint: Row(
                                    children: <Widget>[
                                      Label(
                                        title: _invitationType ,
                                        color: Colors.black,
                                        fontSize: 16,
                                        textAlign: TextAlign.center,
                                      ),
                                      Icon(
                                        Icons.keyboard_arrow_down_sharp,
                                        color: Colors.white,
                                        size: 20,
                                      ),
                                    ],
                                  ),
                                  items: dummyOptions.map((String val) {
                                    return new DropdownMenuItem<String>(
                                      value: val,
                                      child: new Text(
                                        val,
                                        style: TextStyle(
                                            color: Colors.black54,
                                            fontSize: 16),
                                      ),
                                    );
                                  }).toList(),
                                  onChanged: (newVal) {
                                    _invitationType = newVal;
                                    inviteTypeBool = true;
                                    setState(() {});
                                  }),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 30, top: 25, right: 30),
                              child: InputField(
                                controller: groomNameController,
                                focusNode: groomNameFocus,
                                onSubmitted: (term) {
                                  CommonMethods.inputFocusChange(
                                      context, groomNameFocus, brideNameFocus);
                                },
                                validator: (text) {
                                  return text.isEmpty
                                      ? AppLocalizations.of(context).translate(
                                          'Groom name cannot be empty')
                                      : null;
                                },
                                hint: AppLocalizations.of(context)
                                    .translate("Groom Name"),
                                inputType: TextInputType.name,
                                inputFormatter: FilteringTextInputFormatter.allow(RegExp("[a-zA-Z]")),
                              ),
                            ),
                            Container(
                              margin:
                                  EdgeInsets.only(left: 30, top: 25, right: 30),
                              child: InputField(
                                controller: brideNameController,
                                focusNode: brideNameFocus,
                                onSubmitted: (term) {
                                  CommonMethods.inputFocusChange(
                                      context, brideNameFocus, mobileFocus);
                                },
                                validator: (text) {
                                  return text.isEmpty
                                      ? AppLocalizations.of(context).translate(
                                          "Bride Name cannot be empty")
                                      : null;
                                },
                                hint: AppLocalizations.of(context)
                                    .translate("Bride's Name"),
                                inputType: TextInputType.name,
                                inputFormatter: FilteringTextInputFormatter.allow(RegExp("[a-zA-Z]")),
                              ),
                            ),
                            Container(
                              margin:
                                  EdgeInsets.only(left: 30, top: 25, right: 30),
                              child: InputField(
                                controller: mobileController,
                                focusNode: mobileFocus,
                                onSubmitted: (term) {
                                  FocusScope.of(context).unfocus();
                                },
                                validator: (text) {
                                  return text.isEmpty
                                      ? AppLocalizations.of(context).translate(
                                          'Bride contact cannot be empty')
                                      : text.length < 8
                                          ? AppLocalizations.of(context).translate(
                                              'Bride contact cannot be less than 8')
                                          : null;
                                },
                                maxLength: 15,
                                hint: AppLocalizations.of(context)
                                    .translate("Enter Bride's Contact Number"),
                                inputType: TextInputType.phone,
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                showDatePickerDialog(context).then((date) {
                                  if (date != null) {
                                    weddingInviteDate =
                                        DateFormat("dd/MM/yyyy").format(date);
                                    weddingDate =
                                        DateFormat("yyyy-MM-dd").format(date);
                                    dateBool = true;
                                    setState(() {});
                                  }
                                });
                              },
                              child: Container(
                                  height: 50,
                                  margin: EdgeInsets.only(
                                      left: 30, top: 20, right: 30),
                                  padding: EdgeInsets.only(left: 20, right: 10),
                                  width: MediaQuery.of(context).size.width,
                                  alignment: Alignment.centerLeft,
                                  decoration: new BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(25),
                                    border: Border.all(
                                        color: Colors.white, width: 1),
                                  ),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Label(
                                        title: weddingInviteDate,
                                        fontSize: 16,
                                        color: Colors.black,
                                      ),
                                      Icon(
                                        Icons.date_range_rounded,
                                        color: Colors.black,
                                      ),
                                    ],
                                  )),
                            ),
                            GestureDetector(
                              onTap: () {
                                _selectTime(context);
                              },
                              child: Container(
                                  height: 50,
                                  margin: EdgeInsets.only(
                                      left: 30, top: 20, right: 30),
                                  padding: EdgeInsets.only(left: 20, right: 10),
                                  width: MediaQuery.of(context).size.width,
                                  alignment: Alignment.centerLeft,
                                  decoration: new BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(25),
                                    border: Border.all(
                                        color: Colors.white, width: 1),
                                  ),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Label(
                                        title: weddingTime,
                                        fontSize: 16,
                                        color: Colors.black,
                                      ),
                                      Icon(
                                        Icons.alarm,
                                        color: Colors.black,
                                      ),
                                    ],
                                  )),
                            ),
                            servicePriceData!=null?Container(
                              margin: EdgeInsets.only(
                                  top: 20,
                                  bottom: 10, left: 20, right: 20),
                              child: Label(
                                title:
                                "OMR ${servicePriceData
                                    .price} for Wedding Invitation",
                                fontSize: 16,
                                color: Colors.white,
                                textAlign: TextAlign.center,
                              ),
                            ):Container(),
                            Container(
                              height: 60,
                              margin: EdgeInsets.only(
                                  left: 30, top: 50, right: 30, bottom: 30),
                              child: BlocListener<WeddingInviteBloc,
                                  WeddingInviteState>(
                                listener: (context, state) {
                                  if (state is WeddingInviteError) {
                                    CommonMethods.showToast(
                                        fToast: fToast,
                                        message: state.failure.message,
                                        status: false);
                                  } else if (state is WeddingInviteSuccess) {
                                    Navigator.of(context).push(
                                      MaterialPageRoute(
                                        builder: (context) =>
                                            WeddingSummaryScreen("", "", ""),
                                      ),
                                    );
                                  }
                                },
                                child: BlocBuilder<WeddingInviteBloc,
                                    WeddingInviteState>(
                                  builder: (_, state) {
                                    return Button(
                                      title: AppLocalizations.of(context)
                                          .translate("Next"),
                                      color: Color(appColors.buttonColor),
                                      borderColor: Colors.transparent,
                                      textColor: Colors.white,
                                      onTap: () {
                                        if (!weddingTypeBool) {
                                          CommonMethods.showToast(
                                              fToast: fToast,
                                              message: AppLocalizations.of(
                                                      context)
                                                  .translate(
                                                      "Please select wedding type"),
                                              status: false);
                                        } else if (!inviteTypeBool) {
                                          CommonMethods.showToast(
                                              fToast: fToast,
                                              message: AppLocalizations.of(
                                                      context)
                                                  .translate(
                                                      "Please select an invitation type"),
                                              status: false);
                                        } else if (_formKey.currentState
                                            .validate()) {
                                          if (!dateBool) {
                                            CommonMethods.showToast(
                                                fToast: fToast,
                                                message: AppLocalizations.of(
                                                        context)
                                                    .translate(
                                                        "Please select a date"),
                                                status: false);
                                          } else if (!timeBool) {
                                            CommonMethods.showToast(
                                                fToast: fToast,
                                                message: AppLocalizations.of(
                                                        context)
                                                    .translate(
                                                        "Please select a time"),
                                                status: false);
                                          } else
                                            Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                builder: (context) =>
                                                    WeddingInviteTemplate(
                                                        invitationType:
                                                            _invitationType,
                                                        weddingType:
                                                            _weddingType,
                                                        mobileNumber:
                                                            mobileController.text
                                                                .toString()
                                                                .trim(),
                                                        brideName:
                                                            brideNameController
                                                                .text
                                                                .toString()
                                                                .trim(),
                                                        groomName:
                                                            groomNameController
                                                                .text
                                                                .toString()
                                                                .trim(),
                                                        weddingInviteDate:
                                                            weddingDate,
                                                        weddingTime:
                                                            weddingTime),
                                              ),
                                            );
                                        }
                                      },
                                    );
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> apiServicePriceCall() async {
    final _token = await appPreferences.getStringPreference("AccessToken");
    var response = await http.get(Uri.parse("http://93.115.18.236/public/api/service/get?type=wedding-booking"), headers: {
      'Content-type': 'application/json',
      AUTHORIZATION: BEARER + " " + _token
    });
    if (response.statusCode == 200) {
      final Map<String, dynamic> _responseJson = json.decode(response.body);
      print('ServicePriceResponse ${response.statusCode} and ${_responseJson.toString()}');
      servicePriceModel = ServicePriceModel.fromJson(_responseJson);
      servicePriceData = servicePriceModel.data;
      setState(() {});
    }
  }

  Future<DateTime> showDatePickerDialog(_context) {
    Future<DateTime> selectedDate = showDatePicker(
            context: context,
            firstDate: DateTime.now(),
            initialDate: _dateTimeNotifier.value != null
                ? _dateTimeNotifier.value
                : DateTime.now(),
            lastDate: DateTime(2100))
        .then((DateTime dateTime) => _dateTimeNotifier.value = dateTime);
    return selectedDate;
  }

  Future<void> _selectTime(BuildContext context) async {
    final TimeOfDay picked_s = await showTimePicker(
        context: context,
        initialTime: selectedTime,
        builder: (BuildContext context, Widget child) {
          return MediaQuery(
            data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: false),
            child: child,
          );
        });

    setState(() {
      selectedTime = picked_s;
      if (picked_s.minute == 0) {
        weddingTime =
            picked_s.hour.toString() + ":" + picked_s.minute.toString() + "0";
      } else {
        weddingTime =
            picked_s.hour.toString() + ":" + picked_s.minute.toString();
      }
      timeBool = true;
    });
  }

}
