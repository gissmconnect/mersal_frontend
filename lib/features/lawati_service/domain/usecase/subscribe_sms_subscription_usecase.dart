import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/lawati_service/domain/repository/sms_service_repo.dart';
import 'package:meta/meta.dart';

class SubscribeSmsServiceUseCase
    extends UseCase<dynamic, SubscriptionRequestParams> {
  final SMSServiceRepo smsServiceRepo;

  SubscribeSmsServiceUseCase({@required this.smsServiceRepo});

  @override
  Future<Either<Failure, dynamic>> call(SubscriptionRequestParams params) async {
    return await smsServiceRepo.subscribe(params: params);
  }
}

class SubscriptionRequestParams {
  final int id;
  final String number;
  final String fName;
  final String mName;
  final String lName;
  final String notificationType;
  final String relationId;

  SubscriptionRequestParams(
      {@required this.id,
      @required this.number,
      @required this.fName,
      @required this.mName,
      @required this.lName,
      @required this.notificationType,
      @required this.relationId});

  Map<String, String> toJSON() {
    return {
      "service_id": id.toString(),
      "phone": number,
      "first_name": fName,
      "middle_name": mName,
      "last_name": lName,
      "notification_type": notificationType,
      "relation_id": relationId
    };
  }
}
