import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mersal/core/localization/app_localization.dart';
import 'package:mersal/core/network/vo/Status.dart';
import 'package:mersal/core/widgets/common/Button.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/core/widgets/widgets.dart';
import 'package:mersal/features/child_service/child-presentation/child-enrollment/ui/childEnrollSuccess.dart';
import 'package:mersal/features/donation/domain/entity/donation_type_entity.dart';
import 'package:mersal/features/donation/donation-presentation/bloc/donation_bloc.dart';
import 'package:mersal/features/payment/payment_webview.dart';
import 'package:mersal/injection_container.dart';
import 'package:mersal/resources/colors.dart';

class DonationSummary extends StatelessWidget {
  final String amount;
  final DonationTypeEntity selectedDonationnType;
  final Function onDonationCreated;

  DonationSummary(
      this.amount, this.selectedDonationnType, this.onDonationCreated);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<DonationBloc>(
        create: (_) => serviceLocator(),
        child: Scaffold(
          body: _DonationSummaryBody(
              amount, selectedDonationnType, onDonationCreated),
        ));
  }
}

class _DonationSummaryBody extends StatefulWidget {
  final String amount;
  final DonationTypeEntity selectedDonationnType;
  final Function onDonationCreated;

  _DonationSummaryBody(
      this.amount, this.selectedDonationnType, this.onDonationCreated);

  @override
  _DonationSummaryState createState() => _DonationSummaryState();
  
}

class _DonationSummaryState extends State<_DonationSummaryBody> {

  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  BuildContext mContextLoader;
  FToast fToast;

  @override
  void initState() {
    fToast = FToast();
    fToast.init(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<DonationBloc, DonationState>(
      listenWhen: (oldState, newState) {
        return oldState.donateResource == null || oldState.donateResource.status != newState.donateResource.status;
      },
      listener: (context, state) {
        if(state.donateResource != null && state.donateResource.status != STATUS.LOADING) {
          Navigator.of(context).pop(true);
          if (state.donateResource.status == STATUS.SUCCESS) {
            widget.onDonationCreated();
            // CommonMethods.showToast(
            //     fToast: fToast,
            //     message: state.donateResource.data,
            //     status: true);
            CommonMethods().animatedNavigation(
                context: context,
                currentScreen: DonationSummary("",null,null),
                landingScreen: PaymentWebView(type: "donation",id: state.donateResource.data['id'].toString(),));
          } else if (state.donateResource.status == STATUS.ERROR) {
            CommonMethods.showToast(
                fToast: fToast,
                message: state.donateResource.failure.message,
                status: false);
          }
        }
      },
      child: BlocBuilder<DonationBloc, DonationState>(
        builder: (context, state) => GestureDetector(
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Label(
                    title: AppLocalizations.of(context)
                        .translate("Donation Summary"),
                    textAlign: TextAlign.center,
                    fontWeight: FontWeight.w600,
                    fontSize: 20,
                    color: Color(appColors.buttonColor),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 40.0),
                    child: Column(
                      children: [
                        Container(
                          margin: EdgeInsets.only(left: 25, right: 25, top: 20),
                          child: Column(
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 25),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Label(
                                          title: AppLocalizations.of(context)
                                              .translate("Donation Amount:"),
                                          textAlign: TextAlign.center,
                                          fontSize: 15,
                                          color: Color(appColors.buttonColor),
                                        ),
                                        Expanded(
                                          child: Label(
                                            title: "OMR " + widget.amount,
                                            textAlign: TextAlign.center,
                                            fontSize: 15,
                                            color: Color(appColors.buttonColor),
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 15,
                                    ),
                                    Row(
                                      children: [
                                        Label(
                                          title: AppLocalizations.of(context)
                                              .translate("Donation Reason:"),
                                          textAlign: TextAlign.center,
                                          fontSize: 15,
                                          color: Color(appColors.buttonColor),
                                        ),
                                        Expanded(
                                          child: Label(
                                            title: widget
                                                .selectedDonationnType.reason,
                                            textAlign: TextAlign.center,
                                            fontSize: 15,
                                            color: Color(appColors.buttonColor),
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 15,
                                    ),
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Label(
                                          title: AppLocalizations.of(context)
                                              .translate("Donation Description:"),
                                          textAlign: TextAlign.center,
                                          fontSize: 15,
                                          color: Color(appColors.buttonColor),
                                        ),
                                        Expanded(
                                          child: Label(
                                            title: widget.selectedDonationnType
                                                .description,
                                            textAlign: TextAlign.center,
                                            fontSize: 15,
                                            color: Color(appColors.buttonColor),
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 15,
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                height: 50,
                                margin: EdgeInsets.only(
                                    top: 55, bottom: 40, left: 20, right: 20),
                                child: BlocBuilder<DonationBloc, DonationState>(
                                  buildWhen: (oldState, newState) {
                                    return oldState.donateResource == null ||
                                        oldState.donateResource.status !=
                                            newState.donateResource.status;
                                  },
                                  builder: (context, state) {
                                    if (state.donateResource != null && state.donateResource.status == STATUS.LOADING) {
                                      return circularProgressIndicator();
                                    }
                                    return Button(
                                      title: AppLocalizations.of(context).translate("Confirm"),
                                      textColor: Colors.white,
                                      borderColor: Colors.transparent,
                                      color: Color(appColors.buttonColor),
                                      onTap: () {
                                        BlocProvider.of<DonationBloc>(context)
                                            .add(
                                          DonateEvent(
                                              reasonId: widget.selectedDonationnType.id,
                                              donationAmount: widget.amount,
                                              donationDescription: widget.selectedDonationnType.description),
                                        );
                                      },
                                    );
                                  },
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
