part of 'sms_service_info_bloc.dart';

class SmsServiceInfoState extends Equatable {
  final Resource serviceInfoResource, subscribeService;

  SmsServiceInfoState({this.serviceInfoResource, this.subscribeService});

  @override
  List<Object> get props => [serviceInfoResource, subscribeService];

  factory SmsServiceInfoState.initial() {
    return SmsServiceInfoState(
      serviceInfoResource: Resource.loading(),
    );
  }

  SmsServiceInfoState copy(
      {Resource serviceInfoResource,
      Resource subscribeService}) {
    return SmsServiceInfoState(
        serviceInfoResource: serviceInfoResource ?? this.serviceInfoResource,
        subscribeService: subscribeService ?? this.subscribeService);
  }
}
