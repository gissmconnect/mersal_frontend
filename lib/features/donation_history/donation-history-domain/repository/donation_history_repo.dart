import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/features/donation_history/donation-history-domain/entity/donation_history_entity.dart';

abstract class DonationHistoryRepo {
  Future<Either<Failure, DonationHistoryEntity>> getDonationHistory();
}
