import 'package:mersal/features/child_service/child-domain/model/child-enrollment/registered-child-data/RegisteredChildData.dart';

class RegisteredChildModel {
  List<RegisteredChildData> childDataList;
  bool status;

  RegisteredChildModel({this.childDataList, this.status});

  factory RegisteredChildModel.fromJson(Map<String, dynamic> json) {
    return RegisteredChildModel(
      childDataList: json['data'] != null
          ? (json['data'] as List)
          .map((i) => RegisteredChildData.fromJson(i))
          .toList()
          : null,
      status: json['status'],
    );
  }
}
