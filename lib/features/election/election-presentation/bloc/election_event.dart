part of 'election_bloc.dart';

abstract class ElectionEvent extends Equatable {
  const ElectionEvent();
}

class GetElections extends ElectionEvent {
  @override
  List<Object> get props => [];
}