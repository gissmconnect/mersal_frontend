import 'package:mersal/features/homescreen/home-data/model/alarm/AlarmModel.dart';

class AlarmDataModel {
  AlarmModel alarmData;
  bool status;

  AlarmDataModel({this.alarmData,this.status});

  factory AlarmDataModel.fromJson(Map<String, dynamic> json) {
    return AlarmDataModel(
      alarmData: json['data'].length>0  ? AlarmModel.fromJson(json['data']) : null,
      status: json['status'],
    );
  }

}