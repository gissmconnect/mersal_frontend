import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:mersal/core/localization/app_localization.dart';
import 'package:mersal/core/network/api_endpoints.dart';
import 'package:mersal/core/network/vo/Status.dart';
import 'package:mersal/core/widgets/common/Button.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/InputField.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/core/widgets/common/shared_preference.dart';
import 'package:mersal/core/widgets/widgets.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/child_service/child-domain/model/child-registration/ChildDataList.dart';
import 'package:mersal/features/child_service/child-domain/model/child-registration/ChildRelationDataModel.dart';
import 'package:mersal/features/lawati_service/domain/entity/sms_serviceInfo_entity.dart';
import 'package:mersal/features/lawati_service/domain/usecase/subscribe_sms_subscription_usecase.dart';
import 'package:mersal/features/lawati_service/presentation/service_info/bloc/sms_service_info_bloc.dart';
import 'package:mersal/features/lawati_service/presentation/subscriptions/ui/lawati_service_subscription.dart';
import 'package:mersal/features/payment/payment_webview.dart';
import 'package:mersal/injection_container.dart';
import 'package:mersal/resources/colors.dart';

class OthersLawatiService extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<SmsServiceInfoBloc>(
        create: (_) => serviceLocator<SmsServiceInfoBloc>(),
        child: Scaffold(body: _OthersLawatiServiceBody()));
  }
}

class _OthersLawatiServiceBody extends StatefulWidget {
  @override
  _OthersLawatiServiceBodyState createState() =>
      _OthersLawatiServiceBodyState();
}

class _OthersLawatiServiceBodyState extends State<_OthersLawatiServiceBody> {
  final msisdnController = TextEditingController();
  final fNameController = TextEditingController();
  final sNameController = TextEditingController();
  final lNameController = TextEditingController();
  final FocusNode fNameFocus = new FocusNode();
  final FocusNode sNameFocus = new FocusNode();
  final FocusNode lNameFocus = new FocusNode();

  String _selectedLocation = 'Choose Relation';
  String _selectedRelationId = "";
  List<String> dummyOptions = ['Family', 'Friends', 'Others'];
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  BuildContext mContextLoader;
  final _formKey = GlobalKey<FormState>();
  bool relationBool = false,
      showAppNotificationBool = false,
      showSMSNotificationBool = false;
  List<ChildDataList> childDataList;
  ChildRelationDataModel childRelationDataModel;
  String _notificationType = "";
  FToast fToast;

  @override
  void initState() {
    super.initState();
    apiRelationCall();
    BlocProvider.of<SmsServiceInfoBloc>(context).add(GetSmsServiceInfoEvent());
    fToast = FToast();
    fToast.init(context);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bg_image.jpg"),
            fit: BoxFit.fill,
          ),
        ),
        child: Column(
          children: [
            SizedBox(
              height: 50,
            ),
            Stack(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(bottom: 5, left: 15),
                  alignment: Alignment.bottomLeft,
                  child: GestureDetector(
                    child: Icon(
                      Icons.arrow_back_ios_rounded,
                      color: Colors.white,
                    ),
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 6),
                  alignment: Alignment.bottomCenter,
                  child: Text(
                    AppLocalizations.of(context)
                        .translate("Lawati SMS Service"),
                    textAlign: TextAlign.end,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.w400),
                  ),
                ),
              ],
            ),
            Expanded(
              child: ListView(
                shrinkWrap: true,
                children: [
                  Form(
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        childDataList != null && childDataList.length > 0
                            ? Container(
                                alignment: Alignment.center,
                                height: 50,
                                padding: EdgeInsets.only(left: 20, right: 20),
                                margin: EdgeInsets.only(left: 30, right: 30),
                                decoration: new BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(25),
                                  border:
                                      Border.all(color: Colors.white, width: 1),
                                ),
                                child: DropdownButton<ChildDataList>(
                                    isExpanded: true,
                                    hint: Row(
                                      children: <Widget>[
                                        Label(
                                          title: _selectedLocation ?? "",
                                          color: Colors.black,
                                          fontSize: 16,
                                          textAlign: TextAlign.center,
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: 5),
                                          child: Icon(
                                            Icons.keyboard_arrow_down_sharp,
                                            color: Colors.white,
                                            size: 20,
                                          ),
                                        ),
                                      ],
                                    ),
                                    underline: Container(),
                                    items:
                                        childDataList.map((ChildDataList val) {
                                      return new DropdownMenuItem<
                                          ChildDataList>(
                                        value: val,
                                        child: Label(
                                          title: val.title,
                                          fontSize: 18,
                                          color: Colors.black,
                                        ),
                                      );
                                    }).toList(),
                                    onChanged: (newVal) {
                                      _selectedLocation = newVal.title;
                                      _selectedRelationId =
                                          newVal.id.toString();
                                      relationBool = true;
                                      setState(() {});
                                    }),
                              )
                            : Container(),
                        Container(
                          margin: EdgeInsets.only(left: 30, top: 25, right: 30),
                          child: InputField(
                            controller: msisdnController,
                            onSubmitted: (term) {},
                            hint: "MSISDN",
                            maxLength: 20,
                            enabled: true,
                            validator: (text) {
                              return text.isEmpty
                                  ? AppLocalizations.of(context)
                                      .translate('MSISDN cannot be null')
                                  : null;
                            },
                            inputType: TextInputType.phone,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 30, top: 25, right: 30),
                          child: InputField(
                            controller: fNameController,
                            focusNode: fNameFocus,
                            onSubmitted: (term) {
                              CommonMethods.inputFocusChange(
                                  context, fNameFocus, sNameFocus);
                            },
                            validator: (text) {
                              return text.isEmpty
                                  ? AppLocalizations.of(context)
                                      .translate('First name cannot be null')
                                  : null;
                            },
                            hint: AppLocalizations.of(context)
                                .translate("Enter First Name"),
                            maxLength: 20,
                            inputType: TextInputType.name,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 30, top: 25, right: 30),
                          child: InputField(
                            controller: sNameController,
                            focusNode: sNameFocus,
                            onSubmitted: (term) {
                              CommonMethods.inputFocusChange(
                                  context, sNameFocus, lNameFocus);
                            },
                            validator: (text) {
                              return text.isEmpty
                                  ? AppLocalizations.of(context)
                                      .translate('Second Name cannot be null')
                                  : null;
                            },
                            hint: AppLocalizations.of(context)
                                .translate("Enter Second Name"),
                            maxLength: 20,
                            inputType: TextInputType.name,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 30, top: 25, right: 30),
                          child: InputField(
                            controller: lNameController,
                            focusNode: lNameFocus,
                            onSubmitted: (term) {
                              FocusScope.of(context).unfocus();
                            },
                            validator: (text) {
                              return text.isEmpty
                                  ? AppLocalizations.of(context)
                                      .translate('Last Name cannot be null')
                                  : null;
                            },
                            hint: AppLocalizations.of(context)
                                .translate("Enter Last Name"),
                            maxLength: 20,
                            inputType: TextInputType.name,
                          ),
                        ),
                        BlocBuilder<SmsServiceInfoBloc, SmsServiceInfoState>(
                          builder: (context, state) {
                            if (state.serviceInfoResource.status ==
                                STATUS.SUCCESS) {
                              final SMSServiceInfoEntity _service =
                                  state.serviceInfoResource.data;
                              return Column(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(
                                        top: 30,
                                        bottom: 10,
                                        left: 20,
                                        right: 20),
                                    child: Label(
                                      title: _service.title,
                                      fontSize: 16,
                                      color: Colors.white,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                        bottom: 10, left: 20, right: 20),
                                    child: Label(
                                      title:
                                          "OMR ${_service.price} for ${_service.duration} ${_service.duration > 1 ? "Years" : "Year"}",
                                      fontSize: 16,
                                      color: Colors.white,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ],
                              );
                            } else if (state.serviceInfoResource.status ==
                                STATUS.LOADING) {
                              return circularProgressIndicator();
                            }
                            return Container();
                          },
                        ),
                        Container(
                          margin: EdgeInsets.only(
                              top: 10, bottom: 30, left: 20, right: 20),
                          child: Label(
                            title: AppLocalizations.of(context).translate(
                                "Select Your Preferred Notification"),
                            fontSize: 18,
                            fontWeight: FontWeight.w600,
                            textAlign: TextAlign.center,
                            color: Colors.white,
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            showAppNotificationBool = !showAppNotificationBool;
                            _notificationType = "0";
                            setState(() {});
                          },
                          child: Center(
                            child: Container(
                              margin: EdgeInsets.only(left: 30, right: 30),
                              padding: EdgeInsets.only(
                                  top: 10, bottom: 10, left: 20),
                              decoration: BoxDecoration(
                                  color: showAppNotificationBool
                                      ? Colors.white
                                      : Colors.transparent,
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(color: Colors.white)),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(right: 20),
                                    alignment: Alignment.centerRight,
                                    child: Icon(
                                      showAppNotificationBool
                                          ? Icons.check_box
                                          : Icons.check_box_outline_blank,
                                      size: 30,
                                      color: Color(appColors.buttonColor),
                                    ),
                                  ),
                                  Label(
                                    title: AppLocalizations.of(context)
                                        .translate("App Notification"),
                                    fontSize: 16,
                                    fontWeight: FontWeight.w600,
                                    textAlign: TextAlign.center,
                                    color: showAppNotificationBool
                                        ? Colors.black
                                        : Colors.white,
                                  ),
                                  Container(
                                      margin: EdgeInsets.only(right: 20),
                                      child: SvgPicture.asset(
                                          "assets/images/notification_icon.svg",
                                          width: 25,
                                          height: 25,
                                          color: showAppNotificationBool
                                              ? Colors.black
                                              : Colors.white)),
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        GestureDetector(
                          onTap: () {
                            showSMSNotificationBool = !showSMSNotificationBool;
                            _notificationType = "1";
                            setState(() {});
                          },
                          child: Center(
                            child: Container(
                              margin: EdgeInsets.only(left: 30, right: 30),
                              padding: EdgeInsets.only(
                                  top: 10, bottom: 10, left: 20),
                              decoration: BoxDecoration(
                                  color: showSMSNotificationBool
                                      ? Colors.white
                                      : Colors.transparent,
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(color: Colors.white)),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(right: 20),
                                    alignment: Alignment.centerRight,
                                    child: Icon(
                                      showSMSNotificationBool
                                          ? Icons.check_box
                                          : Icons.check_box_outline_blank,
                                      size: 30,
                                      color: Color(appColors.buttonColor),
                                    ),
                                  ),
                                  Label(
                                    title: AppLocalizations.of(context)
                                        .translate("SMS"),
                                    fontSize: 16,
                                    fontWeight: FontWeight.w600,
                                    textAlign: TextAlign.center,
                                    color: showSMSNotificationBool
                                        ? Colors.black
                                        : Colors.white,
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(right: 20),
                                    child: Icon(Icons.email,
                                        size: 30,
                                        color: showSMSNotificationBool
                                            ? Colors.black
                                            : Colors.white),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        BlocBuilder<SmsServiceInfoBloc, SmsServiceInfoState>(
                          builder: (context, state) {
                            if (state.serviceInfoResource.status ==
                                STATUS.LOADING) {
                              return Container(
                                width: 200,
                                height: 60,
                                margin: EdgeInsets.only(
                                  top: 35,
                                  bottom: 40,
                                ),
                              );
                            } else if (state.serviceInfoResource.status ==
                                STATUS.SUCCESS) {
                              return BlocListener<SmsServiceInfoBloc,
                                  SmsServiceInfoState>(
                                listener: (context, listnerState) {
                                  if (listnerState.subscribeService != null) {
                                    if (listnerState.subscribeService.status ==
                                        STATUS.ERROR) {
                                      CommonMethods.showToast(
                                          fToast: fToast,
                                          message: listnerState
                                              .subscribeService.failure.message,
                                          status: false);
                                    }
                                    if (listnerState.subscribeService.status ==
                                        STATUS.SUCCESS) {
                                      // CommonMethods.showToast(
                                      //     fToast: fToast,
                                      //     message: listnerState
                                      //         .subscribeService.data,
                                      //     status: true);
                                      Timer(Duration(seconds: 2), () {
                                        CommonMethods().animatedNavigation(
                                            context: context,
                                            currentScreen:
                                                OthersLawatiService(),
                                            landingScreen: PaymentWebView(
                                              type: "lawati-sms",
                                              id: listnerState.subscribeService.data['id'].toString(),
                                              activity: "",
                                            ));
                                        // Navigator.of(context).pop(true);
                                      });
                                    }
                                  }
                                },
                                child: Container(
                                  height: 60,
                                  margin: EdgeInsets.only(
                                      top: 35, left: 30, right: 30),
                                  child: BlocBuilder<SmsServiceInfoBloc,
                                      SmsServiceInfoState>(
                                    builder: (context, subscribingState) {
                                      if (subscribingState.subscribeService !=
                                              null &&
                                          subscribingState
                                                  .subscribeService.status ==
                                              STATUS.LOADING) {
                                        return circularProgressIndicator();
                                      } else {
                                        return Button(
                                          title: AppLocalizations.of(context)
                                              .translate("Subscribe Now"),
                                          color: Colors.transparent,
                                          borderColor:
                                              Color(appColors.buttonColor),
                                          textColor:
                                              Color(appColors.buttonColor),
                                          onTap: () {
                                            if (!relationBool) {
                                              CommonMethods.showToast(
                                                  fToast: fToast,
                                                  message: AppLocalizations.of(
                                                          context)
                                                      .translate(
                                                          "Please select relation"),
                                                  status: false);
                                            } else if (_formKey.currentState
                                                .validate()) {
                                              final _firstName = fNameController
                                                  .text
                                                  .toString()
                                                  .trim();
                                              final _middleName =
                                                  sNameController.text
                                                      .toString()
                                                      .trim();
                                              final _lastName = lNameController
                                                  .text
                                                  .toString()
                                                  .trim();
                                              final _number = msisdnController
                                                  .text
                                                  .toString()
                                                  .trim();
                                              final _params = SubscriptionRequestParams(
                                                  id: (state.serviceInfoResource
                                                              .data
                                                          as SMSServiceInfoEntity)
                                                      .id,
                                                  number: _number,
                                                  fName: _firstName,
                                                  mName: _middleName,
                                                  lName: _lastName,
                                                  notificationType:
                                                      _notificationType,
                                                  relationId:
                                                      _selectedRelationId);
                                              BlocProvider.of<
                                                          SmsServiceInfoBloc>(
                                                      context)
                                                  .add(SubscribeEvent(
                                                      params: _params));
                                            }
                                          },
                                        );
                                      }
                                    },
                                  ),
                                ),
                              );
                            } else
                              return Container();
                          },
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Container(
                          height: 60,
                          margin:
                              EdgeInsets.only(left: 30, bottom: 35, right: 30),
                          child: Button(
                            title: AppLocalizations.of(context)
                                .translate("Check Subscription"),
                            color: Color(appColors.buttonColor),
                            borderColor: Colors.transparent,
                            textColor: Colors.white,
                            onTap: () {
                              CommonMethods().animatedNavigation(
                                  context: context,
                                  currentScreen: OthersLawatiService(),
                                  landingScreen: LawatiServiceSubscription());
                            },
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<Widget> summaryBottomSheet() {
    return showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30.0), topRight: Radius.circular(30.0)),
      ),
      builder: (BuildContext context) {
        return Container(
            height: MediaQuery.of(context).size.height / 2,
            color: Colors.white,
            margin: EdgeInsets.only(top: 40),
            // child: PaymentOptions("donation"));
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 40),
                      child: Label(
                        title: AppLocalizations.of(context)
                            .translate("Lawati Service Summary"),
                        textAlign: TextAlign.center,
                        fontWeight: FontWeight.w700,
                        fontSize: 20,
                        color: Colors.black,
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 40.0),
                      child: Column(
                        children: [
                          Container(
                            margin:
                                EdgeInsets.only(left: 25, right: 25, top: 20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                SizedBox(height: 40),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    Expanded(
                                      child: Container(
                                        margin: EdgeInsets.only(left: 15),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Label(
                                              title:
                                                  AppLocalizations.of(context)
                                                      .translate("First Name:"),
                                              textAlign: TextAlign.center,
                                              fontSize: 18,
                                              color: Colors.black,
                                            ),
                                            SizedBox(height: 10),
                                            Label(
                                              title: AppLocalizations.of(
                                                      context)
                                                  .translate("Second Name:"),
                                              textAlign: TextAlign.center,
                                              fontSize: 18,
                                              color: Colors.black,
                                            ),
                                            SizedBox(height: 10),
                                            Label(
                                              title:
                                                  AppLocalizations.of(context)
                                                      .translate("Last Name:"),
                                              textAlign: TextAlign.center,
                                              fontSize: 18,
                                              color: Colors.black,
                                            ),
                                            SizedBox(height: 10),
                                            Label(
                                              title: AppLocalizations.of(
                                                      context)
                                                  .translate("Phone Number:"),
                                              textAlign: TextAlign.center,
                                              fontSize: 18,
                                              color: Colors.black,
                                            ),
                                            SizedBox(height: 10),
                                            Label(
                                              title:
                                                  AppLocalizations.of(context)
                                                      .translate("Relation:"),
                                              textAlign: TextAlign.center,
                                              fontSize: 18,
                                              color: Colors.black,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Label(
                                            title: fNameController.text
                                                .toString()
                                                .trim(),
                                            textAlign: TextAlign.center,
                                            fontSize: 18,
                                            color: Colors.black,
                                          ),
                                          SizedBox(height: 10),
                                          Label(
                                            title: sNameController.text
                                                .toString()
                                                .trim(),
                                            textAlign: TextAlign.center,
                                            fontSize: 18,
                                            color: Colors.black,
                                          ),
                                          SizedBox(height: 10),
                                          Label(
                                            title: lNameController.text
                                                .toString()
                                                .trim(),
                                            textAlign: TextAlign.center,
                                            fontSize: 18,
                                            color: Colors.black,
                                          ),
                                          SizedBox(height: 10),
                                          Label(
                                            title: msisdnController.text
                                                .toString(),
                                            textAlign: TextAlign.center,
                                            fontSize: 18,
                                            color: Colors.black,
                                          ),
                                          SizedBox(height: 10),
                                          Label(
                                            title: _selectedLocation,
                                            textAlign: TextAlign.center,
                                            fontSize: 18,
                                            color: Colors.black,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                BlocBuilder<SmsServiceInfoBloc,
                                    SmsServiceInfoState>(
                                  builder: (context, state) {
                                    if (state.serviceInfoResource.status ==
                                        STATUS.LOADING) {
                                      return Container(
                                        width: 200,
                                        height: 60,
                                        margin: EdgeInsets.only(
                                          top: 35,
                                          bottom: 40,
                                        ),
                                      );
                                    } else if (state
                                            .serviceInfoResource.status ==
                                        STATUS.SUCCESS) {
                                      return BlocListener<SmsServiceInfoBloc,
                                          SmsServiceInfoState>(
                                        listener: (context, listnerState) {
                                          if (listnerState.subscribeService !=
                                              null) {
                                            if (listnerState
                                                    .subscribeService.status ==
                                                STATUS.ERROR) {
                                              CommonMethods.showToast(
                                                  fToast: fToast,
                                                  message: listnerState
                                                      .subscribeService
                                                      .failure
                                                      .message,
                                                  status: false);
                                            }
                                            if (listnerState
                                                    .subscribeService.status ==
                                                STATUS.SUCCESS) {
                                              CommonMethods.showToast(
                                                  fToast: fToast,
                                                  message: listnerState
                                                      .subscribeService.data,
                                                  status: false);
                                              Timer(Duration(seconds: 3), () {
                                                Navigator.of(context).pop(true);
                                              });
                                            }
                                          }
                                        },
                                        child: Container(
                                          height: 60,
                                          margin: EdgeInsets.only(
                                              top: 35, left: 30, right: 30),
                                          child: BlocBuilder<SmsServiceInfoBloc,
                                              SmsServiceInfoState>(
                                            builder:
                                                (context, subscribingState) {
                                              if (subscribingState
                                                          .subscribeService !=
                                                      null &&
                                                  subscribingState
                                                          .subscribeService
                                                          .status ==
                                                      STATUS.LOADING) {
                                                return circularProgressIndicator();
                                              } else {
                                                return Button(
                                                  title: AppLocalizations.of(
                                                          context)
                                                      .translate("Confirm"),
                                                  color: Colors.transparent,
                                                  borderColor: Color(
                                                      appColors.buttonColor),
                                                  textColor: Color(
                                                      appColors.buttonColor),
                                                  onTap: () {
                                                    if (!relationBool) {
                                                      CommonMethods.showToast(
                                                          fToast: fToast,
                                                          message: AppLocalizations
                                                                  .of(context)
                                                              .translate(
                                                                  "Please select relation"),
                                                          status: false);
                                                    } else if (_formKey
                                                        .currentState
                                                        .validate()) {
                                                      final _firstName =
                                                          fNameController.text
                                                              .toString()
                                                              .trim();
                                                      final _middleName =
                                                          sNameController.text
                                                              .toString()
                                                              .trim();
                                                      final _lastName =
                                                          lNameController.text
                                                              .toString()
                                                              .trim();
                                                      final _number =
                                                          msisdnController.text
                                                              .toString()
                                                              .trim();
                                                      final _params = SubscriptionRequestParams(
                                                          id: (state.serviceInfoResource
                                                                      .data
                                                                  as SMSServiceInfoEntity)
                                                              .id,
                                                          number: _number,
                                                          fName: _firstName,
                                                          mName: _middleName,
                                                          lName: _lastName,
                                                          notificationType:
                                                              _notificationType,
                                                          relationId:
                                                              _selectedRelationId);
                                                      BlocProvider.of<
                                                                  SmsServiceInfoBloc>(
                                                              context)
                                                          .add(SubscribeEvent(
                                                              params: _params));
                                                    }
                                                  },
                                                );
                                              }
                                            },
                                          ),
                                        ),
                                      );
                                    } else
                                      return Container();
                                  },
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
      },
    );
  }

  Future<void> apiRelationCall() async {
    final _token = await appPreferences.getStringPreference("AccessToken");

    if (context != null) {
      _showLoader(context);
    }
    var response = await http.get(Uri.parse(GET_RELATION), headers: {
      'Content-type': 'application/json',
      AUTHORIZATION: BEARER + " " + _token
    });

    if (response.statusCode == 200) {
      final Map<String, dynamic> _responseJson = json.decode(response.body);
      if (mContextLoader != null) Navigator.pop(mContextLoader);
      print(
          'RelationFinalResponse ${response.statusCode} and ${_responseJson.toString()}');
      childRelationDataModel = ChildRelationDataModel.fromJson(_responseJson);
      childDataList = childRelationDataModel.childDataList;
      setState(() {});
    }
  }

  Future<Null> _showLoader(BuildContext context) async {
    return showDialog<Null>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context1) {
        mContextLoader = context1;
        return Scaffold(
          backgroundColor: Colors.transparent.withOpacity(0.3),
          key: _keyLoader,
          body: Container(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    child: CircularProgressIndicator(),
                    height: 40.0,
                    width: 40.0,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
