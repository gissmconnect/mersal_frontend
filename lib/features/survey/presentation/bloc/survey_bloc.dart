import 'dart:async';
import 'dart:math';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/network/vo/Resource.dart';
import 'package:mersal/core/usecases/reauthenticate_usecase.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/survey/domain/entity/survey_entity.dart';
import 'package:mersal/features/survey/domain/usecase/get_survey_usecase.dart';
import 'package:mersal/features/survey/domain/usecase/submit_question_survey_usecase.dart';
import 'package:meta/meta.dart';

part 'survey_event.dart';

part 'survey_state.dart';

class SurveyBloc extends Bloc<SurveyEvent, SurveyState> {
  SurveyBloc.initial(this.getSurveyUseCase, this.submitQuestionSurveyUseCase, this.reAuthenticateUseCase) : super(null);

  final GetSurveyUseCase getSurveyUseCase;
  final SubmitQuestionSurveyUseCase submitQuestionSurveyUseCase;
  final ReAuthenticateUseCase reAuthenticateUseCase;

  SurveyBloc(
      {this.getSurveyUseCase,
      this.reAuthenticateUseCase,
      this.submitQuestionSurveyUseCase})
      : super(SurveyState.initial());

  @override
  Stream<SurveyState> mapEventToState(
    SurveyEvent event,
  ) async* {
    if (event is GetSurveyEvent) {
      yield state.copy(getSurveyResource: Resource.loading());
      final _surveysCall = await getSurveyUseCase(NoParams());
      yield* _surveysCall.fold((l) async* {
        if (l is AuthFailure) {
          final _reauthenticateResponse =
              await reAuthenticateUseCase(NoParams());
          _reauthenticateResponse.fold((l) async* {
            yield Left(l);
          }, (r) => add(GetSurveyEvent()));
        } else {
          yield state.copy(getSurveyResource: Resource.error(failure: l));
        }
      }, (r) async* {
        yield state.copy(getSurveyResource: Resource.success(data: r));
      });
    }

    if (event is ChooseAnswerEvent) {
      final SurveyEntity _survey = state.getSurveyResource.data;
      final _question = _survey.questions
          .firstWhere((element) => element.id == event.questionId);
      final _questionIndex = _survey.questions.indexOf(_question);
      _survey.questions[_questionIndex] =
          _question.copy(answerId: event.optionId);
      yield state.copy(
          randomInt: Random().nextInt(10000),
          getSurveyResource: Resource.success(data: _survey));
    }

    if (event is SubmitAnswerEvent) {
      yield SurveyQuestionSubmitting();
      final result = await submitQuestionSurveyUseCase(SurveySubmitParams(
          surveyId: event.surveyId,
          surveyQuestionId: event.surveyQuestionId,
          surveyOptionId: event.surveyOptionId));
      yield* result.fold(
        (l) async* {
          yield SurveyQuestionError(l);
        },
        (r) async* {
          yield SurveyQuestionSuccess();
        },
      );
    }
  }
}
