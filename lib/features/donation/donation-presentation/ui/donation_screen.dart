import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mersal/core/localization/app_localization.dart';
import 'package:mersal/core/network/vo/Status.dart';
import 'package:mersal/core/widgets/common/Button.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/InputField.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/features/donation/domain/entity/donation_type_entity.dart';
import 'package:mersal/features/donation/donation-presentation/bloc/donation_bloc.dart';
import 'package:mersal/features/donation_history/donation-history-presentation/ui/donation_history.dart';
import 'package:mersal/injection_container.dart';
import 'package:mersal/resources/colors.dart';

import 'donation_summary.dart';

class DonationScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<DonationBloc>(
        create: (_) => serviceLocator(), child: _DonationScreenBody());
  }
}

class _DonationScreenBody extends StatefulWidget {
  @override
  __DonationScreenBodyState createState() => __DonationScreenBodyState();
}

class __DonationScreenBodyState extends State<_DonationScreenBody> {
  final _formKey = GlobalKey<FormState>();
  final amountController = TextEditingController();
  final addNotesController = TextEditingController();

  //String donationReason = '', descriptionText = "Description";
  FToast fToast;

  @override
  void initState() {
    BlocProvider.of<DonationBloc>(context).add(GetDonationTypeEvent());
    super.initState();
    // donationReason = '';
    fToast = FToast();
    fToast.init(context);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bg_image.jpg"),
            fit: BoxFit.fill,
          ),
        ),
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                SizedBox(
                  height: 30,
                ),
                Stack(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(bottom: 6),
                      alignment: Alignment.bottomCenter,
                      child: Text(
                        AppLocalizations.of(context).translate("Donations"),
                        textAlign: TextAlign.end,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(right: 15),
                      alignment: Alignment.centerRight,
                      child: GestureDetector(
                        child: Container(
                          margin: EdgeInsets.only(left: 20),
                          child: SvgPicture.asset(
                            "assets/images/donation_history.svg",
                            width: 30,
                            height: 30,
                          ),
                        ),
                        onTap: () {
                          CommonMethods().animatedNavigation(
                              context: context,
                              currentScreen: DonationScreen(),
                              landingScreen: DonationHistory());
                        },
                      ),
                    ),
                  ],
                ),
                Container(
                  margin: EdgeInsets.only(top: 20),
                  child: SvgPicture.asset(
                    "assets/images/donation_box.svg",
                    fit: BoxFit.cover,
                    width: 250,
                    height: 200,
                  ),
                ),
                BlocBuilder<DonationBloc, DonationState>(
                  builder: (context, state) {
                    return Column(
                      children: [
                        Container(
                          margin:
                              EdgeInsets.only(left: 30, right: 30, top: 20),
                          alignment: Alignment.center,
                          decoration: new BoxDecoration(
                            shape: BoxShape.rectangle,
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(25),
                          ),
                          child: DropdownButton<DonationTypeEntity>(
                            underline: Container(),
                            items: state.donationTypeResource.status ==
                                    STATUS.SUCCESS
                                ? (state.donationTypeResource.data
                                        as List<DonationTypeEntity>)
                                    .map(
                                      (e) => DropdownMenuItem<
                                          DonationTypeEntity>(
                                        value: e,
                                        child: Label(
                                          title: e.reason,
                                          color: Colors.black,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 16,
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                    )
                                    .toList()
                                : [],
                            hint: Label(
                              title:AppLocalizations.of(context).translate('Select a Donation Type'),
                              color: Colors.black,
                              fontSize: 16,
                              textAlign: TextAlign.center,
                            ),
                            value: state.selectedDonationType,
                            onChanged: (newVal) {
                              // donationReason = state.selectedDonationType.reason;
                              // descriptionText = state.selectedDonationType.reason ?? "N/A";
                              BlocProvider.of<DonationBloc>(context).add(
                                SelectDonationType(donationType: newVal),
                              );
                              //donationType = newVal.reason.reason;
                            },
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          margin:
                              EdgeInsets.only(top: 20, left: 30, right: 30),
                          padding:
                              EdgeInsets.only(top: 15, bottom: 15, left: 20),
                          decoration: new BoxDecoration(
                            shape: BoxShape.rectangle,
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(25),
                          ),
                          child: Label(
                            title: state.selectedDonationType != null
                                ? state.selectedDonationType.description
                                : AppLocalizations.of(context).translate("Description"),
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                            fontSize: 16,
                          ),
                        ),
                      ],
                    );
                  },
                ),
                Container(
                  height: 80,
                  margin: EdgeInsets.only(left: 30, top: 25, right: 30),
                  child: TextFormField(
                    controller: amountController,
                    validator: (text) {
                      return text.isEmpty ? AppLocalizations.of(context).translate('Amount cannot be empty') : null;
                    },
                    decoration: InputDecoration(
                      filled: true,
                      counterText: "",
                      fillColor: Colors.white,
                      contentPadding:
                          const EdgeInsets.only(bottom: 15.0, top: 15.0),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(25.7),
                      ),
                      errorStyle: TextStyle(
                          fontSize: 14.0,
                          color: Colors.red,
                          fontWeight: FontWeight.w600),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(25.7),
                      ),
                      focusedErrorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25.0),
                          borderSide: BorderSide(
                            color: Colors.red,
                          )),
                      errorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25.0),
                          borderSide: BorderSide(
                            color: Colors.red,
                          )),
                      floatingLabelBehavior: FloatingLabelBehavior.auto,
                      prefixIcon: Container(
                        margin: EdgeInsets.only(left: 20, top: 13, right: 5),
                        child: Label(
                          title:AppLocalizations.of(context).translate("OMR"),
                          color: Colors.black,
                          fontSize: 16,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    onFieldSubmitted: (term) {
                      FocusScope.of(context).unfocus();
                    },
                    maxLength: 5,
                    keyboardType: TextInputType.phone,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 30, right: 30),
                  child: InputField(
                    controller: addNotesController,
                    onSubmitted: (term) {
                      FocusScope.of(context).unfocus();
                    },
                    validator: (text) {
                      return text.isEmpty
                          ?AppLocalizations.of(context).translate('Add notes cannot be empty')
                          : null;
                    },
                    hint:AppLocalizations.of(context).translate("Add notes"),
                    maxLength: 60,
                    inputType: TextInputType.text,
                  ),
                ),
                Container(
                  width: 250,
                  height: 60,
                  margin: EdgeInsets.only(
                      left: 40, top: 35, right: 40, bottom: 30),
                  child: BlocListener<DonationBloc, DonationState>(
                    listener: (context, state) {
                      /*if (state is DonationError) {
                        showSnackBarMessage(
                            context: context,
                            message: state.failure.message);
                      } else if (state is DonationSuccess) {
                        CommonMethods().animatedNavigation(
                            context: context,
                            currentScreen: DonationScreen(),
                            landingScreen: PaymentOptions("donation"));
                      }*/
                    },
                    child: BlocBuilder<DonationBloc, DonationState>(
                        builder: (_, state) {
                      /*if (state is DonationInitiated) {
                            return Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                CircularProgressIndicator(),
                              ],
                            );
                          }*/
                      return Button(
                        title: AppLocalizations.of(context).translate("Donate Now"),
                        textColor: Colors.white,
                        borderColor: Colors.transparent,
                        color: Color(appColors.buttonColor),
                        onTap: () {
                          if (state.selectedDonationType == null) {
                            CommonMethods.showToast(
                                fToast: fToast,
                                message: "Selection a donation type",
                                status: false);
                            return;
                          }
                          if (_formKey.currentState.validate()) {
                            donationHistoryBottomSheet(
                                state.selectedDonationType, "payment");
                          }
                        },
                      );
                    }),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );

  }

  Future<void> donationHistoryBottomSheet(
      DonationTypeEntity donationType, String type) {
    return showModalBottomSheet<void>(
      isScrollControlled: true,
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30.0), topRight: Radius.circular(30.0)),
      ),
      builder: (BuildContext context) {
        return Container(
            height: MediaQuery.of(context).size.height / 2,
            color: Colors.white,
            margin: EdgeInsets.only(top: 40),
            child: DonationSummary(amountController.text.toString(),
                donationType, onDonationCreated));
      },
    );
  }

  void onDonationCreated() {
    BlocProvider.of<DonationBloc>(context).add(GetDonationTypeEvent());
    // amountController.clear();
    // addNotesController.clear();
  }

}
