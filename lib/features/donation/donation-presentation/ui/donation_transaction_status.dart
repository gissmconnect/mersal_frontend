import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mersal/core/widgets/common/Button.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/features/auth/auth_presentation/doc_upload_presentation/bloc/doc_upload_bloc.dart';
import 'package:mersal/features/dashboard/dashboard_page.dart';
import 'package:mersal/features/donation_history/donation-history-presentation/ui/donation_history.dart';
import 'package:mersal/injection_container.dart';
import 'package:mersal/resources/colors.dart';

class DonationStatusPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<DocUploadBloc>(
      create: (_) => serviceLocator<DocUploadBloc>(),
      child: Scaffold(
        body: _DonationStatusPageBody(),
      ),
    );
  }
}

class _DonationStatusPageBody extends StatefulWidget {
  @override
  _DonationStatusPageBodyState createState() => _DonationStatusPageBodyState();
}

class _DonationStatusPageBodyState extends State<_DonationStatusPageBody> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Container(
        height: 400,
        padding: const EdgeInsets.all(8.0),
        margin: EdgeInsets.only(top: 20, left: 20, right: 20),
        decoration: new BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.white,
          borderRadius: BorderRadius.circular(25.0),
        ),
        child: donationSuccessLayout(),
      ),
    );
  }

  Widget textLayout(String message) {
    return Container(
      margin: EdgeInsets.only(top: 30),
      child: Label(
        textAlign: TextAlign.center,
        title: message,
        fontSize: 16,
        color: Colors.black,
      ),
    );
  }

  Widget donationFailureLayout() {
    return Column(
      children: [
        Container(
          child: Icon(
            Icons.cancel,
            color: Colors.red,
            size: 50,
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 30),
          child: Label(
            textAlign: TextAlign.center,
            title: "Car holder name",
            fontSize: 20,
            color: Colors.blue,
          ),
        ),
        textLayout("Your Donation is failed"),
        GestureDetector(
          onTap: () {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (context) => DashboardPage(),
              ),
            );
          },
          child: Container(
            margin:
            EdgeInsets.only(left: 30, top: 45, right: 30, bottom: 10),
            height: 60,
            decoration: BoxDecoration(
                border: Border.all(color: Color(appColors.buttonColor)),
                color: Colors.white,
                borderRadius: BorderRadius.circular(30)),
            child: Stack(
              children: <Widget>[
                Align(
                    alignment: Alignment.centerRight,
                    child: Container(
                      margin: EdgeInsets.only(left: 10, right: 15),
                      child: SvgPicture.asset(
                        "assets/images/home_icon.svg",
                        fit: BoxFit.cover,
                        width: 30,
                        height: 30,
                        color: Colors.black,
                      ),
                    )),
                Align(
                    alignment: Alignment.center,
                    child: Container(
                      margin: EdgeInsets.only(right: 15),
                      child: Label(
                        title: "Go to home page",
                        color: Colors.black,
                        fontWeight: FontWeight.w700,
                        fontSize: 17,
                        textAlign: TextAlign.center,
                      ),
                    ))
              ],
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(left: 10, right: 10, top: 15),
          height: 55,
          child: Button(
              title: "Try Again",
              textColor: Colors.white,
              borderColor: Colors.transparent,
              color: Color(appColors.buttonColor),
              onTap: () {
                Navigator.pop(context);
              }),
        )
      ],
    );
  }

  Widget donationSuccessLayout() {
    return Column(
      children: [
        Container(
            child: Icon(Icons.check_circle_rounded,color: Colors.lightBlue,size: 50,),
        ),
        Container(
          margin: EdgeInsets.only(top: 30),
          child: Label(
            textAlign: TextAlign.center,
            title: "Car holder name",
            fontSize: 20,
            color: Colors.blue,
          ),
        ),
        textLayout("Thank you for your Donation to AR"),
        GestureDetector(
          onTap: () {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (context) => DashboardPage(),
              ),
            );
          },
          child: Container(
            margin:
            EdgeInsets.only(left: 30, top: 45, right: 30, bottom: 10),
            height: 60,
            decoration: BoxDecoration(
                border: Border.all(color: Color(appColors.buttonColor)),
                color: Colors.white,
                borderRadius: BorderRadius.circular(30)),
            child: Stack(
              children: <Widget>[
                Align(
                    alignment: Alignment.centerRight,
                    child: Container(
                      margin: EdgeInsets.only(left: 10, right: 15),
                      child: SvgPicture.asset(
                        "assets/images/home_icon.svg",
                        fit: BoxFit.cover,
                        width: 30,
                        height: 30,
                        color: Colors.black,
                      ),
                    )),
                Align(
                    alignment: Alignment.center,
                    child: Container(
                      margin: EdgeInsets.only(right: 15),
                      child: Label(
                        title: "Go to home page",
                        color: Colors.black,
                        fontWeight: FontWeight.w700,
                        fontSize: 16,
                        textAlign: TextAlign.center,
                      ),
                    ))
              ],
            ),
          ),
        ),
        GestureDetector(
          onTap: () {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (context) => DonationHistory(),
              ),
            );
          },
          child: Container(
            margin:
            EdgeInsets.only(left: 30, top: 20, right: 30, bottom: 10),
            height: 60,
            decoration: BoxDecoration(
                color: Color(appColors.buttonColor),
                borderRadius: BorderRadius.circular(30)),
            child: Stack(
              children: <Widget>[
                Align(
                    alignment: Alignment.centerRight,
                    child: Container(
                      margin: EdgeInsets.only(left: 10, right: 15),
                      child: SvgPicture.asset(
                        "assets/images/donation_history.svg",
                        fit: BoxFit.cover,
                        width: 25,
                        height: 25,
                        color: Colors.white,
                      ),
                    )),
                Align(
                    alignment: Alignment.center,
                    child: Container(
                      margin: EdgeInsets.only(right: 15),
                      child: Label(
                        title: "Go to Donation History",
                        color: Colors.white,
                        fontWeight: FontWeight.w700,
                        fontSize: 16,
                        textAlign: TextAlign.center,
                      ),
                    ))
              ],
            ),
          ),
        ),
      ],
    );
  }
}
