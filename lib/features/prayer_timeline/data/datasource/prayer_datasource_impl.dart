import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:mersal/core/network/api_endpoints.dart';
import 'package:mersal/core/network/http_call.dart';
import 'package:mersal/core/network/http_error_handler.dart';
import 'package:mersal/features/prayer_timeline/data/data/prayer_data_model.dart';
import 'package:mersal/features/prayer_timeline/data/datasource/prayer_datasource.dart';
import 'package:meta/meta.dart';

class PrayerDataSourceImpl implements PrayerDataSource {
  final http.Client httpClient;

  PrayerDataSourceImpl({@required this.httpClient});

  @override
  Future<List<PrayerDataModel>> getPrayerTimes({String token}) async {
    var _response = await httpGetRequest(
        httpClient: httpClient, url: GET_PRAYER_TIMES_ENDPOINT, token: token);
    if (_response.statusCode == 200) {
      List<dynamic> _prayerListJson =json.decode(_response.body)['data'] as List;
      return _prayerListJson
          .map((element) => PrayerDataModel.fromJson(element))
          .toList();
    } else
      handleError(_response);
  }

  @override
  Future<String> setAlarm({String token, int id}) async {
    var _params = <String, String>{'prayer_id': id.toString()};
    var _response = await httpPostRequest(
        httpClient: httpClient,
        url: CREATE_ALARM_END_POINT,
        token: token,
        params: _params);
    if (_response.statusCode == 200) {
      Map<String, dynamic> responseJSON = json.decode(_response.body);
      return responseJSON['message'];
    } else
      handleError(_response);
  }
}
