import 'package:dartz/dartz.dart';
import 'package:mersal/core/usecases/save_user_login_data_usecase.dart';
import 'package:mersal/features/auth/auth_data/model/user_data_model.dart';
import 'package:meta/meta.dart';

import '../../../../core/error/failures.dart';
import '../entity/user_data.dart';

abstract class AuthRepository {
  Future<Either<Failure, UserData>> getUserSession();

  Future<Either<Failure, UserData>> login(
      {@required String phoneNumber, @required String password, @required String device_token});

  Future<Either<Failure, void>> register(
      {@required String phoneNumber,
      @required String password,
      @required String userName,
      @required String gender});

  Future<Either<Failure, String>> sendOtp({@required String phoneNumber});

  Future<Either<Failure, void>> weddingBooking({
    @required String name,
    @required String date,
    @required String time,
  });

  Future<Either<Failure, void>> childRegistration(
      {@required String name, @required String age});

  Future<Either<Failure, void>> weddingInvitation(
      {@required String invitationType,
      @required String date,
      @required String time});

  Future<Either<Failure, void>> verifyOtp(
      {@required phoneNumber, @required String otp});

  Future<Either<Failure, String>> notification({@required String service_id});

  Future<void> saveUserLoginData({@required LoginParams loginParams});

  Future<Either<Failure, LoginParams>> getLoginData();

  Future<Either<Failure, void>> logout();

  Future<Either<Failure, int>> getDocumentStatus();

  Future<Either<Failure, void>> changePassword(
      {@required String oldPassword, @required String newPassword});

  Future<Either<Failure, void>> uploadDocuments(
      {@required String frontImage,
      @required String backImage,
      @required String name,
      @required String dob,
      @required String expDate,
      @required String idNumber});

  Future<void> saveUserDate({@required UserDataModel userDataModel});
}
