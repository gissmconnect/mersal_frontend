import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:mersal/core/localization/app_localization.dart';
import 'package:mersal/core/network/api_endpoints.dart';
import 'package:mersal/core/network/vo/Status.dart';
import 'package:mersal/core/util/constants.dart';
import 'package:mersal/core/widgets/common/Button.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/InputField.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/core/widgets/common/shared_preference.dart';
import 'package:mersal/core/widgets/message.dart';
import 'package:mersal/core/widgets/widgets.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/child_service/child-domain/model/child-registration/ChildDataList.dart';
import 'package:mersal/features/child_service/child-domain/model/child-registration/ChildRelationDataModel.dart';
import 'package:mersal/features/lawati_service/domain/entity/sms_serviceInfo_entity.dart';
import 'package:mersal/features/lawati_service/domain/usecase/subscribe_sms_subscription_usecase.dart';
import 'package:mersal/features/lawati_service/presentation/service_info/bloc/my_sms_service/my_sms_service_bloc.dart';
import 'package:mersal/features/payment/payment_webview.dart';
import 'package:mersal/resources/colors.dart';

import '../../../../../injection_container.dart';

class SelfSmsSubscriptionFormPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<MySmsServiceBloc>(
        create: (_) => serviceLocator<MySmsServiceBloc>(),
        child: Scaffold(body: SelfSMSSubscriptionBody()));
  }
}

class SelfSMSSubscriptionBody extends StatefulWidget {
  @override
  _SelfSMSSubscriptionBodyState createState() =>
      _SelfSMSSubscriptionBodyState();
}

class _SelfSMSSubscriptionBodyState extends State<SelfSMSSubscriptionBody> {
  final fNameController = TextEditingController();
  final sNameController = TextEditingController();
  final lNameController = TextEditingController();
  final FocusNode fNameFocus = new FocusNode();
  final FocusNode sNameFocus = new FocusNode();
  final FocusNode lNameFocus = new FocusNode();

  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  BuildContext mContextLoader;
  final _formKey = GlobalKey<FormState>();
  bool relationBool = false,
      showSelected,
      showAppNotificationBool = false,
      showSMSNotificationBool = false;
  List<ChildDataList> childDataList;
  ChildRelationDataModel childRelationDataModel;
  String _selectedNotification = "",
      msidNumber = '',
      userName = '';

  FToast fToast;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _asyncMethod();
    });
    apiRelationCall();
    BlocProvider.of<MySmsServiceBloc>(context).add(GetServiceInfoEvent());
    fToast = FToast();
    fToast.init(context);
  }

  _asyncMethod() async {
    msidNumber = await appPreferences.getStringPreference("UserPhone");
    userName = await appPreferences.getStringPreference("UserName");
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bg_image.jpg"),
            fit: BoxFit.fill,
          ),
        ),
        child: ListView(
          children: [
            SizedBox(
              height: 20,
            ),
            Stack(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(bottom: 5, left: 15),
                  alignment: Alignment.bottomLeft,
                  child: GestureDetector(
                    child: Icon(
                      Icons.arrow_back_ios_rounded,
                      color: Colors.white,
                    ),
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 6),
                  alignment: Alignment.bottomCenter,
                  child: Text(
                    AppLocalizations.of(context).translate("Lawati SMS Service"),
                    textAlign: TextAlign.end,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.w400),
                  ),
                ),
              ],
            ),
            Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    width: MediaQuery
                        .of(context)
                        .size
                        .width,
                    height: 50,
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.only(left: 20),
                    margin: EdgeInsets.only(left: 30, top: 25, right: 30),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(25)),
                    child: Label(
                      title: msidNumber,
                      fontSize: 16,
                      color: Colors.black,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Container(
                    width: MediaQuery
                        .of(context)
                        .size
                        .width,
                    height: 50,
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.only(left: 20),
                    margin: EdgeInsets.only(left: 30, top: 25, right: 30),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(25)),
                    child: Label(
                      title: userName,
                      fontSize: 16,
                      color: Colors.black,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  BlocBuilder<MySmsServiceBloc, MySmsServiceState>(
                    builder: (context, state) {
                      if (state.serviceInfoResource.status == STATUS.SUCCESS) {
                        SMSServiceInfoEntity smsServiceInfo =
                            state.serviceInfoResource.data;
                        return Column(
                          children: [
                            Container(
                              margin: EdgeInsets.only(
                                  top: 30, bottom: 10, left: 20, right: 20),
                              child: Label(
                                title: smsServiceInfo.title,
                                fontSize: 16,
                                color: Colors.white,
                                textAlign: TextAlign.center,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                  bottom: 10, left: 20, right: 20),
                              child: Label(
                                title:
                                "OMR ${smsServiceInfo
                                    .price} for ${smsServiceInfo
                                    .duration} ${smsServiceInfo.duration > 1
                                    ? "Years"
                                    : "Year"}",
                                fontSize: 16,
                                color: Colors.white,
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ],
                        );
                      } else if (state.mySubscriptionInfo.status ==
                          STATUS.LOADING) {
                        return circularProgressIndicator();
                      }
                      return Container();
                    },
                  ),
                  /* Center(
                    child: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          GestureDetector(
                            onTap: () {
                              showSelected = true;
                              _selectedNotification = NOTIFICATION_TYPE_SMS;
                              setState(() {});
                            },
                            child: Container(
                              margin: EdgeInsets.only(right: 20),
                              alignment: Alignment.centerRight,
                              child: Icon(Icons.notifications_active_outlined,
                                  size: 50,
                                  color: showSelected != null && showSelected
                                      ? Colors.black
                                      : Colors.white),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              showSelected = false;
                              _selectedNotification = NOTIFICATION_TYPE_SMS;
                              setState(() {});
                            },
                            child: Container(
                              margin: EdgeInsets.only(right: 20),
                              alignment: Alignment.centerRight,
                              child: Icon(Icons.textsms_outlined,
                                  size: 50,
                                  color: showSelected != null && !showSelected
                                      ? Colors.black
                                      : Colors.white),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),*/
                  Container(
                    margin: EdgeInsets.only(
                        top: 10, bottom: 30, left: 20, right: 20),
                    child: Label(
                      title:AppLocalizations.of(context).translate(
                          "Select Your Preferred Notification"),
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                      textAlign: TextAlign.center,
                      color: Colors.white,
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      // showAppNotificationBool = !showAppNotificationBool;
                      _selectedNotification = NOTIFICATION_TYPE_PUSH;
                      showSelected = true;
                      setState(() {});
                    },
                    child: Center(
                      child: Container(
                        margin: EdgeInsets.only(left: 30, right: 30),
                        padding: EdgeInsets.only(top: 10, bottom: 10, left: 20),
                        decoration: BoxDecoration(
                            color: showSelected != null && showSelected
                                ? Colors.white
                                : Colors.transparent,
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(color: Colors.white)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              margin: EdgeInsets.only(right: 20),
                              alignment: Alignment.centerRight,
                              child: Icon(
                                showSelected != null && showSelected
                                    ? Icons.check_box
                                    : Icons.check_box_outline_blank,
                                size: 30,
                                color: Color(appColors.buttonColor),
                              ),
                            ),
                            Label(
                              title: AppLocalizations.of(context)
                                  .translate("App Notification"),                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                              textAlign: TextAlign.center,
                              color: showSelected != null && showSelected
                                  ? Colors.black
                                  : Colors.white,
                            ),
                            Container(
                                margin: EdgeInsets.only(right: 20),
                                child: SvgPicture.asset(
                                    "assets/images/notification_icon.svg",
                                    width: 25,
                                    height: 25,
                                    color: showSelected != null && showSelected
                                        ? Colors.black
                                        : Colors.white)),
                          ],
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  GestureDetector(
                    onTap: () {
                      // showSMSNotificationBool = !showSMSNotificationBool;
                      _selectedNotification = NOTIFICATION_TYPE_SMS;
                      showSelected = false;
                      setState(() {});
                    },
                    child: Center(
                      child: Container(
                        margin: EdgeInsets.only(left: 30, right: 30),
                        padding: EdgeInsets.only(top: 10, bottom: 10, left: 20),
                        decoration: BoxDecoration(
                            color: showSelected != null && !showSelected
                                ? Colors.white
                                : Colors.transparent,
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(color: Colors.white)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              margin: EdgeInsets.only(right: 20),
                              alignment: Alignment.centerRight,
                              child: Icon(
                                showSelected != null && !showSelected
                                    ? Icons.check_box
                                    : Icons.check_box_outline_blank,
                                size: 30,
                                color: Color(appColors.buttonColor),
                              ),
                            ),
                            Label(
                              title: AppLocalizations.of(context)
                                .translate("SMS"),
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                              textAlign: TextAlign.center,
                              color: showSelected != null && !showSelected
                                  ? Colors.black
                                  : Colors.white,
                            ),
                            Container(
                              margin: EdgeInsets.only(right: 20),
                              child: Icon(Icons.email,
                                  size: 30,
                                  color: showSelected != null && !showSelected
                                      ? Colors.black
                                      : Colors.white),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  BlocBuilder<MySmsServiceBloc, MySmsServiceState>(
                    builder: (context, state) {
                      if (state.serviceInfoResource.status == STATUS.SUCCESS) {
                        final SMSServiceInfoEntity smsService =
                            state.serviceInfoResource.data;
                        return Container(
                          height: 60,
                          margin: EdgeInsets.only(
                              top: 35, bottom: 40, left: 20, right: 20),
                          child:
                          BlocListener<MySmsServiceBloc, MySmsServiceState>(
                            listener: (context, listnerState) {
                              if (listnerState.subscribeService != null) {
                                if (listnerState.subscribeService.status ==
                                    STATUS.SUCCESS) {
                                  // CommonMethods.showToast(fToast: fToast,
                                  //     message: listnerState.subscribeService
                                  //         .data,
                                  //     status: true);
                                  Timer(Duration(seconds: 2), () {
                                    CommonMethods().animatedNavigation(
                                        context: context,
                                        currentScreen:
                                        SelfSmsSubscriptionFormPage(),
                                        landingScreen: PaymentWebView(
                                          type: "lawati-sms",
                                          id: listnerState.subscribeService.data['id'].toString(),
                                          activity: "",
                                        ));
                                    // Navigator.of(context).pop(true);
                                  });
                                } else if (listnerState .subscribeService.status == STATUS.ERROR) {
                                  CommonMethods.showToast(
                                      fToast: fToast,
                                      message: listnerState
                                          .subscribeService.failure.message,
                                      status: false);
                                }
                              }
                            },
                            child: BlocBuilder<MySmsServiceBloc,
                                MySmsServiceState>(
                              builder: (context, state) {
                                if (state.subscribeService != null &&
                                    state.subscribeService.status ==
                                        STATUS.LOADING) {
                                  return circularProgressIndicator();
                                } else {
                                  return Button(
                                    title: AppLocalizations.of(context)
                                        .translate("Subscribe Now"),
                                    color: Color(appColors.buttonColor),
                                    borderColor: Colors.transparent,
                                    textColor: Colors.white,
                                    onTap: () {
                                      if (_formKey.currentState.validate()) {
                                        final _firstName = fNameController.text
                                            .toString()
                                            .trim();
                                        final _middleName = sNameController.text
                                            .toString()
                                            .trim();
                                        final _lastName = lNameController.text
                                            .toString()
                                            .trim();
                                        final _number = msidNumber.trim();
                                        final _params = SubscriptionRequestParams(
                                            id: (state.serviceInfoResource.data as SMSServiceInfoEntity) .id,
                                            number: _number,
                                            fName: userName,
                                            // mName: _middleName,
                                            // lName: _lastName,
                                            notificationType:
                                            _selectedNotification,
                                            relationId: "");
                                        BlocProvider.of<MySmsServiceBloc>(
                                            context)
                                            .add(SubscribeEvent(
                                            params: _params));
                                      }
                                    },
                                  );
                                }
                              },
                            ),
                          ),
                        );
                      } else if (state.mySubscriptionInfo.status ==
                          STATUS.LOADING) {
                        return circularProgressIndicator();
                      }
                      return Container();
                    },
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Future<void> apiRelationCall() async {
    final _token = await appPreferences.getStringPreference("AccessToken");

    if (context != null) {
      _showLoader(context);
    }
    var response = await http.get(Uri.parse(GET_RELATION), headers: {
      'Content-type': 'application/json',
      AUTHORIZATION: BEARER + " " + _token
    });

    if (response.statusCode == 200) {
      final Map<String, dynamic> _responseJson = json.decode(response.body);
      if (mContextLoader != null) Navigator.pop(mContextLoader);
      print(
          'RelationFinalResponse ${response.statusCode} and ${_responseJson
              .toString()}');
      childRelationDataModel = ChildRelationDataModel.fromJson(_responseJson);
      childDataList = childRelationDataModel.childDataList;
      setState(() {});
    }
  }

  Future<Null> _showLoader(BuildContext context) async {
    return showDialog<Null>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context1) {
        mContextLoader = context1;
        return Scaffold(
          backgroundColor: Colors.transparent.withOpacity(0.3),
          key: _keyLoader,
          body: Container(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    child: CircularProgressIndicator(),
                    height: 40.0,
                    width: 40.0,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
