import 'package:mersal/features/child_service/child-domain/entity/child-enrollment-entity/registered-child-activity-entity/child_activity_model_entity.dart';
import 'package:meta/meta.dart';

class ChildActivityModel extends ChildActivityModelEntity {
  ChildActivityModel(
      {@required bool status,
      @required List<ChildActivityDataModel> childNewDataList})
      : super(status: status, childActivityList: childNewDataList);

  ChildActivityModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['data'] != null) {
      childActivityList = List<ChildActivityData>();
      json['data'].forEach((v) {
        childActivityList.add(ChildActivityDataModel.fromJson(v));
      });
    }
  }
}

class ChildActivityDataModel extends ChildActivityData {
  ChildActivityDataModel({int id, String created_at, String title, String description})
      : super(id: id, created_at: created_at, title: title, description: description);

  ChildActivityDataModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    created_at = json['created_at'];
    title = json['title'];
    description = json['description'];
  }
}
