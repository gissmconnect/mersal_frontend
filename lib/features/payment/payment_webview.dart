import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:mersal/core/localization/app_localization.dart';
import 'package:mersal/core/network/api_endpoints.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/CustomAppBar.dart';
import 'package:mersal/core/widgets/common/shared_preference.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/dashboard/dashboard_page.dart';
import 'package:mersal/features/payment/payment_transaction_status_screen.dart';
import 'package:webview_flutter/webview_flutter.dart';

class PaymentWebView extends StatefulWidget {
  final String type, id,activity;

  const PaymentWebView({Key key, this.id, this.type, this.activity}) : super(key: key);

  @override
  _PaymentWebViewState createState() => _PaymentWebViewState();
}

class _PaymentWebViewState extends State<PaymentWebView> {
  String webUrl = "";
  FToast fToast;

  @override
  void initState() {
    super.initState();
    paymentApiCall();
    fToast = FToast();
    fToast.init(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        title: AppLocalizations.of(context).translate("Payment Gateway"),
        isBackButton: true,
        showText: true,
        rightIconTwo: true,
        rightIconOne: true,
      ),
      body: webUrl.isNotEmpty
          ? Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: WebView(
                onPageFinished: (webUrl) {
                  if (webUrl
                      .split("?")[0]
                      .contains("http://93.115.18.236/public/payment/cancel")) {
                    FocusScope.of(context).unfocus();
                      Navigator.of(context).pushAndRemoveUntil(
                          MaterialPageRoute(
                            builder: (context) => PaymentTransactionStatusScreen(widget.type,"fail",activity:widget.activity),
                          ),
                              (route) => false);
                  } else if (webUrl.split("?")[0].contains(
                      "http://93.115.18.236/public/payment/success")) {
                    FocusScope.of(context).unfocus();
                      Navigator.of(context).pushAndRemoveUntil(
                          MaterialPageRoute(
                            builder: (context) => PaymentTransactionStatusScreen(widget.type,"success",activity:widget.activity),
                          ),
                          (route) => false);
                  }
                },
                javascriptMode: JavascriptMode.unrestricted,
                initialUrl: webUrl,
              ))
          : Center(child: CircularProgressIndicator()),
    );
  }

  Future<void> paymentApiCall() async {
    final _token = await appPreferences.getStringPreference("AccessToken");
    var response = await http.get(
      Uri.parse(PAYMENT_GENERATE_SESSION + "?type=${widget.type}&id=${widget.id}"),
      headers: {
        'Content-type': 'application/json',
        AUTHORIZATION: BEARER + " " + _token
      },
    );

    if (response.statusCode == 200) {
      final Map<String, dynamic> _responseJson = json.decode(response.body);
      print('PaymentApiResponse ${response.statusCode} and ${_responseJson.toString()}');
      if(_responseJson["data"]!=null){
      webUrl = _responseJson["data"]["url"];
      setState(() {});
      }
    }
  }
}
