import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/core/widgets/common/shared_preference.dart';
import 'package:mersal/features/dashboard/dashboard_page.dart';
import 'package:mersal/resources/colors.dart';

class ChildEnrollSuccess extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _ChildEnrollStatusPageBody(),
    );
  }
}

class _ChildEnrollStatusPageBody extends StatefulWidget {
  @override
  _ChildEnrollStatusPageBodyState createState() =>
      _ChildEnrollStatusPageBodyState();
}

class _ChildEnrollStatusPageBodyState
    extends State<_ChildEnrollStatusPageBody> {

  String userName="";

  @override
  void initState() {
    super.initState();
    _asyncMethod();
  }
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Container(
        height: 400,
        padding: const EdgeInsets.all(8.0),
        margin: EdgeInsets.only(top: 20, left: 20, right: 20),
        decoration: new BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.white,
          borderRadius: BorderRadius.circular(25.0),
        ),
        child: successLayout(),
      ),
    );
  }

  Widget textLayout(String message) {
    return Container(
      margin: EdgeInsets.only(top: 30),
      child: Label(
        textAlign: TextAlign.center,
        title: message,
        fontSize: 16,
        color: Colors.black,
      ),
    );
  }

  _asyncMethod() async {
    userName = await appPreferences.getStringPreference("UserName");
  }

  Widget successLayout()  {
    return Column(
      children: [
        Container(
          child: Icon(
            Icons.check_circle_rounded,
            color: Colors.lightBlue,
            size: 50,
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 30),
          child: Label(
            textAlign: TextAlign.center,
            title: userName??"UserName",
            fontSize: 20,
            color: Colors.blue,
          ),
        ),
        textLayout("Successfully registered activity for your child"),
        GestureDetector(
          onTap: () {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (context) => DashboardPage(),
              ),
            );
          },
          child: Container(
            margin: EdgeInsets.only(left: 30, top: 45, right: 30, bottom: 10),
            height: 60,
            decoration: BoxDecoration(
                color: Color(appColors.buttonColor),
                borderRadius: BorderRadius.circular(30)),
            child: Stack(
              children: <Widget>[
                Align(
                    alignment: Alignment.centerRight,
                    child: Container(
                      margin: EdgeInsets.only(left: 10, right: 15),
                      child: SvgPicture.asset(
                        "assets/images/home_icon.svg",
                        fit: BoxFit.cover,
                        width: 30,
                        height: 30,
                        color: Colors.white,
                      ),
                    )),
                Align(
                    alignment: Alignment.center,
                    child: Container(
                      margin: EdgeInsets.only(right: 15),
                      child: Label(
                        title: "Go to home page",
                        color: Colors.white,
                        fontWeight: FontWeight.w700,
                        fontSize: 16,
                        textAlign: TextAlign.center,
                      ),
                    ))
              ],
            ),
          ),
        ),
      ],
    );
  }
}
