import 'package:mersal/features/survey/data/model/survey_model.dart';
import 'package:meta/meta.dart';

abstract class SurveyDataSource {
  Future<SurveyModel>getSurvey({@required String token});

  Future<void> surveyQuestion(
      {@required String token,
        @required int surveyId,
        @required int surveyQuestionId,
        @required int surveyOptionId});
}
