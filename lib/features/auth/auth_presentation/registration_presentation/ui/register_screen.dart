import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mersal/core/localization/app_localization.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/features/auth/auth_presentation/otp_presentation/ui/otp_screen.dart';
import 'package:mersal/features/auth/auth_presentation/registration_presentation/bloc/registration_bloc.dart';
import 'package:mersal/features/country_listing/presentation/ui/country_listing.dart';
import 'package:mersal/resources/colors.dart';

import '../../../../../injection_container.dart';

class RegisterScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (_) => serviceLocator<RegistrationBloc>(),
        child: RegisterScreenBody());
  }
}

class RegisterScreenBody extends StatefulWidget {
  @override
  _RegisterScreenBodyState createState() => _RegisterScreenBodyState();
}

class _RegisterScreenBodyState extends State<RegisterScreenBody> {
  final nameController = TextEditingController();
  final msisdnController = TextEditingController();
  final passwordController = TextEditingController();
  final confirmPasswordController = TextEditingController();
  FToast fToast;
  bool changeBorderBool = false;
  bool obscureText = true, confirmBoolObscure = true;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  BuildContext mContextLoader;

  String passwordText = "", countryCode = "+968";

  final FocusNode nameFocus = new FocusNode();
  final FocusNode msisdnFocus = new FocusNode();
  final FocusNode passwordFocus = new FocusNode();
  final FocusNode confirmPasswordFocus = new FocusNode();
  final FocusNode otpFocus = new FocusNode();
  final _formKey = GlobalKey<FormState>();


  @override
  void initState() {
    super.initState();
    fToast = FToast();
    fToast.init(context);
  }

  @override
  Widget build(BuildContext context) {
    var _genderType = AppLocalizations.of(context).translate('Select Gender');
    List<String> genderOptions = [AppLocalizations.of(context).translate('Male'),AppLocalizations.of(context).translate('Female')];
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                margin: EdgeInsets.only(top: 30),
                child: Label(
                  title:  AppLocalizations.of(context).translate("Register"),
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                  color: Colors.black,
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 30),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 30, top: 25, right: 30),
                        child: TextFormField(
                          controller: nameController,
                          focusNode: nameFocus,
                          onFieldSubmitted: (term) {
                            CommonMethods.inputFocusChange(
                                context, nameFocus, msisdnFocus);
                          },
                          validator: (text) {
                            return text.isEmpty ?  AppLocalizations.of(context).translate('Name cannot be empty'): null;
                          },
                          decoration: InputDecoration(
                            fillColor: Colors.blueGrey[50],
                            filled: true,
                            counterText: "",
                            hintText:  AppLocalizations.of(context).translate("Enter Your Full Name"),
                            hintStyle: TextStyle(color: Colors.black),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(25.7),
                            ),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(25.7),
                            ),
                            focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.red,
                                )),
                            errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.red,
                                )),
                            prefixIcon: Container(
                              width: 100,
                              child: Row(
                                children: [
                                  IconButton(
                                    onPressed: () {},
                                    icon: Icon(
                                      Icons.person_outline,
                                      size: 25,
                                      color: Colors.black,
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 10),
                                    width: 1,
                                    height: 30,
                                    color: Colors.black,
                                  )
                                ],
                              ),
                            ),
                          ),
                          keyboardType: TextInputType.name,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 30, top: 25, right: 30),
                        child: TextFormField(
                          controller: msisdnController,
                          focusNode: msisdnFocus,
                          maxLength: 20,
                          onFieldSubmitted: (term) {
                            CommonMethods.inputFocusChange(
                                context, msisdnFocus, passwordFocus);
                          },
                          validator: (text) {
                            return text.isEmpty
                                ?AppLocalizations.of(context).translate( 'Phone number cannot be empty')
                                : text.length < 8
                                    ?
                            AppLocalizations.of(context).translate('Phone number should be min 8 digit')
                                    // :text.length>10
                                    // ? 'Phone number should be max 10 digit'
                                    : null;
                          },
                          decoration: InputDecoration(
                            fillColor: Colors.blueGrey[50],
                            counterText: "",
                            filled: true,
                            hintText: AppLocalizations.of(context).translate("Enter Phone Number",),
                            hintStyle: TextStyle(color: Colors.black),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(25.7),
                            ),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(25.7),
                            ),
                            focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.red,
                                )),
                            errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.red,
                                )),
                            prefixIcon: Container(
                              width: 100,
                              child: Row(
                                children: [
                                  GestureDetector(
                                    onTap: () {
                                      _countryCodeButtonTapped();
                                    },
                                    child: Container(
                                      margin: EdgeInsets.only(left: 10,right: 10),
                                      child: Label(
                                        title: countryCode,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.black,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 10),
                                    width: 1,
                                    height: 30,
                                    color: Colors.black,
                                  )
                                ],
                              ),
                            ),
                          ),
                          keyboardType: TextInputType.phone,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 30, top: 25, right: 30),
                        child: TextFormField(
                          controller: passwordController,
                          focusNode: passwordFocus,
                          obscureText: obscureText,
                          validator: (text) {
                            passwordText = text;
                            if (text.isEmpty) {
                              return AppLocalizations.of(context).translate('Password cannot be empty');
                            }
                            else if (text.length < 6) {
                              return
                                AppLocalizations.of(context).translate('Password must be 6 Character')
                              ;
                            }
                            else if (!validateStructure(text)) {
                              return null;
                            }
                            return null;
                          },
                          onFieldSubmitted: (term) {
                            CommonMethods.inputFocusChange(
                                context, passwordFocus, confirmPasswordFocus);
                          },
                          decoration: InputDecoration(
                            fillColor: Colors.blueGrey[50],
                            filled: true,
                            counterText: "",
                            hintText:
                            AppLocalizations.of(context).translate("Enter Password"),
                            hintStyle: TextStyle(color: Colors.black),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(25.7),
                            ),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(25.7),
                            ),
                            focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.red,
                                )),
                            errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.red,
                                )),
                            prefixIcon: Container(
                              width: 100,
                              child: Row(
                                children: [
                                  IconButton(
                                    onPressed: () {},
                                    icon: Icon(
                                      Icons.lock_outline,
                                      size: 25,
                                      color: Colors.black,
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 10),
                                    width: 1,
                                    height: 30,
                                    color: Colors.black,
                                  )
                                ],
                              ),
                            ),
                            suffixIcon: IconButton(
                              onPressed: () {
                                obscureText = !obscureText;
                                setState(() {});
                              },
                              icon: Icon(
                                  obscureText
                                      ? Icons.visibility_off_outlined
                                      : Icons.remove_red_eye_outlined,
                                  size: 25,
                                  color: Colors.black),
                            ),
                          ),
                          maxLength: 20,
                          keyboardType: TextInputType.visiblePassword,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 35, right: 30),
                        child: Text(
                          AppLocalizations.of(context).translate("Your password must have at least 6 letters, include numbers and symbols(like ! and %)"),
                          style: TextStyle(
                            fontSize: 13,
                            color: Colors.red,
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 30, top: 25, right: 30),
                        child: TextFormField(
                          controller: confirmPasswordController,
                          focusNode: confirmPasswordFocus,
                          obscureText: confirmBoolObscure,
                          validator: (text) {
                            if (text.isEmpty) {
                              return  AppLocalizations.of(context).translate('Confirm Password cannot be empty');
                            } else if (text != passwordText) {
                              return
                                AppLocalizations.of(context).translate('Password and Confirm Password don\'t match') ;
                            }
                            return null;
                          },
                          onFieldSubmitted: (term) {
                            CommonMethods.inputFocusChange(
                                context, confirmPasswordFocus, otpFocus);
                          },
                          decoration: InputDecoration(
                            fillColor: Colors.blueGrey[50],
                            counterText: "",
                            filled: true,
                            hintText: AppLocalizations.of(context).translate("Enter Confirm Password"),
                            hintStyle:
                                TextStyle(color: Colors.black, fontSize: 16),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(25.7),
                            ),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(25.7),
                            ),
                            focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.red,
                                )),
                            errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.red,
                                )),
                            prefixIcon: Container(
                              width: 100,
                              child: Row(
                                children: [
                                  IconButton(
                                    onPressed: () {},
                                    icon: Icon(
                                      Icons.lock_outline,
                                      size: 25,
                                      color: Colors.black,
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 10),
                                    width: 1,
                                    height: 30,
                                    color: Colors.black,
                                  )
                                ],
                              ),
                            ),
                            suffixIcon: IconButton(
                              onPressed: () {
                                confirmBoolObscure = !confirmBoolObscure;
                                setState(() {});
                              },
                              icon: Icon(
                                  confirmBoolObscure
                                      ? Icons.visibility_off_outlined
                                      : Icons.remove_red_eye_outlined,
                                  size: 25,
                                  color: Colors.black),
                            ),
                          ),
                          maxLength: 20,
                          keyboardType: TextInputType.visiblePassword,
                        ),
                      ),
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.only(left: 20, right: 10),
                        margin: EdgeInsets.only(left: 30, right: 30, top: 30),
                        decoration: new BoxDecoration(
                          shape: BoxShape.rectangle,
                          color: Colors.blueGrey[50],
                          borderRadius: BorderRadius.circular(25),
                        ),
                        child: DropdownButton<String>(
                            underline: Container(),
                            isExpanded: true,
                            hint: Row(
                              children: <Widget>[
                                Label(
                                  title: _genderType ?? "",
                                  color: Colors.black,
                                  fontSize: 16,
                                  textAlign: TextAlign.center,
                                ),
                                Icon(
                                  Icons.keyboard_arrow_down_sharp,
                                  color: Colors.white,
                                  size: 20,
                                ),
                              ],
                            ),
                            items: genderOptions.map((String val) {
                              return new DropdownMenuItem<String>(
                                value: val,
                                child: new Text(
                                  val,
                                  style: TextStyle(
                                      color: Colors.black54, fontSize: 16),
                                ),
                              );
                            }).toList(),
                            onChanged: (newVal) {
                              _genderType = newVal;
                              // inviteTypeBool = true;
                              setState(() {});
                            }),
                      ),
                      Container(
                        child:
                            BlocListener<RegistrationBloc, RegistrationState>(
                          listener: (context, state) {
                            if (state is RegistrationError) {
                              CommonMethods.showToast(fToast:fToast,message:state.failure.message,status:false);

                            } else if (state is RegistrationSuccess) {
                              final _phoneNumber =
                                  msisdnController.text.toString().trim();
                              Navigator.of(context).pushReplacement(
                                MaterialPageRoute(
                                  builder: (context) => OtpScreen(
                                    phoneNumber: _phoneNumber,
                                  ),
                                ),
                              );
                            }
                          },
                          child:
                              BlocBuilder<RegistrationBloc, RegistrationState>(
                                  builder: (_, state) {
                            if (state is Registering) {
                              return Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [CircularProgressIndicator()],
                              );
                            } else {
                              return GestureDetector(
                                onTap: () {
                                  if (_formKey.currentState.validate()) {
                                    final _userName =
                                        nameController.text.toString().trim();
                                    final _phoneNumber = countryCode +
                                        msisdnController.text.toString().trim();
                                    final _password = passwordController.text
                                        .toString()
                                        .trim();
                                    final _gender =
                                        _genderType == "Male" ? "0" : "1";
                                    BlocProvider.of<RegistrationBloc>(context)
                                        .add(
                                      UserRegistrationEvent(
                                        phoneNumber: _phoneNumber,
                                        password: _password,
                                        userName: _userName,
                                        gender: _gender,
                                      ),
                                    );
                                  }
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(25),
                                    color: Color(appColors.buttonColor),
                                  ),
                                  alignment: Alignment.center,
                                  height: 50,
                                  margin: EdgeInsets.only(
                                      left: 30, top: 35, right: 30, bottom: 20),
                                  child: Label(
                                    title:AppLocalizations.of(context).translate("Register"),
                                    fontSize: 18,
                                    fontWeight: FontWeight.w700,
                                    color: Colors.white,
                                  ),
                                ),
                              );
                            }
                          }),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future _countryCodeButtonTapped() async {
    Map results = await Navigator.of(context)
        .push(new MaterialPageRoute(builder: (BuildContext context) {
      return new CountryListing();
    }));
    if (results != null && results.containsKey('country_id')) {
      setState(() {
        countryCode = "+" + results['country_code'].toString();
      });
    }
  }

  bool validateStructure(String value){
    String  pattern = r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
    RegExp regExp = new RegExp(pattern);
    return regExp.hasMatch(value);
  }

}
