class WeddingTemplateEntity {
  bool status;
  List<TemplateModelEntity> data;

  WeddingTemplateEntity({this.status, this.data});

  WeddingTemplateEntity copy({
    int id,
    String title,
    String description,
    List<TemplateModelEntity> data,
  }) {
    return WeddingTemplateEntity(
        status: status ?? this.status, data: data ?? this.data);
  }
}

class TemplateModelEntity {
  int id;
  String title;
  String template;
  String created_at;

  TemplateModelEntity({this.id, this.created_at, this.template, this.title});

  TemplateModelEntity copy(
      {int id, String question, String template, int answerId}) {
    return TemplateModelEntity(
        id: id ?? this.id,
        template: template ?? this.template,
        title: title ?? this.title,
        created_at: created_at ?? this.created_at);
  }
}
