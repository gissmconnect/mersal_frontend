import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mersal/core/widgets/common/Button.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/core/widgets/message.dart';
import 'package:mersal/core/widgets/widgets.dart';
import 'package:mersal/features/auth/auth_presentation/doc_upload_presentation/bloc/doc_upload_bloc.dart';
import 'package:mersal/features/dashboard/dashboard_page.dart';
import 'package:mersal/features/knowMore/ui/know_more.dart';
import 'package:mersal/features/profile/profile_presentation/ui/upload_id_screen.dart';
import 'package:mersal/injection_container.dart';
import 'package:mersal/resources/colors.dart';
import 'package:mersal/resources/fonts.dart';

class DocUploadPage extends StatelessWidget {
  final String from;

  DocUploadPage(this.from);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<DocUploadBloc>(
      create: (_) => serviceLocator<DocUploadBloc>(),
      child: Scaffold(
        body: Container(
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/bg_image.jpg"),
                fit: BoxFit.fill,
              ),
            ),
            child: _DocUploadPageBody(from)),
      ),
    );
  }
}

class _DocUploadPageBody extends StatefulWidget {
  final String from;

  _DocUploadPageBody(this.from);

  @override
  _DocUploadPageBodyState createState() => _DocUploadPageBodyState();
}

class _DocUploadPageBodyState extends State<_DocUploadPageBody> {

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: ListView(
        shrinkWrap: true,
        children: [
          Container(
            height: 400,
            padding: const EdgeInsets.all(8.0),
            margin: EdgeInsets.only(top: 60, left: 20, right: 20),
            decoration: new BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white,
              borderRadius: BorderRadius.circular(25.0),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                // Container(
                //   child: Label(
                //     title: "Mabrook",
                //     fontSize: 20,
                //     fontWeight: FontWeight.w600,
                //     color: Colors.black,
                //   ),
                // ),
                Container(
                  child: Icon(Icons.check_circle_rounded,color: Colors.lightBlue,size: 50,),
                ),
                widget.from == "vote"
                    ? successLayout("You have successfully Voted")
                    : widget.from == "service"
                        ? successLayout(
                            "Your Lawati SMS Subscription was successful")
                        : widget.from == "wedding"
                            ? successLayout("Your wedding booking is confirmed")
                            : widget.from == "child"
                                ? successLayout(
                                    "Successfully registered activity for your child")
                                : widget.from == "donation"
                                    ? successLayout(
                                        "You donation was successful")
                                    : widget.from == "survey"
                                        ? successLayout(
                                            "You survey is completed")
                                        : widget.from == "uploadID"
                                            ? successLayout(
                                                "Your documents uploaded successfully and are being verified")
                                            : widget.from == "register"
                                                ? registerSuccessLayout()
                                                : successLayout(
                                                    "Your Wedding invitation is sent successfully")
              ],
            ),
          ),
          widget.from == "register"
              ? GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                UploadIdScreen(isNewAccount: true)));
                  },
                  child: Container(
                    margin: EdgeInsets.only(
                        left: 70, top: 45, right: 70, bottom: 10),
                    height: 60,
                    decoration: BoxDecoration(
                        color: Color(appColors.buttonColor),
                        borderRadius: BorderRadius.circular(30)),
                    child: Stack(
                      children: <Widget>[
                        Align(
                            alignment: Alignment.centerRight,
                            child: Container(
                              margin: EdgeInsets.only(right: 15),
                              child: SvgPicture.asset(
                                "assets/images/upload_id.svg",
                                fit: BoxFit.cover,
                                width: 30,
                                height: 30,
                                color: Colors.white,
                              ),
                            )),
                        Align(
                            alignment: Alignment.center,
                            child: Label(
                              title: "Upload my ID",
                              color: Colors.white,
                              fontWeight: FontWeight.w700,
                              fontSize: 17,
                              textAlign: TextAlign.center,
                            ))
                      ],
                    ),
                  ),
                )
              : Container(),
          BlocListener<DocUploadBloc, DocUploadState>(
            listener: (context, state) {
              print('state $state');
              if (state is SoftLoginFailed) {
                showSnackBarMessage( context: context, message: state.failure.message);
              } else if (state is SoftLoginSuccess) {
                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(
                      builder: (context) => DashboardPage(),
                    ),
                    (route) => false);
              }
            },
            child: Container(
              height: 60,
              width: 220,
              margin: EdgeInsets.only(left: 70, top: 35, right: 70, bottom: 30),
              child: BlocBuilder<DocUploadBloc, DocUploadState>(
                builder: (context, state) {
                  // if (state is SoftLoginInitiated) {
                  //   return circularProgressIndicator();
                  // }
                  return Button(
                    title: widget.from == "register"
                        ? "Skip For Now":"Go to Homepage",
                    textColor: Colors.white,
                    borderColor: Colors.white,
                    color: Colors.transparent,
                    onTap: () {
                      Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(
                      builder: (context) => DashboardPage(),
                    ),
                    (route) => false);
                      // BlocProvider.of<DocUploadBloc>(context)
                      //     .add(SoftLoginEvent());
                    },
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget registerSuccessLayout() {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(top: 30),
          child: Label(
            textAlign: TextAlign.center,
            title:
                "You have successfully registered as a light user to Mersal App",
            fontSize: 18,
            color: Colors.black,
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 30),
          child: Label(
            textAlign: TextAlign.center,
            fontFamily: "segoeuiz",
            title:
                "We just need your ID to get the full access",
            fontSize: 16,
            color: Colors.black,
          ),
        ),
        GestureDetector(
          onTap: (){
            CommonMethods().animatedNavigation(
                context: context,
                currentScreen:DocUploadPage("register"),
                landingScreen: KnowMoreScreen());
          },
          child: Container(
            padding: EdgeInsets.only(bottom: 40),
            margin: EdgeInsets.only(top: 40),
            child: Label(
                title: "Know More",
                fontSize: 14,
                fontStyle: FontStyle.italic,
                showUnderLine: true,
                color: Colors.blue),
          ),
        ),
      ],
    );
  }

  Widget successLayout(String message) {
    return Container(
      margin: EdgeInsets.only(top: 30),
      child: Label(
        textAlign: TextAlign.center,
        title: message,
        fontSize: 18,
        color: Colors.black,
      ),
    );
  }
}
