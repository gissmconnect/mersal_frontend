import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/CustomAppBar.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/core/widgets/message.dart';
import 'package:mersal/features/committee_management/committee_member_detail.dart';
import 'package:mersal/features/committee_management/data/model/committee_member_model.dart';
import 'package:mersal/features/committee_management/presentation/member/bloc/committer_member_bloc.dart';
import 'package:mersal/features/info_presentation/ui/info_screen.dart';
import 'package:mersal/injection_container.dart';
import 'package:mersal/resources/colors.dart';

class CommitteeMember extends StatelessWidget {
  final int id;
  final String committeeName,description;

  const CommitteeMember(
      {Key key, @required this.id, @required this.committeeName, @required this.description})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CommitterMemberBloc>(
      create: (_) => serviceLocator<CommitterMemberBloc>(),
      child: Scaffold(
        body: _CommitteeMemberBody(id: id, committeeName: committeeName, description: description),
      ),
    );
  }
}

class _CommitteeMemberBody extends StatefulWidget {
  final int id;
  final String committeeName,description;

  const _CommitteeMemberBody(
      {Key key, @required this.id, @required this.committeeName, @required this.description})
      : super(key: key);

  @override
  _CommitteeMemberBodyState createState() => _CommitteeMemberBodyState();
}

class _CommitteeMemberBodyState extends State<_CommitteeMemberBody> {

  @override
  void initState() {
    BlocProvider.of<CommitterMemberBloc>(context)
        .add(GetCommitteeMemberEvent(committeeId: widget.id));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<CommitterMemberBloc, CommitterMemberState>(
      listenWhen: (oldState, newsState) {
        return oldState != newsState;
      },
      listener: (context, state) {
        if (state is CommitteeMemberLoadFailureState) {
          showSnackBarMessage(context: context, message: state.failure.message);
        }
      },
      child: BlocBuilder<CommitterMemberBloc, CommitterMemberState>(
        builder: (context, state) {
          if (state is CommitteeMemberLoadedState) {
            return Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/bg_image.jpg"),
                  fit: BoxFit.fill,
                ),
              ),
              child: Column(
                children: [
                  SizedBox(
                    height: 60,
                  ),
                  Stack(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(bottom: 5, left: 15),
                        alignment: Alignment.bottomLeft,
                        child: GestureDetector(
                          child: Icon(
                            Icons.arrow_back_ios_rounded,
                            color: Colors.white,
                          ),
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(bottom: 6),
                        alignment: Alignment.bottomCenter,
                        child: Text(
                         widget.committeeName,
                          textAlign: TextAlign.end,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(right: 15),
                        alignment: Alignment.centerRight,
                        child: GestureDetector(
                          child: Container(
                            margin: EdgeInsets.only(left: 20),
                            child: SvgPicture.asset(
                              "assets/images/info_icon.svg",
                              width: 30,
                              height: 30,
                            ),
                          ),
                          onTap: () {
                            CommonMethods().animatedNavigation(
                                context: context,
                                currentScreen: CommitteeMember(id:null,committeeName: "",),
                                landingScreen: InfoScreen(description:widget.description));
                          },
                        ),
                      ),
                    ],
                  ),
                  Expanded(
                    child: ListView(
                      shrinkWrap: true,
                      children: [
                        GridView.builder(
                          padding: EdgeInsets.only(top: 7),
                          shrinkWrap: true,
                          physics: ScrollPhysics(),
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 3,
                          ),
                          itemCount: state.member.length,
                          itemBuilder: (BuildContext context, int index) {
                            return menuGridItem(state.member[index]);
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            );
          }else if(state is CommitteeMemberLoadingState){
            return Center(child: CircularProgressIndicator(),);
          }
          return Container();
        },
      ),
    );
  }

  Widget menuGridItem(CommitteeMemberModel committeeMember) {
    return GestureDetector(
      onTap: () {
        committeeDetailBottomSheet(committeeMember);
      },
      child: Container(
        margin: EdgeInsets.all(10),
        decoration: new BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
          border: Border.all(color: Colors.black, width: 2),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.only(top: 10),
              decoration: new BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(color: Colors.black, width: 2),
              ),
              child: Center(
                child: CircleAvatar(
                  backgroundImage: CachedNetworkImageProvider(committeeMember.member.image),
                  radius: 20,
                ),
              ),
            ),
            Container(
                margin: EdgeInsets.only(bottom: 10, top: 10),
                child: Label(
                  title: committeeMember.member.name??"User",
                  textAlign: TextAlign.center,
                  fontSize: 14,
                  color: Color(appColors.blue00008B),
                ))
          ],
        ),
      ),
    );
  }

  Future<Widget> committeeDetailBottomSheet(
      CommitteeMemberModel committeeMember) {
    return showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      backgroundColor: Colors.transparent,
      builder: (context) {
        return Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.vertical(top: Radius.circular(30.0)),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.black,
                  offset: Offset(0.0, 1.0), //(x,y)
                  blurRadius: 5.0,
                ),
              ],
            ),
            child: CommitteeMemberDetails(committeeMember: committeeMember,));
      },
    );
  }
}
