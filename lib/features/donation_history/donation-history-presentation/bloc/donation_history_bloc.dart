import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/donation_history/donation-history-data/model/donation_history_model.dart';
import 'package:mersal/features/donation_history/donation-history-domain/usecase/donation_history_usecase.dart';
import 'package:meta/meta.dart';

part 'donation_history_event.dart';

part 'donation_history_state.dart';

class DonationHistoryBloc
    extends Bloc<DonationHistoryEvent, DonationHistoryState> {
  final DonationHistoryUseCase donationHistoryUseCase;

  DonationHistoryBloc({@required this.donationHistoryUseCase})
      : super(DonationHistoryInitial());

  @override
  Stream<DonationHistoryState> mapEventToState(
    DonationHistoryEvent event,
  ) async* {
    if (event is GetHistoryEvent) {
      yield DonationHistoryInitialising();
      final result = await donationHistoryUseCase(NoParams());
      yield* result.fold(
        (l) async* {
          yield DonationHistoryError(l);
        },
        (data) async* {
          yield DonationHistoryLoaded(activity: data);
        },
      );
    }
  }
}
