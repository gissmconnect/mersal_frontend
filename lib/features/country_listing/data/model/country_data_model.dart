import 'package:mersal/features/country_listing/data/model/country_data_list.dart';

class CountryDataModel {
  List<CountryDataList> countryList;
  String message;
  bool status;

  CountryDataModel({this.countryList, this.status});

  factory CountryDataModel.fromJson(Map<String, dynamic> json) {
    return CountryDataModel(
      countryList: json['data'] != null
          ? (json['data'] as List)
              .map((i) => CountryDataList.fromJson(i))
              .toList()
          : null,
      status: json['status'],
    );
  }
}
