import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/features/wedding_invite_presentation/domain/entity/wedding_template_entity.dart';

abstract class WeddingTemplateRepo {
  Future<Either<Failure, WeddingTemplateEntity>> getWeddingTemplate();
}
