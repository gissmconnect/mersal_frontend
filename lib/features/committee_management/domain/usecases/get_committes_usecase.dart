import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/committee_management/domain/entity/committe_entity.dart';
import 'package:mersal/features/committee_management/domain/repository/committe_repo.dart';
import 'package:meta/meta.dart';

class GetCommitteesUseCase extends UseCase<List<CommitteeEntity>, NoParams> {
  final CommitteeRepo committeeRepo;

  GetCommitteesUseCase({@required this.committeeRepo});

  @override
  Future<Either<Failure, List<CommitteeEntity>>> call(NoParams params) {
    return committeeRepo.getCommittees();
  }
}
