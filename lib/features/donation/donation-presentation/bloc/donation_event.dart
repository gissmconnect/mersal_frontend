part of 'donation_bloc.dart';

abstract class DonationEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class DonateEvent extends DonationEvent {
  final int reasonId;
  final String donationAmount,donationDescription;

  DonateEvent({ @required this.reasonId,@required this.donationAmount, @required this.donationDescription});
}

class GetDonationTypeEvent extends DonationEvent{}

class SelectDonationType extends DonationEvent{
  final DonationTypeEntity donationType;

  SelectDonationType({this.donationType});

  @override
  List<Object> get props => [donationType];
}