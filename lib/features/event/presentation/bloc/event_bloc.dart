import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/reauthenticate_usecase.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/core/util/date_formatter.dart';
import 'package:mersal/features/event/domain/enity/event_entity.dart';
import 'package:mersal/features/event/domain/usecase/get_event_usecase.dart';
import 'package:meta/meta.dart';

part 'event_event.dart';

part 'event_state.dart';

class EventBloc extends Bloc<EventEvent, EventState> {
  final GetEventUseCase getEventUseCase;
  final ReAuthenticateUseCase reAuthenticateUseCase;

  EventBloc(
      {@required this.getEventUseCase, @required this.reAuthenticateUseCase})
      : super(EventInitial());

  @override
  Stream<EventState> mapEventToState(
    EventEvent event,
  ) async* {
    if (event is GetEventEvent) {
      yield EventLoadingState();
      final String formattedDate = formatYYYYMMDD(date: event.date);
      final _response = await getEventUseCase(formattedDate);
      yield* _response.fold((l) async* {
        if (l is AuthFailure) {
          final _reauthenticateResponse =
              await reAuthenticateUseCase(NoParams());
          _reauthenticateResponse.fold((l) async* {
            yield EventFailureState(failure: l);
          }, (r) => GetEventEvent(date: event.date));
        } else {
          yield EventFailureState(failure: l);
        }
      }, (r) async* {
        yield EventLoadedState(event: r);
      });
    }
  }
}
