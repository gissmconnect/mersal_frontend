
import 'package:mersal/features/child_service/child-domain/model/child-registration/ChildDataList.dart';

class ChildRelationDataModel {
  List<ChildDataList> childDataList;
  bool status;

  ChildRelationDataModel({this.childDataList, this.status});

  factory ChildRelationDataModel.fromJson(Map<String, dynamic> json) {
    return ChildRelationDataModel(
      childDataList: json['data'] != null
          ? (json['data'] as List)
              .map((i) => ChildDataList.fromJson(i))
              .toList()
          : null,
      status: json['status'],
    );
  }
}
