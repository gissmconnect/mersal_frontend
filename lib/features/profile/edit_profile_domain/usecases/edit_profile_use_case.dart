import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/auth/auth_data/model/user_data_model.dart';
import 'package:mersal/features/profile/edit_profile_domain/repository/edit_profile_repository.dart';
import 'package:meta/meta.dart';

class EditProfileUseCase extends UseCase<UserDataModel, Params> {
  final EditProfileRepository editProfileRepository;

  EditProfileUseCase({@required this.editProfileRepository});

  @override
  Future<Either<Failure, UserDataModel>> call(Params params) async {
    return await editProfileRepository.updateProfile(
        name: params.name,
        dob: params.dob,
        gender: params.gender,
        image: params.image,
        bloodGroup: params.bloodGroup);
  }
}

class Params {
  final String name, dob, bloodGroup, image;
  final int gender;

  Params({this.name, this.dob, this.bloodGroup, this.image, this.gender});
}
