import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mersal/core/util/helper_functions.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/core/widgets/message.dart';
import 'package:mersal/features/event/domain/enity/event_entity.dart';
import 'package:mersal/features/event/presentation/bloc/event_bloc.dart';
import 'package:mersal/injection_container.dart';
import 'package:mersal/resources/colors.dart';
import 'package:meta/meta.dart';

class EventPage extends StatelessWidget {
  final DateTime date;

  const EventPage({Key key, @required this.date}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<EventBloc>(
      create: (_) => serviceLocator<EventBloc>(),
      child: _EventPageBody(
        date: date,
      ),
    );
  }
}

class _EventPageBody extends StatefulWidget {
  final DateTime date;

  const _EventPageBody({Key key, @required this.date});

  @override
  __EventPageBodyState createState() => __EventPageBodyState();
}

class __EventPageBodyState extends State<_EventPageBody> {

  FToast fToast;


  @override
  void initState() {
    BlocProvider.of<EventBloc>(context).add(GetEventEvent(date: widget.date));
    super.initState();
    fToast = FToast();
    fToast.init(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<EventBloc, EventState>(
      listenWhen: (oldState, newState) {
        return oldState != newState;
      },
      listener: (context, state) {
        if (state is EventFailureState) {
          CommonMethods.showToast(fToast:fToast,message: state.failure.message,status:false);
        }
      },
      child: BlocBuilder<EventBloc, EventState>(
        buildWhen: (oldState, newState) {
          return oldState != newState;
        },
        builder: (context, state) {
          if (state is EventLoadingState) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          if (state is EventLoadedState) {
            final EventEntity event = state.event;
            if(event.eventDataList!=null && event.eventDataList.length>0){
              return Container(
                height: MediaQuery.of(context).size.height,
                margin: EdgeInsets.only(left: 20, right: 20),
                padding: EdgeInsets.only(bottom: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                        decoration: new BoxDecoration(
                            color: Color(appColors.blue).withOpacity(0.6),
                            borderRadius: BorderRadius.circular(25)),
                        child: Column(
                          children: [
                           /* Container(
                              padding: EdgeInsets.only(top: 10, bottom: 20),
                              margin: EdgeInsets.all(10),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(left: 20),
                                    child: Label(
                                      title: "Share",
                                      color: Colors.white,
                                      fontWeight: FontWeight.w600,
                                      fontSize: 18,
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(right: 20),
                                    child: Label(
                                      title: "Add To Calendar",
                                      color: Colors.white,
                                      fontWeight: FontWeight.w600,
                                      fontSize: 18,
                                    ),
                                  ),
                                ],
                              ),
                            ),*/
                            Container(
                              height: 50,
                              margin: EdgeInsets.only(left: 10, right: 10),
                              alignment: Alignment.center,
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                  color: Color(appColors.blue),
                                  borderRadius: BorderRadius.circular(25)),
                              child: Label(
                                title: event.eventDataList[0].title,
                                color: Colors.white,
                                fontSize: 18,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                margin: EdgeInsets.only(top: 10),
                                height: MediaQuery.of(context).size.height / 20,
                                width: MediaQuery.of(context).size.width / 2,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    color: Color(appColors.blue)),
                                child: Center(
                                    child: Text(
                                      event.eventDataList[0].description,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16),
                                    )),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top: 20, bottom: 20, left: 10, right: 10),
                              margin: EdgeInsets.only(bottom: 20, right: 10),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(left: 20),
                                    child: Column(
                                      children: [
                                        Label(
                                          title: "Starts at",
                                          color: Colors.white,
                                          fontSize: 16,
                                          fontWeight: FontWeight.w600,
                                        ),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Container(
                                          padding: EdgeInsets.all(10),
                                          decoration: BoxDecoration(
                                              borderRadius:
                                              BorderRadius.circular(30),
                                              color: Colors.white),
                                          child: Label(
                                            // title: formatToTime(dateTime:event.eventDataList[0].startDate),
                                            title: event
                                                .eventDataList[0].startDate ??
                                                "N/A",
                                            color: Colors.black,
                                            fontSize: 18,
                                            fontWeight: FontWeight.w600,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    width: 15,
                                  ),
                                  Column(
                                    children: [
                                      Label(
                                        title: "Ends at",
                                        color: Colors.white,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w600,
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Container(
                                        padding: EdgeInsets.all(10),
                                        decoration: BoxDecoration(
                                            borderRadius:
                                            BorderRadius.circular(30),
                                            color: Colors.white),
                                        child: Label(
                                          // title: formatToTime(dateTime:event.eventDataList[0].endDate),
                                          title: event.eventDataList[0].endDate ??
                                              "N/A",
                                          color: Colors.black,
                                          fontSize: 18,
                                          fontWeight: FontWeight.w600,
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                launchURL(url: event.eventDataList[0].link);
                              },
                              child: Container(
                                padding: EdgeInsets.only(bottom: 40),
                                child: Text(
                                  event.eventDataList[0].link,
                                  style: TextStyle(
                                    fontSize: 18,
                                    fontStyle: FontStyle.italic,
                                    fontWeight: FontWeight.w700,
                                    color: Color(appColors.buttonColor),
                                    decoration:TextDecoration.underline,

                                  ),
                                ),
                              ),
                            ),
                          ],
                        )),

                  ],
                ),
              );
            }
          }
          return Container();
        },
      ),
    );
  }
}
