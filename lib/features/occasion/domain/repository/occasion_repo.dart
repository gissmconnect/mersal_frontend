import 'package:mersal/core/error/failures.dart';
import 'package:meta/meta.dart';
import 'package:dartz/dartz.dart';
import 'package:mersal/features/occasion/domain/entity/occasion_entity.dart';

abstract class OccasionRepo{
  Future<Either<Failure,OccasionEntity>>getOccasion({@required String date});
}