import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:mersal/core/localization/app_localization.dart';
import 'package:mersal/core/network/api_endpoints.dart';
import 'package:mersal/core/widgets/common/Button.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/InputField.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/core/widgets/common/image_chooser.dart';
import 'package:mersal/core/widgets/common/shared_preference.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/child_service/child-domain/model/child-registration/ChildDataList.dart';
import 'package:mersal/features/child_service/child-domain/model/child-registration/ChildRelationDataModel.dart';
import 'package:mersal/features/child_service/child-presentation/child-registration/ui/registered_child_list.dart';
import 'package:mersal/features/info_presentation/ui/info_screen.dart';
import 'package:mersal/resources/colors.dart';

class ChildRegistration extends StatefulWidget {
  @override
  _ChildRegistrationState createState() => _ChildRegistrationState();
}

class _ChildRegistrationState extends State<ChildRegistration> {
  File _image;

  final nameController = TextEditingController();
  final ageController = TextEditingController();
  final fatherNameController = TextEditingController();
  final gFatherNameController = TextEditingController();
  final familyNameController = TextEditingController();
  final residenceController = TextEditingController();
  final emailController = TextEditingController();
  final nationalIdController = TextEditingController();
  final occupationController = TextEditingController();

  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  BuildContext mContextLoader;

  final FocusNode nameFocus = new FocusNode();
  final FocusNode ageFocus = new FocusNode();
  final FocusNode fatherNameFocus = new FocusNode();
  final FocusNode gFatherNameFocus = new FocusNode();
  final FocusNode familyNameFocus = new FocusNode();
  final FocusNode residenceFocus = new FocusNode();
  final FocusNode emailFocus = new FocusNode();
  final FocusNode nationalIdFocus = new FocusNode();
  final FocusNode occupationFocus = new FocusNode();

  final _formKey = GlobalKey<FormState>();

  String _selectedArea = 'Choose Area',_selectedRelation = 'Choose Relation',
      childDOB = "Select DOB",
      childDob = "",
      childRelationType = "";

  ValueNotifier<DateTime> _dateTimeNotifier =
      ValueNotifier<DateTime>(DateTime.now());
  bool dateBool = false, relationBool = false;

  List<ChildDataList> childDataList;
  ChildRelationDataModel childRelationDataModel;

  List<ChildDataList> residenceDataList;
  ChildRelationDataModel residenceDataModel;

  FToast fToast;

  List<String> dummyOptions = ['Dofhar', 'Ruwi', 'Mabela', 'Qurum'];

  String base64Image="";

  @override
  void initState() {
    super.initState();
    apiRelationCall();
    fToast = FToast();
    fToast.init(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/bg_image.jpg"),
              fit: BoxFit.fill,
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: 60,
              ),
              Stack(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(bottom: 5, left: 15),
                    alignment: Alignment.bottomLeft,
                    child: GestureDetector(
                      child: Icon(
                        Icons.arrow_back_ios_rounded,
                        color: Colors.white,
                      ),
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 6),
                    alignment: Alignment.bottomCenter,
                    child: Text(
                      AppLocalizations.of(context)
                          .translate("Child Registration") ,
                      textAlign: TextAlign.end,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 50),
                    alignment: Alignment.centerRight,
                    child: GestureDetector(
                      child: Icon(
                        Icons.view_list,
                        color: Colors.white,
                        size: 35,
                      ),
                      onTap: () {
                        CommonMethods().animatedNavigation(
                            context: context,
                            currentScreen: ChildRegistration(),
                            landingScreen: RegisteredChildListScreen());
                      },
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 15),
                    alignment: Alignment.centerRight,
                    child: GestureDetector(
                      child: Container(
                        margin: EdgeInsets.only(left: 20),
                        child: SvgPicture.asset(
                          "assets/images/info_icon.svg",
                          width: 30,
                          height: 30,
                        ),
                      ),
                      onTap: () {
                        CommonMethods().animatedNavigation(
                            context: context,
                            currentScreen: ChildRegistration(),
                            landingScreen: InfoScreen());
                      },
                    ),
                  ),
                ],
              ),
              Expanded(
                child: ListView(
                  shrinkWrap: true,
                  children: [
                    Form(
                      key: _formKey,
                      child: Column(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 10),
                            child: Stack(
                              children: <Widget>[
                                Align(
                                  alignment: Alignment.topCenter,
                                  child: Container(
                                    height: 110,
                                    width: 110,
                                    padding: const EdgeInsets.all(10.0),
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      shape: BoxShape.circle,
                                    ),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(55),
                                      child: Container(
                                          child: _image != null
                                              ? Image.file(
                                                  _image,
                                                  height: 100,
                                                  width: 100,
                                                  fit: BoxFit.fill,
                                                )
                                              : Icon(Icons.person_outline,
                                                  size: 70,
                                                  color: Color(
                                                      appColors.buttonColor))),
                                    ),
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.topCenter,
                                  child: Container(
                                    margin: const EdgeInsets.only(
                                        top: 25, left: 100),
                                    child: GestureDetector(
                                      onTap: () {
                                        imageChooser
                                            .showImageChooser(context)
                                            .then((picture) {
                                          if (picture != null) {
                                            this._image = picture;
                                            print(picture);
                                            List<int> imageBytes = this._image.readAsBytesSync();
                                            print(imageBytes);
                                            base64Image = base64Encode(imageBytes);
                                            print("Image========="+base64Image);
                                            setState(() {});
                                          }
                                        });
                                      },
                                      child: Container(
                                        width: 30,
                                        height: 30,
                                        decoration: new BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.black,
                                        ),
                                        child: Center(
                                          child: Icon(Icons.camera_alt_outlined,
                                              size: 15, color: Colors.white),
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Container(
                            margin:
                                EdgeInsets.only(left: 30, top: 25, right: 30),
                            child: InputField(
                              controller: nameController,
                              focusNode: nameFocus,
                              onSubmitted: (term) {
                                CommonMethods.inputFocusChange(
                                    context, nameFocus, fatherNameFocus);
                              },
                              validator: (text) {
                                return text.isEmpty
                                    ? AppLocalizations.of(context)
                                    .translate('Name cannot be empty')
                                    : null;
                              },
                              hint: AppLocalizations.of(context)
                                  .translate("Enter Child First Name"),
                              inputType: TextInputType.name,
                            ),
                          ),
                          Container(
                            margin:
                                EdgeInsets.only(left: 30, top: 25, right: 30),
                            child: InputField(
                              controller: fatherNameController,
                              focusNode: fatherNameFocus,
                              onSubmitted: (term) {
                                CommonMethods.inputFocusChange(
                                    context, fatherNameFocus, gFatherNameFocus);
                              },
                              validator: (text) {
                                return text.isEmpty
                                    ? AppLocalizations.of(context)
                                    .translate('Father Name cannot be empty')
                                    : null;
                              },
                              hint: AppLocalizations.of(context)
                                  .translate("Enter Father Name"),
                              inputType: TextInputType.name,
                            ),
                          ),
                          Container(
                            margin:
                                EdgeInsets.only(left: 30, top: 25, right: 30),
                            child: InputField(
                              controller: gFatherNameController,
                              focusNode: gFatherNameFocus,
                              onSubmitted: (term) {
                                CommonMethods.inputFocusChange(
                                    context, gFatherNameFocus, familyNameFocus);
                              },
                              validator: (text) {
                                return text.isEmpty
                                    ? AppLocalizations.of(context)
                                    .translate('Grandfather name cannot be empty')
                                    : null;
                              },
                              hint: AppLocalizations.of(context)
                                  .translate("Enter Grandfather Name"),
                              inputType: TextInputType.name,
                            ),
                          ),
                          Container(
                            margin:
                                EdgeInsets.only(left: 30, top: 25, right: 30),
                            child: InputField(
                              controller: familyNameController,
                              focusNode: familyNameFocus,
                              onSubmitted: (term) {
                                CommonMethods.inputFocusChange(
                                    context, familyNameFocus, residenceFocus);
                              },
                              validator: (text) {
                                return text.isEmpty
                                    ? AppLocalizations.of(context)
                                    .translate('Family name cannot be empty')
                                    : null;
                              },
                              hint: AppLocalizations.of(context)
                                  .translate("Enter Family Name"),
                              inputType: TextInputType.name,
                            ),
                          ),
                          residenceDataList != null && residenceDataList.length > 0
                              ? Container(
                            alignment: Alignment.center,
                            height: 50,
                            padding: EdgeInsets.only(left: 20, right: 10),
                            margin: EdgeInsets.only(
                                left: 30, right: 30, top: 20),
                            decoration: new BoxDecoration(
                              shape: BoxShape.rectangle,
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(25),
                              border: Border.all(
                                  color: Colors.white, width: 1),
                            ),
                            child: DropdownButton<ChildDataList>(
                                isExpanded: true,
                                hint: Row(
                                  children: <Widget>[
                                    Label(
                                      title: _selectedArea ?? "",
                                      color: Colors.black,
                                      fontSize: 16,
                                      textAlign: TextAlign.center,
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(left: 5),
                                      child: Icon(
                                        Icons.keyboard_arrow_down_sharp,
                                        color: Colors.white,
                                        size: 20,
                                      ),
                                    ),
                                  ],
                                ),
                                underline: Container(),
                                items: residenceDataList
                                    .map((ChildDataList val) {
                                  return new DropdownMenuItem<
                                      ChildDataList>(
                                    value: val,
                                    child: Label(
                                      title: val.title,
                                      fontSize: 18,
                                      color: Colors.black,
                                    ),
                                  );
                                }).toList(),
                                onChanged: (newVal) {
                                  _selectedArea = newVal.title;
                                  setState(() {});
                                }),
                          )
                              : Container(),
                          Container(
                            margin:
                                EdgeInsets.only(left: 30, top: 25, right: 30),
                            child: InputField(
                              controller: emailController,
                              focusNode: emailFocus,
                              onSubmitted: (term) {
                                CommonMethods.inputFocusChange(
                                    context, emailFocus, nationalIdFocus);
                              },
                              validator: (text) {
                                bool emailValid = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(text);
                                if (text.isEmpty) {
                                  return AppLocalizations.of(context)
                                      .translate('Email cannot be empty');
                                }
                                else if(!emailValid){
                                  return AppLocalizations.of(context)
                                      .translate('Please enter valid Email');
                                }
                              },
                              hint: AppLocalizations.of(context)
                                  .translate("Enter Email"),
                              inputType: TextInputType.emailAddress,
                            ),
                          ),
                          Container(
                            margin:
                                EdgeInsets.only(left: 30, top: 25, right: 30),
                            child: InputField(
                              controller: nationalIdController,
                              focusNode: nationalIdFocus,
                              onSubmitted: (term) {
                                CommonMethods.inputFocusChange(
                                    context, nationalIdFocus, occupationFocus);
                              },
                              validator: (text) {
                                return text.isEmpty
                                    ? AppLocalizations.of(context)
                                    .translate('National ID cannot be empty')
                                    : null;
                              },
                              hint:AppLocalizations.of(context)
                                  .translate("Enter National Id") ,
                              inputType: TextInputType.text,
                            ),
                          ),
                          childDataList != null && childDataList.length > 0
                              ? Container(
                                  alignment: Alignment.center,
                                  height: 50,
                                  padding: EdgeInsets.only(left: 20, right: 10),
                                  margin: EdgeInsets.only(
                                      left: 30, right: 30, top: 20),
                                  decoration: new BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(25),
                                    border: Border.all(
                                        color: Colors.white, width: 1),
                                  ),
                                  child: DropdownButton<ChildDataList>(
                                      isExpanded: true,
                                      hint: Row(
                                        children: <Widget>[
                                          Label(
                                            title: _selectedRelation ?? "",
                                            color: Colors.black,
                                            fontSize: 16,
                                            textAlign: TextAlign.center,
                                          ),
                                          Padding(
                                            padding: EdgeInsets.only(left: 5),
                                            child: Icon(
                                              Icons.keyboard_arrow_down_sharp,
                                              color: Colors.white,
                                              size: 20,
                                            ),
                                          ),
                                        ],
                                      ),
                                      underline: Container(),
                                      items: childDataList
                                          .map((ChildDataList val) {
                                        return new DropdownMenuItem<
                                            ChildDataList>(
                                          value: val,
                                          child: Label(
                                            title: val.title,
                                            fontSize: 18,
                                            color: Colors.black,
                                          ),
                                        );
                                      }).toList(),
                                      onChanged: (newVal) {
                                        _selectedRelation = newVal.title;
                                        childRelationType = newVal.relation_type.toString();
                                        relationBool = true;
                                        setState(() {});
                                      }),
                                )
                              : Container(),
                          GestureDetector(
                            onTap: () {
                              showDatePickerDialog(context).then((date) {
                                if (date != null) {
                                  childDOB =
                                      DateFormat("dd/MM/yyyy").format(date);
                                  childDob =
                                      DateFormat("yyyy-MM-dd").format(date);
                                  dateBool = true;
                                  setState(() {});
                                }
                              });
                            },
                            child: Container(
                                height: 50,
                                margin: EdgeInsets.only(
                                    left: 30, top: 25, right: 30),
                                padding: EdgeInsets.only(left: 20, right: 10),
                                width: MediaQuery.of(context).size.width,
                                alignment: Alignment.centerLeft,
                                decoration: new BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(25),
                                  border:
                                      Border.all(color: Colors.white, width: 1),
                                ),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Label(
                                      title: childDOB,
                                      fontSize: 16,
                                      color: Colors.black,
                                    ),
                                    Icon(
                                      Icons.date_range_rounded,
                                      color: Colors.black,
                                    ),
                                  ],
                                )),
                          ),
                          Container(
                            height: 60,
                            margin: EdgeInsets.only(
                                left: 30, top: 35, right: 30, bottom: 20),
                            child: Button(
                                title: AppLocalizations.of(context)
                                    .translate("Register"),
                                color: Color(appColors.buttonColor),
                                borderColor: Colors.transparent,
                                textColor: Colors.white,
                                onTap: () {
                                  if (_formKey.currentState.validate()) {
                                    if (!dateBool) {
                                      CommonMethods.showToast(fToast:fToast,message: AppLocalizations.of(context)
                                          .translate("Please select DOB"),status:false);
                                    } else if (!relationBool) {
                                      CommonMethods.showToast(fToast:fToast,message: AppLocalizations.of(context)
                                          .translate("Please select relation"),status:false);
                                    } else if (_image == null) {
                                      CommonMethods.showToast(fToast:fToast,message: AppLocalizations.of(context)
                                              .translate("Please select image"),status:false);
                                    }
                                    else{
                                      apiRegisterCall();
                                    }
                                  }
                                }),
                            // child: BlocListener<ChildRegistrationBloc,
                            //     ChildRegistrationState>(
                            //   listener: (context, state) {
                            //     if (state is ChildRegistrationError) {
                            //       showSnackBarMessage(
                            //           context: context,
                            //           message: state.failure.message);
                            //     } else if (state
                            //         is ChildRegistrationSuccess) {
                            //       Navigator.of(context).push(
                            //         MaterialPageRoute(
                            //             builder: (context) =>
                            //                 ChildRegistrationSummary(
                            //                     null, null)),
                            //       );
                            //     }
                            //   },
                            //   child: BlocBuilder<ChildRegistrationBloc,
                            //           ChildRegistrationState>(
                            //       builder: (_, state) {
                            //     return Button(
                            //         title: "Register",
                            //         textColor: Color(appColors.blue00008B),
                            //         onTap: () {
                            //           if (!dateBool) {
                            //             showSnackBarMessage(
                            //                 context: context,
                            //                 message: "Please select DOB");
                            //           } else if (!relationBool) {
                            //             showSnackBarMessage(
                            //                 context: context,
                            //                 message:
                            //                     "Please select relation");
                            //           } else if (_formKey.currentState
                            //               .validate()) {
                            //             apiRegisterCall();
                            //           }
                            //         });
                            //   }),
                            // ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<void> apiRelationCall() async {
    final _token = await appPreferences.getStringPreference("AccessToken");

    if (context != null) {
      _showLoader(context);
    }
    var response = await http.get(Uri.parse(GET_CHILD_RELATION), headers: {
      'Content-type': 'application/json',
      AUTHORIZATION: BEARER + " " + _token
    });

    if (response.statusCode == 200) {
      final Map<String, dynamic> _responseJson = json.decode(response.body);
      if (mContextLoader != null) Navigator.pop(mContextLoader);
      print(
          'RelationFinalResponse ${response.statusCode} and ${_responseJson.toString()}');
      childRelationDataModel = ChildRelationDataModel.fromJson(_responseJson);
      childDataList = childRelationDataModel.childDataList;
      setState(() {});
      apiResidenceCall();
    }
  }

  Future<void> apiResidenceCall() async {
    final _token = await appPreferences.getStringPreference("AccessToken");

    var response = await http.get(Uri.parse(GET_WEDDING_RESIDENCES), headers: {
      'Content-type': 'application/json',
      AUTHORIZATION: BEARER + " " + _token
    });

    if (response.statusCode == 200) {
      final Map<String, dynamic> _responseJson = json.decode(response.body);
       print('ResidenceFinalResponse ${response.statusCode} and ${_responseJson.toString()}');
      residenceDataModel = ChildRelationDataModel.fromJson(_responseJson);
      residenceDataList = residenceDataModel.childDataList;
      setState(() {});
    }
  }

  Future<void> apiRegisterCall() async {
    final _params = <String, String>{
      'name': nameController.text.toString().trim(),
      'relation_type': childRelationType,
      'dob': childDob,
      "father_name": fatherNameController.text.toString().trim(),
      "grand_father_name": gFatherNameController.text.toString().trim(),
      "family_name": familyNameController.text.toString().trim(),
      "area_of_residence": _selectedArea??"",
      "email": emailController.text.toString().trim(),
      "national_id_number": nationalIdController.text.toString().trim(),
      "occupation": occupationController.text.toString().trim(),
      "photo": base64Image
    };

    final _token = await appPreferences.getStringPreference("AccessToken");

    if (context != null) {
      _showLoader(context);
    }
    var response = await http.post(Uri.parse(CHILD_REGISTRATION),
        headers: {
          'Content-type': 'application/json',
          AUTHORIZATION: BEARER + " " + _token
        },
        body: json.encode(_params));

    if (response.statusCode == 200) {
      final Map<String, dynamic> _responseJson = json.decode(response.body);
      if (mContextLoader != null) Navigator.pop(mContextLoader);
      print('ChildRegisterFinalResponse ${response.statusCode} and ${_responseJson.toString()}');
      CommonMethods.showToast(fToast:fToast,message: "Child Registered Successfully",status:true);
      Future.delayed(Duration(milliseconds: 500), () {
        Navigator.of(context).pop();
      });
    }
    else{
      if (mContextLoader != null) Navigator.pop(mContextLoader);
      CommonMethods.showToast(fToast:fToast,message: "Something went wrong",status:false);
    }
  }

  Future<Null> _showLoader(BuildContext context) async {
    return showDialog<Null>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context1) {
        mContextLoader = context1;
        return Scaffold(
          backgroundColor: Colors.transparent.withOpacity(0.3),
          key: _keyLoader,
          body: Container(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    child: CircularProgressIndicator(),
                    height: 40.0,
                    width: 40.0,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Future<DateTime> showDatePickerDialog(_context) {
    Future<DateTime> selectedDate = showDatePicker(
            context: context,
            firstDate: DateTime(1900),
            initialDate: _dateTimeNotifier.value != null
                ? _dateTimeNotifier.value
                : DateTime.now(),
            lastDate: DateTime.now())
        .then((DateTime dateTime) => _dateTimeNotifier.value = dateTime);
    return selectedDate;
  }
}
