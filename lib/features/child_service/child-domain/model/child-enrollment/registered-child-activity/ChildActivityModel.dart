import 'package:mersal/features/child_service/child-domain/model/child-enrollment/registered-child-activity/ChildActivityData.dart';

class ChildActivityModel {
  List<ChildActivityData> childDataList;
  bool status;

  ChildActivityModel({this.childDataList, this.status});

  factory ChildActivityModel.fromJson(Map<String, dynamic> json) {
    return ChildActivityModel(
      childDataList: json['data'] != null
          ? (json['data'] as List)
              .map((i) => ChildActivityData.fromJson(i))
              .toList()
          : null,
      status: json['status'],
    );
  }
}
