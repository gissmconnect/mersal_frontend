import 'package:mersal/features/child_service/child-data/model/child-enroll-model/enrolled_child_list_model/EnrolledChildActivityModel.dart';
import 'package:mersal/features/child_service/child-data/model/child-enroll-model/enrolled_child_list_model/EnrolledChildModel.dart';

class EnrolledChildDataModel {
  EnrolledChildActivityModel enrolledChildActivity;
  EnrolledChildModel enrolledChildModel;
  int id;

  EnrolledChildDataModel(
      {this.enrolledChildModel,
      this.enrolledChildActivity,
      this.id});

  factory EnrolledChildDataModel.fromJson(Map<String, dynamic> json) {
    return EnrolledChildDataModel(
      enrolledChildActivity: json['activity'] != null
          ? EnrolledChildActivityModel.fromJson(json['activity'])
          : null,
      enrolledChildModel: json['child'] != null
          ? EnrolledChildModel.fromJson(json['child'])
          : null,
      id: json['id'],
    );
  }
}
