import 'package:dart_date/dart_date.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mersal/core/bloc/user_bloc.dart';
import 'package:mersal/core/localization/app_localization.dart';
import 'package:mersal/core/util/date_formatter.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/features/lawati_service/domain/entity/sms_subscription_entity.dart';
import 'package:mersal/resources/colors.dart';

class SubscriptionDetailsScreen extends StatefulWidget {
  final SmsSubscriptionEntity subscription;

  SubscriptionDetailsScreen(this.subscription);

  @override
  _SubscriptionPageState createState() => _SubscriptionPageState();
}

class _SubscriptionPageState extends State<SubscriptionDetailsScreen> {
  @override
  void initState() {
    BlocProvider.of<UserBloc>(context).add(GetUserSessionEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: SubscriptionDetailsBody(
          subscription: widget.subscription,
        ),
      ),
    );
  }
}

class SubscriptionDetailsBody extends StatelessWidget {
  const SubscriptionDetailsBody({Key key, @required this.subscription})
      : super(key: key);

  final SmsSubscriptionEntity subscription;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.only(bottom: 25, top: 20),
        margin: EdgeInsets.only(left: 20, right: 20),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(25)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(top: 10),
              child: Label(
                title: AppLocalizations.of(context)
                    .translate("Your Subscription Details"),
                textAlign: TextAlign.center,
                fontSize: 20,
                fontWeight: FontWeight.w600,
                color: Color(appColors.buttonColor),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 40, left: 20, right: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Label(
                    title: AppLocalizations.of(context).translate("Name:"),
                    textAlign: TextAlign.center,
                    fontSize: 16,
                    color: Color(appColors.buttonColor),
                  ),
                  Label(
                    title:
                        "${subscription.firstName != null?subscription.firstName:"" + " "+
                            subscription.middleName != null ? subscription.middleName:"" + " " +
                            subscription.lastName != null ? subscription.lastName:""} ",
                    textAlign: TextAlign.center,
                    fontSize: 16,
                    color: Color(appColors.buttonColor),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 20, left: 20, right: 20),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Label(
                    title: AppLocalizations.of(context).translate("Phone Number:"),
                    textAlign: TextAlign.center,
                    fontSize: 16,
                    color: Color(appColors.buttonColor),
                  ),
                  Label(
                    title: "${subscription.phone}",
                    textAlign: TextAlign.center,
                    fontSize: 16,
                    color: Color(appColors.buttonColor),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 20, left: 20, right: 20),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Label(
                    title: AppLocalizations.of(context)
                        .translate("Your subscription expires on"),
                    textAlign: TextAlign.center,
                    fontSize: 16,
                    color: Color(appColors.buttonColor),
                  ),
                  Label(
                    title:
                        "${formatDDMMYYY(date: Date.parse(subscription.endDate))}",
                    textAlign: TextAlign.center,
                    fontSize: 16,
                    color: Color(appColors.buttonColor),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 20, left: 20, right: 20),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Label(
                    title: AppLocalizations.of(context)
                        .translate("Notification Type"),
                    textAlign: TextAlign.center,
                    fontSize: 16,
                    color: Color(appColors.buttonColor),
                  ),
                  Label(
                    title:
                        "${subscription.notificationType == 0 ? AppLocalizations.of(context).translate("Push Notification") : subscription.notificationType == 2 ? AppLocalizations.of(context).translate("Push Notification and SMS") : AppLocalizations.of(context).translate("SMS")}",
                    textAlign: TextAlign.center,
                    fontSize: 16,
                    color: Color(appColors.buttonColor),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
