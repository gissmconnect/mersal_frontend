import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/network/vo/Resource.dart';
import 'package:mersal/core/usecases/reauthenticate_usecase.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/lawati_service/domain/usecase/get_sms_subscriptions_usecase.dart';
import 'package:meta/meta.dart';

part 'sms_subscriptions_event.dart';

part 'sms_subscriptions_state.dart';

class SmsSubscriptionsBloc
    extends Bloc<SmsSubscriptionsEvent, SmsSubscriptionsState> {
  SmsSubscriptionsBloc(
      {@required this.getSmsSubscriptionUseCase,
      @required this.reAuthenticateUseCase})
      : super(SmsSubscriptionsState.initial());

  final GetSmsSubscriptionUseCase getSmsSubscriptionUseCase;
  final ReAuthenticateUseCase reAuthenticateUseCase;

  @override
  Stream<SmsSubscriptionsState> mapEventToState(
    SmsSubscriptionsEvent event,
  ) async* {
    if (event is GetSubscriptionsEvent) {
      yield state.copy(subscriptions: Resource.loading());
      final _response = await getSmsSubscriptionUseCase(NoParams());
      yield* _response.fold(
        (l) async* {
          if (l is AuthFailure) {
            final _reauthenticateResponse =
                await reAuthenticateUseCase(NoParams());
            _reauthenticateResponse.fold((l) async* {
              yield state.copy(subscriptions: Resource.error(failure: l));
            }, (r) => GetSubscriptionsEvent());
          } else {
            yield state.copy(subscriptions: Resource.error(failure: l));
          }
        },
        (r) async* {
          yield state.copy(subscriptions: Resource.success(data: r));
        },
      );
    }
  }
}
