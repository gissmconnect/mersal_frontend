import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mersal/resources/colors.dart';

class AppGradient  {

  LinearGradient green(){
    return LinearGradient(
      begin: Alignment.topRight,
      end: Alignment.bottomLeft,
      // Add one stop for each color. Stops should increase from 0 to 1
      stops: [0.1, 0.9],
      colors: [
        // Colors are easy thanks to Flutter's Colors class.
        Color(appColors.getColorHexFromStr("#40D753")),
        Color(appColors.getColorHexFromStr("#31B042")),
      ],
    );
  }

  LinearGradient white(){
    return LinearGradient(
      begin: Alignment.topRight,
      end: Alignment.bottomLeft,
      // Add one stop for each color. Stops should increase from 0 to 1
      stops: [0.1, 0.9],
      colors: [
        // Colors are easy thanks to Flutter's Colors class.
        Colors.white,
        Colors.white,
      ],
    );
  }

  static final AppGradient _appGradient = new AppGradient._internal();
  factory AppGradient() {
    return _appGradient;
  }
  AppGradient._internal();
}
AppGradient appGradient = new AppGradient();