part of 'committer_member_bloc.dart';

abstract class CommitterMemberEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class GetCommitteeMemberEvent extends CommitterMemberEvent{
  final int committeeId;

  GetCommitteeMemberEvent({@required this.committeeId});
  @override
  List<Object> get props => [committeeId];
}