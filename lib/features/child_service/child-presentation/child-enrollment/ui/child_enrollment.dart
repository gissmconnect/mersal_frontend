import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:mersal/core/localization/app_localization.dart';
import 'package:mersal/core/network/api_endpoints.dart';
import 'package:mersal/core/widgets/common/Button.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/InputField.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/core/widgets/common/shared_preference.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/child_service/child-domain/entity/child-enrollment-entity/registered-child-activity-entity/child_activity_model_entity.dart';
import 'package:mersal/features/child_service/child-domain/model/child-enrollment/registered-child-data/RegisteredChildData.dart';
import 'package:mersal/features/child_service/child-domain/model/child-enrollment/registered-child-data/RegisteredChildModel.dart';
import 'package:mersal/features/child_service/child-presentation/child-enrollment/bloc/child_enroll_bloc.dart';
import 'package:mersal/features/child_service/child-presentation/child-enrollment/ui/child_enrolled_summary.dart';
import 'package:mersal/features/child_service/child-presentation/child-enrollment/ui/enrolled_child_list.dart';
import 'package:mersal/features/child_service/child-presentation/child-registration/ui/child_registration.dart';
import 'package:mersal/features/info_presentation/ui/info_screen.dart';
import 'package:mersal/features/wedding_invite_presentation/data/model/ServicePriceModel.dart';
import 'package:mersal/injection_container.dart';
import 'package:mersal/resources/colors.dart';

class ChildEnrollment extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (_) => serviceLocator<ChildEnrollmentBloc>(),
        child: Scaffold(body: ChildEnrollmentBody()));
  }
}

class ChildEnrollmentBody extends StatefulWidget {
  @override
  _ChildEnrollmentBodyState createState() => _ChildEnrollmentBodyState();
}

class _ChildEnrollmentBodyState extends State<ChildEnrollmentBody> {
  final nameController = TextEditingController();
  final emailController = TextEditingController();
  final currentClassController = TextEditingController();
  final childPhoneNumberController = TextEditingController();

  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  BuildContext mContextLoader;
  String passwordText = "";
  int currentIndex = 0;

  final FocusNode nameFocus = new FocusNode();
  final FocusNode emailFocus = new FocusNode();
  final FocusNode currentClassFocus = new FocusNode();
  final FocusNode childPhoneNumberFocus = new FocusNode();

  final _formKey = GlobalKey<FormState>();

  List<String> dummyOptions = ['Dummy1', 'Dummy2', 'Dummy3'];

  bool nameBool = false, relationBool = false;

  RegisteredChildModel registeredChildModel;
  RegisteredChildData registeredChildData;
  List<RegisteredChildData> registeredChildList = [];
  ChildActivityData childActivityData;
  ServicePriceModel servicePriceModel;
  ServicePriceData servicePriceData;

  FToast fToast;
  String _selectedLocation = "Select Child Name",
      _selectedActivity = 'Select Activity',
      activityId = "",
      childId = "",
      descriptionText = "Description";

  @override
  void initState() {
    super.initState();
    apiChildDetailsCall();
    apiServicePriceCall();
    BlocProvider.of<ChildEnrollmentBloc>(context).add(GetActivityEvent());
    // BlocProvider.of<ChildEnrollmentBloc>(context).add(GetChildDetailsEvent());
    fToast = FToast();
    fToast.init(context);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bg_image.jpg"),
            fit: BoxFit.fill,
          ),
        ),
        child: ListView(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: 20,
                ),
                Stack(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(bottom: 5, left: 15),
                      alignment: Alignment.bottomLeft,
                      child: GestureDetector(
                        child: Icon(
                          Icons.arrow_back_ios_rounded,
                          color: Colors.white,
                        ),
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(bottom: 6),
                      alignment: Alignment.bottomCenter,
                      child: Text(
                        AppLocalizations.of(context)
                            .translate('Child Enrollment'),
                        textAlign: TextAlign.end,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(right: 50),
                      alignment: Alignment.centerRight,
                      child: GestureDetector(
                        child: Icon(
                          Icons.view_list,
                          color: Colors.white,
                          size: 35,
                        ),
                        onTap: () {
                          CommonMethods().animatedNavigation(
                              context: context,
                              currentScreen: ChildEnrollment(),
                              landingScreen: EnrolledChildList());
                        },
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(right: 15),
                      alignment: Alignment.centerRight,
                      child: GestureDetector(
                        child: Container(
                          margin: EdgeInsets.only(left: 20),
                          child: SvgPicture.asset(
                            "assets/images/info_icon.svg",
                            width: 30,
                            height: 30,
                          ),
                        ),
                        onTap: () {
                          CommonMethods().animatedNavigation(
                              context: context,
                              currentScreen: ChildRegistration(),
                              landingScreen: InfoScreen());
                        },
                      ),
                    ),
                  ],
                ),
                Container(
                  margin: EdgeInsets.only(top: 80),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        BlocBuilder<ChildEnrollmentBloc, ChildEnrollmentState>(
                          builder: (context, state) {
                            if (state is ChildDetailsFetch) {
                              return Center(
                                child: CircularProgressIndicator(),
                              );
                            }
                            if (state is ChildActivityLoaded) {
                              List<ChildActivityData> childActivityList =
                                  state.activity.childActivityList;
                              return Container(
                                margin: EdgeInsets.only(
                                    left: 30, right: 30, top: 10),
                                padding: EdgeInsets.only(left: 20, right: 10),
                                height: 50,
                                alignment: Alignment.centerLeft,
                                width: MediaQuery.of(context).size.width,
                                decoration: new BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(25),
                                ),
                                child: DropdownButton<ChildActivityData>(
                                    underline: Container(),
                                    isExpanded: true,
                                    hint: Row(
                                      children: <Widget>[
                                        Label(
                                          title: _selectedActivity ?? "",
                                          color: Colors.black,
                                          fontSize: 16,
                                          textAlign: TextAlign.center,
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: 5),
                                          child: Icon(
                                            Icons.keyboard_arrow_down_sharp,
                                            color: Colors.white,
                                            size: 20,
                                          ),
                                        ),
                                      ],
                                    ),
                                    items: childActivityList
                                        .map((ChildActivityData val) {
                                      return new DropdownMenuItem<
                                          ChildActivityData>(
                                        value: val,
                                        child: Label(
                                          title: val.title,
                                          fontSize: 18,
                                          color: Colors.black,
                                        ),
                                      );
                                    }).toList(),
                                    onChanged: (newVal) {
                                      _selectedActivity = newVal.title;
                                      descriptionText =
                                          newVal.description ?? "N/A";
                                      activityId = newVal.id.toString();
                                      relationBool = true;
                                      childActivityData = newVal;
                                      setState(() {});
                                    }),
                              );
                            } else {
                              return CircularProgressIndicator();
                            }
                          },
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          margin: EdgeInsets.only(top: 20, left: 30, right: 30),
                          padding:
                              EdgeInsets.only(top: 15, bottom: 15, left: 20),
                          decoration: new BoxDecoration(
                            shape: BoxShape.rectangle,
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(25),
                          ),
                          child: Label(
                            title: descriptionText,
                            color: Colors.black,
                            fontSize: 16,
                          ),
                        ),
                        registeredChildList != null && registeredChildList.length > 0
                            ? Container(
                                margin: EdgeInsets.only(
                                    left: 30, right: 30, top: 25, bottom: 10),
                                padding: EdgeInsets.only(left: 20, right: 10),
                                height: 50,
                                alignment: Alignment.centerLeft,
                                width: MediaQuery.of(context).size.width,
                                decoration: new BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(25),
                                  border:
                                      Border.all(color: Colors.white, width: 1),
                                ),
                                child: DropdownButton<RegisteredChildData>(
                                    isExpanded: true,
                                    hint: Row(
                                      children: <Widget>[
                                        Label(
                                          title: _selectedLocation ?? "",
                                          color: Colors.black,
                                          fontSize: 16,
                                          textAlign: TextAlign.center,
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: 5),
                                          child: Icon(
                                            Icons.keyboard_arrow_down_sharp,
                                            color: Colors.white,
                                            size: 20,
                                          ),
                                        ),
                                      ],
                                    ),
                                    underline: Container(),
                                    items: registeredChildList
                                        .map((RegisteredChildData val) {
                                      return new DropdownMenuItem<
                                          RegisteredChildData>(
                                        value: val,
                                        child: Label(
                                          title: val.name ?? "",
                                          fontSize: 18,
                                          color: Colors.black,
                                        ),
                                      );
                                    }).toList(),
                                    onChanged: (newVal) {
                                      _selectedLocation = newVal.name;
                                      registeredChildData = newVal;
                                      nameBool = true;
                                      childId = newVal.id.toString();
                                      setState(() {});
                                    }),
                              )
                            : Container(),
                        Container(
                          margin: EdgeInsets.only(left: 30, top: 20, right: 30),
                          child: InputField(
                            controller: currentClassController,
                            focusNode: currentClassFocus,
                            hint: AppLocalizations.of(context)
                                .translate("Enter Current Class"),
                            validator: (text) {
                              if (text.isEmpty) {
                                return 'Current class cannot be empty';
                              }
                              return null;
                            },
                            maxLength: 2,
                            onSubmitted: (term) {
                              CommonMethods.inputFocusChange(context,
                                  currentClassFocus, childPhoneNumberFocus);
                            },
                            inputType: TextInputType.phone,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 30, top: 25, right: 30),
                          child: InputField(
                            controller: childPhoneNumberController,
                            focusNode: childPhoneNumberFocus,
                            hint: AppLocalizations.of(context)
                                .translate("Enter Child Phone Number"),
                            validator: (text) {
                              return text.isEmpty
                                  ? AppLocalizations.of(context).translate(
                                      'Child phone number cannot be empty')
                                  : text.length < 8
                                      ? AppLocalizations.of(context).translate(
                                          'Child phone cannot be less than 8')
                                      : null;
                            },
                            maxLength: 15,
                            onSubmitted: (term) {
                              FocusScope.of(context).unfocus();
                            },
                            inputType: TextInputType.phone,
                          ),
                        ),
                        servicePriceData!=null?Container(
                          margin: EdgeInsets.only(
                            top: 20,
                              bottom: 10, left: 20, right: 20),
                          child: Label(
                            title:
                            "OMR ${servicePriceData
                                .price} for Child Enrollment",
                            fontSize: 16,
                            color: Colors.white,
                            textAlign: TextAlign.center,
                          ),
                        ):Container(),
                        Container(
                          height: 60,
                          margin: EdgeInsets.only(
                              left: 30, top: 70, right: 30, bottom: 20),
                          child: BlocListener<ChildEnrollmentBloc,
                              ChildEnrollmentState>(
                            listener: (context, state) {
                              if (state is ChildEnrollmentError) {
                                CommonMethods.showToast(
                                    fToast: fToast,
                                    message: state.failure.message,
                                    status: false);
                              } else if (state is ChildActivityLoaded) {
                                // Navigator.of(context).push(
                                //   MaterialPageRoute(
                                //       builder: (context) =>
                                //           ChildRegistrationSummary(
                                //               null, null)),
                                // );
                              }
                            },
                            child: BlocBuilder<ChildEnrollmentBloc,
                                ChildEnrollmentState>(builder: (_, state) {
                              return Button(
                                  title: AppLocalizations.of(context)
                                      .translate("Next"),
                                  textColor: Colors.white,
                                  borderColor: Colors.transparent,
                                  color: Color(appColors.buttonColor),
                                  onTap: () {
                                    if (!relationBool) {
                                      CommonMethods.showToast(
                                          fToast: fToast,
                                          message: AppLocalizations.of(context)
                                              .translate(
                                                  "Please select activity"),
                                          status: false);
                                    } else if (!nameBool) {
                                      CommonMethods.showToast(
                                          fToast: fToast,
                                          message: AppLocalizations.of(context)
                                              .translate(
                                                  "Please select child name"),
                                          status: false);
                                    } else if (_formKey.currentState
                                        .validate()) {
                                      // apiEnrollCall();
                                      childEnrollSummarySheet();
                                    }
                                  });
                            }),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  calculateAge(DateTime birthDate) {
    DateTime currentDate = DateTime.now();
    int age = currentDate.year - birthDate.year;
    int month1 = currentDate.month;
    int month2 = birthDate.month;
    if (month2 > month1) {
      age--;
    } else if (month1 == month2) {
      int day1 = currentDate.day;
      int day2 = birthDate.day;
      if (day2 > day1) {
        age--;
      }
    }
    return age;
  }

  Future<void> apiChildDetailsCall() async {
    final _token = await appPreferences.getStringPreference("AccessToken");
    if (context != null) {
      _showLoader(context);
    }

    var response = await http.get(Uri.parse(GET_REGISTERED_CHILD), headers: {
      'Content-type': 'application/json',
      AUTHORIZATION: BEARER + " " + _token
    });

    if (response.statusCode == 200) {
      if (mContextLoader != null) Navigator.pop(mContextLoader);
      final Map<String, dynamic> _responseJson = json.decode(response.body);
      print(
          'RelationRegisteredResponse ${response.statusCode} and ${_responseJson.toString()}');
      registeredChildModel = RegisteredChildModel.fromJson(_responseJson);
      registeredChildList = registeredChildModel.childDataList;
      if (registeredChildList.length == 0) {
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) => ChildRegistration()));
      }
      setState(() {});
      // Future.delayed(Duration(milliseconds: 500), () {
      //   apiActivityListCall();
      // });
    }
  }

  Future<void> apiServicePriceCall() async {
    final _token = await appPreferences.getStringPreference("AccessToken");
    var response = await http.get(Uri.parse("http://93.115.18.236/public/api/service/get?type=wedding-booking"), headers: {
      'Content-type': 'application/json',
      AUTHORIZATION: BEARER + " " + _token
    });
    if (response.statusCode == 200) {
      final Map<String, dynamic> _responseJson = json.decode(response.body);
      print('ServicePriceResponse ${response.statusCode} and ${_responseJson.toString()}');
      servicePriceModel = ServicePriceModel.fromJson(_responseJson);
      servicePriceData = servicePriceModel.data;
      setState(() {});
    }
  }

  Future<Null> _showLoader(BuildContext context) async {
    return showDialog<Null>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context1) {
        mContextLoader = context1;
        return Scaffold(
          backgroundColor: Colors.transparent.withOpacity(0.3),
          key: _keyLoader,
          body: Container(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    child: CircularProgressIndicator(),
                    height: 40.0,
                    width: 40.0,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Future<Widget> childEnrollSummarySheet() {
    var email = emailController.text.toString().trim();
    var phone = childPhoneNumberController.text.toString().trim();
    var className = currentClassController.text.toString().trim();

    return showModalBottomSheet<Widget>(
      isScrollControlled: true,
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30.0), topRight: Radius.circular(30.0)),
      ),
      builder: (BuildContext context) {
        return Container(
            height: MediaQuery.of(context).size.height / 2,
            color: Colors.white,
            margin: EdgeInsets.only(top: 40),
            child: Container(
                child: ChildEnrollmentSummary(registeredChildData,
                    childActivityData, email, phone, className)));
      },
    );
  }
}
