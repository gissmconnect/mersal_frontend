import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:matcher/matcher.dart';
import 'package:mersal/core/error/exceptions.dart';
import 'package:mersal/core/network/api_endpoints.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/election/election-data/datasource/election_data_source.dart';
import 'package:mersal/features/election/election-data/datasource/election_data_source_impl.dart';
import 'package:meta/meta.dart';
import 'package:mockito/mockito.dart';

import '../../../../fixtures/fixture_reader.dart';
import '../../../auth/auth_data/datasource/auth_data_source_test.dart';

void main() {
  MockHttpClient _mockHttpClient;
  ElectionDataSourceImpl _electionDataSource;

  setUp(() {
    _mockHttpClient = MockHttpClient();
    _electionDataSource = ElectionDataSourceImpl(httpClient: _mockHttpClient);
  });

  void setUpMockHttpClientFailure404() {
    when(_mockHttpClient.get(any, headers: anyNamed('headers')))
        .thenAnswer((_) async => http.Response('Something went wrong', 404));
  }

  void setUpMockHttpClientFailure422() {
    when(_mockHttpClient.get(any, headers: anyNamed('headers')))
        .thenAnswer((_) async => http.Response(
            '{"status": false,"message": "The password field is required."}',
            422));
  }

  void setUpMockHttpClientFailure500() {
    when(_mockHttpClient.get(any, headers: anyNamed('headers')))
        .thenAnswer((_) async => http.Response(
            '{"status": false,"message": "Something went wrong"}', 500));
  }

  void setUpMockHttpClientFailure401() {
    when(_mockHttpClient.get(any, headers: anyNamed('headers')))
        .thenAnswer((_) async =>
            http.Response('{"status": false,"error": "Unauthorized"}', 401));
  }

  final _token = 'test_token';

  void testApiFailures({@required Function() call}) {
    test('should throw  Unauthorized failure when the response code is 401',
        () async {
      //arrange
      setUpMockHttpClientFailure401();

      //act

      //assert
      expect(() => call(), throwsA(TypeMatcher<AuthException>()));
    });

    test('should throw ServerFailure when the response code is 422', () {
      //arrange
      setUpMockHttpClientFailure500();

      //act

      //assert
      expect(() => call(), throwsA(TypeMatcher<ServerException>()));
    });

    test('should throw ServerFailure when the response code is 500', () {
      //arrange
      setUpMockHttpClientFailure422();

      //act

      //assert
      expect(() => call(), throwsA(TypeMatcher<ServerException>()));
    });
  }

  group('http_test', () {
    test(
        '''should perform GET request on a URL with page_no and page_size with application/json header''',
        () async {
      //arrange
      when(_mockHttpClient.get(any, headers: anyNamed('headers'))).thenAnswer(
          (_) async => http.Response(fixture('elections_response.json'), 200));

      //act
      await _electionDataSource.getElections(token: _token);

      //assert
      verify(_mockHttpClient.get(Uri.parse(GET_ELECTIONS_ENDPOINT), headers: {
        'Content-type': 'application/json',
        AUTHORIZATION: BEARER + " " + _token
      }));
    });

    testApiFailures(call: () async => _electionDataSource.getElections(token: _token));
  });
}
