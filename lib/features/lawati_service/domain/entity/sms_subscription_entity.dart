import 'package:mersal/features/lawati_service/domain/entity/sms_serviceInfo_entity.dart';
import 'package:meta/meta.dart';

class SmsSubscriptionEntity {
  int id, notificationType,expiringIn;
  String phone, firstName, middleName, lastName, startDate, endDate, relation;

  SmsSubscriptionEntity(
      {@required this.id,
      @required this.phone,
      @required this.firstName,
      @required this.middleName,
      @required this.lastName,
      @required this.startDate,
      @required this.endDate,
      @required this.expiringIn,
      @required this.relation,
      @required this.notificationType});

}
