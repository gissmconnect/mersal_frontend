import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/exceptions.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/network/network_info.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/donation_history/donation-history-data/datasource/donation_history_source.dart';
import 'package:mersal/features/donation_history/donation-history-domain/entity/donation_history_entity.dart';
import 'package:mersal/features/donation_history/donation-history-domain/repository/donation_history_repo.dart';
import 'package:meta/meta.dart';

class DonationHistoryRepoImpl implements DonationHistoryRepo {
  final DonationHistoryDataSource donationHistoryDataSource;
  final NetworkInfo networkInfo;
  final UserDataLocalDataSource userDataLocalDataSource;

  DonationHistoryRepoImpl(
      {@required this.donationHistoryDataSource,
      @required this.networkInfo,
      @required this.userDataLocalDataSource});

  @override
  Future<Either<Failure, DonationHistoryEntity>> getDonationHistory() async {
    if (await networkInfo.isConnected) {
      final _token = await userDataLocalDataSource.getAuthToken();
      try {
        final _service =
            await donationHistoryDataSource.getDonationHistory(token: _token);
        return Right(_service);
      } on ServerException catch (ex) {
        return Left(ServerFailure()..message = ex.message);
      } on AuthException catch (ex) {
        return Left(AuthFailure()..message = ex.message);
      } on Exception {
        return Left(ServerFailure());
      }
    } else {
      return Left(NetworkFailure());
    }
  }
}
