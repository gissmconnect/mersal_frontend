import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/features/banner/banner-domain/entity/banner_entity.dart';
import 'package:mersal/features/homescreen/home-domain/entity/BannerListDataEntity.dart';

abstract class HomeRepository {

  Future<Either<Failure, List<BannerEntity>>> getBanners();

}
