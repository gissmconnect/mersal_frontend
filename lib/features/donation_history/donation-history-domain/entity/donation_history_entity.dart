class DonationHistoryEntity {
  bool status;
  List<DonationDataListEntity> donationDataList;

  DonationHistoryEntity({this.status, this.donationDataList});

  DonationHistoryEntity copy({
    int id,
    String title,
    String description,
    List<DonationDataListEntity> donationDataList,
  }) {
    return DonationHistoryEntity(
        status: status ?? this.status,
        donationDataList: donationDataList ?? this.donationDataList);
  }
}

class DonationDataListEntity {
  int id;
  String description;
  String created_at;
  DonationReasonDataEntity reason;

  DonationDataListEntity({this.id, this.created_at,  this.description, this.reason});

  DonationDataListEntity copy(
      {int id,
      String question,
      String created_at,
      String description,
      DonationReasonDataEntity reason,
      int answerId}) {
    return DonationDataListEntity(
        id: id ?? this.id,
        created_at: created_at ?? this.created_at,
        description: description ?? this.description,
        reason: reason ?? this.reason);
  }
}

class DonationReasonDataEntity {
  int id;
  String reason;
  String description;

  DonationReasonDataEntity({
    this.id,
    this.reason,
    this.description,
  });

  DonationReasonDataEntity copy(
      {int id,
       String description,
       String reason}) {
    return DonationReasonDataEntity(
        id: id ?? this.id,
        description: description ?? this.description,
        reason: reason ?? this.reason);
  }
}
