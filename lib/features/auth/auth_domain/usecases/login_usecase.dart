import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/auth/auth_domain/entity/user_data.dart';
import 'package:mersal/features/auth/auth_domain/repository/auth_repository.dart';
import 'package:meta/meta.dart';

class LoginUseCase extends UseCase<UserData, Params> {
  final AuthRepository authRepository;

  LoginUseCase({@required this.authRepository});

  @override
  Future<Either<Failure, UserData>> call(params) async {
    return await authRepository.login(
        phoneNumber: params.phoneNumber,
        password: params.password,
        device_token: params.device_token);
  }
}

class Params extends Equatable {
  final String phoneNumber, password, device_token;

  Params(
      {@required this.phoneNumber,
      @required this.password,
      @required this.device_token});

  @override
  List<Object> get props => [phoneNumber, password, device_token];
}
