import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/child_service/child-data/model/child-enroll-model/registered-child-activity/child_activity_model.dart';
import 'package:mersal/features/child_service/child-domain/repository/child_repository.dart';
import 'package:meta/meta.dart';

class ChildActivityUseCase extends UseCase<ChildActivityModel, NoParams> {
  final ChildRepository childRepository;

  ChildActivityUseCase({@required this.childRepository});

  @override
  Future<Either<Failure, ChildActivityModel>> call(NoParams params) {
    return childRepository.getActivity();
  }
}
