import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;
import 'package:mersal/core/network/network_info.dart';
import 'package:mersal/core/usecases/get_user_login_data_usecase.dart';
import 'package:mersal/core/usecases/logout_usecase.dart';
import 'package:mersal/core/usecases/reauthenticate_usecase.dart';
import 'package:mersal/core/usecases/save_user_data_usecase.dart';
import 'package:mersal/core/usecases/save_user_login_data_usecase.dart';
import 'package:mersal/features/auth/auth_data/datasource/auth_data_source.dart';
import 'package:mersal/features/auth/auth_data/datasource/auth_data_source_impl.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source_impl.dart';
import 'package:mersal/features/auth/auth_data/repository/auth_repository_impl.dart';
import 'package:mersal/features/auth/auth_domain/repository/auth_repository.dart';
import 'package:mersal/features/auth/auth_domain/usecases/change_password_usecase.dart';
import 'package:mersal/features/auth/auth_domain/usecases/child_registration_usecase.dart';
import 'package:mersal/features/auth/auth_domain/usecases/register_user_usecase.dart';
import 'package:mersal/features/auth/auth_domain/usecases/send_otp_usecase.dart';
import 'package:mersal/features/auth/auth_domain/usecases/upload_document_usecase.dart';
import 'package:mersal/features/auth/auth_domain/usecases/verify_otp_usecase.dart';
import 'package:mersal/features/auth/auth_domain/usecases/wedding_booking_usecase.dart';
import 'package:mersal/features/auth/auth_domain/usecases/wedding_invite_usecase.dart';
import 'package:mersal/features/auth/auth_presentation/change_password_presentation/bloc/change_password_bloc.dart';
import 'package:mersal/features/auth/auth_presentation/doc_upload_presentation/bloc/doc_upload_bloc.dart';
import 'package:mersal/features/auth/auth_presentation/otp_presentation/bloc/otp_bloc.dart';
import 'package:mersal/features/auth/auth_presentation/registration_presentation/bloc/registration_bloc.dart';
import 'package:mersal/features/banner/banner-domain/usecases/get_banner_usercase.dart';
import 'package:mersal/features/banner/banner-presentation/bloc/banner_bloc.dart';
import 'package:mersal/features/child_service/child-data/datasource/child_data_source.dart';
import 'package:mersal/features/child_service/child-data/datasource/child_data_source_impl.dart';
import 'package:mersal/features/child_service/child-data/repository/child_repository_impl.dart';
import 'package:mersal/features/child_service/child-domain/repository/child_repository.dart';
import 'package:mersal/features/child_service/child-domain/usecase/child_activity_usecase.dart';
import 'package:mersal/features/child_service/child-domain/usecase/child_details_usecase.dart';
import 'package:mersal/features/child_service/child-presentation/child-enrollment/bloc/child_enroll_bloc.dart';
import 'package:mersal/features/committee_management/data/datasource/committee_datasource.dart';
import 'package:mersal/features/committee_management/data/datasource/committee_datasource_impl.dart';
import 'package:mersal/features/committee_management/data/repository/committee_repo_impl.dart';
import 'package:mersal/features/committee_management/domain/repository/committe_repo.dart';
import 'package:mersal/features/committee_management/domain/usecases/get_committer_member_usecase.dart';
import 'package:mersal/features/committee_management/domain/usecases/get_committes_usecase.dart';
import 'package:mersal/features/committee_management/presentation/management/bloc/committe_management_bloc.dart';
import 'package:mersal/features/committee_management/presentation/member/bloc/committer_member_bloc.dart';
import 'package:mersal/features/donation/data/datasource/donation_datasource.dart';
import 'package:mersal/features/donation/data/datasource/donation_datasource_impl.dart';
import 'package:mersal/features/donation/data/repository/donation_repo_impl.dart';
import 'package:mersal/features/donation/domain/repository/donation_repo.dart';
import 'package:mersal/features/donation/domain/usecase/get_donation_type_usecase.dart';
import 'package:mersal/features/donation/domain/usecase/give_donation_usecase.dart';
import 'package:mersal/features/donation/donation-presentation/bloc/donation_bloc.dart';
import 'package:mersal/features/donation_history/donation-history-data/datasource/donation_history_source.dart';
import 'package:mersal/features/donation_history/donation-history-data/datasource/donation_history_source_impl.dart';
import 'package:mersal/features/donation_history/donation-history-data/repository/donation_history_repo_impl.dart';
import 'package:mersal/features/donation_history/donation-history-domain/repository/donation_history_repo.dart';
import 'package:mersal/features/donation_history/donation-history-domain/usecase/donation_history_usecase.dart';
import 'package:mersal/features/donation_history/donation-history-presentation/bloc/donation_history_bloc.dart';
import 'package:mersal/features/election/election-data/datasource/election_data_source.dart';
import 'package:mersal/features/election/election-data/datasource/election_data_source_impl.dart';
import 'package:mersal/features/election/election-data/repository/election_repository_impl.dart';
import 'package:mersal/features/election/election-domain/repository/election_repository.dart';
import 'package:mersal/features/election/election-domain/usecases/get_elections_usecase.dart';
import 'package:mersal/features/election/election-domain/usecases/vote_usecase.dart';
import 'package:mersal/features/election/election-presentation/bloc/election_bloc.dart';
import 'package:mersal/features/election_candidate/election_candidate_domain/usecases/get_election_candidates_usecase.dart';
import 'package:mersal/features/election_candidate/election_candidate_presentation/bloc/election_candidate_bloc.dart';
import 'package:mersal/features/event/data/datasource/event_data_source.dart';
import 'package:mersal/features/event/data/datasource/event_data_source_impl.dart';
import 'package:mersal/features/event/domain/repository/event_repo.dart';
import 'package:mersal/features/event/domain/usecase/get_event_usecase.dart';
import 'package:mersal/features/event/presentation/bloc/event_bloc.dart';
import 'package:mersal/features/homescreen/home-data/datasource/home_data_source_impl.dart';
import 'package:mersal/features/homescreen/home-domain/repository/home_repository.dart';
import 'package:mersal/features/homescreen/home-domain/repository/home_repository_impl.dart';
import 'package:mersal/features/homescreen/home-presentation/bloc/home_bloc.dart';
import 'package:mersal/features/lawati_service/data/datasource/sms_service_datasource.dart';
import 'package:mersal/features/lawati_service/data/datasource/sms_service_datasource_impl.dart';
import 'package:mersal/features/lawati_service/data/repository/sms_service_repo_impl.dart';
import 'package:mersal/features/lawati_service/domain/repository/sms_service_repo.dart';
import 'package:mersal/features/lawati_service/domain/usecase/get_my_subscription_usecase.dart';
import 'package:mersal/features/lawati_service/domain/usecase/get_sms_info_usecase.dart';
import 'package:mersal/features/lawati_service/domain/usecase/get_sms_subscriptions_usecase.dart';
import 'package:mersal/features/lawati_service/domain/usecase/renew_subscription_usecase.dart';
import 'package:mersal/features/lawati_service/domain/usecase/subscribe_sms_subscription_usecase.dart';
import 'package:mersal/features/lawati_service/presentation/service_info/bloc/my_sms_service/my_sms_service_bloc.dart';
import 'package:mersal/features/lawati_service/presentation/service_info/bloc/sms_service_info_bloc.dart';
import 'package:mersal/features/lawati_service/presentation/subscriptions/bloc/sms_subscriptions_bloc.dart';
import 'package:mersal/features/notification/notification_data/datasource/notification_data_source.dart';
import 'package:mersal/features/notification/notification_data/datasource/notification_data_source_impl.dart';
import 'package:mersal/features/notification/notification_data/repository/notification_repo_impl.dart';
import 'package:mersal/features/notification/notification_domain/repository/notification_repo.dart';
import 'package:mersal/features/notification/notification_domain/usecases/archive_notification_usecase.dart';
import 'package:mersal/features/notification/notification_domain/usecases/delete_notification_usecase.dart';
import 'package:mersal/features/notification/notification_domain/usecases/get_notification_usecase.dart';
import 'package:mersal/features/notification/notification_presentation/bloc/notification_bloc.dart';
import 'package:mersal/features/occasion/data/datasource/occasion_data_source.dart';
import 'package:mersal/features/occasion/data/datasource/occasion_data_source_impl.dart';
import 'package:mersal/features/occasion/data/repository/occasion_repo_impl.dart';
import 'package:mersal/features/occasion/domain/repository/occasion_repo.dart';
import 'package:mersal/features/occasion/domain/usecases/get_occasion_usecase.dart';
import 'package:mersal/features/occasion/presentation/bloc/occasion_bloc.dart';
import 'package:mersal/features/prayer_timeline/data/datasource/prayer_datasource.dart';
import 'package:mersal/features/prayer_timeline/data/datasource/prayer_datasource_impl.dart';
import 'package:mersal/features/prayer_timeline/data/repository/prayer_repository_impl.dart';
import 'package:mersal/features/prayer_timeline/domain/repository/prayer_repository.dart';
import 'package:mersal/features/prayer_timeline/domain/usecase/get_prayertimeline_usecase.dart';
import 'package:mersal/features/prayer_timeline/domain/usecase/set_alarm_usecase.dart';
import 'package:mersal/features/prayer_timeline/ui/presentation/bloc/prayer_bloc.dart';
import 'package:mersal/features/profile/edit_profile_data/data_source/edit_profile_data_source.dart';
import 'package:mersal/features/profile/edit_profile_data/data_source/edit_profile_date_source_impl.dart';
import 'package:mersal/features/profile/edit_profile_data/repository/edit_profile_repository_impl.dart';
import 'package:mersal/features/profile/edit_profile_domain/repository/edit_profile_repository.dart';
import 'package:mersal/features/profile/edit_profile_domain/usecases/edit_profile_use_case.dart';
import 'package:mersal/features/survey/data/datasource/survey_datasource.dart';
import 'package:mersal/features/survey/data/datasource/survey_datasource_impl.dart';
import 'package:mersal/features/survey/data/repository/survey_repository_impl.dart';
import 'package:mersal/features/survey/domain/repository/survey_repository.dart';
import 'package:mersal/features/survey/domain/usecase/get_survey_usecase.dart';
import 'package:mersal/features/survey/domain/usecase/submit_question_survey_usecase.dart';
import 'package:mersal/features/survey/presentation/bloc/survey_bloc.dart';
import 'package:mersal/features/weddingBooking/bloc/wedding_booking_bloc.dart';
import 'package:mersal/features/wedding_invite_presentation/data/datasource/wedding_invite_datasource.dart';
import 'package:mersal/features/wedding_invite_presentation/data/datasource/wedding_invite_datasource_impl.dart';
import 'package:mersal/features/wedding_invite_presentation/data/repository/wedding_invite_template_repo_impl.dart';
import 'package:mersal/features/wedding_invite_presentation/domain/repository/wedding_template_repo.dart';
import 'package:mersal/features/wedding_invite_presentation/domain/usecase/wedding_invite_usecase.dart';
import 'package:mersal/features/wedding_invite_presentation/presentation/bloc/wedding_invite_bloc.dart';
import 'package:mersal/features/wedding_invite_presentation/presentation/wedding_invite_template/bloc/wedding_invite_template_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'core/bloc/user_bloc.dart';
import 'core/usecases/get_user_document_status_usecase.dart';
import 'features/auth/auth_domain/usecases/get_user_session.dart';
import 'features/auth/auth_domain/usecases/login_usecase.dart';
import 'features/auth/auth_presentation/login_presentation/bloc/login_bloc.dart';
import 'features/event/data/respository/event_repo_impl.dart';
import 'features/splash_presentation/bloc/splash_bloc.dart';

final serviceLocator = GetIt.instance;

Future<void> init() async {
  //Bloc
  //splash
  serviceLocator.registerFactory(
    () => SplashBloc(
      userSessionUseCase: serviceLocator(),
    ),
  );
  //Login
  serviceLocator.registerFactory(() => LoginBloc(
      loginUseCase: serviceLocator(),
      saveUserLoginDataUseCase: serviceLocator()));
  //Register
  serviceLocator.registerFactory(() => RegistrationBloc(
      registerUserUseCase: serviceLocator(),
      saveUserLoginDataUseCase: serviceLocator()));
  //OTP
  serviceLocator.registerFactory(() => OtpBloc(
      otpUseCase: serviceLocator(), verifyOtpUseCase: serviceLocator()));
  //WeddingBooking
  serviceLocator.registerFactory(
      () => WeddingBookingBloc(weddingBookingUseCase: serviceLocator()));
  //ChildRegistration
  serviceLocator.registerFactory(() => ChildEnrollmentBloc(
      childActivityUseCase: serviceLocator(),
      childDetailsUseCase: serviceLocator()));
  //WeddingInvitation
  serviceLocator.registerFactory(
      () => WeddingInviteBloc(weddingInviteUseCase: serviceLocator()));
  //HomeBloc
  serviceLocator
      .registerFactory(() => HomeBloc(getUserSessionUseCase: serviceLocator()));
  //DonationsBloc
  serviceLocator.registerFactory(() => DonationBloc(
      donationUseCase: serviceLocator(),
      reAuthenticateUseCase: serviceLocator(),
      getDonationTypeUseCase: serviceLocator()));
  //ElectionBloc
  serviceLocator.registerFactory(
    () => ElectionBloc(
        getElectionUseCase: serviceLocator(),
        reAuthenticateUseCase: serviceLocator(),
        getUserLoginDataUseCase: serviceLocator()),
  );
  //UserBloc
  serviceLocator.registerLazySingleton(
    () => UserBloc(
        logoutUseCase: serviceLocator(),
        getUserLoginDataUseCase: serviceLocator(),
        reAuthenticateUseCase: serviceLocator(),
        getUserDocumentStatusUseCase: serviceLocator(),
        getUserSession: serviceLocator(),
        editProfileUseCase: serviceLocator(),
        saveUserDataUseCase: serviceLocator()),
  );

  //NoficationBloc
  serviceLocator.registerFactory(
    () => NotificationBloc(
        notificationUseCase: serviceLocator(),
        reAuthenticateUseCase: serviceLocator(),
        getUserLoginDataUseCase: serviceLocator(),
        deleteNotificationUseCase: serviceLocator(),
        archiveNotificationUseCase: serviceLocator()),
  );
  //DocUploadBloc
  serviceLocator.registerFactory(() => DocUploadBloc(
        loginUseCase: serviceLocator(),
        getUserLoginDataUseCase: serviceLocator(),
        uploadDocumentUseCase: serviceLocator(),
      ));

  //ElectionCandidateBloc
  serviceLocator.registerFactory(() => ElectionCandidateBloc(
      getElectionCandidatesUseCase: serviceLocator(),
      voteUseCase: serviceLocator()));

  //ChangePasswordBloc
  serviceLocator.registerFactory(() => ChangePasswordBloc(
      changePasswordUseCase: serviceLocator(),
      saveUserLoginDataUseCase: serviceLocator()));

  //SurveyBloc
  serviceLocator.registerFactory(() => SurveyBloc(
      reAuthenticateUseCase: serviceLocator(),
      getSurveyUseCase: serviceLocator(),
      submitQuestionSurveyUseCase: serviceLocator()));

  //SMSServiceInfBloc
  serviceLocator.registerFactory(() => SmsServiceInfoBloc(
      smsServiceInfoUseCase: serviceLocator(),
      reAuthenticateUseCase: serviceLocator(),
      subscribeSmsServiceUseCase: serviceLocator()));

  //BannerBlocBloc
  serviceLocator
      .registerFactory(() => BannerBloc(getBannerUseCase: serviceLocator()));

  //GetSubscriptionsBloc
  serviceLocator.registerFactory(() => SmsSubscriptionsBloc(
      getSmsSubscriptionUseCase: serviceLocator(),
      reAuthenticateUseCase: serviceLocator()));
  serviceLocator.registerFactory(() => CommitteManagementBloc(
      getCommitteesUseCase: serviceLocator(),
      reAuthenticateUseCase: serviceLocator()));
  serviceLocator.registerFactory(
    () => CommitterMemberBloc(
      getCommitteeMemberUseCase: serviceLocator(),
      reAuthenticateUseCase: serviceLocator(),
    ),
  );

  //Occasion Bloc
  serviceLocator.registerFactory(
    () => OccasionBloc(
      getOccasionUseCase: serviceLocator(),
      reAuthenticateUseCase: serviceLocator(),
    ),
  );
  //Event Bloc
  serviceLocator.registerFactory(
    () => EventBloc(
      getEventUseCase: serviceLocator(),
      reAuthenticateUseCase: serviceLocator(),
    ),
  );

  //Donation History Bloc
  serviceLocator.registerFactory(
    () => DonationHistoryBloc(
      donationHistoryUseCase: serviceLocator(),
    ),
  );
  //MySmsServiceBloc
  serviceLocator.registerFactory(() => MySmsServiceBloc(
        reAuthenticateUseCase: serviceLocator(),
        getMySubscriptionUseCase: serviceLocator(),
        getSMSServiceInfoUseCase: serviceLocator(),
        subscribeSmsServiceUseCase: serviceLocator(),
        renewSubscriptionUseCase: serviceLocator(),
      ));

//Wedding template Bloc
  serviceLocator.registerFactory(
    () => WeddingInviteTemplateBloc(
      weddingInviteTemplateUseCase: serviceLocator(),
    ),
  );

  //Prayer Bloc
  serviceLocator.registerFactory(
    () => PrayerBloc(
        getPrayerTimeLineUseCase: serviceLocator(),
        getUserLoginDataUseCase: serviceLocator(),
        reAuthenticateUseCase: serviceLocator(),
        setAlarmUseCase: serviceLocator()),
  );

  //Use Cases
  serviceLocator.registerLazySingleton<GetDonationTypeUseCase>(
      () => GetDonationTypeUseCase(donationRepo: serviceLocator()));

  serviceLocator.registerLazySingleton(
      () => GetUserSession(authRepository: serviceLocator()));
  serviceLocator.registerLazySingleton(
      () => LoginUseCase(authRepository: serviceLocator()));
  serviceLocator.registerLazySingleton(
      () => RegisterUserUseCase(authRepo: serviceLocator()));
  serviceLocator.registerLazySingleton(
      () => SendOtpUseCase(authRepository: serviceLocator()));
  serviceLocator.registerLazySingleton(
      () => WeddingBookingUseCase(authRepo: serviceLocator()));
  serviceLocator.registerLazySingleton(
      () => ChildRegistrationUseCase(authRepo: serviceLocator()));
  serviceLocator.registerLazySingleton(
      () => WeddingInviteUseCase(authRepo: serviceLocator()));
  serviceLocator.registerLazySingleton(
      () => VerifyOtpUseCase(authRepository: serviceLocator()));
  serviceLocator.registerLazySingleton(
      () => GiveDonationUseCase(donationRepo: serviceLocator()));
  serviceLocator.registerLazySingleton(
      () => GetElectionUseCase(electionRepo: serviceLocator()));
  serviceLocator.registerLazySingleton(
      () => ReAuthenticateUseCase(authRepository: serviceLocator()));
  serviceLocator.registerLazySingleton(
      () => GetUserLoginDataUseCase(authRepository: serviceLocator()));
  serviceLocator.registerLazySingleton(
      () => SaveUserLoginDataUseCase(authRepository: serviceLocator()));
  serviceLocator.registerLazySingleton(
      () => LogoutUseCase(authRepository: serviceLocator()));
  serviceLocator.registerLazySingleton(
      () => GetNotificationUseCase(notificationRepo: serviceLocator()));
  serviceLocator.registerLazySingleton(
      () => GetUserDocumentStatusUseCase(authRepository: serviceLocator()));
  serviceLocator.registerLazySingleton<GetElectionCandidatesUseCase>(
      () => GetElectionCandidatesUseCase(electionRepository: serviceLocator()));
  serviceLocator.registerLazySingleton<VoteUseCase>(
      () => VoteUseCase(electionRepository: serviceLocator()));
  serviceLocator.registerLazySingleton<EditProfileUseCase>(
      () => EditProfileUseCase(editProfileRepository: serviceLocator()));
  serviceLocator.registerLazySingleton(
      () => ChangePasswordUseCase(authRepository: serviceLocator()));
  serviceLocator.registerLazySingleton<UploadDocumentUseCase>(
      () => UploadDocumentUseCase(authRepository: serviceLocator()));
  serviceLocator.registerLazySingleton(
      () => GetSurveyUseCase(surveyRepository: serviceLocator()));
  serviceLocator.registerLazySingleton(
      () => SubmitQuestionSurveyUseCase(surveyRepository: serviceLocator()));
  serviceLocator.registerLazySingleton(
      () => GetSMSServiceInfoUseCase(smsServiceRepo: serviceLocator()));
  serviceLocator.registerLazySingleton<ArchiveNotificationUseCase>(
      () => ArchiveNotificationUseCase(notificationRepo: serviceLocator()));
  serviceLocator.registerLazySingleton<DeleteNotificationUseCase>(
      () => DeleteNotificationUseCase(notificationRepo: serviceLocator()));
  serviceLocator.registerLazySingleton<GetBannerUseCase>(
      () => GetBannerUseCase(homeRepo: serviceLocator()));
  serviceLocator.registerLazySingleton<GetSmsSubscriptionUseCase>(
      () => GetSmsSubscriptionUseCase(smsServiceRepo: serviceLocator()));
  serviceLocator.registerLazySingleton<GetCommitteesUseCase>(
      () => GetCommitteesUseCase(committeeRepo: serviceLocator()));
  serviceLocator.registerLazySingleton<GetCommitteeMemberUseCase>(
      () => GetCommitteeMemberUseCase(committeeRepo: serviceLocator()));
  serviceLocator.registerLazySingleton<GetOccasionUseCase>(
      () => GetOccasionUseCase(occasionRepo: serviceLocator()));
  serviceLocator.registerLazySingleton<GetEventUseCase>(
      () => GetEventUseCase(eventRepo: serviceLocator()));
  serviceLocator.registerLazySingleton(
      () => ChildActivityUseCase(childRepository: serviceLocator()));
  serviceLocator.registerLazySingleton(
      () => ChildDetailsUseCase(childRepository: serviceLocator()));
  serviceLocator.registerLazySingleton(
      () => DonationHistoryUseCase(donationHistoryRepo: serviceLocator()));
  serviceLocator.registerLazySingleton(
      () => GetMySubscriptionUseCase(smsServiceRepo: serviceLocator()));
  serviceLocator.registerLazySingleton(
      () => SubscribeSmsServiceUseCase(smsServiceRepo: serviceLocator()));
  serviceLocator.registerLazySingleton(
      () => WeddingTemplateUseCase(weddingTemplateRepo: serviceLocator()));
  serviceLocator.registerLazySingleton(
      () => RenewSubscriptionUseCase(smsServiceRepo: serviceLocator()));
  serviceLocator.registerLazySingleton(
      () => SaveUserDataUseCase(authRepository: serviceLocator()));
  serviceLocator.registerLazySingleton(
      () => GetPrayerTimeLineUseCase(prayerRepository: serviceLocator()));
  serviceLocator.registerLazySingleton(
      () => SetAlarmUseCase(prayerRepository: serviceLocator()));

  // Repositories
  serviceLocator.registerLazySingleton<AuthRepository>(
    () => AuthRepositoryImpl(
      networkInfo: serviceLocator(),
      userDataLocalDataSource: serviceLocator(),
      authDataSource: serviceLocator(),
    ),
  );
  serviceLocator.registerLazySingleton<ElectionRepository>(() =>
      ElectionRepositoryImpl(
          networkInfo: serviceLocator(),
          electionDataSource: serviceLocator(),
          userDataLocalDataSource: serviceLocator()));
  serviceLocator.registerLazySingleton<NotificationRepo>(() =>
      NotificationRepoImpl(
          userDataLocalDataSource: serviceLocator(),
          networkInfo: serviceLocator(),
          notificationDataSource: serviceLocator()));
  serviceLocator.registerLazySingleton<EditProfileRepository>(() =>
      EditProfileRepositoryImpl(
          networkInfo: serviceLocator(),
          editProfileDataSource: serviceLocator(),
          userDataLocalDataSource: serviceLocator()));
  serviceLocator.registerLazySingleton<SurveyRepository>(() =>
      SurveyRepositoryImpl(
          surveyDataSource: serviceLocator(),
          networkInfo: serviceLocator(),
          userDataLocalDataSource: serviceLocator()));
  serviceLocator.registerLazySingleton<HomeRepository>(() => HomeRepositoryImpl(
      networkInfo: serviceLocator(),
      homeDataSource: serviceLocator(),
      userDataLocalDataSource: serviceLocator()));
  serviceLocator.registerLazySingleton<SMSServiceRepo>(() => SMSServiceRepoImpl(
      networkInfo: serviceLocator(),
      smsServiceDataSource: serviceLocator(),
      userDataLocalDataSource: serviceLocator()));
  serviceLocator.registerLazySingleton<ChildRepository>(() =>
      ChildRepositoryImpl(
          networkInfo: serviceLocator(),
          childDataSource: serviceLocator(),
          userDataLocalDataSource: serviceLocator()));
  serviceLocator.registerLazySingleton<CommitteeRepo>(() => CommitteeRepoImpl(
      networkInfo: serviceLocator(),
      userDataLocalDataSource: serviceLocator(),
      committeeDataSource: serviceLocator()));
  serviceLocator.registerLazySingleton<OccasionRepo>(() => OccasionRepoImpl(
      occasionDateSource: serviceLocator(),
      networkInfo: serviceLocator(),
      userDataLocalDataSource: serviceLocator()));
  serviceLocator.registerLazySingleton<EventRepo>(() => EventRepoImpl(
      eventDataSource: serviceLocator(),
      networkInfo: serviceLocator(),
      userDataLocalDataSource: serviceLocator()));

  serviceLocator.registerLazySingleton<DonationHistoryRepo>(() =>
      DonationHistoryRepoImpl(
          donationHistoryDataSource: serviceLocator(),
          networkInfo: serviceLocator(),
          userDataLocalDataSource: serviceLocator()));
  serviceLocator.registerLazySingleton<DonationRepo>(() => DonationRepoImpl(
      donationDataSource: serviceLocator(),
      networkInfo: serviceLocator(),
      userDataLocalDataSource: serviceLocator()));

  serviceLocator.registerLazySingleton<WeddingTemplateRepo>(() =>
      WeddingInviteRepoImpl(
          weddingTemplateDataSource: serviceLocator(),
          networkInfo: serviceLocator(),
          userDataLocalDataSource: serviceLocator()));
  serviceLocator.registerLazySingleton<PrayerRepository>(() =>
      PrayerRepositoryImpl(
          prayerDataSource: serviceLocator(),
          networkInfo: serviceLocator(),
          userDataLocalDataSource: serviceLocator()));

  //Data Sources
  serviceLocator.registerLazySingleton<UserDataLocalDataSource>(
      () => UserDataLocalDataSourceImpl(sharedPreferences: serviceLocator()));
  serviceLocator.registerLazySingleton<AuthDataSource>(
      () => AuthDataSourceImpl(httpClient: serviceLocator()));
  serviceLocator.registerLazySingleton<NotificationDataSource>(
      () => NotificationDataSourceImpl(httpClient: serviceLocator()));
  serviceLocator.registerLazySingleton<ElectionDataSource>(
      () => ElectionDataSourceImpl(httpClient: serviceLocator()));
  serviceLocator.registerLazySingleton<EditProfileDataSource>(
      () => EditProfileDataSourceImpl(httpClient: serviceLocator()));
  serviceLocator.registerLazySingleton<SurveyDataSource>(
    () => SurveyDataSourceImpl(client: serviceLocator()),
  );
  serviceLocator.registerLazySingleton<HomeDataSourceImpl>(
    () => HomeDataSourceImpl(httpClient: serviceLocator()),
  );
  serviceLocator.registerLazySingleton<SMSServiceDataSource>(
    () => SMSServiceDataSourceImpl(client: serviceLocator()),
  );
  serviceLocator.registerLazySingleton<ChildDataSource>(
    () => ChildDataSourceImpl(httpClient: serviceLocator()),
  );
  serviceLocator.registerLazySingleton<CommitteeDataSource>(
      () => CommitteeDataSourceImpl(client: serviceLocator()));
  serviceLocator.registerLazySingleton<OccasionDateSource>(
      () => OccasionDataSourceImpl(client: serviceLocator()));
  serviceLocator.registerLazySingleton<EventDataSource>(
      () => EventDataSourceImpl(client: serviceLocator()));
  serviceLocator.registerLazySingleton<DonationHistoryDataSource>(
      () => DonationHistoryDataSourceImpl(httpClient: serviceLocator()));
  serviceLocator.registerLazySingleton<DonationDataSource>(
      () => DonationDataSourceImpl(client: serviceLocator()));
  serviceLocator.registerLazySingleton<WeddingInviteDataSource>(
      () => WeddingInviteDataSourceImpl(client: serviceLocator()));
  serviceLocator.registerLazySingleton<PrayerDataSource>(
      () => PrayerDataSourceImpl(httpClient: serviceLocator()));

  //Core
  serviceLocator.registerLazySingleton<NetworkInfo>(
      () => NetworkInfoImpl(serviceLocator()));

  //External
  final sharedPreferences = await SharedPreferences.getInstance();
  serviceLocator.registerLazySingleton(() => sharedPreferences);
  serviceLocator.registerLazySingleton(() => http.Client());
  serviceLocator.registerLazySingleton(() => DataConnectionChecker());
}
