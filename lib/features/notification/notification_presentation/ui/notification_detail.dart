import 'package:flutter/material.dart';
import 'package:mersal/core/widgets/common/CustomAppBar.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/features/notification/notification_data/model/ArchivedNotificationModel/ArchivedDataList.dart';
import 'package:mersal/resources/colors.dart';

class NotificationDetails extends StatefulWidget {
  // final ArchivedDataList archivedNotificationList;
  // NotificationDetails(this.archivedNotificationList);

  @override
  _NotificationDetailsState createState() => _NotificationDetailsState();
}

class _NotificationDetailsState extends State<NotificationDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
          title: "Notification Details", isBackButton: true, showText: true),
      body: Column(
        children: [
          Expanded(
            child: ListView(
              shrinkWrap: true,
              children: [
                Container(
                  margin: EdgeInsets.only(top: 50),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                          margin: EdgeInsets.only(left: 20),
                          child: Label(
                            title: "Demo Notification",
                            color: Color(appColors.blue00008B),
                            fontSize: 18,
                          )),
                      Center(
                        child: Container(
                          padding: EdgeInsets.only(top: 20),
                          child: FlutterLogo(
                            size: 200,
                          ),
                        ),
                      ),
                      Center(
                        child: Container(
                            margin: EdgeInsets.only(top: 30),
                            child: Label(
                              title: "Dummy description of ui.notification",
                              color: Color(appColors.blue00008B),
                              fontSize: 18,
                            )),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget commonDetailsRow(String detailTitle, String detailValue) {
    return Container(
      padding: EdgeInsets.only(bottom: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Label(
              title: detailTitle,
              color: Color(appColors.blue007D9E),
              fontSize: 18,
            ),
          ),
          Container(
            margin: EdgeInsets.only(right: 20),
            child: Label(
              title: detailValue,
              color: Color(appColors.blue00008B),
              fontSize: 18,
            ),
          ),
          // Icon(Icons.chevron_right_outlined)
        ],
      ),
    );
  }
}
