part of 'doc_upload_bloc.dart';

abstract class DocUploadEvent extends Equatable {
  const DocUploadEvent();
}

class SoftLoginEvent extends DocUploadEvent {
  @override
  List<Object> get props => [];
}

class UploadDocEvent extends DocUploadEvent {
  final bool isNewAccount;
  final String frontImage, backImage, name, dob, expDate, idNumber;

  UploadDocEvent(
      {@required this.isNewAccount,
      @required this.frontImage,
      @required this.backImage,
      @required this.name,
      @required this.dob,
      @required this.expDate,
      @required this.idNumber});

  @override
  List<Object> get props =>
      [frontImage, backImage, name, dob, expDate, idNumber];
}

class OnDocSelectedEvent extends DocUploadEvent {
  final String frontImage, backImage;

  OnDocSelectedEvent({this.frontImage, this.backImage});

  @override
  List<Object> get props => [frontImage, backImage];
}
