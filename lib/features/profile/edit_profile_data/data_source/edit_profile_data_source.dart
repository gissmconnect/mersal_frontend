import 'package:mersal/features/auth/auth_data/model/user_data_model.dart';
import 'package:meta/meta.dart';

abstract class EditProfileDataSource {
  Future<UserDataModel> editProfile(
      {@required String token,
      @required String name,
      @required String image,
      @required String dob,
      @required int gender,
      @required String bloodGroup});
}
