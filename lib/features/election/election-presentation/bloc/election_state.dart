part of 'election_bloc.dart';

abstract class ElectionState extends Equatable {}

class ElectionInitial extends ElectionState{
  @override
  List<Object> get props => [];
}

//Election Loading
class ElectionLoading extends ElectionState {
  @override
  List<Object> get props => [];
}

class ElectionLoaded extends ElectionState {
  final List<ElectionDataEntity> elections;

  ElectionLoaded({@required this.elections});

  @override
  List<Object> get props => [elections];
}

class ElectionLoadFiled extends ElectionState {
  final Failure failure;

  ElectionLoadFiled({this.failure});

  @override
  List<Object> get props => [failure];
}