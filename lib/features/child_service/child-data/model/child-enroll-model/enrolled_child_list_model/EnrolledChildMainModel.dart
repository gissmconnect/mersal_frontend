import 'package:mersal/features/child_service/child-data/model/child-enroll-model/enrolled_child_list_model/EnrolledChildDataModel.dart';

class EnrolledChildMainModel {
  List<EnrolledChildDataModel> enrolledChildList;
  bool status;
  String age;
  String email;
  String phone;
  String class_name;

  EnrolledChildMainModel(
      {this.enrolledChildList,
      this.status,
      this.phone,
      this.email,
      this.class_name,
      this.age});

  factory EnrolledChildMainModel.fromJson(Map<String, dynamic> json) {
    return EnrolledChildMainModel(
      enrolledChildList: json['data'] != null
          ? (json['data'] as List)
              .map((i) => EnrolledChildDataModel.fromJson(i))
              .toList()
          : null,
      status: json['status'],
      age: json['age'],
      email: json['email'],
      class_name: json['class_name'],
      phone: json['phone'],
    );
  }
}
