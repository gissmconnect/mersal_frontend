import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/auth/auth_domain/repository/auth_repository.dart';
import 'package:meta/meta.dart';

class WeddingBookingUseCase extends UseCase<void, WeddingParams> {

  final AuthRepository authRepo;

  WeddingBookingUseCase({@required this.authRepo});

  @override
  Future<Either<Failure, void>> call(WeddingParams params) async {
    return await authRepo.weddingBooking(
        name: params.name, date: params.date, time: params.time);
  }
}

class WeddingParams {
  final String name, date,time;

  WeddingParams({@required this.name, @required this.date, @required this.time});
}
