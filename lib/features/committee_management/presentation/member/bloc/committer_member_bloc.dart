import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/reauthenticate_usecase.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/committee_management/domain/entity/committee_member_entity.dart';
import 'package:mersal/features/committee_management/domain/usecases/get_committer_member_usecase.dart';
import 'package:meta/meta.dart';

part 'committer_member_event.dart';

part 'committer_member_state.dart';

class CommitterMemberBloc
    extends Bloc<CommitterMemberEvent, CommitterMemberState> {
  final GetCommitteeMemberUseCase getCommitteeMemberUseCase;
  final ReAuthenticateUseCase reAuthenticateUseCase;

  CommitterMemberBloc(
      {@required this.getCommitteeMemberUseCase,
      @required this.reAuthenticateUseCase})
      : super(CommitterMemberInitial());

  @override
  Stream<CommitterMemberState> mapEventToState(
    CommitterMemberEvent event,
  ) async* {
    if (event is GetCommitteeMemberEvent) {
      yield CommitteeMemberLoadingState();
      final _response = await getCommitteeMemberUseCase(event.committeeId);
      yield* _response.fold((l) async* {
        if (l is AuthFailure) {
          final _reauthenticateResponse = await reAuthenticateUseCase(NoParams());
          _reauthenticateResponse.fold((l) async* {
            yield CommitteeMemberLoadFailureState(failure: l);
          }, (r) => add(GetCommitteeMemberEvent(committeeId: event.committeeId)));
        } else {
          yield CommitteeMemberLoadFailureState(failure: l);
        }
      }, (r) async* {
        yield CommitteeMemberLoadedState(member: r);
      });
    }
  }
}
