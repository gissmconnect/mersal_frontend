import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mersal/resources/fonts.dart';

class InputField extends StatefulWidget {
  final TextInputType inputType;
  final String hint;
  final ValueChanged<String> validator;
  final ValueChanged<String> onSubmitted;
  final ValueChanged<String> onChanged;
  final TextEditingController controller;
  final FocusNode focusNode;
  final TextInputAction textInputAction;
  final bool enabled;
  final bool obscureText;
  final int maxLines;
  final int maxLength;
  final TextCapitalization textCapitalization;
  final TextInputFormatter inputFormatter;

  @override
  _InputFieldState createState() => _InputFieldState();

  InputField(
      {this.inputType = TextInputType.text,
      this.hint = "",
      this.controller,
      this.validator,
      this.enabled = true,
      this.focusNode,
      this.textInputAction = TextInputAction.next,
      this.onSubmitted,
      this.onChanged,
      this.obscureText = false,
      this.maxLines = 1,
      this.maxLength,
      this.inputFormatter,
      this.textCapitalization = TextCapitalization.sentences});
}

class _InputFieldState extends State<InputField> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      textCapitalization: widget.textCapitalization,
      enabled: widget.enabled,
      keyboardType: widget.inputType,
      textInputAction: widget.textInputAction,
      maxLines: widget.maxLines,
      maxLength: widget.maxLength,
      // textAlign: TextAlign.center,
      inputFormatters:
          widget.inputFormatter != null ? [widget.inputFormatter] : null,
      style: TextStyle(
          color: Colors.black,
          fontSize: 16,
          fontFamily: fonts.segoeui,
          fontWeight: FontWeight.w400),
      focusNode: widget.focusNode,
      controller: widget.controller,
      onFieldSubmitted: widget.onSubmitted,
      obscureText: widget.obscureText,
      validator: widget.validator,
      onChanged: widget.onChanged,
      decoration: InputDecoration(
        filled: true,
        counterText: "",
        fillColor: Colors.white,
        hintText: widget.hint,
        hintStyle: TextStyle( color: Colors.black,),
        contentPadding:
            const EdgeInsets.only(left: 20.0, bottom: 15.0, top: 15.0,right: 10),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.white),
          borderRadius: BorderRadius.circular(25.7),
        ),
        errorStyle: TextStyle(
          fontSize: 16.0,
          color: Colors.red,
        ),
        errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(25.0),
            borderSide: BorderSide(
              color: Colors.red,
            )),
        focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(25.0),
            borderSide: BorderSide(
              color: Colors.red,
            )),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.white),
          borderRadius: BorderRadius.circular(25.7),
        ),
      ),

      /* decoration: InputDecoration(
          border: OutlineInputBorder(),
          // hintText: widget.hint,
          hintStyle: TextStyle(
              color: Colors.black54,
              fontWeight: FontWeight.w500),
          focusedBorder: OutlineInputBorder(
            borderRadius:
            BorderRadius.all(Radius.circular(10)),
            borderSide:
            BorderSide(width: 1, color: Colors.white),
          ),
          focusedErrorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(6.0),
              borderSide: BorderSide(
                color: Colors.red,
              )),
          errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(6.0),
              borderSide: BorderSide(
                color: Colors.red,
              )),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10.0),
              borderSide: BorderSide(color: Colors.white)),
          labelText: widget.hint,
          labelStyle: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w500),
        ) */
    );
  }
}
