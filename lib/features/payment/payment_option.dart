import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/features/donation/donation-presentation/ui/donation_payment_screen.dart';
import 'package:mersal/features/payment/payment_gateway.dart';
import 'package:mersal/resources/colors.dart';

enum SingingCharacter { Debit, Credit }

class PaymentOptions extends StatefulWidget {
  final String from;

  PaymentOptions(this.from);

  @override
  _PaymentOptionsState createState() => _PaymentOptionsState();
}

class _PaymentOptionsState extends State<PaymentOptions> {

  bool isSwitched = false, selectedCard = false;
  SingingCharacter _character = SingingCharacter.Debit;
  final amountController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Container(
          height: MediaQuery.of(context).size.height,
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.only(top: 20.0, left: 30, bottom: 20),
                  child: Label(
                    title: "Amount",
                    fontSize: 18,
                    color: Colors.grey,
                  ),
                ),
                Center(
                  child: Container(
                    width: 250,
                    child: TextFormField(
                      controller: amountController,
                      validator: (text) {
                        return text.isEmpty ? 'Amount cannot be null' : null;
                      },
                      keyboardType: TextInputType.phone,
                      maxLength: 5,
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      margin: const EdgeInsets.only(top: 40.0, left: 30),
                      child: Label(
                        title: "Pay With",
                        fontSize: 18,
                        color: Colors.grey,
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 40.0, right: 30),
                      child: Label(
                        title: "Amount",
                        fontSize: 20,
                        fontWeight: FontWeight.w700,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          selectedCard = true;
                          setState(() {});
                        },
                        child: Container(
                          height: 90,
                          alignment: Alignment.center,
                          padding: EdgeInsets.only(left: 10, right: 10),
                          margin:
                              EdgeInsets.only(top: 40.0, right: 10, left: 20),
                          decoration: BoxDecoration(
                              color: selectedCard
                                  ? Color(appColors.blue)
                                  : Colors.grey,
                              borderRadius: BorderRadius.circular(10)),
                          child: Label(
                            title: "Omani issue Debit Card",
                            textAlign: TextAlign.center,
                            fontSize: 15,
                            color: selectedCard
                                ? Colors.white
                                : Colors.grey.shade200,
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          selectedCard = false;
                          setState(() {});
                        },
                        child: Container(
                          height: 90,
                          alignment: Alignment.center,
                          padding: EdgeInsets.only(left: 10, right: 10),
                          margin:
                              EdgeInsets.only(top: 40.0, right: 20, left: 10),
                          decoration: BoxDecoration(
                              color: !selectedCard
                                  ? Color(appColors.blue)
                                  : Colors.grey,
                              borderRadius: BorderRadius.circular(10)),
                          child: Label(
                            title: "Omani issue Credit Card",
                            textAlign: TextAlign.center,
                            fontSize: 15,
                            color: !selectedCard
                                ? Colors.white
                                : Colors.grey.shade200,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Container(
                  margin: const EdgeInsets.only(top: 30),
                  child: Row(
                    children: [
                      Expanded(
                        child: Container(
                          margin: const EdgeInsets.only(left: 20, right: 20),
                          child: Label(
                              title: "Save Card For Future Transactions",
                              fontSize: 16,
                              color: Color(appColors.buttonColor)),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(right: 10),
                        child: FlutterSwitch(
                          width: 100.0,
                          height: 40.0,
                          valueFontSize: 15.0,
                          toggleSize: 30.0,
                          value: isSwitched,
                          borderRadius: 25.0,
                          padding: 8.0,
                          showOnOff: true,
                          onToggle: (val) {
                            setState(() {
                              isSwitched = val;
                            });
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                Center(
                  child: GestureDetector(
                    onTap: () {
                      widget.from!="donation"?
                      CommonMethods().animatedNavigation(
                          context: context,
                          currentScreen: PaymentOptions(""),
                          landingScreen: PaymentGateWay(widget.from)):
                      CommonMethods().animatedNavigation(
                          context: context,
                          currentScreen: PaymentOptions(""),
                          landingScreen: DonationPaymentGateWay());
                    },
                    child: Container(
                      margin: const EdgeInsets.only(top: 40, bottom: 20),
                      height: 60,
                      width: 250,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          color: Color(appColors.buttonColor),
                          borderRadius: BorderRadius.circular(30)),
                      child: Label(
                        title: "Pay Now",
                        color: Colors.white,
                        fontSize: 18,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget paymentCardLayout() {
    return Row(
      children: [
        Expanded(child: commonRadioButton("Good", SingingCharacter.Debit)),
        Container(
          margin: EdgeInsets.only(right: 25),
          child: Label(
            title: "OMR1",
            fontSize: 22,
            fontWeight: FontWeight.w600,
            color: Colors.black,
          ),
        ),
      ],
    );
  }

  Widget commonRadioButton(
      String firstString, SingingCharacter singingCharacter) {
    return ListTile(
      title: Text(firstString),
      leading: Radio<SingingCharacter>(
        value: singingCharacter,
        groupValue: _character,
        onChanged: (SingingCharacter value) {
          setState(() {
            _character = value;
          });
        },
      ),
    );
  }
}
