part of 'committer_member_bloc.dart';

abstract class CommitterMemberState extends Equatable {
  @override
  List<Object> get props => [];
}

class CommitterMemberInitial extends CommitterMemberState {}

class CommitteeMemberLoadingState extends CommitterMemberState {}

class CommitteeMemberLoadedState extends CommitterMemberState {
  final List<CommitteeMemberEntity> member;

  CommitteeMemberLoadedState({@required this.member});

  @override
  List<Object> get props => [member];
}

class CommitteeMemberLoadFailureState extends CommitterMemberState {
  final Failure failure;

  CommitteeMemberLoadFailureState({@required this.failure});
}
