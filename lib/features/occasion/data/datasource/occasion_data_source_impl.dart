import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:mersal/core/error/exceptions.dart';
import 'package:mersal/core/network/api_endpoints.dart';
import 'package:mersal/core/network/http_call.dart';
import 'package:mersal/core/network/http_error_handler.dart';
import 'package:mersal/features/occasion/data/datasource/occasion_data_source.dart';
import 'package:mersal/features/occasion/data/model/occasion_model.dart';
import 'package:mersal/features/occasion/domain/entity/occasion_entity.dart';
import 'package:meta/meta.dart';

class OccasionDataSourceImpl implements OccasionDateSource {
  final http.Client client;

  OccasionDataSourceImpl({@required this.client});

  @override
  Future<OccasionEntity> getOccasion({String token, String date}) async {
    final _response = await httpGetRequest(
        httpClient: client,
        url: OCCASION_ENDPOINT + "?date=" + date,
        token: token);
    if (_response.statusCode == 200) {
      final _serviceResponse = json.decode(_response.body);
      if (_serviceResponse['data'] != null && _serviceResponse['data'].length>0) {
        return OccasionModel.fromJson(_serviceResponse);
      } else {
        throw ServerException()
          ..message = _serviceResponse['message'] ?? 'No Occasion';
      }
    } else {
      handleError(_response);
    }
  }
}
