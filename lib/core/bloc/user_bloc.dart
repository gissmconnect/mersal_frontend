import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/network/vo/Resource.dart';
import 'package:mersal/core/usecases/get_user_document_status_usecase.dart';
import 'package:mersal/core/usecases/get_user_login_data_usecase.dart';
import 'package:mersal/core/usecases/logout_usecase.dart';
import 'package:mersal/core/usecases/reauthenticate_usecase.dart';
import 'package:mersal/core/usecases/save_user_data_usecase.dart';
import 'package:mersal/core/usecases/save_user_login_data_usecase.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/auth/auth_domain/usecases/get_user_session.dart';
import 'package:mersal/features/profile/edit_profile_domain/usecases/edit_profile_use_case.dart'
    as editProfile;
import 'package:meta/meta.dart';

part 'user_event.dart';

part 'user_state.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  final LogoutUseCase logoutUseCase;
  final ReAuthenticateUseCase reAuthenticateUseCase;
  final GetUserLoginDataUseCase getUserLoginDataUseCase;
  final GetUserDocumentStatusUseCase getUserDocumentStatusUseCase;
  final GetUserSession getUserSession;
  final editProfile.EditProfileUseCase editProfileUseCase;
  final SaveUserDataUseCase saveUserDataUseCase;

  UserBloc(
      {@required this.logoutUseCase,
      @required this.reAuthenticateUseCase,
      @required this.getUserLoginDataUseCase,
      @required this.getUserDocumentStatusUseCase,
      @required this.getUserSession,
      @required this.editProfileUseCase,
      @required this.saveUserDataUseCase})
      : super(UserState.initial());

  @override
  Stream<UserState> mapEventToState(
    UserEvent event,
  ) async* {
    if (event is LogoutEvent) {
      yield state.copy(logoutResource: Resource.loading());
      final _logoutResponse = await logoutUseCase(NoParams());
      yield* _logoutResponse.fold(
        (failure) async* {
          if (failure is AuthFailure) {
            final _loginData = await reAuthenticateUseCase(NoParams());
            yield* _loginData.fold(
              (l) async* {
                yield state.copy(logoutResource: Resource.error(failure: l));
              },
              (r) async* {
                add(LogoutEvent());
              },
            );
          } else {
            yield state.copy(logoutResource: Resource.error(failure: failure));
          }
        },
        (success) async* {
          yield state.copy(logoutResource: Resource.success());
        },
      );
    }
    if (event is VerifyDocumentStatusEvent) {
      yield state.copy(documentVerificationResource: Resource.loading());
      final _verificationResponse =
          await getUserDocumentStatusUseCase(NoParams());
      yield* _verificationResponse.fold(
        (l) async* {
          yield state.copy(
              documentVerificationResource: Resource.error(failure: l));
        },
        (r) async* {
          yield state.copy(
              documentVerificationResource: Resource.success(data: r));
        },
      );
    }

    if (event is GetUserSessionEvent) {
      yield state.copy(userDataResource: Resource.loading());
      final _userData = await getUserSession(NoParams());
      yield* _userData.fold(
        (l) async* {
          yield state.copy(userDataResource: Resource.error(failure: l));
        },
        (r) async* {
          yield state.copy(userDataResource: Resource.success(data: r));
        },
      );
    }

    if (event is UpdateProfileEvent) {
      yield state.copy(profileResource: Resource.loading());
      final _response = await editProfileUseCase(
        editProfile.Params(
            name: event.name,
            dob: event.dob,
            bloodGroup: event.bloodGroup,
            image: event.image,
            gender: event.gender),
      );
      yield* _response.fold(
        (l) async* {
          yield state.copy(profileResource: Resource.error(failure: l));
        },
        (r) async* {
          add(GetUserSessionEvent());
          yield state.copy(profileResource: Resource.success());
        },
      );
    }
  }
}
