import 'package:mersal/features/auth/auth_data/model/user_data_model.dart';
import 'package:meta/meta.dart';

abstract class AuthDataSource {
  Future<UserLoginModel> login(
      {@required String phoneNumber, @required String password, @required String device_token});

  Future<void> register(
      {@required String phoneNumber,
      @required String password,
      @required String gender,
      @required String userName});

  Future<String> sendOtp({@required String phoneNumber});

  Future<void> weddingBooking({
    @required String name,
    @required String date,
    @required String time,
  });

  Future<void> childRegistration({@required String name, @required String age});

  Future<void> weddingInvite(
      {@required String invitationType,
      @required String date,
      @required String time});

  Future<void> notification({@required String service_id});

  Future<void> verifyOtp({@required String phoneNumber, @required String otp});

  Future<void> logout({@required String token});

  Future<void> changePassword(
      {@required String token,
      @required String oldPassword,
      @required String newPassword});

  Future<void> uploadDocument(
      {@required String token,
      @required String frontImage,
      @required String backImage,
      @required String name,
      @required String dob,
      @required String expDate,
      @required String idNumber});
}
