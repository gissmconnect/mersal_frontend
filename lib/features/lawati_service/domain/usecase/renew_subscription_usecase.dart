import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/lawati_service/domain/repository/sms_service_repo.dart';
import 'package:meta/meta.dart';

class RenewSubscriptionUseCase extends UseCase<String, int> {
  final SMSServiceRepo smsServiceRepo;

  RenewSubscriptionUseCase({@required this.smsServiceRepo});

  @override
  Future<Either<Failure, String>> call(int params) async {
    return await smsServiceRepo.renewSubscription(subscriptionId: params);
  }
}
