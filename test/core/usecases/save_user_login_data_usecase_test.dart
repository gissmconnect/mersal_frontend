import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mersal/core/usecases/save_user_login_data_usecase.dart';
import 'package:mockito/mockito.dart';

import '../../feature/auth/auth_domain/usecases/get_user_session_test.dart';

void main() {
  MockAuthRepository _mockAuthRepository;
  SaveUserLoginDataUseCase _saveUserLoginDataUseCase;

  setUp(() {
    _mockAuthRepository = MockAuthRepository();
    _saveUserLoginDataUseCase =
        SaveUserLoginDataUseCase(authRepository: _mockAuthRepository);
  });

  final _loginId = 'test';
  final _password = 'test';
  test('should save login data', () async {
    //arrange
    when(
      _mockAuthRepository.login(
        phoneNumber: anyNamed('phoneNumber'),
        password: anyNamed('password'),
      ),
    ).thenAnswer((realInvocation) async => Right(null));

    //act
    await _saveUserLoginDataUseCase(
      LoginParams(loginId: _loginId, password: _password),
    );

    //assert
    verify(
      _mockAuthRepository.login(
        phoneNumber: _loginId, password: _password,
      ),
    );
  });
}
