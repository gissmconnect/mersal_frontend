import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/features/auth/auth_domain/usecases/wedding_invite_usecase.dart';
import 'package:meta/meta.dart';

part 'wedding_invite_event.dart';

part 'wedding_invite_state.dart';

class WeddingInviteBloc extends Bloc<WeddingInvitationEvent, WeddingInviteState> {
  final WeddingInviteUseCase weddingInviteUseCase;

  WeddingInviteBloc({@required this.weddingInviteUseCase}) : super(WeddingInviteInitial());

  @override
  Stream<WeddingInviteState> mapEventToState(
      WeddingInvitationEvent event,
      ) async* {
    if (event is WeddingInvitationEvent) {
      yield WeddingInviting();

      final result = await weddingInviteUseCase(
          WeddingInviteParams(invitationType:event.invitationType,date: event.date, time: event.time));

      yield* result.fold((l) async* {
        yield WeddingInviteError(l);
      }, (r) async* {
        yield WeddingInviteSuccess();
      });
    }
  }

  @override
  WeddingInviteState get initialState => WeddingInviteInitial();
}
