import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/exceptions.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/network/network_info.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/occasion/data/datasource/occasion_data_source.dart';
import 'package:mersal/features/occasion/domain/entity/occasion_entity.dart';
import 'package:mersal/features/occasion/domain/repository/occasion_repo.dart';
import 'package:meta/meta.dart';

class OccasionRepoImpl implements OccasionRepo {
  final OccasionDateSource occasionDateSource;
  final NetworkInfo networkInfo;
  final UserDataLocalDataSource userDataLocalDataSource;

  OccasionRepoImpl(
      {@required this.occasionDateSource,
      @required this.networkInfo,
      @required this.userDataLocalDataSource});

  @override
  Future<Either<Failure, OccasionEntity>> getOccasion({String date}) async {
    if (await networkInfo.isConnected) {
      final _token = await userDataLocalDataSource.getAuthToken();
      try {
        final _service =
            await occasionDateSource.getOccasion(token: _token, date: date);
        return Right(_service);
      } on ServerException catch (ex) {
        return Left(ServerFailure()..message = ex.message);
      } on AuthException catch (ex) {
        return Left(AuthFailure()..message = ex.message);
      } on Exception {
        return Left(ServerFailure());
      }
    } else {
      return Left(NetworkFailure());
    }
  }
}
