import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mersal/core/localization/app_localization.dart';
import 'package:mersal/core/network/vo/Status.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/CustomAppBar.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/core/widgets/widgets.dart';
import 'package:mersal/features/notification/notification_domain/entity/notification_entity.dart';
import 'package:mersal/features/notification/notification_presentation/bloc/notification_bloc.dart';
import 'package:mersal/features/notification/notification_presentation/ui/archivedNotification.dart';
import 'package:mersal/features/notification/notification_presentation/ui/notification_detail.dart';
import 'package:mersal/injection_container.dart';
import 'package:mersal/resources/colors.dart';

enum PopUpValue { Archive, Delete }

class NotificationScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => serviceLocator<NotificationBloc>(),
      child: Scaffold(
        appBar: CustomAppBar(
            title: AppLocalizations.of(context)
                .translate("Notifications"), isBackButton: true, showText: true),
        body: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/bg_image.jpg"),
                fit: BoxFit.fill,
              ),
            ),
            child: _NotificationScreenBody()),
      ),
    );
  }
}

class _NotificationScreenBody extends StatefulWidget {
  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<_NotificationScreenBody>
    with SingleTickerProviderStateMixin {
  String result = "", _selection = "";
  TabController _tabController;

  FToast fToast;

  @override
  void initState() {
    _tabController = TabController(length: 2, vsync: this);
    BlocProvider.of<NotificationBloc>(context).add(NotificationsEvent());
    super.initState();
    fToast = FToast();
    fToast.init(context);
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(left: 20, right: 20, top: 20),
            height: 45,
            decoration: BoxDecoration(
              color: Colors.grey[300],
              borderRadius: BorderRadius.circular(
                25.0,
              ),
            ),
            child: TabBar(
              controller: _tabController,
              // give the indicator a decoration (color and border radius)
              indicator: BoxDecoration(
                borderRadius: BorderRadius.circular(
                  25.0,
                ),
                color: Color(appColors.buttonColor),
              ),
              labelColor: Colors.white,
              unselectedLabelColor: Colors.black,
              tabs: [
                // first tab [you can add an icon using the icon property]
                Tab(
                  text:AppLocalizations.of(context)
                      .translate("Notifications"),
                ),
                // second tab [you can add an icon using the icon property]
                Tab(
                  text: AppLocalizations.of(context)
                      .translate("Archive"),
                ),
              ],
            ),
          ),
          Expanded(
            child: TabBarView(
              controller: _tabController,
              children: [
                MultiBlocListener(
                  listeners: [
                    BlocListener<NotificationBloc, NotificationState>(
                      listenWhen: (oldState, newState) {
                        return oldState.getNotificationResource == null ||
                            oldState.getNotificationResource.status !=
                                newState.getNotificationResource.status;
                      },
                      listener: (context, state) {
                        if (state.getNotificationResource.status ==
                            STATUS.ERROR) {
                          CommonMethods.showToast(
                              fToast: fToast,
                              message:
                                  state.getNotificationResource.failure.message,
                              status: false);
                        }
                      },
                    ),
                    BlocListener<NotificationBloc, NotificationState>(
                      listenWhen: (oldState, newState) {
                        return oldState.deleteNotificationResource == null ||
                            oldState.deleteNotificationResource.status !=
                                newState.deleteNotificationResource.status;
                      },
                      listener: (context, state) {
                        if (state.deleteNotificationResource != null) {
                          if (state.deleteNotificationResource.status ==
                              STATUS.LOADING) {
                            showProgressDialog(context);
                          } else {
                            Navigator.of(context).pop();
                            if (state.deleteNotificationResource.status ==
                                STATUS.ERROR) {
                              CommonMethods.showToast(
                                  fToast: fToast,
                                  message: state.deleteNotificationResource
                                      .failure.message,
                                  status: false);
                            }
                            if (state.deleteNotificationResource.status ==
                                STATUS.SUCCESS) {
                              CommonMethods.showToast(
                                  fToast: fToast,
                                  message:
                                      state.deleteNotificationResource.data,
                                  status: true);
                            }
                          }
                        }
                      },
                    ),
                    BlocListener<NotificationBloc, NotificationState>(
                      listenWhen: (oldState, newState) {
                        return oldState.archiveNotificationResource == null ||
                            oldState.archiveNotificationResource.status !=
                                newState.archiveNotificationResource.status;
                      },
                      listener: (context, state) {
                        if (state.archiveNotificationResource != null) {
                          if (state.archiveNotificationResource.status ==
                              STATUS.LOADING) {
                            showProgressDialog(context);
                          } else {
                            Navigator.of(context).pop();
                            if (state.archiveNotificationResource.status ==
                                STATUS.ERROR) {
                              CommonMethods.showToast(
                                  fToast: fToast,
                                  message: state.archiveNotificationResource
                                      .failure.message,
                                  status: false);
                            }
                            if (state.archiveNotificationResource.status ==
                                STATUS.SUCCESS) {
                              CommonMethods.showToast(
                                  fToast: fToast,
                                  message:
                                      state.archiveNotificationResource.data,
                                  status: true);
                            }
                          }
                        }
                      },
                    )
                  ],
                  child: BlocBuilder<NotificationBloc, NotificationState>(
                      builder: (context, state) {
                    if (state.getNotificationResource.status ==
                        STATUS.LOADING) {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    } else if (state.getNotificationResource.status ==
                        STATUS.SUCCESS) {
                      final _notifications = state.getNotificationResource.data
                          as List<NotificationEntity>;
                      return ListView.builder(
                        padding: EdgeInsets.zero,
                        shrinkWrap: true,
                        itemCount: _notifications.length,
                        itemBuilder: (context, int index) {
                          return notificationListItem(
                              notification: _notifications[index]);
                        },
                      );
                    }
                    return Container();
                  }),
                ),
                ArchivedNotification()
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget notificationListItem({@required NotificationEntity notification}) {
    return GestureDetector(
      onTap: () {
        CommonMethods().animatedNavigation(
            context: context,
            landingScreen: NotificationDetails(),
            currentScreen: NotificationScreen());
      },
      child: Container(
        margin: EdgeInsets.only(top: 20, left: 20, right: 20),
        padding: EdgeInsets.only(bottom: 20, left: 10),
        decoration: new BoxDecoration(
            borderRadius: BorderRadius.circular(22), color: Colors.white),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Row(
                    children: [
                      Container(
                        padding: EdgeInsets.only(left: 10, top: 10, bottom: 10),
                        margin: EdgeInsets.only(left: 15, top: 20, bottom: 20),
                        width: 45,
                        height: 45,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.grey.shade300),
                      ),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(left: 10),
                          child: Label(
                            title: notification.title,
                            color: Color(appColors.buttonColor),
                            fontSize: 16,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                PopupMenuButton<PopUpValue>(
                  onSelected: (PopUpValue result) {
                    BlocProvider.of<NotificationBloc>(context).add(
                        result == PopUpValue.Archive
                            ? ArchiveNotificationEvent(id: notification.id)
                            : DeleteNotificationEvent(id: notification.id));
                  },
                  itemBuilder: (BuildContext context) =>
                      <PopupMenuEntry<PopUpValue>>[
                    const PopupMenuItem<PopUpValue>(
                      value: PopUpValue.Archive,
                      child: Text('Archive'),
                    ),
                    const PopupMenuItem<PopUpValue>(
                      value: PopUpValue.Delete,
                      child: Text('Delete'),
                    ),
                  ],
                )
              ],
            ),
            Container(
              margin: EdgeInsets.only(left: 20,right: 20),
              child: Label(
                title: notification.text,
                color: Color(appColors.buttonColor),
                fontSize: 14,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
