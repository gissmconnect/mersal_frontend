part of 'event_bloc.dart';

abstract class EventState extends Equatable {
  const EventState();
}

class EventInitial extends EventState {
  @override
  List<Object> get props => [];
}

class EventLoadingState extends EventState {
  @override
  List<Object> get props => [];
}

class EventLoadedState extends EventState {
  final EventEntity event;

  EventLoadedState({@required this.event});

  @override
  List<Object> get props => [event];
}

class EventFailureState extends EventState {
  final Failure failure;

  EventFailureState({@required this.failure});

  @override
  List<Object> get props => [failure];
}
