import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/network/vo/Resource.dart';
import 'package:mersal/core/usecases/reauthenticate_usecase.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/lawati_service/domain/usecase/get_my_subscription_usecase.dart';
import 'package:mersal/features/lawati_service/domain/usecase/get_sms_info_usecase.dart';
import 'package:mersal/features/lawati_service/domain/usecase/renew_subscription_usecase.dart';
import 'package:mersal/features/lawati_service/domain/usecase/subscribe_sms_subscription_usecase.dart';
import 'package:meta/meta.dart';

part 'my_sms_service_event.dart';

part 'my_sms_service_state.dart';

class MySmsServiceBloc extends Bloc<MySmsServiceEvent, MySmsServiceState> {
  final GetMySubscriptionUseCase getMySubscriptionUseCase;
  final ReAuthenticateUseCase reAuthenticateUseCase;
  final GetSMSServiceInfoUseCase getSMSServiceInfoUseCase;
  final SubscribeSmsServiceUseCase subscribeSmsServiceUseCase;
  final RenewSubscriptionUseCase renewSubscriptionUseCase;

  MySmsServiceBloc(
      {@required this.reAuthenticateUseCase,
      @required this.getMySubscriptionUseCase,
      @required this.getSMSServiceInfoUseCase,
      @required this.subscribeSmsServiceUseCase,
      @required this.renewSubscriptionUseCase})
      : super(MySmsServiceState.initial());

  @override
  Stream<MySmsServiceState> mapEventToState(
    MySmsServiceEvent event,
  ) async* {
    if (event is GetMyServiceEvent) {
      print('GetMyServiceEvent called');
      state.copy(mySubscriptionInfo: Resource.loading());
      final _response = await getMySubscriptionUseCase(NoParams());
      yield* _response.fold(
        (l) async* {
          if (l is AuthFailure) {
            final _reauthenticateResponse =
                await reAuthenticateUseCase(NoParams());
            _reauthenticateResponse.fold((l) async* {
              yield state.copy(mySubscriptionInfo: Resource.error(failure: l));
            }, (r) => add(GetMyServiceEvent()));
          } else {
            yield state.copy(mySubscriptionInfo: Resource.error(failure: l));
          }
        },
        (r) async* {
          print('bloc sub $r');
          yield state.copy(mySubscriptionInfo: Resource.success(data: r));
        },
      );
    }
    if (event is GetServiceInfoEvent) {
      state.copy(serviceInfoResource: Resource.loading());
      final _response = await getSMSServiceInfoUseCase(NoParams());
      yield* _response.fold(
        (l) async* {
          if (l is AuthFailure) {
            final _reauthenticateResponse =
                await reAuthenticateUseCase(NoParams());
            _reauthenticateResponse.fold((l) async* {
              yield state.copy(serviceInfoResource: Resource.error(failure: l));
            }, (r) => add(GetMyServiceEvent()));
          } else {
            yield state.copy(serviceInfoResource: Resource.error(failure: l));
          }
        },
        (r) async* {
          yield state.copy(serviceInfoResource: Resource.success(data: r));
        },
      );
    }

    if (event is SubscribeEvent) {
      yield state.copy(subscribeService: Resource.loading());
      final _response = await subscribeSmsServiceUseCase(event.params);
      yield* _response.fold((l) async* {
        if (l is AuthFailure) {
          final _reauthenticateResponse =
              await reAuthenticateUseCase(NoParams());
          _reauthenticateResponse.fold((l) async* {
            yield state.copy(subscribeService: Resource.error(failure: l));
          }, (r) => add(SubscribeEvent(params: event.params)));
        } else {
          yield state.copy(subscribeService: Resource.error(failure: l));
        }
      }, (r) async* {
        yield state.copy(subscribeService: Resource.success(data: r));
      });
    }

    if (event is RenewServiceEvent) {
      yield state.copy(renewSubscription: Resource.loading());
      final _response = await renewSubscriptionUseCase(event.subscriptionId);
      yield* _response.fold((l) async* {
        if (l is AuthFailure) {
          final _reauthenticateResponse =
              await reAuthenticateUseCase(NoParams());
          _reauthenticateResponse.fold((l) async* {
            yield state.copy(renewSubscription: Resource.error(failure: l));
          }, (r) => add(RenewServiceEvent(subscriptionId: event.subscriptionId)));
        } else {
          yield state.copy(renewSubscription: Resource.error(failure: l));
        }
      }, (r) async* {
        yield state.copy(renewSubscription: Resource.success(data: r));
        add(GetMyServiceEvent());
      });
    }
  }
}
