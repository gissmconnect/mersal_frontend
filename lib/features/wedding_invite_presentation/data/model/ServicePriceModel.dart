
class ServicePriceModel {
  ServicePriceData data;

  ServicePriceModel({this.data});

  factory ServicePriceModel.fromJson(Map<String, dynamic> json) {
    return ServicePriceModel(
        data: json['data'] != null
          ? ServicePriceData.fromJson(json['data'])
          : null
    );
  }
}


class ServicePriceData {
  int id;
  String price;
  String title;

  ServicePriceData({this.price,this.id,this.title});

  factory ServicePriceData.fromJson(Map<String, dynamic> json) {
    return ServicePriceData(
      title: json['title'],
      price: json['price'],
      id: json['id'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    return data;
  }
}