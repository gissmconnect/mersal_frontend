import 'package:mersal/features/committee_management/domain/entity/committee_member_entity.dart' as entity;
import 'package:meta/meta.dart';

class CommitteeMemberModel extends entity.CommitteeMemberEntity {
  CommitteeMemberModel(
      {@required int id,
      @required Member member,
      @required String quote,
      @required String otherInfo})
      : super(member: member, quote: quote, otherInfo: otherInfo);

  CommitteeMemberModel.fromJson(Map<String, dynamic> json) {
    member  = json['user'] != null
        ? new Member.fromJson(json['user'])
        : null;
    quote = json['quote'];
    otherInfo = json['other_info'];
  }

  Map<String, dynamic> toJSON() {
    return {'user': member, 'quote': quote, 'other_info': otherInfo};
  }
}

class Member extends entity.Member {
  String authToken;
  Document doc;
  Member(
      {int id,
      int userId,
      String name,
      String userName,
      String email,
      String phone,
      String gender,
      this.authToken,
      String bloodGroup,
      String dob,
      String address,
      String image,
      bool isVendor,
      this.doc,
      int documentStatus,
      bool addService})
      : super(
            id: id,
            userId: userId,
            name: name,
            userName: userName,
            email: email,
            phone: phone,
            gender: gender,
            bloodGroup: bloodGroup,
            dob: dob,
            address: address,
            image: image,
            document: doc,
            documentStatus: documentStatus,
            addService: addService,
            isVendor: isVendor);

  Member.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    name = json['name'];
    userName = json['user_name'];
    email = json['email'];
    image = json['icon'];
    phone = json['phone'];
    gender = json['gender'];
    authToken = json['auth_token'];
    bloodGroup = json['blood_group'];
    dob = json['dob'];
    address = json['address'];
    image = json['image'];
    isVendor = json['is_vender'];
    document = json['document'] != null
        ? new Document.fromJson(json['document'])
        : null;
    documentStatus = json['document_status'];
    addService = json['add_services'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['name'] = this.name;
    data['user_name'] = this.userName;
    data['email'] = this.email;
    data['icon'] = this.image;
    data['phone'] = this.phone;
    data['gender'] = this.gender;
    data['auth_token'] = this.authToken;
    data['blood_group'] = this.bloodGroup;
    data['dob'] = this.dob;
    data['address'] = this.address;
    data['image'] = this.image;
    data['is_vender'] = this.isVendor;
    if (this.document != null) {
      data['document'] = this.doc.toJson();
    }
    data['document_status'] = this.documentStatus;
    data['add_services'] = this.addService;
    return data;
  }
}

class Document extends entity.Document {
  Document(
      {int id,
        int documentStatus,
        String name,
        String number,
        String dob,
        String expDate,
        String frontImage,
        String backImage});

  Document.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['id_name'];
    number = json['id_number'];
    dob = json['id_dob'];
    expDate = json['id_expiry_date'];
    frontImage = json['id_front'];
    backImage = json['id_back'];
    documentStatus = json['document_status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['id_name'] = this.name;
    data['id_number'] = this.number;
    data['id_dob'] = this.dob;
    data['id_expiry_date'] = this.expDate;
    data['id_front'] = this.frontImage;
    data['id_back'] = this.backImage;
    data['document_status'] = this.documentStatus;
    return data;
  }
}
