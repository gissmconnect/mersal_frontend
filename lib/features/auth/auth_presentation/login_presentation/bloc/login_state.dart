part of 'login_bloc.dart';

abstract class LoginState extends Equatable {
  @override
  List<Object> get props => [];
}

class LoginInitial extends LoginState {}

class LoginSuccess extends LoginState {}

class LoginInitiated extends LoginState {}

class LoginError extends LoginState {
  final Failure failure;

  LoginError({@required this.failure});
}
