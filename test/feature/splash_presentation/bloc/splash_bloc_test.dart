import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/splash_presentation/bloc/splash_bloc.dart';
import 'package:mockito/mockito.dart';

import '../../../mocked_classes.dart';


void main() {
  MockGetUserSession mockGetUserSession;
  SplashBloc bloc;
  setUp(() {
    mockGetUserSession = MockGetUserSession();
    bloc = SplashBloc(userSessionUseCase: mockGetUserSession);
  });

  test('should initial state be Empty', () {
    expect(bloc.state, equals(Empty()));
  });

  group('GetUserSession', () {
    test('should get success from GetUserSessionUseCase', () async {
      when(mockGetUserSession.call(any)).thenAnswer((
          realInvocation) async => Right(null));
      bloc.add(GetUserSessionEvent());
      await untilCalled(mockGetUserSession.call(any));
      verify(mockGetUserSession.call(NoParams()));
    });

    test('should emit [Loading, Error] with NetworkFailure', () async {
      //arrange
      when(mockGetUserSession(any))
          .thenAnswer((realInvocation) async => Left(NetworkFailure()));

      //act
      bloc.add(GetUserSessionEvent());

      //assert later
      final _expectedStates = [
        Loading(),
        Error(failure: NetworkFailure())
      ];
      expectLater(bloc.stream, emitsInOrder(_expectedStates));


    });

    test('should emit [Loading, Loaded]', () async {
      //arrange
      when(mockGetUserSession(any))
          .thenAnswer((realInvocation) async => Right(null));

      //act
      bloc.add(GetUserSessionEvent());

      //assert later
      final _expectedStates = [
        Loading(),
        Loaded()
      ];

      expectLater(bloc.stream, emitsInOrder(_expectedStates));

    });
  });
}
