import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/features/lawati_service/domain/entity/sms_serviceInfo_entity.dart';
import 'package:mersal/features/lawati_service/domain/entity/sms_subscription_entity.dart';
import 'package:mersal/features/lawati_service/domain/usecase/subscribe_sms_subscription_usecase.dart';
import 'package:meta/meta.dart';

abstract class SMSServiceRepo {
  Future<Either<Failure,SMSServiceInfoEntity>> getSMSServiceInfo();
  Future<Either<Failure,SmsSubscriptionEntity>>getMySubscription();
  Future<Either<Failure,List<SmsSubscriptionEntity>>> getSubscriptions();
  Future<Either<Failure,dynamic>>subscribe({@required SubscriptionRequestParams params});
  Future<Either<Failure,String>>renewSubscription({@required int subscriptionId});
}
