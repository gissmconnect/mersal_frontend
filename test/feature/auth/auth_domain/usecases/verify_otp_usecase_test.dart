import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mersal/features/auth/auth_domain/usecases/verify_otp_usecase.dart';
import 'package:mockito/mockito.dart';

import '../../../../mocked_classes.dart';

void main() {
  MockAuthRepository mockAuthRepository;
  VerifyOtpUseCase verifyOtpUseCase;

  setUp(() {
    mockAuthRepository = MockAuthRepository();
    verifyOtpUseCase = VerifyOtpUseCase(authRepository: mockAuthRepository);
  });

  final _otp = '123456';
  final _phoneNumber = '1234567890';
  test('should return Right with null when otp verification is successful',
      () async {
    //arrange
    when(mockAuthRepository.verifyOtp(
            phoneNumber: _phoneNumber, otp: anyNamed('otp')))
        .thenAnswer((_) async => Right(null));

    //act
    final _result = await verifyOtpUseCase(
        VerifyOtpParams(phoneNumber: _phoneNumber, otp: _otp));

    //asset
    verify(mockAuthRepository.verifyOtp(phoneNumber: _phoneNumber, otp: _otp));
    expect(_result, Right(null));
  });
}
