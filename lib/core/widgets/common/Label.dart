import 'package:flutter/material.dart';
import 'package:mersal/resources/fonts.dart';

class Label extends StatelessWidget {
  final String title;
  final Color color;
  final double fontSize;
  final FontWeight fontWeight;
  final FontStyle fontStyle;
  final String fontFamily;
  final TextAlign textAlign;
  final int maxLines;
  final bool showUnderLine;

  Label(
      {this.title,
        this.color = Colors.black,
        this.fontSize = 14,
        this.maxLines,
        this.fontStyle,
        this.showUnderLine,
        this.fontFamily,
        this.fontWeight = FontWeight.w400,
        this.textAlign = TextAlign.left});

  @override
  Widget build(BuildContext context) {
    final DefaultTextStyle parent = DefaultTextStyle.of(context);
    return Text(
      title,
      textAlign: textAlign,
      maxLines: maxLines ?? parent.maxLines,
      style: TextStyle(
          color: color,
          fontSize: fontSize,
          fontStyle: fontStyle,
          fontFamily: fontFamily??fonts.segoeui,
          fontWeight: fontWeight,
        decoration:showUnderLine!=null && showUnderLine? TextDecoration.underline:TextDecoration.none,
      ),

    );
  }
}
