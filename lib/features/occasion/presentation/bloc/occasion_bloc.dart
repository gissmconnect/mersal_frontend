import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/reauthenticate_usecase.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/core/util/date_formatter.dart';
import 'package:mersal/features/occasion/domain/entity/occasion_entity.dart';
import 'package:mersal/features/occasion/domain/usecases/get_occasion_usecase.dart';
import 'package:meta/meta.dart';

part 'occasion_event.dart';

part 'occasion_state.dart';

class OccasionBloc extends Bloc<OccasionEvent, OccasionState> {
  final GetOccasionUseCase getOccasionUseCase;
  final ReAuthenticateUseCase reAuthenticateUseCase;

  OccasionBloc({@required this.getOccasionUseCase, @required this.reAuthenticateUseCase})
      : super(OccasionInitial());

  @override
  Stream<OccasionState> mapEventToState(
    OccasionEvent event,
  ) async* {
    if (event is GetOccasionEvent) {
      yield OccasionLoadingState();
      final String formattedDate = formatYYYYMMDD(date: event.date);
      final _response = await getOccasionUseCase(formattedDate);
      yield* _response.fold((l) async* {
        if (l is AuthFailure) {
          final _reauthenticateResponse =
              await reAuthenticateUseCase(NoParams());
          _reauthenticateResponse.fold((l) async* {
            yield OccasionFailureState(failure: l);
          }, (r) => add(GetOccasionEvent(date: event.date)));
        } else {
          yield OccasionFailureState(failure: l);
        }
      }, (r) async* {
        yield OccasionLoadedState(occasion: r);
      });
    }
  }
}
