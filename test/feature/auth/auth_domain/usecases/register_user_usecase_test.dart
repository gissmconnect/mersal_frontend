import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mersal/features/auth/auth_domain/usecases/register_user_usecase.dart';
import 'package:mockito/mockito.dart';

import '../../../../mocked_classes.dart';

void main() {
  MockAuthRepository mockAuthRepository;
  RegisterUserUseCase useCase;

  setUp(() {
    mockAuthRepository = MockAuthRepository();
    useCase = RegisterUserUseCase(authRepo: mockAuthRepository);
  });

  test('should return with no data when registration successful', () async {
    final _phoneNumber = '1234567';
    final _password = 'abcd1234';
    //arrange
    when(mockAuthRepository.register(
            phoneNumber: anyNamed('phoneNumber'),
            password: anyNamed('password')))
        .thenAnswer(
      (_) async => Right(null),
    );

    //act
    final _result =
        await useCase(Params(phoneNumber: _phoneNumber, password: _password));

    //assert
    verify(mockAuthRepository.register(
        phoneNumber: _phoneNumber, password: _password));
    expect(_result, equals(Right(null)));
  });
}
