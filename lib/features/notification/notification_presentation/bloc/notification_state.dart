part of 'notification_bloc.dart';

class NotificationState extends Equatable {
  final Resource getNotificationResource,
      deleteNotificationResource,
      archiveNotificationResource;
  final int randomId;

  NotificationState(
      {this.randomId = 0,
      this.getNotificationResource,
      this.deleteNotificationResource,
      this.archiveNotificationResource});

  @override
  List<Object> get props => [
        randomId,
        getNotificationResource,
        deleteNotificationResource,
        archiveNotificationResource
      ];

  factory NotificationState.initial() {
    return NotificationState(getNotificationResource: Resource.loading());
  }

  NotificationState copy(
      {Resource getNotificationResource,
      Resource deleteNotificationResource,
      Resource archiveNotificationResource}) {
    return NotificationState(
        getNotificationResource: getNotificationResource ?? this.getNotificationResource,
        deleteNotificationResource:
            deleteNotificationResource ?? this.deleteNotificationResource,
        archiveNotificationResource:
            archiveNotificationResource ?? this.archiveNotificationResource);
  }
}
