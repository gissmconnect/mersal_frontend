import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:mersal/core/localization/app_language.dart';
import 'package:mersal/core/localization/app_localization.dart';
import 'package:mersal/core/network/api_endpoints.dart';
import 'package:mersal/core/network/http_error_handler.dart';
import 'package:mersal/core/widgets/common/Button.dart';
import 'package:mersal/core/widgets/common/CustomAppBar.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/core/widgets/common/shared_preference.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/resources/colors.dart';
import 'package:provider/provider.dart';

class SettingsScreen extends StatefulWidget {
  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  bool isSwitched = false;
  bool languageBool = true;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  BuildContext mContextLoader;
  String languageValue = 'English';
  bool englishSelectBool = true, arabicSelectBool = false;

  @override
  void initState() {
    super.initState();
    apiGetSettingCall();
  }

  @override
  Widget build(BuildContext context) {
    var appLanguage = Provider.of<AppLanguage>(context);
    return Scaffold(
      appBar: CustomAppBar(
        title: AppLocalizations.of(context).translate("Settings"),
        isBackButton: true,
        showText: true,
        rightIconTwo: true,
      ),
      backgroundColor: Colors.cyan.shade50,
      body: Container(
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bg_image.jpg"),
            fit: BoxFit.fill,
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: const EdgeInsets.only(right: 20, left: 20),
              child: Row(
                children: [
                  Expanded(
                    child: Container(
                      margin: const EdgeInsets.only(left: 20),
                      child: Label(
                        title: AppLocalizations.of(context)
                            .translate("Notifications"),
                        fontSize: 18,
                        fontWeight: FontWeight.w700,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(right: 20),
                    child: FlutterSwitch(
                      width: 100.0,
                      height: 40.0,
                      valueFontSize: 15.0,
                      toggleSize: 30.0,
                      value: isSwitched,
                      borderRadius: 25.0,
                      padding: 8.0,
                      showOnOff: true,
                      onToggle: (val) {
                        setState(() {
                          isSwitched = val;
                        });
                      },
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.only(right: 20, left: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    margin: const EdgeInsets.only(top: 20, left: 20),
                    child: Label(
                      title: AppLocalizations.of(context).translate("Language"),
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                      color: Colors.white,
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      showLanguageDialog(appLanguage);
                      setState(() {});
                    },
                    child: Container(
                      padding: EdgeInsets.only(
                          top: 10, bottom: 10, left: 20, right: 20),
                      margin: const EdgeInsets.only(top: 20, right: 20),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(25),
                          color: Colors.white),
                      child: Label(
                        title: languageValue ?? "",
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        color: Color(appColors.buttonColor),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 50,
              margin: EdgeInsets.only(top: 55, bottom: 40, left: 30, right: 30),
              child: Button(
                title:
                    AppLocalizations.of(context).translate("Update Settings"),
                color: Color(appColors.buttonColor),
                borderColor: Colors.transparent,
                textColor: Colors.white,
                onTap: () {
                  apiSettingUpdateCall();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<Null> _showLoader(BuildContext context) async {
    return showDialog<Null>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context1) {
        mContextLoader = context1;
        return Scaffold(
          backgroundColor: Colors.transparent.withOpacity(0.3),
          key: _keyLoader,
          body: Container(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    child: CircularProgressIndicator(),
                    height: 40.0,
                    width: 40.0,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Future<void> apiGetSettingCall() async {
    final _token = await appPreferences.getStringPreference("AccessToken");

    if (context != null) {
      _showLoader(context);
    }

    var response = await http.get(Uri.parse(GET_USER_SETTINGS), headers: {
      'Content-type': 'application/json',
      AUTHORIZATION: BEARER + " " + _token
    });

    if (response.statusCode == 200) {
      try {
        final Map<String, dynamic> _responseJson = json.decode(response.body);
        if (mContextLoader != null) Navigator.pop(mContextLoader);
        print(
            'GetSettingResponse ${response.statusCode} and ${_responseJson.toString()}');
      } catch (e) {}
    }
  }

  Future<void> apiSettingUpdateCall() async {
    final _token = await appPreferences.getStringPreference("AccessToken");

    final _params = <String, String>{
      'language': languageBool ? "en" : "ar",
      'enable_notification': isSwitched ? "1" : "0",
    };

    if (context != null) {
      _showLoader(context);
    }

    print("UPDATE_USER_SETTINGS Params====${_params.toString()}");

    var response = await http.post(Uri.parse(UPDATE_USER_SETTINGS),
        headers: {
          'Content-type': 'application/json',
          AUTHORIZATION: BEARER + " " + _token
        },
        body: json.encode(_params));

    if (response.statusCode == 200) {
      if (mContextLoader != null) Navigator.pop(mContextLoader);
      print(
          'UpdateSettingResponse ${response.statusCode} and ${response.body}');
      final _responseJson = json.decode(response.body);
      if (_responseJson["status"]) {
        Fluttertoast.showToast(
            msg: "Settings Updated Successfully",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.grey.shade200.withOpacity(0.7),
            textColor: Colors.black,
            fontSize: 16.0);
      }
    } else {
      handleError(response);
    }
  }

  void showLanguageDialog(AppLanguage appLanguage) {
    showDialog<void>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Label(
              title: AppLocalizations.of(context).translate('Change Language'),
              fontSize: 22,
              textAlign: TextAlign.center,
              fontWeight: FontWeight.w700,
              color: Colors.black,
            ),
            content: StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
                return Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    GestureDetector(
                      onTap: () {
                        englishSelectBool = true;
                        arabicSelectBool = false;
                        appLanguage.changeLanguage(Locale('en'));
                        setState(() {});
                        Navigator.pop(context);
                      },
                      child: Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(right: 20),
                            alignment: Alignment.centerRight,
                            child: Icon(
                              englishSelectBool
                                  ? Icons.check_box
                                  : Icons.check_box_outline_blank,
                              size: 30,
                              color: Color(appColors.blue),
                            ),
                          ),
                          Label(
                            title: AppLocalizations.of(context)
                                .translate('English'),
                            fontSize: 18,
                            textAlign: TextAlign.center,
                            color: Colors.black,
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    GestureDetector(
                      onTap: () {
                        englishSelectBool = false;
                        arabicSelectBool = true;
                        appLanguage.changeLanguage(Locale('ar'));
                        setState(() {});
                        Navigator.pop(context);
                      },
                      child: Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(right: 20),
                            alignment: Alignment.centerRight,
                            child: Icon(
                              arabicSelectBool
                                  ? Icons.check_box
                                  : Icons.check_box_outline_blank,
                              size: 30,
                              color: Color(appColors.blue),
                            ),
                          ),
                          Label(
                            title: AppLocalizations.of(context)
                                .translate('Arabic'),
                            fontSize: 18,
                            textAlign: TextAlign.center,
                            color: Colors.black,
                          )
                        ],
                      ),
                    ),
                  ],
                );
              },
            ),
          );
        });
  }
}
