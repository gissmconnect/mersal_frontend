import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/get_user_login_data_usecase.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/auth/auth_domain/usecases/login_usecase.dart';
import 'package:mersal/features/auth/auth_domain/usecases/upload_document_usecase.dart';
import 'package:meta/meta.dart';

part 'doc_upload_event.dart';

part 'doc_upload_state.dart';

class DocUploadBloc extends Bloc<DocUploadEvent, DocUploadState> {
  final LoginUseCase loginUseCase;
  final GetUserLoginDataUseCase getUserLoginDataUseCase;
  final UploadDocumentUseCase uploadDocumentUseCase;

  DocUploadBloc(
      {@required this.loginUseCase,
      @required this.getUserLoginDataUseCase,
      @required this.uploadDocumentUseCase})
      : super(DocUploadInitial());

  @override
  Stream<DocUploadState> mapEventToState(
    DocUploadEvent event,
  ) async* {
    if (event is SoftLoginEvent) {
      yield SoftLoginInitiated();
      final _loginCredential = await getUserLoginDataUseCase(NoParams());
      yield* _loginCredential.fold((l) async* {
        yield SoftLoginFailed(failure: l);
      }, (r) async* {
        yield* _initiateLogin(loginId: r.loginId, password: r.password);
      });
    }

    if (event is UploadDocEvent) {
      yield UploadInitiatedState();
      final _params = UploadDocParams(
          frontImage: _getBase64Image(event.frontImage),
          backImage: _getBase64Image(event.backImage),
          name: event.name,
          dob: event.dob,
          expDate: event.expDate,
          idNumber: event.idNumber);
      final _uploadDoc = await uploadDocumentUseCase(
        _params,
      );
      yield* _uploadDoc.fold(
        (l) async* {
          yield UploadFailedState(failure: l);
        },
        (r) async* {
          if(event.isNewAccount) {
            final _loginCredential = await getUserLoginDataUseCase(NoParams());
            yield* _loginCredential.fold(
                  (l) async* {
                yield UploadFailedState(failure: l);
              },
                  (r) async* {
                yield* _initiateLogin(loginId: r.loginId, password: r.password);
              },
            );
          }else{
            yield UploadSuccessState();
          }
        },
      );
    }
  }

  Stream<DocUploadState> _initiateLogin(
      {@required String loginId, @required String password}) async* {
    final _loginResponse = await loginUseCase
        .call(Params(phoneNumber: loginId, password: password));
    yield* _loginResponse.fold(
      (l) async* {
        yield SoftLoginFailed(failure: l);
      },
      (r) async* {
        yield SoftLoginSuccess();
      },
    );
  }

  String _getBase64Image(String filePath) {
    File imgFile = File(filePath);
    final imageBytes = imgFile.readAsBytesSync();
    return base64Encode(imageBytes);
  }
}
