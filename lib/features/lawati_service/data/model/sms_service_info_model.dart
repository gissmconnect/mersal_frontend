import 'package:mersal/features/lawati_service/domain/entity/sms_serviceInfo_entity.dart';
import 'package:meta/meta.dart';

class SMSServiceInfoModel extends SMSServiceInfoEntity {
  SMSServiceInfoModel(
      {@required int id,
      @required String title,
      @required int status,
      @required String price,
      @required int duration})
      : super(
            id: id,
            title: title,
            status: status,
            duration: duration,
            price: price);

  SMSServiceInfoModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    status = json['status'];
    price = json['price'];
    duration = json['duration'];
  }

  Map<String, dynamic> toJSON() {
    return {
      'id': id,
      'title': title,
      'status': status,
      'price': price,
      'duration': duration
    };
  }
}
