import 'dart:convert';
import 'package:dartz/dartz.dart';
import 'package:dartz/dartz_streaming.dart';
import 'package:http/http.dart' as http;
import 'package:mersal/core/error/exceptions.dart';
import 'package:mersal/main.dart';

void handleError(http.Response response, [Function handleError]) {
  print('logout response ${response.statusCode} and ${response.body}');
  if (response.statusCode == 401) {
    final _message = json.decode(response.body)['message'];
    print('handleError _message $_message');
    Right(null);
    showMyDialog("Session Expired", "You session has been expired, Please login again to continue");
    throw AuthException()..message = _message;
  } else if (response.statusCode == 422 ||
      response.statusCode == 500 ||
      response.statusCode == 404) {
    final _message = json.decode(response.body)['message'];
    throw ServerException()..message = _message;
  } else {
    throw ServerException();
  }
}