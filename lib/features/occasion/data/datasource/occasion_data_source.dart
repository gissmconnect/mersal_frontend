import 'package:mersal/features/occasion/domain/entity/occasion_entity.dart';
import 'package:meta/meta.dart';

abstract class OccasionDateSource {
  Future<OccasionEntity> getOccasion(
      {@required String token, @required String date});
}
