import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/election/election-domain/entity/election_entity.dart';
import 'package:mersal/features/election/election-domain/repository/election_repository.dart';
import 'package:meta/meta.dart';

class GetPrayerUseCase extends UseCase<List<ElectionDataEntity>, NoParams> {
  final ElectionRepository electionRepo;

  GetPrayerUseCase({@required this.electionRepo});

  @override
  Future<Either<Failure, List<ElectionDataEntity>>> call(NoParams params) async {
    return await electionRepo.getElections();
  }
}
