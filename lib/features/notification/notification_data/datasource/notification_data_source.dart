import 'package:mersal/features/notification/notification_data/model/notification_model.dart';
import 'package:meta/meta.dart';

abstract class NotificationDataSource {
  Future<List<NotificationModel>> getNotifications({@required String token});

  Future<String> deleteNotification({@required String token, @required int id});

  Future<String> archiveNotification({@required String token, @required int id});
}
