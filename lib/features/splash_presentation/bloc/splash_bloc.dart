import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/auth/auth_domain/usecases/get_user_session.dart';
import 'package:meta/meta.dart';

part 'splash_event.dart';

part 'splash_state.dart';

class SplashBloc extends Bloc<SplashEvent, SplashState> {
  final GetUserSession getUserSession;

  SplashBloc({@required GetUserSession userSessionUseCase})
      : assert(userSessionUseCase != null),
        getUserSession = userSessionUseCase,
        super(Empty());

  @override
  Stream<SplashState> mapEventToState(
    SplashEvent event,
  ) async* {
    if (event is GetUserSessionEvent) {
      yield Loading();
      final _userSessionEither = await getUserSession(NoParams());
      yield* _userSessionEither.fold((l) async* {
        yield Error(failure: l);
      }, (r) async* {
        yield Loaded();
      });
    }
  }
}
