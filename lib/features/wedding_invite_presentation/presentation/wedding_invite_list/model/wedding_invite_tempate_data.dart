class WeddingInviteTemplateData {
  int id;
  String title;
  String template;

  WeddingInviteTemplateData({this.template, this.id, this.title});

  factory WeddingInviteTemplateData.fromJson(Map<String, dynamic> json) {
    return WeddingInviteTemplateData(
        id: json['id'],
        title: json["title"],
        template: json['template']);
  }
}
