import 'package:mersal/features/election/election-domain/entity/election_entity.dart';

class ElectionDataModel extends ElectionDataEntity {
  ElectionDataModel(
      {int id,
      String title,
      String date,
      String endDate,
      String endTime,
      String shortDescription,
      int noOfElectorsSelect})
      : super(
            id: id,
            title: title,
            date: date,
            endDate: endDate,
            endTime: endTime,
            shortDescription: shortDescription,
            noOfElectorsSelect: noOfElectorsSelect);

  ElectionDataModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    date = json['date'];
    endDate = json['end_date'];
    endTime = json['end_time'];
    shortDescription = json['short_description'];
    noOfElectorsSelect = json['no_of_electors_select'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['date'] = this.date;
    data['end_date'] = this.endDate;
    data['end_time'] = this.endTime;
    data['short_description'] = this.shortDescription;
    data['no_of_electors_select'] = this.noOfElectorsSelect;
    return data;
  }
}
