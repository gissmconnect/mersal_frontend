import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/usecase.dart';
import 'package:mersal/features/auth/auth_domain/repository/auth_repository.dart';
import 'package:meta/meta.dart';

class VerifyOtpUseCase extends UseCase<void, VerifyOtpParams> {
  final AuthRepository authRepository;

  VerifyOtpUseCase({@required this.authRepository});

  @override
  Future<Either<Failure, void>> call(VerifyOtpParams params) async {
    return await authRepository.verifyOtp(
        phoneNumber: params.phoneNumber, otp: params.otp);
  }
}

class VerifyOtpParams {
  final String phoneNumber, otp;

  VerifyOtpParams({@required this.phoneNumber, @required this.otp});
}
