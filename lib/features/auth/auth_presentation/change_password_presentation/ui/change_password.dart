import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mersal/core/localization/app_localization.dart';
import 'package:mersal/core/widgets/common/Button.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/CustomAppBar.dart';
import 'package:mersal/core/widgets/common/InputField.dart';
import 'package:mersal/core/widgets/message.dart';
import 'package:mersal/core/widgets/widgets.dart';
import 'package:mersal/features/auth/auth_presentation/change_password_presentation/bloc/change_password_bloc.dart';
import 'package:mersal/injection_container.dart';
import 'package:mersal/resources/colors.dart';

class ChangePassword extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => serviceLocator<ChangePasswordBloc>(),
      child: Scaffold(
        appBar: CustomAppBar(
            title: AppLocalizations.of(context)
                .translate("Change Password"), isBackButton: true, showText: true),
        body: _ResetPasswordBody(),
      ),
    );
  }
}

class _ResetPasswordBody extends StatefulWidget {
  @override
  _ResetPasswordBodyState createState() => _ResetPasswordBodyState();
}

class _ResetPasswordBodyState extends State<_ResetPasswordBody> {
  final oldController = TextEditingController();
  final newPasswordController = TextEditingController();
  bool obscureText = true,newPassObscure=true;
  final FocusNode oldFocus = new FocusNode();
  final FocusNode newPasswordFocus = new FocusNode();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bg_image.jpg"),
            fit: BoxFit.fill,
          ),
        ),
        child: ListView(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Form(
                  key: _formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        height: 80,
                        margin: EdgeInsets.only(left: 30, top: 25, right: 30),
                        child: TextFormField(
                          controller: oldController,
                          focusNode: oldFocus,
                          obscureText: obscureText,
                          decoration: InputDecoration(
                              filled: true,
                              counterText: "",
                              fillColor: Colors.white,
                              hintText: AppLocalizations.of(context)
                                  .translate("Enter Old Password"),
                              contentPadding: const EdgeInsets.only(
                                  left: 20.0, bottom: 15.0, top: 15.0,right: 15),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                                borderRadius: BorderRadius.circular(25.7),
                              ),
                              errorBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(25.0),
                                  borderSide: BorderSide(
                                    color: Colors.red,
                                  )),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                                borderRadius: BorderRadius.circular(25.7),
                              ),
                              suffixIcon: IconButton(
                                onPressed: () {
                                  obscureText = !obscureText;
                                  setState(() {});
                                },
                                icon: Icon(
                                    obscureText
                                        ? Icons.remove_red_eye_outlined
                                        : Icons.visibility_off_outlined,
                                    size: 30,
                                    color: Colors.black),
                              )),
                          onFieldSubmitted: (term) {
                            FocusScope.of(context).unfocus();
                          },
                          maxLength: 20,
                          validator: (text) {
                            return text.isEmpty
                                ? AppLocalizations.of(context)
                                .translate('Old password cannot be empty')
                                : null;
                          },
                          keyboardType: TextInputType.phone,
                        ),
                      ),
                      Container(
                        height: 80,
                        margin: EdgeInsets.only(left: 30, top: 15, right: 30),
                        child: TextFormField(
                          controller: newPasswordController,
                          focusNode: newPasswordFocus,
                          obscureText: newPassObscure,
                          validator: (text) {
                            if (text.isEmpty) {
                              return AppLocalizations.of(context)
                                  .translate('New Password cannot be empty');
                            } else if (text == oldController.text) {
                              return AppLocalizations.of(context)
                                  .translate('Old and New Password cannot be same');
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                              filled: true,
                              counterText: "",
                              fillColor: Colors.white,
                              hintText: AppLocalizations.of(context)
                                  .translate("Enter New Password"),
                              contentPadding: const EdgeInsets.only(
                                  left: 20.0, bottom: 15.0, top: 15.0,right: 15),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                                borderRadius: BorderRadius.circular(25.7),
                              ),
                              errorBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(25.0),
                                  borderSide: BorderSide(
                                    color: Colors.red,
                                  )),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                                borderRadius: BorderRadius.circular(25.7),
                              ),
                              suffixIcon: IconButton(
                                onPressed: () {
                                  newPassObscure = !newPassObscure;
                                  setState(() {});
                                },
                                icon: Icon(
                                    newPassObscure
                                        ? Icons.remove_red_eye_outlined
                                        : Icons.visibility_off_outlined,
                                    size: 30,
                                    color: Colors.black),
                              )),
                          onFieldSubmitted: (term) {
                            FocusScope.of(context).unfocus();
                          },
                          maxLength: 20,
                          keyboardType: TextInputType.name,
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 60,
                  margin: EdgeInsets.only(left: 30, top: 35, right: 30),
                  child: BlocListener<ChangePasswordBloc, ChangePasswordState>(
                    listener: (context, state) {
                      if (state is ChangePasswordSuccess) {
                        Navigator.of(context).pop();
                      } else if (state is ChangePasswordFailed) {
                        showSnackBarMessage(
                            context: context, message: state.failure.message);
                      }
                    },
                    child: BlocBuilder<ChangePasswordBloc, ChangePasswordState>(
                      builder: (context, state) {
                        if (state is ChangePasswordInitiated) {
                          return circularProgressIndicator();
                        }
                        return Button(
                          title: AppLocalizations.of(context)
                              .translate("Update Password"),
                          textColor: Colors.white,
                          borderColor: Colors.transparent,
                          color: Color(appColors.buttonColor),
                          onTap: () {
                            if (_formKey.currentState.validate()) {
                              final _oldPassword =
                                  oldController.text.toString().trim();
                              final _newPassword =
                                  newPasswordController.text.toString().trim();
                              BlocProvider.of<ChangePasswordBloc>(context).add(
                                  ChangePasswordEvent(
                                      oldPassword: _oldPassword,
                                      newPassword: _newPassword));
                            }
                          },
                        );
                      },
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

}
