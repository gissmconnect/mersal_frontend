import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mersal/core/localization/app_localization.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/features/info_presentation/ui/info_screen.dart';
import 'package:mersal/features/lawati_service/presentation/service_info/ui/others_lawati_service.dart';
import 'package:mersal/features/lawati_service/presentation/service_info/ui/self_lawati_service.dart';
import 'package:mersal/resources/colors.dart';

class LawatiServiceOption extends StatefulWidget {
  @override
  _LawatiServiceOptionState createState() => _LawatiServiceOptionState();
}

class _LawatiServiceOptionState extends State<LawatiServiceOption> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bg_image.jpg"),
            fit: BoxFit.fill,
          ),
        ),
        child: Column(
          children: [
            SizedBox(
              height: 50,
            ),
            Stack(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(bottom: 5, left: 15),
                  alignment: Alignment.bottomLeft,
                  child: GestureDetector(
                    child: Icon(
                      Icons.arrow_back_ios_rounded,
                      color: Colors.white,
                    ),
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 6),
                  alignment: Alignment.bottomCenter,
                  child: Text(
                   AppLocalizations.of(context).translate("Lawati SMS Service"),
                    textAlign: TextAlign.end,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.w400),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(right: 15),
                  alignment: Alignment.centerRight,
                  child: GestureDetector(
                    child:  Container(
                      margin: EdgeInsets.only(left: 20),
                      child: SvgPicture.asset(
                        "assets/images/info_icon.svg",
                        width: 30,
                        height: 30,
                      ),
                    ),
                    onTap: () {
                      CommonMethods().animatedNavigation(
                          context: context,
                          currentScreen: LawatiServiceOption(),
                          landingScreen: InfoScreen());
                    },
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 40,
            ),
            SelfLawatiService(),
            Container(
              margin: EdgeInsets.only(
                  top: 20, left: 20, right: 20),
              child: Label(
                title: AppLocalizations.of(context).translate("You can subscribe your friends or family to this service"),
                fontSize: 16,
                fontWeight: FontWeight.w600,
                textAlign: TextAlign.center,
                color: Colors.white,
              ),
            ),
            GestureDetector(
              onTap: () {
                CommonMethods().animatedNavigation(
                    context: context,
                    currentScreen: LawatiServiceOption(),
                    landingScreen: OthersLawatiService());
              },
              child: Container(
                width: 220,
                alignment: Alignment.center,
                padding:
                    EdgeInsets.only(left: 10, right: 10, bottom: 30, top: 10),
                margin: EdgeInsets.only(top: 40.0, right: 10, left: 20),
                decoration: BoxDecoration(
                    color:Colors.white,
                    borderRadius: BorderRadius.circular(20)),
                child: Column(
                  children: [
                    SvgPicture.asset(
                      "assets/images/sms_option_icon.svg",
                      width: 60,
                      height: 60,
                      color:  Colors.black,
                    ),
                    SizedBox(height: 10,),
                    Label(
                        title: AppLocalizations.of(context).translate("Subscribe Others"),
                        textAlign: TextAlign.center,
                        fontSize: 16,
                        color:  Color(appColors.buttonColor)),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
