import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:meta/meta.dart';
import 'package:mersal/features/donation/domain/entity/donation_type_entity.dart';
abstract class DonationRepo {
  Future<Either<Failure,List<DonationTypeEntity>>> getDonationType();
  Future<Either<Failure, dynamic>> donation({
    @required int reasonId,
    @required String donationDescription,
    @required String donationAmount,
  });
}