import 'package:mersal/features/wedding_invite_presentation/data/model/wedding_invite_contacts/wedding_contacts_data.dart';

class WeddingContactDataModel {
  List<WeddingContactsData> weddingDataList;
  bool status;

  WeddingContactDataModel({this.weddingDataList, this.status});

  factory WeddingContactDataModel.fromJson(Map<String, dynamic> json) {
    return WeddingContactDataModel(
      weddingDataList: json['data'] != null
          ? (json['data'] as List)
          .map((i) => WeddingContactsData.fromJson(i))
          .toList()
          : null,
      status: json['status'],
    );
  }
}
