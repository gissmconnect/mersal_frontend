import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/usecases/save_user_login_data_usecase.dart';
import 'package:mersal/features/auth/auth_domain/usecases/change_password_usecase.dart';
import 'package:meta/meta.dart';

part 'change_password_event.dart';

part 'change_password_state.dart';

class ChangePasswordBloc
    extends Bloc<ChangePasswordEvent, ChangePasswordState> {
  final ChangePasswordUseCase changePasswordUseCase;
  final SaveUserLoginDataUseCase saveUserLoginDataUseCase;

  ChangePasswordBloc(
      {@required this.changePasswordUseCase,
      @required this.saveUserLoginDataUseCase})
      : super(ChangePasswordInitial());

  @override
  Stream<ChangePasswordState> mapEventToState(
    ChangePasswordEvent event,
  ) async* {
    if (event is ChangePasswordEvent) {
      yield ChangePasswordInitiated();
      final _response = await changePasswordUseCase(Params(
          oldPassword: event.oldPassword, newPassword: event.newPassword));
      yield* _response.fold((l) async* {
        yield ChangePasswordFailed(failure: l);
      }, (r) async* {
        yield ChangePasswordSuccess();
      });
    }
  }
}
