class AppColors {

  get blueCEE2FF => getColorHexFromStr("#CEE2FF");
  get blue00008B => getColorHexFromStr("#00008B");
  // get blue007D9E => getColorHexFromStr('#728FCE');
  // get blue00008B => getColorHexFromStr("#092058");
  get blue007D9E => getColorHexFromStr('#7F98B5');
  get bgBlueColor => getColorHexFromStr('#F4FAFF');
  get blue566D7E => getColorHexFromStr('#98AFC7');
  get greyE5E5E5 => getColorHexFromStr('#E5E5E5');
  get greyA1A1A1 => getColorHexFromStr('#A1A1A1');
  get greyC2C2C2 => getColorHexFromStr('#C2C2C2');
  get navyBlue => getColorHexFromStr('#000080');
  get creme => getColorHexFromStr('#d9bea0');
  get blue => getColorHexFromStr('#5b99ac');
  get DarkGrey => getColorHexFromStr('#464646');
  get buttonColor => getColorHexFromStr('#122A2C');

  int getColorHexFromStr(String colorStr) {
    colorStr = "FF" + colorStr;
    colorStr = colorStr.replaceAll("#", "");
    int val = 0;
    int len = colorStr.length;
    for (int i = 0; i < len; i++) {
      int hexDigit = colorStr.codeUnitAt(i);
      if (hexDigit >= 48 && hexDigit <= 57) {
        val += (hexDigit - 48) * (1 << (4 * (len - 1 - i)));
      } else if (hexDigit >= 65 && hexDigit <= 70) {
        // A..F
        val += (hexDigit - 55) * (1 << (4 * (len - 1 - i)));
      } else if (hexDigit >= 97 && hexDigit <= 102) {
        // a..f
        val += (hexDigit - 87) * (1 << (4 * (len - 1 - i)));
      } else {
        throw new FormatException("An error occurred when converting a color");
      }
    }
    return val;
  }

  static final AppColors _appColors = new AppColors._internal();
  factory AppColors()  {
    return _appColors;
  }
  AppColors._internal();
}
AppColors appColors = new AppColors();

