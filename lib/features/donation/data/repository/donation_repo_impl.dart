import 'package:dartz/dartz.dart';
import 'package:mersal/core/error/exceptions.dart';
import 'package:mersal/core/error/failures.dart';
import 'package:mersal/core/network/network_info.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/donation/data/datasource/donation_datasource.dart';
import 'package:mersal/features/donation/domain/entity/donation_type_entity.dart';
import 'package:mersal/features/donation/domain/repository/donation_repo.dart';
import 'package:meta/meta.dart';

class DonationRepoImpl implements DonationRepo {
  final DonationDataSource donationDataSource;
  final NetworkInfo networkInfo;
  final UserDataLocalDataSource userDataLocalDataSource;

  DonationRepoImpl(
      {@required this.donationDataSource,
      @required this.networkInfo,
      @required this.userDataLocalDataSource});

  @override
  Future<Either<Failure, List<DonationTypeEntity>>> getDonationType() async {
    if (await networkInfo.isConnected) {
      final _token = await userDataLocalDataSource.getAuthToken();
      try {
        final List<DonationTypeEntity> _donationTypeList =
            await donationDataSource.getDonationType(token: _token);
        return Right((_donationTypeList));
      } on ServerException catch (ex) {
        return Left(ServerFailure()..message = ex.message);
      } on AuthException catch (ex) {
        return Left(AuthFailure()..message = ex.message);
      } on Exception {
        return Left(ServerFailure());
      }
    } else {
      return Left(NetworkFailure());
    }
  }
  @override
  Future<Either<Failure, dynamic>> donation(
      {int reasonId,
        String donationDescription,
        String donationAmount}) async {
    if (await networkInfo.isConnected) {
      final _token = await userDataLocalDataSource.getAuthToken();
      try {
        final _response = await donationDataSource.donation(
          token: _token,
            reasonId: reasonId,
            donationAmount: donationAmount,
            donationDescription: donationDescription);
        return Right(_response);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(NetworkFailure());
    }
  }
}
