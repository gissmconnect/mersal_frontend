import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mersal/core/widgets/common/Button.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/core/widgets/message.dart';
import 'package:mersal/core/widgets/widgets.dart';
import 'package:mersal/features/auth/auth_presentation/doc_upload_presentation/bloc/doc_upload_bloc.dart';
import 'package:mersal/features/dashboard/dashboard_page.dart';
import 'package:mersal/features/knowMore/ui/know_more.dart';
import 'package:mersal/features/profile/profile_presentation/ui/upload_id_screen.dart';
import 'package:mersal/injection_container.dart';
import 'package:mersal/resources/colors.dart';
import 'package:mersal/resources/fonts.dart';

class PaymentTransactionStatusScreen extends StatelessWidget {
  final String from,activity,status;

  PaymentTransactionStatusScreen(this.from, this.status,{this.activity});

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
        body: Container(
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/bg_image.jpg"),
                fit: BoxFit.fill,
              ),
            ),
            child: _PaymentTansactionStatusScreenBody(from,activity,status)),
    );
  }
}

class _PaymentTansactionStatusScreenBody extends StatefulWidget {
  final String from,activity,status;

  _PaymentTansactionStatusScreenBody(this.from,this.activity, this.status);

  @override
  _PaymentTansactionStatusScreenBodyState createState() => _PaymentTansactionStatusScreenBodyState();
}

class _PaymentTansactionStatusScreenBodyState extends State<_PaymentTansactionStatusScreenBody> {

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: ListView(
        shrinkWrap: true,
        children: [
          Container(
            height: 400,
            padding: const EdgeInsets.all(8.0),
            margin: EdgeInsets.only(top: 60, left: 20, right: 20),
            decoration: new BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white,
              borderRadius: BorderRadius.circular(25.0),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  child: Label(
                    title: widget.status=="success"?"Mabrook":"Payment Failed",
                    fontSize: 20,
                    fontWeight: FontWeight.w600,
                    color: Colors.black,
                  ),
                ),
                widget.status=="success"?Container(
                  child: Icon(Icons.check_circle_rounded,color: Colors.lightBlue,size: 50,),
                ):Container(
                  child: Icon(Icons.error,color: Colors.red,size: 50,),
                ),
                widget.status=="success"?successPaymentLayout():
                Container(
                  margin: EdgeInsets.only(top: 40, left: 20, right: 20),
                  child: Label(
                    title: "Transaction failed.Please try again.",
                    fontSize: 18,
                    fontWeight: FontWeight.w400,
                    color: Colors.black,
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: 60,
            width: 220,
            margin: EdgeInsets.only(left: 70, top: 35, right: 70, bottom: 30),
            child:  Button(
              title: "Go to Homepage",
              textColor: Colors.white,
              borderColor: Colors.white,
              color: Colors.transparent,
              onTap: () {
                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(
                      builder: (context) => DashboardPage(),
                    ),
                        (route) => false);
                // BlocProvider.of<DocUploadBloc>(context)
                //     .add(SoftLoginEvent());
              },
            ),
          )
        ],
      ),
    );
  }

  Widget successLayout(String message) {
    return Container(
      margin: EdgeInsets.only(top: 30),
      child: Label(
        textAlign: TextAlign.center,
        title: message,
        fontSize: 18,
        color: Colors.black,
      ),
    );
  }
  
  Widget successPaymentLayout() {
    return  widget.from == "donation"
        ? successLayout("Transaction Successful.Your donation is received.")
        : widget.from == "child-registrations"
        ? successLayout(
        "Transaction Successful.Your child is enrolled for ${widget.activity}.")
        : widget.from == "lawati-sms"
        ? successLayout("Transaction Successful.Your subscription is created.")
        : widget.from == "invitation"
        ? successLayout(
        "Transaction Successful. Your invitation is under approval.")
        : widget.from == "wedding-booking"
        ? successLayout(
        "Transaction Successful. Your wedding booking created successfully.")
        : successLayout(
        "Transaction Failed.Please try again.");
  }

}
