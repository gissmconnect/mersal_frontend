import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mersal/core/bloc/user_bloc.dart';
import 'package:mersal/core/network/vo/Status.dart';
import 'package:mersal/core/widgets/common/CommonMethods.dart';
import 'package:mersal/core/widgets/common/Label.dart';
import 'package:mersal/core/widgets/common/image_chooser.dart';
import 'package:mersal/features/auth/auth_presentation/change_password_presentation/ui/change_password.dart';
import 'package:mersal/features/auth/auth_presentation/email_verification_presentation/ui/email_verification_screen.dart';
import 'package:mersal/features/profile/edit_profile_presentation/ui/edit_profile_screen.dart';
import 'package:mersal/features/profile/profile_presentation/ui/upload_id_screen.dart';
import 'package:mersal/features/settings_presentation/presentation/ui/settings_screen.dart';
import 'package:mersal/resources/colors.dart';
import 'package:mersal/core/localization/app_localization.dart';

class ProfileScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return _ProfilePageBody();
  }
}

class _ProfilePageBody extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<_ProfilePageBody> {
  File _image;

  @override
  void initState() {
    BlocProvider.of<UserBloc>(context).add(GetUserSessionEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserBloc, UserState>(
      /* buildWhen: (oldState,newState){
        return userDataUpdated(oldState, newState);
      },*/
      builder: (context, state) {
        if (state.userDataLoadingResource.status == STATUS.SUCCESS) {
          final _userData = state.userDataLoadingResource.data;
          return Scaffold(
            body: Container(
              height: MediaQuery.of(context).size.height,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/bg_image.jpg"),
                  fit: BoxFit.fill,
                ),
              ),
              child: SingleChildScrollView(
                padding: const EdgeInsets.symmetric(vertical: 50),
                child: Column(
                  children: [
                    Stack(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(bottom: 5, left: 15),
                          alignment: Alignment.bottomLeft,
                          child: GestureDetector(
                            child: Icon(
                              Icons.arrow_back_ios_rounded,
                              color: Colors.white,
                            ),
                            onTap: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(bottom: 6),
                          alignment: Alignment.bottomCenter,
                          child: Text(
                            AppLocalizations.of(context)
                                .translate("Profile"),
                            textAlign: TextAlign.end,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.w400),
                          ),
                        ),
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      child: Stack(
                        children: <Widget>[
                          Align(
                            alignment: Alignment.topCenter,
                            child: Container(
                              height: 110,
                              width: 110,
                              padding: const EdgeInsets.all(10.0),
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(55),
                                child: Container(
                                  child: _image != null
                                      ? Image.file(
                                          _image,
                                          height: 100,
                                          width: 100,
                                          fit: BoxFit.fill,
                                        )
                                      : _userData.image.isEmpty
                                          ? Icon(Icons.person_outline,
                                              size: 70, color: Colors.white)
                                          : CircleAvatar(
                                              radius: 30,
                                              backgroundImage:
                                                  CachedNetworkImageProvider(
                                                _userData.image,
                                              ),
                                            ),
                                ),
                              ),
                            ),
                          ),
                        /*  Align(
                            alignment: Alignment.topCenter,
                            child: Container(
                              margin: const EdgeInsets.only(top: 25, left: 85),
                              child: GestureDetector(
                                onTap: () {
                                  imageChooser
                                      .showImageChooser(context)
                                      .then((picture) {
                                    if (picture != null) {
                                      this._image = picture;
                                      setState(() {});
                                      print(picture);
                                    }
                                  });
                                },
                                child: Container(
                                  width: 30,
                                  height: 30,
                                  decoration: new BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.black,
                                  ),
                                  child: Center(
                                    child: Icon(Icons.camera_alt_outlined,
                                        size: 15, color: Colors.white),
                                  ),
                                ),
                              ),
                            ),
                          )*/
                        ],
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.only( left: 15, right: 15),
                      padding: EdgeInsets.only(left: 15, right: 15, bottom: 20),
                      decoration: new BoxDecoration(
                        color: Colors.white,
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          profileFieldRow(
                              title: AppLocalizations.of(context)
                                  .translate("Name:"), value: _userData.name),
                          Divider(
                            thickness: 1,
                          ),
                          profileFieldRow(
                              title: AppLocalizations.of(context)
                                  .translate("E-mail ID:"), value: _userData.email),
                          Divider(
                            thickness: 1,
                          ),
                          profileFieldRow(
                              title: AppLocalizations.of(context)
                                  .translate("Phone Number:"), value: _userData.phone),
                          Divider(
                            thickness: 1,
                          ),
                          profileFieldRow(title: AppLocalizations.of(context)
                              .translate("DOB:"), value: _userData.dob),
                          Divider(
                            thickness: 1,
                          ),
                          profileFieldRow(
                              title: AppLocalizations.of(context)
                                  .translate("Gender:"), value: _userData.gender),
                          Divider(
                            thickness: 1,
                          ),
                          profileFieldRow(
                              title: AppLocalizations.of(context)
                                  .translate("Blood Group:"),
                              value: _userData.bloodGroup),
                        ],
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.only(top: 25, left: 15, right: 15),
                      padding: EdgeInsets.only(
                          left: 15, right: 15, bottom: 15, top: 20),
                      decoration: new BoxDecoration(
                        color: Colors.white,
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Column(
                        children: [
                          GestureDetector(
                            onTap: () {
                              CommonMethods().animatedNavigation(
                                  context: context,
                                  currentScreen: ProfileScreen(),
                                  landingScreen: EditProfileScreen());
                            },
                            child: Container(
                              height:40,
                              margin: EdgeInsets.only(bottom: 5),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Label(
                                    title: AppLocalizations.of(context)
                                        .translate("Edit Profile"),
                                    color: Colors.black,
                                    fontSize: 16,
                                  ),
                                  Icon(
                                    Icons.arrow_forward_ios_outlined,
                                    size: 18,
                                  )
                                ],
                              ),
                            ),
                          ),
                          Divider(
                            thickness: 1,
                          ),
                          GestureDetector(
                            onTap: () {
                              CommonMethods().animatedNavigation(
                                  context: context,
                                  currentScreen: ProfileScreen(),
                                  landingScreen: ChangePassword());
                            },
                            child: Container(
                              height:40,
                              margin: EdgeInsets.only(bottom: 5,top: 5),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Label(
                                    title:AppLocalizations.of(context)
                                        .translate("Change Password") ,
                                    color: Colors.black,
                                    fontSize: 16,
                                  ),
                                  Icon(
                                    Icons.arrow_forward_ios_outlined,
                                    size: 18,
                                  )
                                ],
                              ),
                            ),
                          ),
                          Divider(
                            thickness: 1,
                          ),
                          GestureDetector(
                            onTap: (){
                              CommonMethods().animatedNavigation(
                                  context: context,
                                  currentScreen: ProfileScreen(),
                                  landingScreen: UploadIdScreen(
                                    isNewAccount: false,
                                  ));
                            },
                            child: Container(
                              height:40,
                              margin: EdgeInsets.only(bottom: 5,top: 5),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Label(
                                    title: AppLocalizations.of(context)
                                        .translate("Upload ID"),
                                    color: Colors.black,
                                    fontSize: 16,
                                  ),
                                  Icon(
                                    Icons.arrow_forward_ios_outlined,
                                    size: 18,
                                  )
                                ],
                              ),
                            ),
                          ),
                          Divider(
                            thickness: 1,
                          ),
                          GestureDetector(
                            onTap: () {
                              CommonMethods().animatedNavigation(
                                  context: context,
                                  currentScreen: ChangePassword(),
                                  landingScreen: SettingsScreen());
                            },
                            child: Container(
                              height:40,
                              margin: EdgeInsets.only(bottom: 5,top: 5),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Label(
                                    title:AppLocalizations.of(context)
                                        .translate("Settings") ,
                                    color: Colors.black,
                                    fontSize: 16,
                                  ),
                                  Icon(
                                    Icons.arrow_forward_ios_outlined,
                                    size: 18,
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        }
        return Container();
      },
    );
  }

  Widget profileFieldRow({String title, String value}) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          margin: EdgeInsets.only(top: 20),
          child: Label(
            title: title,
            textAlign: TextAlign.center,
            fontSize: 16,
            color: Colors.black,
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 20),
          child: Label(
            title: value ?? "N/A",
            textAlign: TextAlign.center,
            fontSize: 16,
            color: Colors.black,
          ),
        ),
      ],
    );
  }
}
