part of 'registration_bloc.dart';

abstract class RegistrationEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class UserRegistrationEvent extends RegistrationEvent {
  final String phoneNumber, password,userName,gender;

  UserRegistrationEvent({@required this.phoneNumber, @required this.password, @required this.userName, @required this.gender});
}
