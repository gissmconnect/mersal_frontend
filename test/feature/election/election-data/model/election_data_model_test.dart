import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:mersal/features/election/election-data/model/election_data_model.dart';
import 'package:mersal/features/election/election-domain/entity/election_entity.dart';

import '../../../../fixtures/fixture_reader.dart';

void main() {
  final _tElectionDataModel = ElectionDataModel(
      id: 1,
      shortDescription: 'test election',
      endTime: 'test end time',
      date: 'test date',
      title: 'test title',
      noOfElectorsSelect: 2,
      endDate: 'test end date');

  final _tElectionDataModelMap = <String, dynamic>{
    "id": 1,
    "title": "test title",
    "date": "test date",
    "end_date": "test end date",
    "end_time": "test end time",
    "short_description": "test election",
    "no_of_electors_select": 2
  };

  test('should be the subclass of ElectionDataEntity', () {
    expect(_tElectionDataModel, isA<ElectionDataEntity>());
  });

  group('fromJson', () {
    test('should a valid model', () {
      final Map<String, dynamic> jsonMap =
          json.decode(fixture('elections_response.json'))['data'][0];
      final _result = ElectionDataModel.fromJson(jsonMap);
      expect(_result, isA<ElectionDataEntity>());
    });
  });

  group('toJson', () {
    test('should return a JSON map containing proper data', () {
      final _result = _tElectionDataModel.toJson();
      expect(_result, _tElectionDataModelMap);
    });
  });
}
