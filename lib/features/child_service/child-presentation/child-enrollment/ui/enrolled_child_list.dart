import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as http;
import 'package:mersal/core/localization/app_localization.dart';
import 'package:mersal/core/network/api_endpoints.dart';
import 'package:mersal/core/widgets/common/shared_preference.dart';
import 'package:mersal/features/auth/auth_data/datasource/user_data_local_data_source.dart';
import 'package:mersal/features/child_service/child-data/model/child-enroll-model/enrolled_child_list_model/EnrolledChildDataModel.dart';
import 'package:mersal/features/child_service/child-data/model/child-enroll-model/enrolled_child_list_model/EnrolledChildMainModel.dart';
import 'package:mersal/features/child_service/child-domain/model/child-enrollment/registered-child-data/RegisteredChildData.dart';
import 'package:mersal/features/child_service/child-domain/model/child-enrollment/registered-child-data/RegisteredChildModel.dart';
import 'package:mersal/features/lawati_service/presentation/subscriptions/bloc/sms_subscriptions_bloc.dart';

import '../../../../../injection_container.dart';

class EnrolledChildList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<SmsSubscriptionsBloc>(
      create: (_) => serviceLocator<SmsSubscriptionsBloc>(),
      child: Scaffold(
        body: EnrolledChildBody(),
      ),
    );
  }
}

class EnrolledChildBody extends StatefulWidget {
  @override
  EnrolledChildListState createState() => EnrolledChildListState();
}

class EnrolledChildListState extends State<EnrolledChildBody> {
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  BuildContext mContextLoader;

  EnrolledChildMainModel enrolledChildModel;
  List<EnrolledChildDataModel> enrolledChildList = [];

  @override
  void initState() {
    super.initState();
    apiChildEnrollListCall();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/images/bg_image.jpg"),
          fit: BoxFit.fill,
        ),
      ),
      child: Column(
        children: [
          SizedBox(
            height: 50,
          ),
          Stack(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(bottom: 5, left: 15),
                alignment: Alignment.bottomLeft,
                child: GestureDetector(
                  child: Icon(
                    Icons.arrow_back_ios_rounded,
                    color: Colors.white,
                  ),
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                ),
              ),
              Container(
                margin: EdgeInsets.only(bottom: 6),
                alignment: Alignment.bottomCenter,
                child: Text(
                  AppLocalizations.of(context)
                      .translate("Enrolled Children List"),
                  textAlign: TextAlign.end,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.w400),
                ),
              ),
            ],
          ),
          Expanded(
            child: enrolledChildList != null && enrolledChildList.length > 0
                ? ListView.builder(
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: () async {},
                  child: Container(
                    padding: EdgeInsets.all(20),
                    margin:
                    EdgeInsets.only(bottom: 10, left: 15, right: 15),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(25)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        RichText(
                          text: TextSpan(
                            text: AppLocalizations.of(context)
                                .translate('Name:'),
                            style: DefaultTextStyle.of(context).style,
                            children: <TextSpan>[
                              TextSpan(
                                  text: enrolledChildList[index].enrolledChildModel.name ??
                                      "",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16)),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        RichText(
                          text: TextSpan(
                            text:  AppLocalizations.of(context)
                                .translate('DOB:'),
                            style: DefaultTextStyle.of(context).style,
                            children: <TextSpan>[
                              TextSpan(
                                  text:
                                  enrolledChildList[index].enrolledChildModel.dob,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16)),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        RichText(
                          text: TextSpan(
                            text:AppLocalizations.of(context)
                                .translate('Activity:'),
                            style: DefaultTextStyle.of(context).style,
                            children: <TextSpan>[
                              TextSpan(
                                  text: enrolledChildList[index].enrolledChildActivity.title ??
                                      "",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16)),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        RichText(
                          text: TextSpan(
                            text: AppLocalizations.of(context)
                                .translate('Activity description:'),
                            style: DefaultTextStyle.of(context).style,
                            children: <TextSpan>[
                              TextSpan(
                                  text:
                                  enrolledChildList[index].enrolledChildActivity.description??"",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16)),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                      ],
                    ),
                  ),
                );
              },
              itemCount: enrolledChildList.length,
            )
                : Center(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Text(
                  AppLocalizations.of(context)
                      .translate('No records found'),
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.black,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> apiChildEnrollListCall() async {
    final _token = await appPreferences.getStringPreference("AccessToken");
    if (context != null) {
      _showLoader(context);
    }

    var response = await http.get(Uri.parse(ENROLLED_CHILD_LIST), headers: {
      'Content-type': 'application/json',
      AUTHORIZATION: BEARER + " " + _token
    });

    if (response.statusCode == 200) {
      if (mContextLoader != null) Navigator.pop(mContextLoader);
      final Map<String, dynamic> _responseJson = json.decode(response.body);
      print(
          'ENROLLED_CHILD_LISTResponse ${response.statusCode} and ${_responseJson.toString()}');
      enrolledChildModel = EnrolledChildMainModel.fromJson(_responseJson);
      enrolledChildList = enrolledChildModel.enrolledChildList;
      setState(() {});
    }
  }

  Future<Null> _showLoader(BuildContext context) async {
    return showDialog<Null>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context1) {
        mContextLoader = context1;
        return Scaffold(
          backgroundColor: Colors.transparent.withOpacity(0.3),
          key: _keyLoader,
          body: Container(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    child: CircularProgressIndicator(),
                    height: 40.0,
                    width: 40.0,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
