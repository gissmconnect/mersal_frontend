part of 'sms_subscriptions_bloc.dart';

abstract class SmsSubscriptionsEvent extends Equatable {
  const SmsSubscriptionsEvent();
  @override
  List<Object> get props => [];
}

class GetSubscriptionsEvent extends SmsSubscriptionsEvent{}